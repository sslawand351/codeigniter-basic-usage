"use strict"
const site_url = (string) => {
	return BASE_URL + string;
}

const DealPipeline = function () {

	const resetForm = (form) => {
		form.find('input[type=text], input[type=hidden], textarea').not('.prevent-reset').val('');
		form.find('input[type=checkbox]:checked, input[type=radio]:checked').prop('checked', false);
		form.find('select option:selected').prop('selected', false);
	}

	const updateChosen = (form) => {
		form.find('select').trigger('chosen:updated');
	}

	const initWaitMe = (element) => {
		element.waitMe({
			text: 'Please Wait...',
			textPos: 'Vertical',
			effect: 'stretch'
		});
	}

	const hideWaitMe = (element) => {
		element.waitMe('hide');
	}

	const initChosen = (element) => {
		console.log(element);
		element.chosen();
	}

	const disableSubmitButton = (button) => {
		let data_click = button.attr('data-click');
		let click_flag = false;

		if (typeof data_click == 'undefined' || data_click == 0) {
			click_flag = true;
			button.attr('data-click', '1');
		}

		return click_flag;
	}

	const enableSubmitButton = (button) => {
		setTimeout(function() {
			button.attr('data-click', '0');
		}, 2000);
	}

	const showErrors = (form, errors) => {
		errors.forEach(function(v, i) {
			form.find('input[name="' + i + '"], select[name="' + i + '"], textarea[name="' + i + '"]').siblings('.error').html(v);
		});
	}

	const selectOption = (dropdown) => {
		if (dropdown.find('option:not([value=""],[value=0])').length == 1) {
			// select option if only one option contains
			dropdown.find('option:not([value=""],[value=0])').prop('selected', true);
			dropdown.trigger('change');
		} else {
			dropdown.find('option:selected').prop('selected', false);
		}

		dropdown.prop('disabled', false).trigger('chosen:updated');
	}

	const setModalHeader = (modal, dropdown, text_selector, type) => {
		let text, name = '', id = dropdown.find('option:selected').val();

		let selected_type = dropdown.find('option:selected').attr('data-type');
		if (isInteger(id) && id > 0 && selected_type === type) {
			text = text_selector.text();
			name = dropdown.find('option:selected').text();
		} else {
			text = text_selector.attr('data-text');
		}
		modal.find('.modal-header h3').text(text + ' ' + name);
	}

	const onCloseSellCarsModal = (modal) => {
		let form = modal.find('form');

		form.find('.option_item').not(':eq(0)').remove();			// remove all other option item divs except first one
		form.find('.option_item').removeClass('previous_sold_car');
		form.find('.show_data').click();
		form.find('.hide_div').css('display', 'none');
	}

	const onCloseAddContactModal = (modal) => {
		let form = modal.find('form');

		form.find('input').val('');
		form.find('.error').html('');
		form.find('select[name="lead_source_id"]').prop('selected', false).trigger('chosen:updated');
	}

	const openAddContactModal = (element, modal) => {
		let parent_modal = element.parents('.modal');
		let organisation_dropdown = parent_modal.find('#contact-list');

		setModalHeader(modal, organisation_dropdown, $('.add-contact-header'), 'organisation');
		modal.modal('show');
	}

	const addContact = (_this, parent_modal) => {
		let selected_type = parent_modal.find("#contact-list").find('option:selected').attr('data-type');
		let modal = _this.parents('.modal');
		let form = modal.find('form');
		let error = form.find('.error');

		if (disableSubmitButton(_this)) {
			error.html('');

			let response = DealApi.addContact(form.serializeArray());
			return response.then(response => {
				if (response.rc) {
					let contact = response.contact;
					let optionTxt = contact.first_name + ' ' + contact.last_name + ' ( ' + contact.email + ' )';
					let optionVal = contact.id;
					let option = '<option value="' + optionVal + '" data-type="contact" data-email="' + contact.email + '" selected="selected">' + optionTxt + '</option>';
					
					var newOption = new Option(optionTxt, optionVal, true, true);

					if (selected_type === 'organisation') {

						let organisation_id = parent_modal.find('#contact-list').find('option:selected').val(), contact_id = contact.id;
						let orgResponse = DealApi.associateContactWithOrganisation({
							organisation_id: organisation_id,
							contact_id: contact_id
						});
						return orgResponse.then(response => {
							let deal_contact_dropdown = parent_modal.find('#deal-contact');
							deal_contact_dropdown.find('option:first-child').after(option).trigger('chosen:updated');
							deal_contact_dropdown.siblings('.error').html('');
							return true;
						});
					} else {
						parent_modal.find('#contact-list').append(newOption);
						parent_modal.find('#contact-list').find('option[value="' + optionVal + '"]').attr({
							'data-type': 'contact',
							'data-email': contact.email,
							'data-first-name': contact.first_name,
							'data-last-name': contact.last_name,
							'data-street': contact.street,
							'data-house-number': contact.house_number,
							'data-postal-code': contact.postal_code,
							'data-mobile': contact.mobile,
							'data-town': contact.city
						});
						parent_modal.find("#contact-list").trigger("change");
					}

					modal.modal('hide');
				} else {
					showErrors(form, response.error);
				}

				enableSubmitButton(_this);

				return true;
			});
		}
	}

	const openAssociateContactWithOrganisationModal = (modal, _this) => {
		let parent_modal = _this.parents('.modal');
		let contact_dropdown = parent_modal.find('#contact-list');

		setModalHeader(modal, contact_dropdown, $('.associate-organisation-header'), 'contact');
		modal.modal('show');
	}

	const openAssociateOrganisationWithContactModal = (modal, _this) => {
		let parent_modal = _this.parents('.modal');
		let organisation_dropdown = parent_modal.find('#contact-list');
		let status = organisation_dropdown.find("option:selected").attr('data-status');

		if (status === '') {
			let open_modal = $('#change_status_popup');
			let organisation_id = parseInt(organisation_dropdown.val());
			open_modal.find('input[name="organisation_id"]').val(organisation_id);
			open_modal.modal('show');
			return false;
		}

		setModalHeader(modal, organisation_dropdown, $('.associate-contact-header'), 'organisation');
		modal.modal('show');
	}

	const openAddOrganisationModal = (modal, _this) => {
		$('.error').html('');
		let parent_modal = _this.parents('.modal');
		let contact_dropdown = parent_modal.find("#contact-list");

		modal.find('form').attr('data-from', 'add-deal');

		//set user id
		let user_id = modal.find('input[name=user_id]');
		user_id.val('');

		//set contact id
		let contact_id = parseInt(contact_dropdown.val());
		modal.find('input[name="contact_id"]').val(contact_id);

		setModalHeader(modal, contact_dropdown, $('.add-organisation-header'), 'contact');

		modal.find('.category-div').show();
		modal.find('.cost_category[value="b2b"]').prop('checked', true);
		modal.modal('show');
	}

	const onCloseAddOrganisationModal = () => {
		//clear all inputs
		modal.find("input[type=text], textarea").val("");
		modal.find("input[type=radio]").prop('checked', false);
	}

	const associateContactWithOrganisation = (modal, parent_modal) => {
		let contact_organisation_dropdown = parent_modal.find('#contact-list');
		let selected_type = contact_organisation_dropdown.find('option:selected').attr('data-type');
		let organisation_id, contact_id, selected_contact, contact_name, selected_organisation, status;

		if (selected_type === 'organisation') {
			selected_contact = $('#existing-contact').find('option:selected');
			contact_id = parseInt(selected_contact.val());
			organisation_id = contact_organisation_dropdown.find('option:selected').val();
			contact_name = selected_contact.text();
			status = contact_organisation_dropdown.find('option:selected').attr('data-status');
		} else {
			selected_organisation = $('#existing-organisations').find('option:selected');
			organisation_id = parseInt(selected_organisation.val());
			contact_id = contact_organisation_dropdown.find('option:selected').val();
			status = selected_organisation.attr('data-status');
		}

		if (status == '') {
			var status_modal = $('#change_status_popup');
			status_modal.find('input[name="organisation_id"]').val(organisation_id);
			status_modal.modal('show');
			return false;
		}

		if (organisation_id != 0 && contact_id != 0) {
			let response = DealApi.associateContactWithOrganisation({
				organisation_id: organisation_id,
				contact_id: contact_id
			})

			return response.then(response => {
				if (response.rc) {
					if (selected_type === 'organisation') {
						var select_contact = parent_modal.find('#deal-contact');
						select_contact.find('option[value="' + contact_id + '"]').remove().trigger('chosen:updated');
						let option = '<option value="' + contact_id + '" selected>' + contact_name + '</option>';

						select_contact.find('option:first-child').after(option).trigger('chosen:updated');
						select_contact.siblings('.error').html('');
					} else {
						var select_organisation = parent_modal.find('#deal-org');
						select_organisation.find('option[value="' + response.organisation.id + '"]').remove().trigger('chosen:updated');
						let option = '<option value="' + response.organisation.id + '" selected>' + response.organisation.name + '</option>';

						select_organisation.find('option:first-child').after(option).trigger('chosen:updated');
					}
					modal.modal('hide');
				} else {
					if (response.status == '') {
						var status_modal = $('#change_status_popup');
						status_modal.find('input[name="organisation_id"]').val(organisation_id);
						status_modal.modal('show');
					} else {
						$.jGrowl(response.msg);
					}
				}
			})
		} else {
			if (organisation_id <= 0) {
				$('#existing-organisations').siblings('.text-danger').show();
			}

			if (contact_id <= 0) {
				$('#existing-contact').siblings('.text-danger').html($('.this_field_is_required').text());
				$('#existing-contact').siblings('.text-danger').show();
			}
		}
	}

	const onCloseAssociateOrganisationModal = () => {
		$('#associate_organisation_popup #existing-contact').val(null).trigger('change');
		$('#associate_organisation_form').get(0).reset();
		$('#existing-contact').siblings('.text-danger').hide();
	}

	const changeOrganisationStatus = (_this, parent_modal) => {
		let modal = _this.parents('.modal');
		let organisation_id = $('.status_organisation_id').val();
		let status = _this.hasClass('accept_org') ? 'accepted' : 'rejected';
		let cost_category = modal.find('input[name="cost_category"]:checked').val();

		let response = DealApi.changeOrganisationStatus({
			organisation_id: organisation_id,
			status: status,
			cost_category: cost_category
		});

		return response.then(response => {
			if (response.rc) {
				if (_this.hasClass('contact_org')) {
					location.reload();
				} else {
					var select_organisation = $('#deal-org');
					var option = '<option value="' + response.organisation.id + '" selected>' + response.organisation.name + '</option>';

					select_organisation.find('option:first-child').after(option).trigger('chosen:updated');
					parent_modal.find("#contact-list option:selected").attr('data-status', "accepted");
				}

				modal.modal('hide');
				$.jGrowl(response.msg);
			} else {
				$.jGrowl(response.msg);
			}
		})
	}

	const onClickAddInterestedCarsButton = () => {
		let waitMeElement = $('.interested-cars-wait');
		initWaitMe(waitMeElement);
		let deal_id = $('#leadEditId').val();

		let response = DealApi.getInterestedCars(deal_id);			// get interested cars of deal
		response.then(response => {
			$('.interested_cars').html(response.cars_data);			// dump all cars in the dropdown
			initChosen($('#lead_has_interest'));
			hideWaitMe(waitMeElement);
			return true;
		});
	}

	const addInterestedCar = (_this) => {
		initWaitMe($('body'));

		let interested_cars = [];
		interested_cars = _this.val();

		let deal_id = $("#leadEditId").val();
		let response = DealApi.addInterestedCar({
			'interested_cars': interested_cars,
			'deal_id': deal_id
		});
		return response.then(response => {
			if (response.rc) {
				get_deal(deal_id, 'add_interested_cars');
				$("#focus_div").focus();
				$.jGrowl("Interested cars updated successfully.");
				fetch_deals();
				$("#deal_status_crm").html(response.deal_status);
			}
			hideWaitMe($('body'));
			return true;
		})
	}

	const buildGenerateProposalHiddenForm = (_this, modal) => {
		let hidden_html = '<div class="" id="generate_proposal_hidden_data" style="display:none;">'
		hidden_html += ' <input type="text" class="deal_id" value="' + _this.attr('data-deal-id') + '"> ';
		hidden_html += ' <input type="text" class="contact_id" value="' + _this.attr('data-contact-id') + '"> ';
		hidden_html += ' <input type="text" class="user_id" value="' + _this.attr('data-user-id') + '"> ';
		hidden_html += ' <input type="text" class="deal_channel_id" value="' + _this.attr('data-channel-id') + '"> ';
		hidden_html += ' <input type="text" class="deal_organisation_id" value="' + _this.attr('data-org-id') + '"> ';
		hidden_html += '</div>';

		modal.find('#generate_proposal_hidden_data').remove();
		modal.append(hidden_html);
	}

	const generateProposalStepOne = (_this, modal) => {
		let street = $("#lead-street").val();
		let house_number = $("#lead-house-number").val();
		let town = $("#lead-town").val();
		let postal_code = $("#lead-postal-code").val();
		let first_name = $("#lead-first-name").val();
		let last_name = $("#lead-last-name").val();
		let mobile = $("#lead-phone-number").val();
		let email = $("#lead-email").val();
		let country = $("#lead-country").val();
		let btw = $("#lead-btw").val();

		if (deal_channel_id != B_RENT_CHANNEL) {
			if (! modal.hasClass('contact-crm-view-proposal')) {
				let editDealModal = $('#leadEditModal');
				contact_id = editDealModal.find('.generate-proposals').attr('data-contact-id');
			}

			let response = DealApi.updateContactAddress({
				contact_id: contact_id,
				street: street,
				house_number: house_number,
				town: town,
				postal_code: postal_code,
				first_name: first_name,
				last_name: last_name,
				mobile: mobile
			});
		} else {
			if (! modal.hasClass('contact-crm-view-proposal')) {
				let editDealModal = $('#leadEditModal');
				contact_id = editDealModal.find('.generate-proposals').attr('data-contact-id');
				deal_organisation_id = editDealModal.find('.generate-proposals').attr('data-org-id');
			}

			let orgResponse = DealApi.updateOrganisation({
				organisation_id: deal_organisation_id,
				street: street,
				street_number: house_number,
				town: town,
				postal_code: postal_code,
				telephone: mobile,
				country: country,
				btw: btw
			});
			let response = DealApi.updateContactAddress({
				contact_id: contact_id,
				first_name: first_name,
				last_name: last_name
			});
		}
	}

	const generateProposalStepTwo = (_this, modal) => {
		let car_proposal_type = $('input[name="car_proposal_type"]').val()
		if (car_proposal_type == 'b2b') {
			let organisation_dropdown = modal.find('select[name="organisation_id"]');
			let organisation_id = organisation_dropdown.val();

			if (organisation_id == '') {
				var text = organisation_dropdown.siblings('.text-danger').data('text');

				organisation_dropdown.siblings('.text-danger').html(text);
				valid = false;
			} else {
				organisation_dropdown.siblings('.text-danger').html('');
			}

			modal.find('.interested_cars_price').each(function() {
				let price = $(this).val();
				price = convert_currency_to_db_format(price);
				if (price != '' && price != 0) {
					$(this).siblings('.text-danger').text('').hide();
				} else {
					$(this).siblings('.text-danger').text($('.enter-valid-cost-text').attr('data-text')).show();
					valid = false;
				}
			});

			if (! valid) {
				return false;
			} else {
				let response = DealApi.addCarProposalDataForBrent($('#form-make-offer').serialize());
				handleCarProposalResponse(response);
			}
		} else {
			let type = _this.attr("data-id");
			$("#make_offer_type").val(type);

			let accessories = "&car_accessories="
			$('input:checkbox:checked.car-accessories').map(() => accessories += this.value + ',').get();

			let response = DealApi.addCarProposalData($('#form-make-offer').serialize() + accessories);
			handleCarProposalResponse(response);
		}
	}

	const handleCarProposalResponse = (response, _this) => {
		return response.then(reponse => {
			if (response.rc) {
				let html = $('.view-proposal-name').text();

				$(_this).text(html);

				if (response.email_unique == false) {
					$('.lead-email-msg').css('display', 'block');
				}
			}

			return true;
		})
	}

	const initGenerateProposalFormWizard = (_this, modal) => {
		$('#rootwizard').bootstrapWizard({
			onTabShow: function(tab, navigation, index) {
				var $total = navigation.find('li').length;
				var $current = index + 1;
				var $percent = ($current / $total) * 100;
				$('#rootwizard .progress-bar').css({
					width: $percent + '%'
				});
				$('.lead-email-msg').css('display', 'none');
			},
			onNext: function(tab, navigation, index) {
				let hiddenForm = modal.find('#generate_proposal_hidden_data');
				let deal_id = hiddenForm.find('.deal_id').val();
				let contact_id = hiddenForm.find('.contact_id').val();
				let user_id = hiddenForm.find('.user_id').val();
				let deal_channel_id = hiddenForm.find('.deal_channel_id').val();
				let deal_organisation_id = hiddenForm.find('.deal_organisation_id').val();

				initChosen(modal.find("#user_choosen_warranty"));
				if (index != 4) {
					var valid = $("#form-make-offer").valid();
					var next_step_not_allowed = true;

					if (deal_channel_id == B_RENT_CHANNEL && index != 2) {
						let btwInput = $("#lead-btw");
						let response = DealApi.checkOrganisationHasUniqueBTW({
							'org_id': deal_organisation_id,
							'btw': btwInput.val()
						});

						response.then(response => {
							if (response.rc) {
								btwInput.siblings('.error').html('');
							} else {
								btwInput.siblings('.error').html(response.msg);
								next_step_not_allowed = false;
							}
						});

						if ($("#lead-country").val() == null) {
							$("#lead-country").siblings('.error').html($(".this_field_is_required").text());
							next_step_not_allowed = false;
						}
					}
					if (! next_step_not_allowed) {
						return false;
					}

					if (! valid) {
						$validator.focusInvalid();
						return false;
					}
				}

				if (index == 1) {
					generateProposalStepOne(_this, modal);
				}

				if (index == 2) {
					generateProposalStepTwo(_this, modal);
				}

			},
			onTabClick: function(tab, navigation, index) {
				return false;
			}
		});
	}

	const getContactDataForCarProposal = (_this, modal) => {
		let single_car_id = 0;
		let car_id = _this.attr('data-car-id');
		let deal_id = _this.attr('data-deal-id');
		let contact_id = _this.attr('data-contact-id');

		$('#contact-id').val(contact_id);
		$('#form-make-offer').find('.deal-id').val(deal_id);

		let response = DealApi.getContactDataForCarProposal({
			'deal_id': deal_id,
			'contact_id': contact_id,
			'car_id': car_id
		}); 

		response.then(response => {
			if (response.salespersons.rc) {
				$('.salesperson-error').hide();

				let html = response.salespersons.data.reduce((html, v) => html += '<option value=' + v.id + '>' + v.first_name + ' ' + v.last_name + ' ' + '</option>', '');

				$('.salesperson_id').html(html);
				$('.salesperson_id option').filter('[value="' + response.deal.salesperson_user_id + '"]').prop('selected', true).trigger('chosen:updated');
				$(".salesperson_id").chosen();
				$('.no-salesperson').hide();
			} else {
				$(".salesperson-error-span").html('(' + response.deal.location + ' ) having channel name - ' + response.deal.channel)
				$('#salesperson_id').siblings('.chosen-container').hide();
				$('#salesperson_id').val('');
				$('.new_car_seller').hide();
				$('.salesperson-error, .no-salesperson, .link_to_change_location').show();
			}

			$('#lead-first-name').val(response.deal.first_name);
			$('#lead-last-name').val(response.deal.last_name);
			$('#lead-email').val(response.deal.email);

			if (response.deal.channel_id == B_RENT_CHANNEL) {
				$(".btw-field").removeClass('hidden');
				$(".org-country-field").removeClass('hidden');

				$("#lead-phone-number").val(response.org_data.telephone);
				$("#lead-street").val(response.org_data.street);
				$("#lead-house-number").val(response.org_data.street_number);
				$("#lead-postal-code").val(response.org_data.postal_code);
				$("#lead-town").val(response.org_data.town);
				$("#lead-country").val(response.org_data.country).trigger("chosen:updated");
				$("#lead-btw").val(response.org_data.btw);

				$('.price-information-b-rent').show();
				$('.price-information-b-rent').replaceWith(response.html_interested_cars);
				initChosen($('select[name="organisation_id"]'));
				$('#car-seller, .price-information').hide();
			} else {
				$(".btw-field").addClass('hidden');
				$(".org-country-field").addClass('hidden');

				$("#lead-phone-number").val(response.deal.mobile);
				$("#lead-street").val(response.deal.street);
				$("#lead-house-number").val(response.deal.house_number);
				$("#lead-postal-code").val(response.deal.postal_code);
				$("#lead-town").val(response.deal.city);

				$('.price-information-b-rent').hide();
				$('#car-seller, .price-information').show();
				var single_car = _this.attr("data-single-car");
				/*if( single_car == "true")
				{*/
				single_car_id = _this.attr('data-car-id');
				$("#car-selling-price-input").val(format_currency(_this.attr("data-selling-cost")));
				/*}*/

				if (response.self_car_seller) {
					$("#car-selling-price-input").attr('readonly', 'readonly');
				}

				var car_id = _this.attr('data-car-id');
				$('#form-make-offer').find('.car-id').val(car_id);

				if (response.offer_data != undefined && response.offer_data != "") {
					var car_id = response.offer_data.car_id;
					$("#car-selling-price-input").val(format_currency(response.offer_data.car_price));

					handleCarAccessories(response.offer_data.accessories, response.offer_data.total_accessory_cost);
					handleCarFinance(response.finance_block_html, response.offer_data.finance_available);
				}

				handleCarWarranty(_this.attr('data-car-status'), response.offer_data.guarantee);
			}

			let carWarrantyDropdown = modal.find('#user_choosen_warranty');

			if (response.deal.status == 'sold') {
				modal.find(":input").not("[name=toEnable]").prop("readonly", true);
				modal.find("input[type='checkbox']").attr('disabled', true);
				modal.find(".salesperson_id").attr('disabled', true).trigger('chosen:updated');
				modal.find("#lead-country").attr('disabled', true).trigger('chosen:updated');
				carWarrantyDropdown.before('<input type="hidden" name="guarantee" value="' + carWarrantyDropdown.val() + '">');
				carWarrantyDropdown.attr('disabled', true).trigger('chosen:updated');
				$('#myAccessoryModal').find('.add-accessory-to-reserve').prop("disabled", true);
				$('.interested_cars_price.price-type').prop("readonly", true);
			} else {
				modal.find(":input").prop("readonly", false);
				modal.find("input[type='checkbox']").attr('disabled', false);
				modal.find(".salesperson_id").attr('disabled', false).trigger('chosen:updated');
				modal.find("#lead-country").attr('disabled', false).trigger('chosen:updated');
				carWarrantyDropdown.siblings('input[name="guarantee"]').remove();
				carWarrantyDropdown.attr('disabled', false).trigger('chosen:updated');
				$('#myAccessoryModal').find('.add-accessory-to-reserve').prop("disabled", false);
				$('.interested_cars_price.price-type').prop("readonly", false);
			}
		}
	}

	const handleCarAccessories = (accessories, total_accessory_cost) => {
		if (accessories.length > 0) {
			$('.selected-opties').removeClass('hidden');
			$('.no-selected-opties').addClass('hidden');

			accessories.forEach((v, i) => {
				$('.add-car-accessories').find("input[value='" + v.accessory_id + "']").siblings('.add-car-accessories .active').show();
				$('.add-car-accessories').find("input[value='" + v.accessory_id + "']").siblings('.add-car-accessories .btn-success').hide();

				$(".car-accessories[value='" + v.accessory_id + "']").prop('checked', true);
			});
		} else {
			$(".selected-opties").addClass('hidden');
			$(".no-selected-opties").removeClass('hidden');
		}

		$('#amount4').val(total_accessory_cost);
		$('.accessory-cost').html(total_accessory_cost);
	}

	const handleCarFinance = (finance_block_html, finance_available) => {
		if (finance_available == true) {
			$('#enable-finance-settings').prop('checked', true);
			$("#finance-block").html(finance_block_html);

			initChosen($('#user_choosen_month'));			// don't know
			initChosen($('#user_choosen_warranty'));		// don't know

			$("#user_choosen_month").trigger("chosen:updated");
			$("#user_choosen_warranty").trigger("chosen:updated");
		} else {
			$('#enable-finance-settings').prop('checked', false);
		}
	}

	const showCarFinanceWarning = () => {
		$('#enable-finance-settings').prop('checked', false);
		$('#finance-block').html('');
		alert("It’s not possible to have a finance on this car.");
	}

	const handleCarWarranty = (car_status, warranty) => {
		if (car_status == "used") {
			$('.guarantee_finance_div').css('display', 'block');

			warranty = typeof warranty != 'undefined' ? warranty : 5;
			$('#user_choosen_warranty option').filter('[value="' + warranty + '"]').prop('selected', true).trigger("chosen:updated");
		}
	}

	const onClickGenerateProposalsButton = (_this, modal) => {
		let car_name = _this.parent('td').siblings('.car-name').text();
		$('.car-names').html(car_name);

		initChosen(modal.find('#lang_dropdown'));
		modal.modal();
		buildGenerateProposalHiddenForm(_this, modal);

		$('#user-id').val(_this.attr('data-user-id'));

		if (_this.hasClass('contact-crm-view-proposal')) {
			modal.addClass('contact-crm-view-proposal');
		} else {
			modal.removeClass('contact-crm-view-proposal');
		}

		initGenerateProposalFormWizard(_this, modal);
		getContactDataForCarProposal(_this, modal);
	}

	const getMonthlyPaymentOfCar = (stock_status, type) => {
		let accessories = $('input:checkbox:checked.car-accessories').map(() => this.value).get();
		let car_id = $("#form-make-offer").find('.car-id').val();
		let user_selected_month = $("#user_choosen_month option:selected").val();
		let selling_price = convert_currency_to_db_format($('#car-selling-price-input').val());
		let advance = convert_currency_to_db_format($('#own_contribution').val());
		let warranty = $("#user_choosen_warranty option:selected").val();
		let is_finance_settings_applicable = $("#enable-finance-settings").prop('checked');

		let json = {
			'accessories': accessories,
			'car_id': car_id,
			'user_selected_month': user_selected_month,
			'car_selling_price': selling_price,
			'user_chosen_advance': advance,
			'user_choosen_warranty': warranty,
			'type': type,
			'is_finance_settings_applicable': is_finance_settings_applicable
		};

		if(typeof stock_status !== 'undefined') json['stock_status'] = stock_status;
		return DealApi.getMonthlyPaymentOfCar(json);
	}

	const onChangeCarPrice = () => {
		if ($("#enable-finance-settings").prop('checked')) {
			let response = getMonthlyPaymentOfCar();
			response.then(response => {
				handleCarAccessories(accessories, response.accessory_cost);

				if (response.success) {
					handleCarFinance(true, response.finance_block_html);
				} else {
					if (response.only_accessory) {
						$("#only_accessory").html(response.finance_block_html);
					}

					if (is_finance_settings_applicable == true) showCarFinanceWarning();
				}
			})
		} else {
			$("#finance-block").html('');
		}
	}

	// own contribution === Car advance both are same
	const onChangeCarAdvance = () => {
		let response = getMonthlyPaymentOfCar();
		response.then(response => {
			handleCarAccessories(accessories, response.accessory_cost);

			if (response.success) {
				handleCarFinance(true, response.finance_block_html);
				user_advance_amount_prev = own_contribution;
			} else {
				$('#amount1, #own_contribution, #user_advance_amount').val(user_advance_amount_prev);

				if (advance != user_advance_amount_prev) $('#own_contribution').trigger("change");

				alert(response.msg);
			}
		})
	}

	const onChangeCarWarranty = () => {
		let response = getMonthlyPaymentOfCar();
		response.then(response => {
			if (response.success) {
				$(".user_choosen_monthly_payment").html(response.user_choosen_monthly_payment);
				$("input[name=user_choosen_monthly_payment]").val(response.user_choosen_monthly_payment);
			} else
				alert(response.msg);
		});
	}

	const onChangeCarMonthlyPeriod = () => {
		let response = getMonthlyPaymentOfCar();
		response.then(response => {
			if (response.success) {
				$("#finance_months_string").attr('data-max', response.months_array_max);
				$("#finance_months_string").attr('data-min', response.months_array_min);

				$(".user_choosen_monthly_payment").html(response.user_choosen_monthly_payment);
				$("input[name=user_choosen_monthly_payment]").val(response.user_choosen_monthly_payment);
			} else
				alert(response.msg);
		});
	}

	const onChangeOfEnableFinanceSettings = () => {
		if ($("#enable-finance-settings").prop('checked')) {
			let response = getMonthlyPaymentOfCar();
			response.then(response => {
				handleCarAccessories(accessories, response.accessory_cost);

				if (response.success) {
					handleCarFinance(true, response.finance_block_html);
				} else {
					if (response.only_accessory) {
						$("#only_accessory").html(response.finance_block_html);
					}

					if (is_finance_settings_applicable == true) showCarFinanceWarning();
				}
			})
		} else {
			$('#finance-block').html('');
		}
	}

	const onClickOfAddRemoveAccessoryButton = (_this) => {
		_this.hide();

		let accessory = _this.siblings('.car-accessories');
		let accessory_id = accessory.attr('data-id');
		let accessory_cost = accessory.attr('data-cost');
		let option_item_id = $("#selected_option_item_id").val();

		if (_this.hasClass('active')) {
			// show add button
			_this.siblings('.add-accessory-to-reserve').show();

			// only for sell car modal
			if (option_item_id != "") {
				$("input[name='sold_cars[" + option_item_id + "][selected_accessories][]'][value='" + accessory_id + "']").remove();
			}
			accessory.prop('checked', true);
		} else {
			// show added button
			_this.siblings('.add-accessory-to-reserve.active').show();

			// only for sell car modal
			if (option_item_id != "") {
				$('.review-sold-cars').find(".option_item#" + option_item_id).append('<input type="hidden" name="sold_cars[' + option_item_id + '][selected_accessories][]" class="sold_cars_accessories_' + option_item_id + '" value="' + accessory_id + '">');
			}
			accessory.prop('checked', false);
		}

		accessory.trigger('click');
	}

	const onChangeCarAccessories = (_this, modal) => {					// if not required, then remove _this and modal
		let response = getMonthlyPaymentOfCar();
		response.then(response => {

			handleCarAccessories(accessories, response.accessory_cost);

			if (response.success) {
				handleCarFinance(true, response.finance_block_html);
				$("#amount4-update-sell-car").val(response.accessory_cost);
			} else {
				if (response.only_accessory) {
					//$("#only_accessory").html(response.finance_block_html);
					$(".amount4").val(response.accessory_cost);
					$("#amount4-update-sell-car").text(response.accessory_cost);
				}

				if (is_finance_settings_applicable == true) showCarFinanceWarning();
			}
		})
	}

	const onCloseAddDealModal = (modal) => {
		let data_from = modal.find("#contact-list").find('option:selected').attr('data-from');

		if (data_from != 'crm') {
			modal.find('select option[value=0], select option[value=""]').prop('selected', true).trigger('chosen:updated');
			modal.find('#contact-list').trigger('change');
		}

		modal.find('.error').text('');
	}

	const addDeal = (_this) => {
		let modal = _this.parents('.modal');
		let form = modal.find('form');
		let error = form.find('.error');

		if (disableSubmitButton(_this)) {
			error.html('');

			let contact_organisation_dropdown = modal.find("#contact-list");
			let selected_contact_organisation_dropdown = contact_organisation_dropdown.find('option:selected');
			let selected_type = selected_contact_organisation_dropdown.attr('data-type');
			let organisation_id, contact_id, status;

			if (selected_type == 'organisation') {
				organisation_id = selected_contact_organisation_dropdown.val();
				contact_id = modal.find("#deal-contact").find('option:selected').val();
				status = selected_contact_organisation_dropdown.attr('data-status');
			} else {
				contact_id = selected_contact_organisation_dropdown.val();
				organisation_id = modal.find("#deal-org").find('option:selected').val();
				status = modal.find("#deal-org").find('option:selected').attr('data-status');
			}

			if (status == '') {
				let status_modal = $('#change_status_popup');
				status_modal.find('input[name="organisation_id"]').val(organisation_id);
				status_modal.modal('show');
				return false;
			}

			let channel_id = modal.find("#deal_channel").find('option:selected').val();
			let location_id = modal.find("#deal-locations").find('option:selected').val();
			let car_seller_id = modal.find("#deal_car_sellers").find('option:selected').val();

			if (channel_id == B_RENT_CHANNEL) {
				if (organisation_id == "") {
					form.find('select[name="organisation_id"]').siblings('.error').html($(".this_field_is_required").html());
					return false;
				} else {
					if (! checkIfOrganisationIsCompleted(organisation_id)) {
						return false;
					}
				}
			}

			let data_from = selected_contact_organisation_dropdown.attr('data-from');

			let waitMeElement = $('#select-contact-modal .modal-content');
			initWaitMe(waitMeElement);

			let response = DealApi.addDeal({
				organisation_id: organisation_id,
				contact_id: contact_id,
				channel_id: channel_id,
				location_id: location_id,
				car_seller_id: car_seller_id,
				function_from: 'lead-pipeline'
			});

			return response.then(response => {
				let contact = response.contact;
				if (response.rc) {
					if (data_from == 'crm') {
						ajax_get_sales_data();
					}

					$.jGrowl(response.msg, {
						life: 30000
					});

					modal.modal('hide');
				} else {
					$.each(response.error, function(i, v) {
						form.find('select[name="' + i + '"]').siblings('.error').html(v);
					});
				}
				
				hideWaitMe(waitMeElement);
				enableSubmitButton(_this);

				if(response.rc) {
					waitMeElement = $('.app-content-body');
					initWaitMe(waitMeElement);

					fetch_deals(0);
					fetch_lead_status_table(0);
					setTimeout(function() {
						get_deal(response.deal.id, 'lead-pipeline')
					}, 2000);
				}
				return true;
			});
		}
	}

	const checkIfOrganisationIsCompleted = (organisation_id) => {
		let response = DealApi.getOrganisation (organisation_id);
		return response.then(response => {
			let flag = false;
			if (response.incomplete_organisation) {
				$(".incomplete_org_msg").find('a').attr('href', BASE_URL + 'organisation/edit/' + organisation_id);
				$(".incomplete_org_msg").removeClass('hidden');
				flag = false;
			} else {
				$(".incomplete_org_msg").addClass('hidden');
				flag = true;
			}

			return flag;
		});
	}

	const onChangeChannel = (_this) => {
		let modal = _this.parents('.modal');
		let organisation_container = modal.find('.organisation-container');
		let channel_id = parseInt(_this.val());
		let location = modal.find('select[name="location_id"]');

		if (isInteger(channel_id) && channel_id == B_RENT_CHANNEL) {
			let contact_id = modal.find('#contact-list option:selected').val();

			// for b-rent channel location will always be Beerens Aartselaar location id 11
			location.find('option[value=' + AART_SELLER_LOCATION_ID + ']').prop('selected', true);
			location.attr('disabled', true).trigger('chosen:updated');

			let response = DealApi.getAllOrganisationsByContactId(contact_id);

			return response.then(response => {
				let organisation_html = '<option value="">Please Select</option>';
				if (response.rc) {
					// organisation will always be pre selected if only contains one organisation
					organisation_html = response.data.length == 1 ? '' : organisation_html;
					response.data.forEach(function(v, i) {
						organisation_html += "<option data-org-type='" + v.cost_category + "' data-status='" + v.status + "'  value='" + v.id + "' >" + v.name + "( " + v.btw + " ) </option>";
					});
				}
				modal.find('#deal-org').html(organisation_html).trigger('chosen:updated');
				organisation_container.show();
				return true;
			});
		} else {
			location.find('option[value=""]').prop('selected', true);
			location.attr('disabled', false).trigger('chosen:updated');
			organisation_container.hide();
		}
	}

	const onSelectOrganizationOrContact = contact_organisation_dropdown => {
		let selected_contact_organisation_dropdown = contact_organisation_dropdown.find('option:selected');
		let id = parseInt(contact_organisation_dropdown.val());
		let error = contact_organisation_dropdown.siblings('.text-danger');

		if (isInteger(id) && id != 0) {
			error.hide();
		} else {
			error.show();
		}
		$(".incomplete_org_msg").addClass('hidden');

		let modal = contact_organisation_dropdown.parents('.modal');
		let selected_type = selected_contact_organisation_dropdown.attr('data-type');

		if (selected_type === 'organisation') {
			onSelectOrganization(modal, id);
		} else if (selected_type === 'contact') {
			onSelectContact(modal, id);
		} else {
			$(".contact-container").hide();
			$(".organisation-container").hide();
			modal.find('#deal_channel, #deal-locations, #deal_car_sellers').prop('disabled', true).trigger('chosen:updated');
		}
	}

	const onSelectContact = (modal, contact_id) => {
		modal.find('#deal-contact').prop('disabled', true).trigger('chosen:updated');
		modal.find(".contact-container").hide();
		modal.find("#form-add-new-deal").attr('data-contact-id', contact_id);

		let channel_dropdown = modal.find('#deal_channel');
		selectOption(channel_dropdown);

		let location_dropdown = modal.find('#deal-locations');
		selectOption(location_dropdown);

		modal.find('#deal_car_sellers').prop('disabled', false).trigger('chosen:updated');

		if (channel_dropdown.val() == B_RENT_CHANNEL) {
			location_dropdown.find('option[value=' + AART_SELLER_LOCATION_ID + ']').prop('selected', true).trigger('chosen:updated');
			location_dropdown.prop('disabled', true).trigger('chosen:updated');
		} else {
			location_dropdown.prop('disabled', false).trigger('chosen:updated');
		}
	}

	const onSelectOrganization = (modal) => {
		let channel_dropdown = modal.find('#deal_channel');
		let location_dropdown = modal.find('#deal-locations');

		channel_dropdown.find('option[value=' + B_RENT_CHANNEL + ']').prop('selected', true).trigger('chosen:updated');
		location_dropdown.find('option[value=' + AART_SELLER_LOCATION_ID + ']').prop('selected', true).trigger('chosen:updated');

		channel_dropdown.prop('disabled', true).trigger('chosen:updated');
		location_dropdown.prop('disabled', true).trigger('chosen:updated');

		let org_id = modal.find('.contact-list option:selected').val();
		let response = DealApi.getAllContactsByOrganisation(org_id);

		return response.then(response => {
			var contact_html = "<option value='0'>Please Select</option>";
			if (response) {
				$.each(response, function(i, v) {
					contact_html += "<option value=" + v.contact_id + ">" + v.first_name + " " + v.last_name + "( " + v.email + " ) </option>";
				});
			}
			modal.find('#deal-contact').html(contact_html);
			modal.find('#deal-contact, #deal_car_sellers').prop('disabled', false).trigger('chosen:updated');
			$(".contact-container").show();
			$(".organisation-container").hide();

			return true;
		})
	}

	const initContactOrganisationDropdown = () => {
		$(".contact-list").select2({
			minimumInputLength: 3,
			ajax: {
				url: site_url('deals/ajax_get_contacts_and_orgs'),
				dataType: 'json',
				type: 'GET',
				data: function(term) {
					return {
						term: term,
						form: 'add_deal'
					};
				}
			},
			templateSelection: function(data, container) {
				if (data.type != "") {
					switch (data.type) {
						case "contact":
							$(data.element).attr({
								"data-type": data.type,
								"data-email": data.email,
								"data-first-name": data.first_name,
								"data-last-name": data.last_name,
								"data-street": data.street,
								"data-house-number": data.house_number,
								"data-postal-code": data.postal_code,
								"data-mobile": data.mobile,
								"data-town": data.city
							});
							break;

						case "organisation":
							$(data.element).attr({
								"data-type": data.type,
								"data-org-type": data.cost_category,
								"data-status": data.status,
								"data-email": data.email
							});
							break;
					}
				}

				return data.text;
			}
		});

		$("#existing-contact").select2({
	        minimumInputLength: 3,
	        ajax: {
	            url: site_url('contact/ajax_get_contacts_for_autocomplete'),
	            dataType: 'json',
	            type: "GET",
	            data: function(term) {
	                return {
	                    term: term
	                };
	            }
	        }
		});
	}

	return {
		init: () => {
			console.log('init');

			$('#select-contact-modal').on('change', '.contact-list', function() {
				onSelectOrganizationOrContact($(this));
				$('#form-add-new-deal #deal_channel').trigger('change');
			});

			$('#select-contact-modal').on('show.bs.modal', function() {
				$('#form-add-new-deal #deal_channel').trigger('change');
			});

			$('#select-contact-modal').on('hidden.bs.modal', function() {
				onCloseAddDealModal($(this));
			});

			$('#deal_channel').on('change', function() {
				onChangeChannel($(this));
			});

			$('.open-associate-contact').on('click', function() {
				openAssociateContactWithOrganisationModal($('#associate-contact'), $(this));
			});

			$('.btn-associate-contact').on('click', function() {
				let parent_modal = $('#select-contact-modal').css('display') == 'block' ? $('#select-contact-modal') : $('#one-step-order-note-modal');
				associateContactWithOrganisation($('#associate-contact'), parent_modal);
			});

			$('.open-add-organisation-modal').on('click', function() {
				openAddOrganisationModal($('#addOrganisation'), $(this));
			});

			$('.open-associate-organisation').on('click', function() {
				openAssociateOrganisationWithContactModal($('#associate_organisation_popup'), $(this));
			});

			$('#associate_organisation_popup').on('hidden.bs.modal', function() {
				onCloseAssociateOrganisationModal();
			});
			$('.accept_org, .reject_org').on("click", function() {
				let parent_modal = $('#select-contact-modal').css('display') == 'block' ? $('#select-contact-modal') : $('#one-step-order-note-modal');
				changeOrganisationStatus($(this), parent_modal);
			});

			$('.open-add-new-contact').on('click', function() {
				openAddContactModal($(this), $('#add-new-contact'));
			});

			$('#add-new-contact').on('hidden.bs.modal', function() {
				onCloseAddContactModal($(this));
			});

			$('#btn-add-new-contact').on('click', function() {
				let parent_modal = $('#select-contact-modal').css('display') == 'block' ? $('#select-contact-modal') : $('#one-step-order-note-modal');
				addContact($(this), parent_modal);
			});

			$('#btn-add-new-deal').on('click', function() {
				addDeal($(this));
			});

			$('#leadEditModal').on('click', '#add_interested_cars', function() {
				onClickAddInterestedCarsButton();
			});

			$('#leadEditModal').on('change', '#lead_has_interest', function() {
				addInterestedCar($(this));
			});

			$('#leadEditModal').on('click', '.generate-proposals', function() {
				onClickGenerateProposalsButton($(this), $('#generate-proposal'));
			});

			$('#soldCar').on('hidden.bs.modal', function() {
				onCloseSellCarsModal($(this));
			});

			if($(".from-module").val() != 'crm') initContactOrganisationDropdown();
		}
	};
}();

class DealApi {

	static addContact (contact) {
		let url = site_url('deals/ajax_add_contact');
		let jsonData = { contact: contact };
		return this.sendRequest(url, jsonData);
	}

	static getContact (contact_id) {
		let url = site_url('contact/ajax_get_contact');
		let jsonData = { contact_id: contact_id };
		return this.sendRequest(url, jsonData);
	}

	static updateContactAddress (jsonData) {
		let url = site_url('contact/update_lead_address');
		return this.sendRequest(url, jsonData);
	}

	static getInterestedCars (deal_id) {
		let url = site_url('deals/ajax_get_interested_cars_deal');
		let jsonData = { deal_id: deal_id };
		return this.sendRequest(url, jsonData);
	}

	static addInterestedCar (jsonData) {
		let url = site_url('deals/ajax_update_interested_cars_for_deals');
		return this.sendRequest(url, jsonData);
	}

	static addCarProposalDataForBrent (formData) {
		let jsonData = {};
		formData.forEach((value, key) => jsonData[key] = value);
		let url = site_url('car/ajax_add_offer_data_b_rent');
		return this.sendRequest(url, jsonData);
	}

	static addCarProposalData (formData) {
		let jsonData = {};
		formData.forEach((value, key) => jsonData[key] = value);
		let url = site_url('car/ajax_add_offer_data');
		return this.sendRequest(url, jsonData);
	}

	static getContactDataForCarProposal (jsonData) {
		let url = site_url('contact/ajax_get_contact_data_for_car_proposal');
		return this.sendRequest(url, jsonData);
	}

	static getMonthlyPaymentOfCar (jsonData) {
		let url = site_url('car/get_monthly_payment_with_accessories');
		return this.sendRequest(url, jsonData);
	}

	static getAllContactsByOrganisation (org_id) {
		let url = site_url('organisation/ajax_get_users_by_organisation_id');
		let jsonData = { org_id: org_id };
		return this.sendRequest(url, jsonData);
	}

	static associateContactWithOrganisation (jsonData) {
		let url = site_url('organisation/ajax_associate_contact_with_organisation');
		return this.sendRequest(url, jsonData);
	}

	static getAllOrganisationsByContactId (contact_id) {
		let url = site_url('organisation/get_organizations_by_contact_id');
		let jsonData = { contact_id: contact_id };
		return this.sendRequest(url, jsonData);
	}

	static getOrganisation (organisation_id) {
		let url = site_url('organisation/ajax_get_organisation');
		let jsonData = { organisation_id: organisation_id };
		return this.sendRequest(url, jsonData);
	}

	static updateOrganisation (jsonData) {
		let url = site_url('organisation/ajax_update_organisation');
		return this.sendRequest(url, jsonData);
	}

	static changeOrganisationStatus (jsonData) {
		let url = site_url('organisation/ajax_change_status');
		return this.sendRequest(url, jsonData);
	}

	static checkOrganisationHasUniqueBTW (jsonData) {
		let url = site_url('organisation/ajax_unique_btw');
		return this.sendRequest(url, jsonData);
	}

	static addDeal (jsonData) {
		let url = site_url('deals/ajax_add_deal');
		return this.sendRequest(url, jsonData);
	}

	static sendRequest(url, jsonData) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		let data = { method: 'POST', headers: headers, body: this.JSON_to_URLEncoded(jsonData)};
		let request = new Request(url, data);

		return fetch(request).then(response => response.json()).then(response => response).catch(error => error);
	}

	static JSON_to_URLEncoded(element, key, list) {
		var list = list || [];
		if(typeof(element)=='object'){
			for (var idx in element)
				this.JSON_to_URLEncoded(element[idx],key ? key+'['+idx+']':idx, list);
		} else {
			list.push(key+'='+encodeURIComponent(element));
		}
		return list.join('&');
	}
}
$(document).ready(function(){
	console.log('document ready');
	DealPipeline.init();
});