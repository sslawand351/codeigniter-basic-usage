/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraAllowedContent = '*{*}';
	config.filebrowserBrowseUrl = site_url('resources/admin/ckfinder/ckfinder.html');
	config.filebrowserImageBrowseUrl = site_url('resources/admin/ckfinder/ckfinder.html?type=Images');
	config.filebrowserUploadUrl = site_url('resources/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files');
	config.filebrowserImageUploadUrl = site_url('resources/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images');
};
