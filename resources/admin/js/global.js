$(document).ready(function(){

	$("#activities").on("click", function() {

        $("#unread-activity-count").text("");
        
        $.ajax({
            type: "POST",
            url: site_url('activity/update_activity_seen_for_user'),
            success: function(response){}
        });
    });

    $('#activities-list').slimScroll({
	    height: '300px'
	});

$(document).on('click', '.close_modal', function(e) {
    $(this).closest('form').find("input[type=text], textarea").val("");
});

$(document).on('click', '.change-language', function(e) {
    e.preventDefault();
    var lang = $(this).attr('data-id');
    $.cookie("lang", lang, { path: '/' });
        window.language = lang; 
        window.location.reload();
    });

})

function reset_form(selector)
{
    $(selector).find('.error').html('');
    $(selector).find('.error').html('');
    $(selector).find('.error').html('');

    // $(selector).find('input').val('');
    $(selector).find('select').val('');
    $(selector).find('textarea').val('');
}

function site_url(url_slug)
{
    if(typeof GLOBALS.site_url === 'undefined')
    {
        return 'javascript:void(0)';
    }
 
    if(typeof GLOBALS.ci_index_page !== 'undefined' && GLOBALS.ci_index_page.length > 0)
        return GLOBALS.site_url + GLOBALS.ci_index_page + "/" + url_slug;
    else
        return GLOBALS.site_url + url_slug;
}
 
function ajax_authenticate(res)
{
    if(res.rc)
    {
        $.jGrowl(res.msg);
        window.location.href = site_url('admin/login');
    }
    else
    {
        return true;
    }
}
  
$(document).ajaxError(function() {
    $.jGrowl('Error occurred in ajax request. Please open console and check an issue.');
});
 
function form_required(val, label)
{
    var response = {};
    if($.trim(val) == "")
    {
        response["msg"] = "The " + label + " field is required.";
        response["error"] = true;    
    }
    else
    {
        response["error"] = false;
    }
    return response;
}