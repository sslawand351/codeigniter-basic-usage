$(document).ready(function(){

	/*********************************************
	* VALIDATION RULES FOR SEND NEWSLETTER
	**********************************************/
	$("#form-send-newsletter").validate({
        rules:{
               'from_email': { required: true, email: true },
               'subject': "required" 
            },
        messages:{
        		'from_email': {
				      required: "Please enter email address",
				      email: "Please enter valid email address"
				    },
				'subject': {
					required : 'The subject field is required'
				}
            }
    });
    /*********************************************
	* SEND NEWSLETTER FORM WIZARD
	**********************************************/
	$('#rootwizard').bootstrapWizard({
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard .progress-bar').css({width:$percent+'%'});
        },
        'onNext': function(tab, navigation, index) {
        	$("#form-send-newsletter").validate();
            var valid = $("#form-send-newsletter").valid();
           	$('.credits-error').html('');
			switch( index )
			{
				case 1:
					var newsletter_template_id = $('#newsletter-template-list').val();
					if( newsletter_template_id == '' )
					{
						var text = $('#newsletter-template-list').siblings('.text-danger').data('text');
						$('#newsletter-template-list').siblings('.text-danger').html(text);
						valid = false;
					}
					else
					{
						$('#newsletter-template-list').siblings('.text-danger').html('');
					}
					break;

				case 2 :
					var group_valid = true, lead_valid = true;
					if( $('#group-list option:selected').length == 0 )
					{
						group_valid = false;
					}
					else
					{
						$('#group-list').siblings('.text-danger').html('');
					}

					if( $('#lead-list option:selected').length == 0 )
					{
						lead_valid = false;
					}
					else
					{
						$('#lead-list').siblings('.text-danger').html('');
					}

					if( !( group_valid || lead_valid) )
					{
						var text = $('.text-danger.group-user-error').data('text');
						$('.text-danger.group-user-error').html(text);
						valid = false;
					}
					else
					{
						$('.text-danger.group-user-error').html('');
					}
					break;
				
				default : break;
			}
			
            if(!valid) {
                return false;
            }
            else
            {
            	if(index == 3)
                {
                	var newsletter_template_id = $("#form-send-newsletter").find('select[name="newsletter_template_id"]').val();
					$('#send-newsletter-popup .newsletter-template-view').attr('src', site_url('admin/newsletter/view/' + newsletter_template_id));
                }

                if(index == 1)
                {
                	var channel_id = $('#newsletter-template-list option:selected').attr('data-channel-id');
                	$.ajax({
			    		url: site_url('admin/newsletter/ajax_get_groups_and_users'),
			    		data: { channel_id : channel_id },
			    		dataType: 'json',
			    		method: 'POST',
			    		success: function(response){
			    			$('#form-send-newsletter #tab2 .controls').html(response.html);
			    			$('#group-list').chosen();
			    			$('#lead-list').chosen();
			    		}
			    	});
                }
            }
        },
        'onTabClick': function(tab, navigation, index) {
            return false;
        }
    });
	/***********************************************************
	* Submit send newsletter
	************************************************************/
    $(document).on('click','.submit-send-newsletter-form', function(){
    	/*$("#form-send-newsletter").submit();*/
    	$('.credits-error').html('');
    	var selected_leads = $('#lead-list optgroup.leads option:selected');
    	var selected_clients = $('#lead-list optgroup.clients option:selected');
    	var userid;
    	var leads = [], user_ids = [];
    	
    	$.each( selected_leads, function(i,v){
    		userid = $(v).attr('data-userid');
    		user_ids.push( userid );
            leads.push( $(v).val() );
        });

        $.each( selected_clients, function(i,v){
        	userid = $(v).attr('data-userid');
    		user_ids.push( userid );
        });
        $('#tab5').append('<div class="ajax_loader" style="width:100%;text-align:center;padding:5px"><img width="40" height="40" src="' + site_url('resources/admin/img/balls.gif') + '" alt="Ajax loader"></div>');
    	$('.submit-send-newsletter-form').hide();
    	$.ajax({
    		url: site_url('admin/newsletter/ajax_send_newsletter'),
    		data: $("#form-send-newsletter").serialize() + '&leads=' + leads + '&user_ids=' + user_ids,
    		dataType: 'json',
    		method: 'POST',
    		success: function(response){
    			$('#tab5 .ajax_loader').hide();
    			$('.submit-send-newsletter-form').show();
    			if( response.rc )
    			{
    				$('.msgs').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
    				$("#send-newsletter-popup").modal('hide');
    			}
    			else
    			{
    				if( ! response.credits.rc )
    				{
    					$('.credits-error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
    				}
    			}
    		}
    	});
	});
	
	/***********************************************************
	* Clear send newsletter on close
	************************************************************/
	$(document).on('hidden.bs.modal','#send-newsletter-popup', function(){
		$('#rootwizard').bootstrapWizard('show',0);
		var _this = $(this);
		_this.find('input[type=text]').val('');
		_this.find('select option').prop('selected', false).trigger('chosen:updated');
		_this.find('.email_preview_html').html('');
		$('.credits-error').html('');

	});

	$(".open-view-newsletter-template-popup").click( function() {
		var _this = $(this);
		var newsletter_template_id = _this.attr('data-newsletter-template-id');
		$('#viewNewsletterTemplate .newsletter-template-view').attr('src', site_url('admin/newsletter/v/' + newsletter_template_id));
	});
});

function iframeLoaded(iFrameID) {
	if(iFrameID) {
	    // here you can make the height, I delete it first, then I make it again
	    iFrameID.height = "";
	    iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
	}   
}