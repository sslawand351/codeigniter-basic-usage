$(document).ready(function(){

    $("#fieldType").change(function(){
        var fieldType = $(this).val();

        switch(fieldType)
        {
            case "textField":
                var template_html = $("#template-text-field").html();
                $(".form-fields").append(template_html);
                break;
                
                case "passwordField":
                var template_html = $("#template-password-field").html();
                $(".form-fields").append(template_html);
                break;

            case "textFieldLarge":
                var template_html = $("#template-textarea-field").html();
                $(".form-fields").append(template_html);
                break;

            case "dateField":
                var template_html = $("#template-date-field").html();
                $(".form-fields").append(template_html);
                break;

            case "emailField":
                var template_html = $("#template-email-field").html();
                $(".form-fields").append(template_html);
                break;

            case "websiteField":
                var template_html = $("#template-website-field").html();
                $(".form-fields").append(template_html);
                break;

            case "radioButtons":
                var template_html = $("#template-radio-button-field").html();
                $(".form-fields").append(template_html);
                break;

            case "uploadField":
                var template_html = $("#template-upload-field").html();
                $(".form-fields").append(template_html);
                break;

            case "checkBoxes":
                var template_html = $("#template-checkbox-field").html();
                $(".form-fields").append(template_html);
                break;

            case "dropdown":
                var template_html = $("#template-dropdown-field").clone();
                $(".form-fields").append(template_html.html());
                $(".form-fields").find('select[name=dropdown]:not(".noChosen")').chosen();
                $('.form-fields .chosen-container').last().remove();
                break;

        }

        check();
        removeClass();
    });


    function check(){
        /*$('.new:input[type=checkbox]').each(function() {
            var span = $('<span class="' + $(this).attr('type') + ' ' + $(this).attr('class') + '"></span>').click(doCheck).mousedown(doDown).mouseup(doUp);
            if ($(this).is(':checked')) {
                span.addClass('checked');
            }
            $(this).wrap(span).hide();
        });*/

        function doCheck() {
            if ($(this).hasClass('checked')) {
                $(this).removeClass('checked');
                $(this).addClass('unchecked');
                $(this).children().prop("checked", false);
                $(this).children().removeAttr('checked');
            } else {
                $(this).addClass('checked');
                $(this).removeClass('unchecked');
                $(this).children().prop("checked", true);
                $(this).children().attr("checked", "checked");
            }
        }

        function doDown() {
            $(this).addClass('clicked');
        }

        function doUp() {
            $(this).removeClass('clicked');
        }
    }
    
    function removeClass(){
        $('.new:input[type=checkbox]').removeClass('new');
    }
    
    $(document).on("click", ".editThisField", function(){
        $(this).parents().siblings(".editFieldForm").slideToggle();
    })

    $(document).on("click", ".closeButton", function(){
        $(this).parents(".editFieldForm").slideUp();
    })

    $(document).on("click", ".addOption", function(){
        $(this).parents(".optionsContainer").append("<div class='control-group'>"+
                                                     
                                                         "<div class='form-group'>"+
                                                            "<label class='control-label'>Option: </label>"+
                                                             "<input type='text' class='form-control'> <i class='fa fa-trash-o deleteOption'></i>"+
                                                         "</div>"+
                                                     "</div>");
    })
    
    $(document).on("click", ".saveButton", function(){

        var labelValue = $(this).parents(".editFieldForm").find("input[type=text]").val();
        $(this).parents(".editFieldForm").parents(".row").find(".col-md-3 label.control-label").html(labelValue);

        if(labelValue == "")
        {
            alert("Please enter the title");
            return false;
        }
        var optionsArray = Array();
        if($(this).parents(".editFieldForm").children(".optionsContainer").length > 0)
        {
            $(this).parents(".editFieldForm").children(".optionsContainer").find("input[type=text]").each(function(i,v){
                if($(v).val() != ""){
                    optionsArray.push($(v).val());
                }
            })
        }               
        
        var dump = "";

        if($(this).parents(".editFieldForm").siblings("div.mainContainer").hasClass("dropdownContainer"))
        {
            dump += "<div class='form-group'>";
            dump += "<select name='dropdown' data-type='dropdown' class='form-control' ui-jq='chosen'>";
            $.each(optionsArray, function(i, v){
                dump += "<option value='"+v+"'>"+v+"</option>";
            })
            dump += "</select></div>";

            $(this).parents(".editFieldForm").siblings("div.dropdownContainer").html(dump);
            $(this).parents(".editFieldForm").siblings("div.dropdownContainer").find('select').chosen();
        }
        else if($(this).parents(".editFieldForm").siblings("div.mainContainer").hasClass("checkBoxesContainer"))
        {
            dump += "<div class='form-group'>";
            $.each(optionsArray, function(i, v){
                dump += "<label class='i-checks'><input style='margin-top:0px;' type='checkbox' data-type='checkbox' name='choice' value='"+v+"'><i></i>&nbsp; "+v+"</label><br/>";
            })
             dump += "</div>";

            $(this).parents(".editFieldForm").siblings("div.checkBoxesContainer").html(dump);
        }
        else if($(this).parents(".editFieldForm").siblings("div.mainContainer").hasClass("radioButtonsContainer"))
        {
            $.each(optionsArray, function(i, v){
                dump += "<div class='form-group'><input style='' type='radio' data-type='radioButton' name='choice' value='"+v+"'>&nbsp; "+v+"</div>";
            })

            $(this).parents(".editFieldForm").siblings("div.radioButtonsContainer").html(dump);
        }

        $(this).parents(".editFieldForm").slideUp();

    })

    $(document).on("click", ".deleteThisField", function(){
        
        if(confirm("Are you sure you want to delete?")==false)
        {
            e.preventDefault();
        }
        else
        {
            $(this).parents("div.row").remove();
        }
    })

    $(document).on("click", ".deleteOption", function(){
        
        if(confirm("Are you sure you want to delete?")==false)
        {
            e.preventDefault();
        }
        else
        {
            $(this).parents("div.form-group").remove();
        }
    })

    $("input[name=createForm]").click(function(e){                                                       
                       
        var formTitle = Array();
        var formArray = Array();
        
        var formName = $("#formTitle").val();
        
        if(formName == "")
        {
            alert("Please enter form name");
            return false; 
        }
        var websiteID = $("#websiteIdHidden").val();
        
        formTitle.push(formName);
        formTitle.push(websiteID);
               
        $("form .mainContainer").each(function(i, v){ 
            var fieldArray = Array();
            var fieldOptions = Array();
            
            var data_type = $(v).find("input").attr("data-type");

            if($(v).find("input").length >= 1 && data_type != undefined){                
                                                
                var type = $(v).find("input").attr("data-type");
                fieldArray.push(type);
                
                var labelValue = $(v).siblings(".editFieldForm").find("input[type=text]").val();
                fieldArray.push(labelValue);                                 
                
                var isRequired = $(v).siblings(".editFieldForm").find(".required:input[type=checkbox]:checked").val();
                
                
                if (typeof isRequired == 'undefined'){
                        
                    var checkbox = "No";
                }
                else{
                    var checkbox = "Yes";
                }                                
                
                fieldArray.push(checkbox);
                
                if(type == 'checkbox' || type == 'radioButton'){
                    
                   $(v).find("input").each(function(){
                        
                        var options1 = $(this).val();                        
                        fieldOptions.push(options1);
                    });
                }
                if(type == 'fileUpload'){
                    var isMultiple = $(v).siblings(".editFieldForm").find(".isMultiple:input[type=checkbox]:checked").val();
                    if (typeof isMultiple == 'undefined'){
                        
                        var checkbox = "No";
                    }
                    else{
                        var checkbox = "Yes";
                    }
                    fieldArray.push(checkbox);
                }
                
                fieldArray.push(fieldOptions);
                formArray.push(fieldArray);                              
            }                            
            
            if($(v).find("textarea").length >= 1){                
                
                $(v).find("textarea").each(function(){
                
                    var type1 = $(v).find("textarea").attr("data-type");
                    fieldArray.push(type1);
                
                    var labelValue1 = $(v).siblings(".editFieldForm").find("input[type=text]").val();
                    fieldArray.push(labelValue1);                                 
                
                    var isRequired1 = $(v).siblings(".editFieldForm").find(".required:input[type=checkbox]:checked").val();                                                            
                        
                    if (typeof isRequired1 == 'undefined'){
                        
                        var checkbox = "No";
                    }
                    else{
                        var checkbox = "Yes";
                    }

                    fieldArray.push(checkbox);                       
                    formArray.push(fieldArray);
                                                   
                });     
            }

            if($(v).find("select").length >= 1){                
                var type2 = $(v).find("select").attr("data-type");                    
                    fieldArray.push(type2);           
                $(v).find("select").each(function(){
                       
                    var labelValue2 = $(v).siblings(".editFieldForm").find("input[type=text]").val();
                    fieldArray.push(labelValue2);                                 
                
                    var isRequired2 = $(v).siblings(".editFieldForm").find(".required:input[type=checkbox]:checked").val();
                    
                    if (typeof isRequired2 == 'undefined'){
                        
                        var checkbox = "No";
                    }
                    else{
                        var checkbox = "Yes";
                    }
                    
                    fieldArray.push(checkbox);
                    
                    $(v).find("select").children("option").each(function(){
                        
                        var options2 = $(this).val();                        
                        fieldOptions.push(options2);
                    });
                    
                    fieldArray.push(fieldOptions);
                    formArray.push(fieldArray);                                        
               });                
            }
            
        });

        formTitle.push(formArray);         
        
        $.ajax({
            
            dataType: "json",
            type: "post",
            data: {'form':formTitle},
            url : site_url('admin/form/ajax_add_form'),
            success: function(data){  
                window.location.href = site_url('admin/form/listing');               
            }, 
            error: function(xhr, textStatus, errorThrown){ 
                
             alert('Error!  Status = ' + xhr.status); 
          }                                  
        });               
    })

    
    $("input[name=saveForm]").click(function(e){  
        var formTitle = Array();
        var formArray = Array();
        
        var formName = $("#formTitle").val();
        
        if(formName == "")
        {
            alert("Please enter the title");
            return false;
        }

        var websiteID = $("#websiteIdHidden").val();
        var formID = $("#formIdHidden").val();
        
        formTitle.push(formName);
        formTitle.push(websiteID);        
        
        $("form .mainContainer").each(function(i, v){ 
            var fieldArray = Array();
            var fieldOptions = Array();
            
            var data_type = $(v).find("input").attr("data-type");

            if($(v).find("input").length >= 1 && data_type != undefined){                
                                                
                var type = $(v).find("input").attr("data-type");
                fieldArray.push(type);                
                
                var labelValue = $(v).siblings(".editFieldForm").find("input[type=text]").val();
                fieldArray.push(labelValue);                                 
                
                var isRequired = $(v).siblings(".editFieldForm").find(".required:input[type=checkbox]:checked").val();
                
                if (typeof isRequired == 'undefined'){
                        
                    var checkbox = "No";
                }
                else{
                    var checkbox = "Yes";
                }                                
                
                fieldArray.push(checkbox);
                
                var field_id = $(v).find("input").attr("data-field-id");
                fieldArray.push({'field_id': field_id});
                
                if(type == 'checkbox' || type == 'radioButton'){
                    
                    var optionId = Array();
                    
                    $(v).find("input").each(function(){
                        
                        var options1 = $(this).val();                        
                                                
                        var option_id = $(this).attr("data-option-id");
                        
                        fieldOptions.push({'option_id':option_id, 'option_value':options1});                        
                        
                    });
                    
                    fieldArray.push(fieldOptions);            
                }   
                
                if(type == 'fileUpload'){
                    var isMultiple = $(v).siblings(".editFieldForm").find(".isMultiple:input[type=checkbox]:checked").val();
                    if (typeof isMultiple == 'undefined'){
                        
                        var checkbox = "No";
                    }
                    else{
                        var checkbox = "Yes";
                    }
                    fieldArray.push(checkbox);
                }
                
                formArray.push(fieldArray);                              
            }
            
            if($(v).find("textarea").length >= 1){                
                
                $(v).find("textarea").each(function(){
                
                    var type1 = $(v).find("textarea").attr("data-type");
                    fieldArray.push(type1);                    
                
                    var labelValue1 = $(v).siblings(".editFieldForm").find("input[type=text]").val();
                    fieldArray.push(labelValue1);                                 
                
                    var isRequired1 = $(v).siblings(".editFieldForm").find(".required:input[type=checkbox]:checked").val();                                                            
                        
                    if (typeof isRequired1 == 'undefined'){
                        
                        var checkbox = "No";
                    }
                    else{
                        var checkbox = "Yes";
                    }

                    fieldArray.push(checkbox);
                    
                    var field_id = $(v).find("textarea").attr("data-field-id");
                    fieldArray.push({'field_id': field_id});
                    
                    formArray.push(fieldArray);
                                                   
                });     
            }
        
            if($(v).find("select").length >= 1){                
                                                
                $(v).find("select").each(function(){
                                                    
                    var type2 = $(v).find("select").attr("data-type");                    
                    fieldArray.push(type2);                    
                    
                    var labelValue2 = $(v).siblings(".editFieldForm").find("input[type=text]").val();
                    fieldArray.push(labelValue2);                                 
                
                    var isRequired2 = $(v).siblings(".editFieldForm").find(".required:input[type=checkbox]:checked").val();
                    
                    if (typeof isRequired2 == 'undefined'){
                        
                        var checkbox = "No";
                    }
                    else{
                        var checkbox = "Yes";
                    }
                    
                    fieldArray.push(checkbox);
                    
                    var field_id = $(v).find("select").attr("data-field-id");
                    fieldArray.push({'field_id': field_id});                    
                    
                    var optionId2 = Array();
                    
                    $(v).find("select").children("option").each(function(){
                        
                        var options2 = $(this).val();                                                
                        
                        var option_id = $(this).attr("data-option-id");
                        
                        fieldOptions.push({'option_id':option_id, 'option_value':options2});                       
                        
                    });
                    
                    fieldArray.push(fieldOptions);                   
                    
                    formArray.push(fieldArray);                                        
               });                
            }
            
        });
        
        formTitle.push(formID);
        formTitle.push(formArray);        
        
        $.ajax({
            
            datatype: "json",
            type: "post",
            data: {'form':formTitle},
            url : site_url('admin/form/ajax_update_form'),
            success: function(data){

                window.location.href = site_url('admin/form/listing');
            }, 
            error: function(xhr, textStatus, errorThrown){ 
                alert('Error!  Status = ' + xhr.status); 
            }                                  
        });               
    })

/*init_form_fields_sort();
function init_form_fields_sort()
{
    $("#sort-form-fields").sortable().bind('sortupdate', function() {

        var data = [];

        $("#sort-form-fields").children().each(function(i, v) {
            data.push($(v).attr('id').replace("data_", ""));
        });
    });
}*/
})