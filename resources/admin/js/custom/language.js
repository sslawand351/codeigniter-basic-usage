$(document).ready(function(){

	var hash = window.location.hash.substr(1);

	$('select[name=json_file_name] option').each(function(i){
		var _this = $(this);
		var json_file_name = _this.val();
		var hash_name = hash + '.json';
		//console.log( json_file_name == hash_name );
		if( json_file_name == hash_name )
        {

        	_this.prop('selected', true).trigger('chosen:updated');
        	//console.log(_this);
        	$.ajax({
				url: site_url('admin/language/ajax_load_translation_forms'),
				data:{ file_name: json_file_name },
				dataType: 'json',
				type: 'post',
				success: function(response){
					$('#english-translations').html(response.en_html);
					$('#dutch-translations').html(response.nl_html);
					$('#french-translations').html(response.fr_html);
				}
			});
			return false;
			//$('select[name=json_file_name]').trigger('change');
		}
	})
	
	$('select[name=json_file_name]').change(function(){
		var _this = $(this);
		var json_file_name = _this.val();

		$.ajax({
			url: site_url('admin/language/ajax_load_translation_forms'),
			data:{ file_name: json_file_name },
			dataType: 'json',
			type: 'post',
			success: function(response){
				$('#english-translations').html(response.en_html);
				$('#dutch-translations').html(response.nl_html);
				$('#french-translations').html(response.fr_html);
			}
		});
	})

	$(document).on('click', '.form-submit', function(){
		var _this = $(this);
		var form = _this.parents('form');
		console.log(form);
		form.submit();
	});
});