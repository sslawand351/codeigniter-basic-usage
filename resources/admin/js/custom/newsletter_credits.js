jQuery(function($) {
    jQuery(document).ready(function(){
        $.fn.credits_purchased_listing(0);
        $.fn.credits_consumed_listing(0);
    });

    $.fn.credits_purchased_listing = function (offset)
    {
        $.ajax({
            url: site_url('admin/newsletter_credit/ajax_get_credits_history/' + offset),
            data: {
                'purchase_credits' : 1
            },
            method: 'post',
            dataType:'json',
            success: function(response)
            {
                if(response.rc)
                {
                    $('.purchased-table-container').replaceWith(response.html);
                }
            }
        })
    }

    $.fn.credits_consumed_listing = function (offset)
    {
        $.ajax({
            url: site_url('admin/newsletter_credit/ajax_get_credits_history/' + offset),
            data: {
                'purchase_credits' : 0
            },
            method: 'post',
            dataType:'json',
            success: function(response)
            {
                if(response.rc)
                {
                    $('.consumed-table-container').replaceWith(response.html);
                }
            }
        })
    }
});