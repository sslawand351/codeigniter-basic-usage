$(document).ready(function(){

	$('.view_forms').on('click',function(){

        var form_id = $(this).attr('data-id');
        var id = $(this).attr('data-form-id');

        $.ajax({
        	url : site_url('admin/form/ajax_get_frontend_form_list'),
            data : {
                form_id : form_id,
                id: id
            },
            dataType : 'json',
            type : 'POST',
            success : function(response) 
            {
            	if(response.success == true)
            	{
            		$("#form_modal .content").replaceWith(response.forms);
            	}
                else
                {
                	$("#form_modal .content").html('<p>No forms added yet</p>');
                }
            }
        })
    })
})
    