$(document).ready( function()
{
	 /**************************************************
    * Delete user from Group
    **************************************************/
    $(document).on("click", ".delete-user-from-group", function(e){
        var _this = $(this)
        var group_id = _this.attr('data-group-id');
        var group_user_id = _this.attr('data-id');
        $(".group_id").val( group_id );
        $(".group_user_id").val( group_user_id );
    });

    $(".delete-user-from-group-btn").on("click",function(){
        var form = $("#form-delete-user-from-group");
        form.submit();
    });

    $(document).on("click", ".unsubscribe-user-from-group", function(e){
        var _this = $(this)
        var group_id = _this.attr('data-group-id');
        var group_user_id = _this.attr('data-id');
        $(".group_id").val( group_id );
        $(".group_user_id").val( group_user_id );
    });

    $(".unsubscribe-user-from-group-btn").on("click",function(){
        var form = $("#form-unsubscribe-user-from-group");
        form.submit();
    });

    $(document).on("click", ".undo-unsubscribe-user-from-group", function(e){
        var _this = $(this)
        var group_id = _this.attr('data-group-id');
        var group_user_id = _this.attr('data-id');
        $(".group_id").val( group_id );
        $(".group_user_id").val( group_user_id );
    });

    $(".undo-unsubscribe-user-from-group-btn").on("click",function(){
        var form = $("#form-undo-unsubscribe-user-from-group");
        form.submit();
    });

    $(document).on("click", ".open_edit_group_user", function(e){
        var _this = $(this);
        var group_id = _this.attr('data-group-id');
        var group_has_users_id = _this.attr('data-id');
        var email = encodeURIComponent( _this.attr('data-email') );

        $.ajax({
            dataType:"JSON",
            type:"POST",
            data:"email="+email,
            url: site_url('admin/group/ajax_get_user_by_group_has_user_id'),
            success:function(response){
                if(response.rc)
                {
                    $('#edit-user-from-group-popup .modal-body').html(response.edit_user_group_modal);
                    $('#edit-user-from-group-popup').modal('show');
                }
            }
        });
    });

    $(document).on("click", "#btn-edit-group-user", function(e){
        var group_user_name = $(".group_user_name").val();
        var group_user_email = $(".group_user_email").val();
        
       /*if( group_user_name == "" )
       {    
            $(".group_user_name").siblings('.text-danger').html('This field is required.');
       }
       else */if( group_user_email == "" )
       {     
            $(".group_user_email").siblings('.text-danger').html('This field is required.');
       }
       else
       {
            $.ajax({
            dataType:"JSON",
            type:"POST",
            data:$("#form-edit-group-user").serializeArray(),
            url: site_url('admin/group/ajax_update_group_user'),
            success:function(response){
                if(response.rc)
                {   
                    $('#edit-user-from-group-popup').modal('hide');
                    $('html,body').animate({
                        scrollTop: $("#content").offset().top},
                        'slow');
                    $('.alert-success').css('display','block');
                    $('#org_success_msg').html(response.msg);
                }
                else
                {
                    $(".group_user_email").siblings('.text-danger').html(response.email_error_msg);
                }
            }
        });
       }
    });
});

jQuery(function($) {
    jQuery(document).ready(function(){

        $(document).on('click','#user-search',function(){
            $.fn.group_users_listing(0);
            $.fn.unsubscribed_users_listing(0);
        });

        $.fn.group_users_listing(0);
        $.fn.unsubscribed_users_listing(0);
    });

    $.fn.group_users_listing = function (offset)
    {
        var group_id = $('#hidden-group-id').val();
        var email = $("#email-search").val();
        $.ajax({
            url: site_url('admin/group/ajax_get_group_users_listing/' + offset),
            data: {
                group_id: group_id,
                'is_unsubscribe' : 0,
                email: email
            },
            method: 'post',
            dataType:'json',
            success: function(users)
            {
                if(users.rc)
                {
                    $('.subscribed-table-container').replaceWith(users.html);
                }
            }
        })
    }

    $.fn.unsubscribed_users_listing = function (offset)
    {
        var group_id = $('#hidden-group-id').val();
        var email = $("#email-search").val();
        $.ajax({
            url: site_url('admin/group/ajax_get_group_users_listing/' + offset),
            data: {
                group_id: group_id,
                'is_unsubscribe' : 1,
                email: email
            },
            method: 'post',
            dataType:'json',
            success: function(users)
            {
                if(users.rc)
                {
                    $('.unsubscribed-table-container').replaceWith(users.html);
                }
            }
        })
    }
});