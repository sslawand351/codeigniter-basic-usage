$(document).ready(function(){
	$('#activities').click(function(){

		var list = $('#activities .list-group').has(".list-group-item");
		if( list.length == 0 )
		{
			$('#activities .list-group').html('<div class="" style="width:100%;text-align:center;padding:5px"><img width="40" height="40" src="' + site_url('resources/img/balls.gif') + '" alt="Ajax loader"></div>');
			$.ajax({
				url: site_url('admin/activity/ajax_get_activity_notifications'),
				data: {},
				method: 'post',
				dataType:'json',
				success: function(activities)
				{
					$('#activities .list-group').html(activities);
				}
		    });
		}
	});
});