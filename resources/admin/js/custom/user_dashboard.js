$(document).ready(function(){
	
	$.ajax({
		url: site_url('admin/activity/ajax_get_activities'),
		data: {},
		method: 'post',
		dataType:'json',
		success: function(activities)
		{
			$('.right-sidebar-activities').replaceWith(activities);
			if($(window).innerWidth() < 992)
			{
				$('.recent-activities-sidebar').removeAttr('id');
			}
			else
			{
				$('.recent-activities-sidebar').attr('id', 'right-aside');
			}
		}
    });
});