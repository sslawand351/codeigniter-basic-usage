$(document).ready(function(){
	
	$(document).on("click", ".delete_user", function(e){
        var user_id = $(this).attr('data-user-id');
        $(".user_id").val(user_id);
        $('#delete_user_popup').css('display', 'block','important');
    });

    $(".delete_user_btn").on("click",function(){
        var form = $("#delete_user_form");
        form.submit();
    });
});