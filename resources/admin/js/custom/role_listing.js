$(document).ready(function(){

	$(document).on("click", ".delete_role", function(e){
        var role_id = $(this).attr('data-role-id');
        $(".role_id").val(role_id);
    });

    $(".delete_role_btn").on("click",function(){
        var form = $("#delete_role_form");
        form.submit();
    });

});