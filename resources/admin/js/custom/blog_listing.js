$(document).ready(function(){

	/***************************************************
	* DELETE BLOG
	****************************************************/
	$(document).on("click", ".delete_blog", function(e){
        var blog_id = $(this).attr('data-blog-id');
        $(".blog_id").val(blog_id);
    });

    $(".delete_blog_btn").on("click",function(){
        var form = $("#delete_blog_form");
        form.submit();
    });

    /***************************************************
    * PUBLISH BLOG
    ****************************************************/
    $(document).on("click", ".publish_blog", function(e){
		e.preventDefault();
		
		$('#publish-blogs input[type="checkbox"]').prop('checked', false);
		$('#publish-blogs input[type="checkbox"]').removeClass('selected');
		$('#publish-blogs').data("blog-id", "");
		$('#publish-blogs #popup-message').text("");

		var blog_id = $(this).data("id");
		
		$.ajax({
			dataType:"JSON",
			type:"POST",
			data:"blog_id="+blog_id,
			url: site_url('blog/ajax_get_blog_channels'),
			success:function(response){
				if(response.success)
				{
					$('#publish-blogs').data("blog-id", blog_id);
					if(response.blog_channels.length > 0)
					{
						$.each(response.blog_channels, function( i, v )
						{
							$('#publish-blogs input[name="blog_channels['+v+']"]').prop('checked', true);
							$('#publish-blogs input[name="blog_channels['+v+']"]').addClass('selected');
						})
					}
				}
				else
					alert(response.msg)
			}
		});
	});


	$(document).on("click",".clickable-row",function(e){    
         
           var _this = $(this).parent('.row1');
           var href = $(this).parent("tr").attr("data-href");

            $(".clickable-row").attr('href',href);
           window.document.location = $(_this).attr("data-href"); 
 
	});

	$(".clickable-row").hover(function(e)
	{
		 $(".clickable-row").css({'cursor': 'pointer'});    
	});
})