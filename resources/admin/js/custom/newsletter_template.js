$(document).ready(function(){
	$(document).on('click','.open-send-newsletter', function(){

	});

	/*********************************************
	* VALIDATION RULES FOR SEND NEWSLETTER
	**********************************************/
	$("#form-send-newsletter").validate({
        rules:{
        		'template_name' : "required"
            },
        messages:{
        		'template_name': {
					required : 'The name field is required'
				}
            }
    });
    /*********************************************
	* SEND NEWSLETTER FORM WIZARD
	**********************************************/

    $(document).on('click', '#newsletter-email-preview, #newsletter-email-submit', function(){
		
		var _this = $(this);
		$("#form-send-newsletter").validate();
        var valid = $("#form-send-newsletter").valid();

		if(!valid)
		{
            return false;
        }
        else
        {        	
	    	$.ajax({
	    		url: site_url('admin/newsletter/ajax_create_newsletter_template'),
	    		data: $("#form-send-newsletter").serialize(),
	    		dataType: 'json',
	    		method: 'POST',
	    		success: function(response){

	    			if( response.rc )
	    			{
	    				$('.table-container').replaceWith(response.html_table_newsletter_template);
	    				$('#send-newsletter-popup').modal('hide');
	    				$("#success-msg").text($("#success-msg").attr('data-msg'));
	    				$(".alert-success").show();
	    			}
	    			else
	    			{
	    				$(".alert-success").hide();
	    			}
	    		}
	    	});
        }
    });

    /************************************************************
    * Build Newsletter Zone
    ************************************************************/
    $("#form-build-newsletter-zone").validate({
        rules:{
        		'zone_name' : "required"
            },
        messages:{
        		'zone_name': {
					required : 'The name field is required'
				}
            }
    });
    $(document).on('click', '.submit-newsletter-build-zone', function(){
		
		var _this = $(this);
		$("#form-build-newsletter-zone").validate();
        var valid = $("#form-build-newsletter-zone").valid();

        var zone_layout = $('#newsletter-zone-layout').val();

        if( zone_layout != "" )
        {
    		$('#newsletter-zone-layout').siblings('.text-danger').html('');

	        var textarea = $('textarea[name=zone_body]').val();
						
			if( $.trim($(textarea).text()) == "")
			{
				var text = $('textarea[name=zone_body]').siblings('.text-danger').data('text');
				$('textarea[name=zone_body]').siblings('.text-danger').html(text);
				valid = false;
			}
			else
			{
				$('textarea[name=zone_body]').siblings('.text-danger').html('');
			}
    	}
    	else
    	{
    		var text = $('#newsletter-zone-layout').siblings('.text-danger').data('text');
			$('#newsletter-zone-layout').siblings('.text-danger').html(text);
    		valid = false;
    	}

		if(!valid)
		{
            return false;
        }
        else
        {        	
        	var update_flag = $('#form-build-newsletter-zone').find('input[name=update_flag]').val();

        	if( update_flag == 1 )
        	{
        		$.ajax({
		    		url: site_url('admin/newsletter/ajax_edit_newsletter_zone'),
		    		data: $("#form-build-newsletter-zone").serialize()/* + '&is_email_preview=' + is_email_preview*/,
		    		dataType: 'json',
		    		method: 'POST',
		    		success: function(response){

		    			if( response.rc )
		    			{
		    				$('.table-container').replaceWith(response.html_table_newsletter_zones);
		    				$('#build-newsletter-zone-popup').modal('hide');
		    				$(".alert-success span").text(response.newsletter_zone.msg);
		    				$(".alert-success").show();
		    				init_newsletter_zones_sort();
		    			}
		    		}
		    	});
        	}
        	else
        	{
		    	$.ajax({
		    		url: site_url('admin/newsletter/ajax_create_newsletter_zone'),
		    		data: $("#form-build-newsletter-zone").serialize()/* + '&is_email_preview=' + is_email_preview*/,
		    		dataType: 'json',
		    		method: 'POST',
		    		success: function(response){

		    			if( response.rc )
		    			{
		    				$('.table-container').replaceWith(response.html_table_newsletter_zones);
		    				$('#build-newsletter-zone-popup').modal('hide');
		    				$(".alert-success span").text(response.newsletter_zone.msg);
		    				$(".alert-success").show();
		    				init_newsletter_zones_sort();
		    			}
		    		}
		    	});
		    }
        }
    });

	$('.build-zone').on('click', function(){
		$('#form-build-newsletter-zone').find('input[name=update_flag]').val('0');
	});

	$(document).on('click', '.edit-newsletter-zone', function(){
		var _this = $(this);
		var newsletter_zone_id = _this.attr('data-newsletter-zone-id');
		$.ajax({
    		url: site_url('admin/newsletter/ajax_get_newsletter_zone'),
    		data: {
    			'newsletter_zone_id' : newsletter_zone_id
    		},
    		dataType: 'json',
    		method: 'POST',
    		success: function(response){
    			$('#build-newsletter-zone-popup .modal-body').html(response.html);
    			$('#newsletter-zone-layout').chosen();
    			
    			CKEDITOR.replace( 'editor' );
				CKEDITOR.add
				CKEDITOR.instances['editor'].on('change', function() { CKEDITOR.instances['editor'].updateElement() });
    		}
		});
	});

	$(document).on('click', '.view-newsletter-template', function(){

		var _this = $(this);
		var newsletter_template_id = _this.attr('data-newsletter-template-id');
		$('#viewNewsletterTemplate .newsletter-template-view').attr('src', site_url('admin/newsletter/view/' + newsletter_template_id));
	});
	/****************************************************************
	* Send Test Newsletter
	****************************************************************/
	$("#form-send-test-newsletter").validate({
        rules:{
        		'from_email' :  { required: true, email: true },
        		'to_email' :  { required: true, email: true }
            },
        messages:{
        		'from_email': {
					required: "Please enter email address",
				    email: "Please enter valid email address"
				},
				'to_email':{
					required: "Please enter email address",
				    email: "Please enter valid email address"
				}
            }
    });

	$(document).on('click', '.send-test-newsletter', function(){
		var _this = $(this);
		var form = $('#form-send-test-newsletter');
		form.find('input[type=text]').val('');console.log('dsgf');
		form.find('label.text-danger').remove();
		var newsletter_template_id = _this.attr('data-newsletter-template-id');
		form.find('input[name=newsletter_template_id]').val(newsletter_template_id);
	});

	$(document).on('click', '#test-newsletter-email-submit', function(){
		var _this = $(this);
		var form = $('#form-send-test-newsletter');
		var valid = form.valid();

		if( valid )
		{
			$.ajax({
	    		url: site_url('admin/newsletter/ajax_send_test_newsletter'),
	    		data: form.serialize(),
	    		dataType: 'json',
	    		method: 'POST',
	    		success: function(response){
	    			if( response.rc )
	    			{
	    				$(".alert-success span").text(response.msg);
	    				$(".alert-success").show();
	    			}
	    			else
	    			{
	    				$(".alert-success").hide();
	    			}
	    			$('#send-test-newsletter-popup').modal('hide');
	    		}
	    	});
		}
	});

	/***********************************************************
	* Clear build newsletter zone on close
	************************************************************/
	$(document).on('hidden.bs.modal','#build-newsletter-zone-popup', function(){
		/*$('#rootwizard').bootstrapWizard('show',0);*/
		var _this = $(this);
		_this.find('input[type=text]').val('');
		_this.find('select option').prop('selected', false).trigger('chosen:updated');
		_this.find('.email_preview_html').html('');
		_this.find('.alert').hide();
		$('#review-newsletter-new-zone').html('');
		$('.newsletter-text-zone').hide();
		if( CKEDITOR.instances['editor'] )
		{
			CKEDITOR.instances['editor'].setData('');
		}

	});
	
	/***********************************************************
	* Clear send newsletter on close
	************************************************************/
	$(document).on('hidden.bs.modal','#send-newsletter-popup', function(){
		/*$('#rootwizard').bootstrapWizard('show',0);*/
		var _this = $(this);
		_this.find('input[type=text]').val('');
		_this.find('select option').prop('selected', false).trigger('chosen:updated');
		_this.find('.email_preview_html').html('');
		$('#review-newsletter-new-zone').html('');
		$('#template-newsletter-new-zone .option_item').html('');
		$('#add-new-zone').hide();
		if( CKEDITOR.instances['editor'] )
		{
			CKEDITOR.instances['editor'].setData('');
		}

	});

	$(document).on('change', '#newsletter-zone-layout', function(){
		var _this = $(this);

		if( _this.val() == 4 )
		{
			$('.newsletter-text-zone').show();
			$('textarea[name=zone_body]').val('');

			if (CKEDITOR.instances['editor']) { CKEDITOR.instances['editor'].destroy(true); }
			CKEDITOR.replace( 'editor' );
			CKEDITOR.add
			CKEDITOR.instances['editor'].on('change', function() { CKEDITOR.instances['editor'].updateElement() });
			CKEDITOR.instances['editor'].setData('');
			$('#review-newsletter-new-zone').html('');
		}
		else if( _this.val() == "" )
		{
			$('#review-newsletter-new-zone').html('');
			$('.newsletter-text-zone').hide();
		}
		else
		{
			$('.newsletter-text-zone').hide();
		}
		
	});

	$(".open-view-newsletter-template-popup").click( function() {
		var _this = $(this);
		var newsletter_template_id = _this.attr('data-newsletter-template-id');

		var html = $('.newsletter_template_html[data-newsletter-template-id=' + newsletter_template_id + ']').html();
		
		var text = $('#viewNewsletterTemplate .newsletter-template-view').html(html).text();
		$('#viewNewsletterTemplate .newsletter-template-view').html(text);
	});

	$(document).on('hidden.bs.modal','#viewNewsletterTemplate', function(){
		
		var _this = $(this);
		if( ($("#send-newsletter-popup").data('bs.modal') || {}).isShown )
		{
			$('body').addClass('modal-open');
		}
	});
	/**************************************************
    * Delete newsletter template
    **************************************************/
    $(document).on("click", ".delete-newsletter-template", function(e){
        var _this = $(this)
        var newsletter_template_id = _this.attr('data-newsletter-template-id');
        $("#form-delete-newsletter-template .newsletter_template_id").val( newsletter_template_id );
    });

    $(".delete-newsletter-template-btn").on("click",function(){
        var form = $("#form-delete-newsletter-template");
        form.submit();
    });

    init_newsletter_zones_sort();

    function init_newsletter_zones_sort()
	{
	    $("#sort-newsletter-zones").sortable().bind('sortupdate', function() {

	        var data = [];

	        $("#sort-newsletter-zones").children().each(function(i, v) {
	            data.push($(v).attr('id').replace("data_", ""));
	        });

	        $.ajax({
	            type:"POST",
	            dataType: "JSON",
	            data:'data='+data,
	            url:site_url('admin/newsletter/ajax_newsletter_zones_sort'),
	            success:function(response)
	            {
	                if(response.status == true)
	                {
	                    $('.successMsg').html("<div class='alert alert-success success_msg'><i class='icon-ok-sign'>"+$('.successfully_sorted').html()+"</i></div>");
	                }
	                else if(response.status == false)
	                {
	                    $('.successMsg').html("<div class='alert alert-success success_msg'><i class='icon-ok-sign'>"+$('.something_went_wrong').html()+"</i></div>");
	                }
	            }
	        });
	    });
	}

	$(document).on("click", ".delete_newsletter_zone", function(e){
        var zone_id = $(this).attr('data-newsletter-zone-id');
        $(".newsletter_zone_id").val(zone_id);
    });

    $(".delete_photo_btn").on("click",function(){
        var form = $("#delete_newsletter_zone_form");
        form.submit();
    });
})

function iframeLoaded(iFrameID) {
	if(iFrameID) {
	    // here you can make the height, I delete it first, then I make it again
	    iFrameID.height = "";
	    iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
	}   
}