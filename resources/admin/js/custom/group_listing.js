
$(document).ready( function()
{
	 /**************************************************
    * Add Group
    **************************************************/
    $(document).on("click", "#btn-add-group", function()
    {
        $.ajax({
            dataType: "json",
            type: "post",
            data: $("#form-add-group").serializeArray(),
            url: site_url('admin/group/ajax_add_group'),
            success: function(response)
            {
                if(response.rc)
                {   
                    $('#addGroup').modal('hide');
                    $('.modal.in').removeClass('in');
                    $('body').removeClass('modal-open').removeAttr('style');
                    $('.modal-backdrop').remove();
                    $('#addGroup .modal-body').html(response.add_modal);
                    $('.table-container').replaceWith(response._table_view);
                    $('.msgs').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                }
                else
                {
                    $('#addGroup .modal-body').html(response.add_modal);
                }
            }
        });
    });

    $(document).on('hidden.bs.modal', '#addGroup', function(){
        var _this = $(this);
        _this.find('input[type=text]').val('');
        _this.find('select option').prop('selected', false).trigger('chosen:updated');
        _this.find('.alert').hide();
        _this.find('.text-danger.error').hide();
    })
    /**************************************************
    * Edit Group
    **************************************************/
    // Check group id is valid or not. open editGroup modal.
    $(document).on("click", ".open-edit-group-popup", function()
    {
        var _this = $(this);
        var group_id = _this.data('group-id');

        $('.msgs').html('');

        $.ajax({
            dataType: "json",
            type: "post",
            data: {
                'group_id': group_id
            },
            url: site_url('admin/group/ajax_edit_group'),
            success: function(response)
            {
                if( response.group.rc )
                {
                    $('#editGroup .modal-body').html(response.edit_modal);
                    $("#form-edit-group").find('.error').html('');
                    $('#editGroup').modal('show');
                }
                else
                {
                    $('.msgs').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                    $('body').scrollTop(0);
                }
            }
        });
    });

    // Update group data.
    $(document).on("click", "#btn-edit-group", function()
    {
        $.ajax({
            dataType: "json",
            type: "post",
            data: $("#form-edit-group").serializeArray(),
            url: site_url('admin/group/ajax_edit_group'),
            success: function(response)
            {
                if( response.group.rc )
                {
                    if(response.rc)
                    {   
                        $('#editGroup').modal('hide');
                        $('.modal.in').removeClass('in');
                        $('body').removeClass('modal-open').removeAttr('style');
                        $('.modal-backdrop').remove();
                        $('.table-container').replaceWith(response._table_view);
                        $('.msgs').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                    }
                    else
                    {
                        $('#editGroup .modal-body').html(response.edit_modal);
                    }
                }
                else
                {
                    $('.msgs').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                    $('body').scrollTop(0);
                }
            }
        });
    });

    /**************************************************
    * Delete Group
    **************************************************/
    $(document).on("click", ".delete-group", function(e){
        var _this = $(this)
        var group_id = _this.attr('data-group-id');
        $(".group_id").val( group_id );
    });

    $(".delete-group-btn").on("click",function(){
        var form = $("#form-delete-group");
        form.submit();
    });

    /**************************************************
    * Add Users to Group
    **************************************************/
    $('#add-users-popup').on('hidden.bs.modal', function () {
        //$("#associated_users").val('').trigger("chosen:updated");
        $('input[type=text]').val('');
        $('.error').html('');
        $('.text-danger').html('');
    })

    $(document).on('click', '.add-users-to-group', function(){
        var _this = $(this)
        var group_id = _this.attr('data-group-id');
        $(".active_group_id").val( group_id );
        var title = $('.add-users-to-group-title').attr('data-value') + ' ' + _this.attr('data-group-name');
        $('.add-users-to-group-title').text(title);
        /*$.ajax({
            url:site_url('group/ajax_add_users_to_group'),
            data: $('#form-add-users-to-group').serialize() + '&group_id=' + group_id ,
            type:'POST',
            dataType:'json',
            success:function(response)
            {*/
                /*if( response.group.rc )
                {
                    $('#add-users-popup .modal-body').html(response.template_modal);
                    $('#add-users-popup .modal-body').find('#associated_users').html( $('#clients_and_leads').html() );
                    $('#associated_users ').prop('selected', false);
                    $("#associated_users").chosen();
                    $('#add-users-popup').modal('show');
                }
                else
                {
                    $('.msgs').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                    $('body').scrollTop(0);
                }*/
                /*if( response.rc )
                  {
                      $.each(response.data.users,function(i,v){
                          $('#associated_users option.users_org_type').filter('[value="'+v.user_id+'"]').prop('selected', true).trigger("chosen:updated");
                      });

                       $.each(response.data.leads,function(i,v){
                          $('#associated_users option.leads_org_type').filter('[value="'+v.lead_id+'"]').prop('selected', true).trigger("chosen:updated");
                      });
                  }*/
            /*}
        })*/
    })

    $("#form-add-users-to-group").validate({
        rules:{
                'name' : 'required',
                'email' : { required: true, email: true }
            },
        messages:{
                'name': {
                    required : 'The name field is required'
                },
                'email': {
                    required : 'The email field is required',
                    email : 'Please enter valid email address'
                }
            }
    });

    $(document).on('click', '.save_group_has_users', function(e){
        e.preventDefault();
        var group_id = $(".active_group_id").val();
        
        var valid = $("#form-add-users-to-group").valid();
        /*var associated_users = $("#associated_users option:selected");
        var seleted_associated_users = new Array();

        $.each(associated_users,function(i,v){
            var leadid = $(v).attr('data-leadid');
            var userid = $(v).attr('data-userid');
            seleted_associated_users.push({'lead_id': leadid, 'user_id': userid, 'group_id' : group_id});

        });*/

        if( ! valid )
        {
            return false;
        }
        else
        {
            $.ajax({
                url:site_url('admin/group/ajax_add_users_to_group'),
                data: $('#form-add-users-to-group').serialize(),
                type:'POST',
                dataType:'json',
                success:function(response)
                {
                    if( response.group.rc )
                    {
                        /*$('#add-users-popup .modal-body').html(response.template_modal);
                        $('#add-users-popup .modal-body').find('#associated_users').html( $('#clients_and_leads').html() );
                        $('#associated_users ').prop('selected', false);
                        $("#associated_users").chosen();*/
                        
                        if(response.rc)
                        {
                            $('.msgs .alert-success').replaceWith('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                            $('.msgs .alert-success').show();
                            $('#add-users-popup').modal('hide');
                        }
                        else
                        {
                            $('#add-users-popup .modal-body').html(response.template_modal);
                        }
                    }
                    else
                    {
                        $('.msgs').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                        $('body').scrollTop(0);
                    }
                }
            })
        }
    })

    $(document).on('click', '.delete-user-from-group', function(e){
        var _this = $(this);
        var group_id = _this.data('group-id');
        var user_id = _this.data('user-id');
        var lead_id = _this.data('lead-id');
        $.ajax({
            url:site_url('admin/group/ajax_delete_user_from_group'),
            data:{ 
                group_id : group_id,
                user_id : user_id,
                lead_id : lead_id
            },
            type:'POST',
            dataType:'json',
            success:function(response)
            {
                if( response.group.rc )
                {
                    $('#add-users-popup .modal-body').html(response.template_modal);
                    $('#add-users-popup .modal-body').find('#associated_users').html( $('#clients_and_leads').html() );
                    $('#associated_users ').prop('selected', false);
                    $("#associated_users").chosen();
                    $('#add-users-popup').modal('show');
                    if(response.rc)
                    {                        
                        $('#add-users-popup .msgs').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                        $('#add-users-popup').scrollTop(0);
                    }
                }
                else
                {
                    $('.msgs').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'+ response.msg +'</div>');
                    $('body').scrollTop(0);
                }
            }
        })
    })

    $(document).on("click",".add-users-to-group-from-csv",function(){
        $("#add-users-to-group-from-csv").modal('show');
        $('.invalid_file').hide();
    });

    $(document).off('click','#btn-add-users-via-csv').on('click','#btn-add-users-via-csv',function(e){
        var _this = $(this);
        var data_click = _this.attr('data-click');
        var click_flag = false;
        if( data_click == undefined || data_click == 0)
        {
            click_flag = true;
            _this.attr('data-click','1');
        }

        var form = $('#form-add-users-to-group-from-csv');
        var valid = true;

        if( form.find('#csv').val() == "")
        {
            $('.invalid_file').text($('.invalid_file').attr('data-text'));
            $('.invalid_file').show();
            valid = false;
        }
        else
        {
            var filename = form.find('#csv').val().split('\\').pop();

            var ext = filename.split('.').pop();

            if( ext == 'csv' )
            {
                $('.invalid_file').text('');
            }
            else
            {
                $('.invalid_file').text($('.invalid_file').attr('data-text'));
                $('.invalid_file').show();
                valid = false;
            }
        }

        if( form.find('#group-list-csv').val() == null || form.find('#group-list-csv').val().length == 0 )
        {
            var text = form.find('#group-list-csv').siblings('.text-danger').data('text');
            form.find('#group-list-csv').siblings('.text-danger').html(text);
            valid = false;
        }
        else
        {
            form.find('#group-list-csv').siblings('.text-danger').html('');
        }

        if( ! valid )
        {
            _this.attr('data-click','0');
        }

        if( valid && click_flag )
        {
            $('#form-add-users-to-group-from-csv').submit();
        }
    });

    if(window.location.href.indexOf('#add-users-to-group-from-csv') != -1) 
    {
        $('#add-users-to-group-from-csv').modal('show');
        $('.invalid_file').show();
    }

    $(document).on('hidden.bs.modal', '#add-users-to-group-from-csv', function(){
        $('#csv').val('');
        $('#group-list-csv').prop('selected',false).trigger('chosen:updated');
    });
});