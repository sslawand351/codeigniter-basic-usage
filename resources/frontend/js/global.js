$(document).ready(function(){

	$(document).on('change', '.change_language', function(e) {

		$('#language_frm').submit();
    
	});

	$('.subscribe-to-newsletter').click(function(){

		var email = $('.subscriber-email').val().toString();
		var filter = /^([a-zA-Z0-9_+\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if($.trim(email != "") && filter.test(email))
		{
			$.ajax({
                dataType: "json",
                type: "post",
                data: { email : email },
                url: BASE_URL+"user/ajax_newsletter_subscription",
				success:function(response)
				{
					$('.alert').hide();

					if(response.rc)
					{
						$('.newsletter-subscription-msg').show();
						$('.newsletter-subscription-msg .alert-success').html(response.msg).show();
					}
					else
					{
						if(response.form_error.rc)
						{
							$('.newsletter-subscription-msg').hide();
							$('.subscriber-email-error').html(response.form_error.email);
						}
						else
						{
							$('.newsletter-subscription-msg').show();
							$('.newsletter-subscription-msg .alert-error').html(response.msg).show();
						}
					}
				}
			});

			$('.subscriber-email').val('');
			$('.subscriber-email-error').html('');
		}
		else
		{
			$('.subscriber-email-error').html('please enter valid email address');
		}
	});

	var forms = $('.forms');

	if(forms.length > 0)
	{
		forms.each(function(){
			var _this = $(this);
			var form_id = _this.attr('data-form-id');
			var form_name = _this.attr('data-form-name');
			var post_data = $('.post_data').val();

			$.ajax({
				url : BASE_URL + 'admin/form/ajax_get_form',
				data : {
					form_id : form_id,
					form_name : form_name,
					post_data : post_data
				},
				dataType : 'json',
				type : 'POST',
				success : function(response) {
					if(response.rc)
					{
						_this.html(response.html_code);
					}
					else
					{

					}
				}
			});
		});
	}

	/*$(document).on('click', '.submit-form', function(e){
		e.preventDefault();
		var _this = $(this);
		var form_id = _this.attr('form-id');
		var form_name = _this.attr('form-name');
		var post_data = $('.post_data').val();

		$.ajax({
			url : BASE_URL + 'admin/form/ajax_get_form',
			data : {
				form_id : form_id,
				form_name : form_name,
				post_data : post_data
			},
			dataType : 'json',
			type : 'POST',
			success : function(response) {
				if(response.rc)
				{
					_this.html(response.html_code);
				}
				else
				{

				}
			}
		}); 
	});*/

	$(document).on('click','.submit-form',function(e)
    {
    	e.preventDefault();
        var link = BASE_URL + 'admin/form/ajax_add_frontend_forms';
        $('#frontend_form').attr('action',link);
        var _this = $(this).parents('form');

        $.ajax({
            url : link,
            data : $(this).parents('form').serialize(),
            dataType : 'json',
            type : 'POST',
            success : function(response) {
                if(response.rc == true)
                {
                    _this.html(response.html_code);
                    $('.alert-danger').css('display','none');
                    $('.alert-success').remove();
                    _this.before('<div class="alert alert-success" style="display: block;">'+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+'<strong class="msg">'+response.msg+'</strong>'+'</div>');
                }
                else if(response.rc == false)
                {
                    _this.html(response.html_code);
                    $('.alert-success').css('display','none');
                    $('.alert-danger').remove();
                   _this.before('<div class="alert alert-danger" style="display: block;">'+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+'<strong class="msg">'+response.msg+'</strong>'+'</div>');
                }
            }
        });
    });
});