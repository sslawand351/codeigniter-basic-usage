<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function pr(&$a)
{
   echo '<pre>';
   print_r($a);
   echo '</pre>';
}

function get_sublinks_for_workspace($parent,$workspace)
{
    $CI =& get_instance();

    /* load the user model */
    $CI->load->model('User_model', 'um');

    $response = $CI->um->get_workspace_link_by_parent_and_workspace($parent,$workspace);
    
    if($response['rc'])
    {
        return $response['data'];
    }
    else
    {
        return array();
    }

}

function is_admin()
{
    $CI =& get_instance();
    
    if($CI->session->userdata('admin_id'))
    {
        // check if its a admin or not
        $CI->load->model('User_model');
        $user_id = $CI->session->userdata('admin_id');
        $is_admin = $CI->User_model->check_is_admin_by_user_id($user_id);

        if($is_admin['rc'])
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    else
    {
        return FALSE;
    }
}
function checkParam($tableName, $columnName, $param, $is_numeric = FALSE)
{
    $CI =& get_instance();
    /* check for numeric parameter*/
    if($is_numeric == TRUE && !is_numeric($param))
    {
        return FALSE;
    }
    
    $result = $CI->db->get_where($tableName, array($columnName => (string)$param))->row_array();
  
    if(isset($result['id']))
        return TRUE;
    else
        return FALSE;
}

function get_activities($limit = "", $offset = "")
{
    $CI =& get_instance();    
    
    /* load the activity model */
    $CI->load->model('Activity_model', 'am');
    $response = $CI->am->get_activities(array(), $limit, $offset);
    
    $result['activity_notifications'] = $response['data'];
        
    foreach ($result['activity_notifications'] as $key => $activity)
    {
        $result['activity_notifications'][$key]['activity_text'] = $CI->am->get_activity_text($activity);
    }
    
    return $result;
}


function get_all_activities_count()
{
    $CI =& get_instance();    
    if($CI->session->userdata('user_id'))
        return $CI->db->count_all_results('activities');
    else
        return 0;
}

function get_unread_activities_count()
{
    $CI =& get_instance();    
    
    /* load the activity model */
    $CI->load->model('Activity_model', 'am');
    if($CI->session->userdata('user_id'))
    {
        $response = $CI->am->get_unread_activities_count($CI->session->userdata('user_id'));

        return $response;
    }
    else
        return "";
}
    function echo_errors() 
    {
        if (!is_array(@$GLOBALS['errors'])) { $GLOBALS['errors'] = array('Unknown error!'); }
        foreach ($GLOBALS['errors'] as $error) { echo '<p style="color:red;font-weight:bold;">Error: '.$error.'</p>'; }
    }

    function resize_image($method, $image_loc, $new_loc, $width, $height) 
    {
        if (!is_array(@$GLOBALS['errors'])) { $GLOBALS['errors'] = array(); }
        
        if (!in_array($method,array('force','max','crop'))) { $GLOBALS['errors'][] = 'Invalid method selected.'; }
        
        if (!$image_loc) { $GLOBALS['errors'][] = 'No source image location specified.'; }
        else {
            if ((substr(strtolower($image_loc),0,7) == 'http://') || (substr(strtolower($image_loc),0,7) == 'https://')) { /*don't check to see if file exists since it's not local*/ }
            elseif (!file_exists($image_loc)) { $GLOBALS['errors'][] = 'Image source file does not exist.'; }
            $extension = strtolower(substr($image_loc,strrpos($image_loc,'.')));
            if (!in_array($extension,array('.jpg','.jpeg','.png','.gif','.bmp'))) { $GLOBALS['errors'][] = 'Invalid source file extension!'; }
        }
        
        if (!$new_loc) { $GLOBALS['errors'][] = 'No destination image location specified.'; }
        else {
            $new_extension = strtolower(substr($new_loc,strrpos($new_loc,'.')));
            if (!in_array($new_extension,array('.jpg','.jpeg','.png','.gif','.bmp'))) { $GLOBALS['errors'][] = 'Invalid destination file extension!'; }
        }

        $width = abs(intval($width));
        if (!$width) { $GLOBALS['errors'][] = 'No width specified!'; }
        
        $height = abs(intval($height));
        if (!$height) { $GLOBALS['errors'][] = 'No height specified!'; }
        
        if (count($GLOBALS['errors']) > 0) { echo_errors(); return false; }
        
        if (in_array($extension,array('.jpg','.jpeg'))) { $image = @imagecreatefromjpeg($image_loc); }
        elseif ($extension == '.png') { $image = @imagecreatefrompng($image_loc); }
        elseif ($extension == '.gif') { $image = @imagecreatefromgif($image_loc); }
        elseif ($extension == '.bmp') { $image = @imagecreatefromwbmp($image_loc); }
        
        if (!$image) { $GLOBALS['errors'][] = 'Image could not be generated!'; }
        else {
            $current_width = imagesx($image);
            $current_height = imagesy($image);
            if ((!$current_width) || (!$current_height)) { $GLOBALS['errors'][] = 'Generated image has invalid dimensions!'; }
        }
        if (count($GLOBALS['errors']) > 0) { @imagedestroy($image); echo_errors(); return false; }

        if ($method == 'force') { $new_image = resize_image_force($image,$width,$height); }
        elseif ($method == 'max') { $new_image = resize_image_max($image,$width,$height); }
        elseif ($method == 'crop') { $new_image = resize_image_crop($image,$width,$height); }
        
        if ((!$new_image) && (count($GLOBALS['errors'] == 0))) { $GLOBALS['errors'][] = 'New image could not be generated!'; }
        if (count($GLOBALS['errors']) > 0) { @imagedestroy($image); echo_errors(); return false; }
        
        $save_error = false;
        if (in_array($extension,array('.jpg','.jpeg'))) { imagejpeg($new_image,$new_loc) or ($save_error = true); }
        elseif ($extension == '.png') { imagepng($new_image,$new_loc) or ($save_error = true); }
        elseif ($extension == '.gif') { imagegif($new_image,$new_loc) or ($save_error = true); }
        elseif ($extension == '.bmp') { imagewbmp($new_image,$new_loc) or ($save_error = true); }
        if ($save_error) { $GLOBALS['errors'][] = 'New image could not be saved!'; }
        if (count($GLOBALS['errors']) > 0) { @imagedestroy($image); @imagedestroy($new_image); echo_errors(); return false; }

        imagedestroy($image);
        imagedestroy($new_image);
        
        return true;
    }

    function resize_image_force($image, $width, $height) {
    $w = @imagesx($image); //current width
    $h = @imagesy($image); //current height
    if ((!$w) || (!$h)) { $GLOBALS['errors'][] = 'Image couldn\'t be resized because it wasn\'t a valid image.'; return false; }
    if (($w == $width) && ($h == $height)) { return $image; } //no resizing needed

    $image2 = imagecreatetruecolor ($width, $height);
    imagecopyresampled($image2,$image, 0, 0, 0, 0, $width, $height, $w, $h);

    return $image2;
}
function createThumbnail($targetPath,$thumbnailPath,$height,$width)
{
    include_once(APPPATH."libraries/phpthumb/phpthumb.class.php"); 

    $phpThumb = new phpThumb();
    $phpThumb->setParameter('w', $width);
    $phpThumb->setParameter('h', $height);
    $phpThumb->setParameter('zc', '1');
    $phpThumb->setSourceFilename($targetPath);
    $THUMBNAIL_PATH = $thumbnailPath;

        $phpThumb->GenerateThumbnail();

        $phpThumb->RenderToFile($THUMBNAIL_PATH);
}
function checkUrlParam($tableName, $columnName, $param, $is_numeric = FALSE)
{
    $CI =& get_instance();
    /* check for numeric parameter*/
    if($is_numeric == TRUE && !is_numeric($param))
    {
        return FALSE;
    }
    
    $result = $CI->db->get_where($tableName, array($columnName => (string)$param))->row_array();
    if(isset($result['id']))
        return TRUE;
    else
        return FALSE;
}
function ago($time)
{    
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60","60","24","7","4.35","12","10");

    $now = time();
    $difference     = $now - $time;

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if($difference != 1) {
        $periods[$j].= "s";
    }

    return "$difference $periods[$j]";
}

function is_unique($tableName, $columnName, $param, $is_numeric = FALSE)
{
    $CI =& get_instance();
    /* check for numeric parameter*/
    if($is_numeric == TRUE && !is_numeric($param))
    {
        return FALSE;
    }
    $result = $CI->db->get_where($tableName, array($columnName => (string)$param))->result_array();
    if(empty($result))
        return TRUE;
    else
        return FALSE;
}

function sortable($data_array = array() , $tableName = '')
{
    $CI =& get_instance();
   
    foreach($data_array as $index=>$data_id)
    {
        $index = $index + 1;
        $CI->db->where('id',$data_id);
        $CI->db->update($tableName,array('order_id' => $index));
    }
    
    return TRUE;
}

function get_frontend_languages()
{
    $CI =& get_instance();
    return $CI->db->get('meta_front_end_languages')->result_array();
}

function get_languages()
{
    $lang = array('en' => 'en', 'nl' => 'nl', 'fr' => 'fr');
    return $lang;
}

function set_title($title, $website_name = WEBSITE_NAME, $position = "right")
{
    $title = $position == "right" ? $title . TITLE_PATTERN . $website_name : $website_name . TITLE_PATTERN . $title;

    return $title;
}