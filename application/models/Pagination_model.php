<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagination_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function config($config = array())
	{
		$this->load->library('pagination');

		$config['num_links'] = 2;
        $config['first_link'] =  '<i class="fa fa-angle-double-left"></i>';
        $config['last_link']  = '<i class="fa fa-angle-double-right"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close']  = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm m-t-none m-b-none">';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['full_tag_close'] = '</ul>';
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        return $links;
	}

    function ajax_config($config = array())
    {
        $this->load->library('Ajax_pagination');

        $config['num_links'] = 2;
        $config['first_link'] =  '<i class="fa fa-angle-double-left"></i>';
        $config['last_link']  = '<i class="fa fa-angle-double-right"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close']  = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm m-t-none m-b-none">';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['full_tag_close'] = '</ul>';
        $this->ajax_pagination->initialize($config);
        $links = $this->ajax_pagination->create_links();

        return $links;
    }
}