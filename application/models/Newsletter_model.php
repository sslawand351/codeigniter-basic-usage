<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Activity_model');
    }

    function get_all_newsletter_templates($limit = array())
    {
        if( ! empty($limit))
        {
            if(isset($limit['limit']) && is_numeric($limit['limit']) && isset($limit['limit_start']) && is_numeric($limit['limit_start']))
                $this->db->limit($limit['limit'],$limit['limit_start']);
        }

        $newsletter_templates = $this->db->get('newsletter_templates')->result_array();

        if( ! empty($newsletter_templates) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $newsletter_templates;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function add_newsletter_template($newsletter_template_data)
    {
        $other_data['template_name'] = $newsletter_template_data['template_name'];
        $activity_data = array(
            'activity_by_user_id' => $this->user['id'],
            'admin_id' => $this->user['id'],
            'user_id' => $this->user['id'],
            'activity_type' => ACTIVITY_ADMIN_ADDED_NEWLETTER_TEMPLATE,
            'other_activity_data' => serialize($other_data),
            'activity_on' => date('Y-m-d G:i:s')
        );

        $this->Activity_model->add_activity($activity_data);
        return $this->db->insert('newsletter_templates', $newsletter_template_data);
    }

    function delete_newsletter_template($id = 0)
    {
        $newletter = $this->get_newsletter_template( $id );
        $newsletter_zones = $this->get_all_newsletter_zones( $id );

        if( ! empty($newsletter_zones['data']) )
        {
            foreach ($newsletter_zones['data'] as $nz_key => $nz)
            {
                $this->delete_newsletter_zone( $nz['id'], $id );
            }
        }

        $this->db->where('id',$id);
        $query = $this->db->delete('newsletter_templates');

        if( $query )
        {
            $other_data['template_name'] = $newletter['data']['template_name'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_DELETED_NEWLETTER_TEMPLATE,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);

            $response['rc'] = TRUE;
            $response['msg'] = lang('newsletter_template_deleted_successfully');
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('error');
        }
        
        return $response;
    }

    function get_newsletter_template($id)
    {
        $this->db->select('newsletter_templates.*');
        $this->db->where('newsletter_templates.id',$id);
        $newsletter_template = $this->db->get('newsletter_templates')->row_array();

        if( isset($newsletter_template['id']) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $newsletter_template;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function mandrill_open_event($results)
    {
        $update_stat = array();
        foreach($results as $result)
        {
            if(isset($result['msg']['opens']) && isset($result['msg']['email']) && isset($result['event']) && $result['event'] == 'open')
            {
                $open_count = count($result['msg']['opens']);
                $hash = $result['_id'];
                $event = $result['event'];
                $open_time = array();

                if( is_array($result['msg']['opens']) && count($result['msg']['opens']) > 0)
                {
                    foreach($result['msg']['opens'] as $key => $value)
                    {
                        $open_time[] = date('Y-m-d G:i:s', $value['ts'] );
                    }
                }

                $opened_time = serialize($open_time);

                $update_stat[] = array(
                   "opend_time" => $opened_time ,
                   "open_count" => $open_count,
                   "event" => $event,
                   "hash" => $hash
                );
            }
        }

        if(count($update_stat) > 0)
        {
            $this->db->update_batch("newsletter_email_stats", $update_stat, "hash");
            return TRUE;
        }
    }

    function mandrill_click_event($results)
    {
        $update_stat = array();
        foreach($results as $result)
        {
            if(isset($result['msg']['clicks']) && isset($result['msg']['email']) && isset($result['event']) && $result['event'] == 'click')
            {
                $click_count = count($result['msg']['clicks']);
                $hash = $result['_id'];
                $event = $result['event'];
                $click_time = array();
                $mandrill_click = array();

                if( is_array($result['msg']['clicks']) && count($result['msg']['clicks']) > 0)
                {
                    foreach($result['msg']['clicks'] as $key => $value)
                    {
                        $mandrill_click['time'] = date('Y-m-d H:i:s', $value['ts'] );
                        $mandrill_click['link'] = $value['url'];
                        $click_time[] = $mandrill_click;//date('Y-m-d H:i:s', $value['ts'] )." -- ".$value['url'] ;
                    }
                }

                $clicked_time = serialize($click_time);

                $update_stat[] = array(
                      "clicked_time" => $clicked_time ,
                      "click_count" => $click_count,
                      "hash" => $hash
                );
            }
        }

        if(count($update_stat) > 0)
        {
            $this->db->update_batch("newsletter_email_stats", $new_array, "hash");
            return TRUE;
        }
    }


    function insert_nl_stat($data)
    {
        $sender = $data->msg->sender;
        $id = $data->msg->_id;
                
        $old_data = $this->db->get_where('newsletter_email_stats', array('newsletter_email_stats.hash'=>$id))->result_array();
            
        if(isset($old_data[0]))
        {
            $this->db->where('hash', $id);
            $this->db->update('newsletter_email_stats', array('sent_time'=>date('Y-m-d G:i:s', $data->ts), 'event'=>$data->event));
            return TRUE;
        }
    }

    function get_newsletter_stats( $limit = array() )
    {
        $limit_condition = "";
        if( ! empty( $limit ) )
        {
            if(isset($limit['limit']) && is_numeric($limit['limit']) && isset($limit['limit_start']) && is_numeric($limit['limit_start']))
            {
                $limit_condition = " LIMIT " . $limit['limit_start'] .", ". $limit['limit'];
            }
        }

        $sql = "SELECT 
                    newsletter_sent.*, 
                    count(newsletter_sent.id) as sent_count, 
                    SUM(newsletter_email_stats.open_count) as open_count, 
                    SUM(newsletter_email_stats.click_count) as click_count, 
                    SUM(IF( newsletter_email_stats.event = 'hard_bounce' OR newsletter_email_stats.event = 'soft_bounce', 1, 0 )) as bounce_count, 
                    SUM(IF( newsletter_email_stats.event = 'reject', 1, 0 )) as reject_count, 
                    SUM(IF( newsletter_email_stats.has_unsubscribed = '1', 1, 0 )) as unsubscribed_count  
                FROM newsletter_sent
                JOIN newsletter_email_stats ON newsletter_email_stats.newsletter_sent_id = newsletter_sent.id
                GROUP BY newsletter_sent.id
                $limit_condition
                ";

        $newsletter_stats = $this->db->query($sql)->result_array();

        if( ! empty($newsletter_stats) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $newsletter_stats;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function add_sent_newsletter( $newsletter_data )
    {
        $this->db->insert('newsletter_sent', $newsletter_data );
        $last_insert_id = $this->db->insert_id();
        return $last_insert_id;
    }

    function add_newletter_sent_groups_relation( $newletter_sent_groups_relation )
    {
        $this->db->insert('sent_newsletter_groups', $newletter_sent_groups_relation );
        $last_insert_id = $this->db->insert_id();
        return $last_insert_id;
    }

    function get_newsletter_opens_list( $newsletter_sent_id, $params )
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(newsletter_email_stats.id) as count ";
        }
        else
        {
            $select_statement = " newsletter_email_stats.to_name, newsletter_email_stats.to_email, newsletter_email_stats.sent_on, newsletter_email_stats.opend_time, newsletter_email_stats.open_count, newsletter_sent.subject ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }

        $sql = " SELECT 
                    " . $select_statement . " 
                FROM newsletter_sent
                JOIN newsletter_email_stats ON newsletter_email_stats.newsletter_sent_id = newsletter_sent.id
                WHERE newsletter_sent.id = '$newsletter_sent_id'
                AND newsletter_email_stats.open_count != 0
                " . $limit;

        $opens_list = $this->db->query($sql)->result_array();

        if( ! empty($opens_list) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $opens_list;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function get_newsletter_clicks_list( $newsletter_sent_id, $params )
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(newsletter_email_stats.id) as count ";
        }
        else
        {
            $select_statement = " newsletter_email_stats.to_name, newsletter_email_stats.to_email, newsletter_email_stats.sent_on, newsletter_email_stats.clicked_time, newsletter_email_stats.click_count, newsletter_sent.subject ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }

        $sql = " SELECT 
                    " . $select_statement . " 
                FROM newsletter_sent
                JOIN newsletter_email_stats ON newsletter_email_stats.newsletter_sent_id = newsletter_sent.id
                WHERE newsletter_sent.id = '$newsletter_sent_id'
                AND newsletter_email_stats.click_count != 0
                " . $limit;

        $clicks_list = $this->db->query($sql)->result_array();

        if( ! empty($clicks_list) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $clicks_list;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function get_newsletter_bounce_list( $newsletter_sent_id )
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(newsletter_email_stats.id) as count ";
        }
        else
        {
            $select_statement = " 
                        newsletter_email_stats.to_name, 
                        newsletter_email_stats.to_email, 
                        newsletter_email_stats.sent_on, 
                        newsletter_email_stats.event,
                        newsletter_sent.subject ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }

        $sql = " SELECT " . $select_statement . "
                FROM newsletter_sent
                JOIN newsletter_email_stats ON newsletter_email_stats.newsletter_sent_id = newsletter_sent.id
                WHERE newsletter_sent.id = '$newsletter_sent_id' 
                AND ( newsletter_email_stats.event = 'hard_bounce' OR newsletter_email_stats.event = 'soft_bounce' )
                " . $limit;

        $bounce_list = $this->db->query($sql)->result_array();

        if( ! empty($bounce_list) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $bounce_list;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function get_newsletter_reject_list( $newsletter_sent_id )
    {
        $sql = "SELECT 
                    newsletter_email_stats.* 
                FROM newsletter_sent
                JOIN newsletter_email_stats ON newsletter_email_stats.newsletter_sent_id = newsletter_sent.id
                WHERE 
                    newsletter_sent.id = '$newsletter_sent_id' AND 
                    newsletter_email_stats.event = 'reject'
                ";

        $reject_list = $this->db->query($sql)->result_array();

        if( ! empty($reject_list) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $reject_list;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function get_newsletter_sent_list( $newsletter_sent_id, $params )
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(newsletter_email_stats.id) as count ";
        }
        else
        {
            $select_statement = " newsletter_email_stats.to_name, newsletter_email_stats.to_email, newsletter_email_stats.sent_on, newsletter_sent.subject ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }

        $sql = "SELECT " . $select_statement . " 
                FROM newsletter_sent
                JOIN newsletter_email_stats ON newsletter_email_stats.newsletter_sent_id = newsletter_sent.id
                WHERE 
                    newsletter_sent.id = '$newsletter_sent_id'
                " . $limit;

        $sent_list = $this->db->query($sql)->result_array();

        if( ! empty($sent_list) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $sent_list;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function get_newsletter_unsubscribe_list( $newsletter_sent_id, $params )
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(newsletter_email_stats.id) as count ";
        }
        else
        {
            $select_statement = " newsletter_email_stats.to_name, newsletter_email_stats.to_email, newsletter_email_stats.sent_on, ugu.unsubscribed_on, newsletter_sent.subject ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }

        $sql = "SELECT " . $select_statement . " 
                FROM newsletter_sent
                JOIN newsletter_email_stats ON newsletter_email_stats.newsletter_sent_id = newsletter_sent.id
                JOIN group_has_users u ON u.email = newsletter_email_stats.to_email
                JOIN unsubscribed_group_users ugu ON ugu.group_user_id =  u.id AND u.group_id = ugu.group_id
                WHERE 
                    newsletter_sent.id = '$newsletter_sent_id'
                AND newsletter_email_stats.has_unsubscribed = 1

                " . $limit;

        $unsubscribe_list = $this->db->query($sql)->result_array();

        if(count($unsubscribe_list) > 0)
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $unsubscribe_list;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function update_newsletter_stats_event_via_sendgrid($event_data)
    {
        $mail_hash = $event_data['mail_hash'];
        $email_stats = $this->db->get_where('newsletter_email_stats', array( 'hash' => $mail_hash ) )->row_array();
        
        if( isset($email_stats['id']) )
        {
            $event = $event_data['event'];
            $ts = $event_data['timestamp'];
            
            switch( $event )
            {
                case 'open':
                    $opened_time = unserialize($email_stats['opend_time']);

                    $opened_time = is_array($opened_time) ? $opened_time : array();

                    array_push( $opened_time, date('Y-m-d G:i:s', $ts) );

                    $open_count = intval($email_stats['open_count']) + 1;
                    $update_data = array(
                        'event' => 'open',
                        'open_count' => $open_count,
                        'opend_time' => serialize($opened_time)
                    );
                    break;
                case 'click':
                    $click_time = unserialize($email_stats['clicked_time']);

                    $click_time = is_array($click_time) ? $click_time : array();
                    
                    $click_array = array(
                        'time' => date('Y-m-d G:i:s', $ts),
                        'link' => $event_data['url']
                    );

                    array_push( $click_time, $click_array );

                    $click_count = intval($email_stats['click_count']) + 1;
                    $update_data = array(
                        'event' => 'click',
                        'click_count' => $click_count,
                        'clicked_time' => serialize($click_time)
                    );
                    break;
                case 'bounce':
                    break;
            }

            if( isset($update_data) )
            {
                $this->db->where( 'hash', $mail_hash );
                $this->db->where( 'vendor_type', 'SendGrid' );
                $this->db->update('newsletter_email_stats', $update_data);
            }
        }
    }

    function unsubscribe_to_newsletter( $id, $newsletter_sent_id, $subscription_key )
    {
        $sql = "SELECT group_has_users.* 
                FROM group_has_users
                LEFT JOIN unsubscribed_group_users ON group_has_users.id = unsubscribed_group_users.group_user_id
                WHERE group_has_users.id = '$id' 
                AND group_has_users.subscription_key = '$subscription_key' 
                AND unsubscribed_group_users.id IS NULL ";
        $group_user = $this->db->query($sql)->row_array();
        
        if( isset($group_user['id']) )
        {
            $newsletter_sent = $this->db->get_where('newsletter_sent');

            $groups = $this->db->get_where('sent_newsletter_groups', array('newsletter_sent_id' => $newsletter_sent_id))->result_array();

            $newsletter_subscriber = $this->db->get_where('newsletter_subscribers', array( 'email_id' => $group_user['email'] ) )->row_array();

            if( isset($newsletter_subscriber['id']) )
            {
                $this->db->update( 'newsletter_subscribers', array('is_unsubscribe_to_newsletter' => 'yes', 'unsubscribed_on' => date('Y-m-d G:i:s') ), array('id' => $newsletter_subscriber['id'] ) );
            }

            $sql = "
                    UPDATE newsletter_email_stats 
                    SET has_unsubscribed = 1
                    WHERE newsletter_sent_id = '" . $newsletter_sent_id. "'
                    AND to_email = '" .$group_user['email']. "'
            ";
            $this->db->query($sql);

            $sql = "SELECT group_has_users.*
                    FROM group_has_users
                    LEFT JOIN unsubscribed_group_users ON group_has_users.id = unsubscribed_group_users.group_user_id
                    WHERE 
                        group_has_users.id = '" .  $group_user['id'] . "' 
                    OR 
                        ( group_has_users.group_id IN ( SELECT group_id FROM sent_newsletter_groups WHERE newsletter_sent_id = '" . $newsletter_sent_id . "')
                        AND 
                            group_has_users.email = '" .  $group_user['email'] . "'
                        )
                    ";

            $group_users = $this->db->query($sql)->result_array();

            foreach ($group_users as $k => $u) {
                $unsubscribed_user = array(
                    'group_id' => $u['group_id'],
                    'group_user_id' => $u['id'],
                    'unsubscribed_on' => date('Y-m-d G:i:s')
                );
                $this->db->insert('unsubscribed_group_users', $unsubscribed_user);
            }

            $response['rc'] = TRUE;
            $response['msg'] = lang('newsletter_unsubscription_desc');
            $response['data'] = $group_user;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('error');
            $response['data'] = array();
        }

        return $response;
    }

    function update_newsletter_subscription_key( $id, $type, $subscription_key )
    {
        switch( $type )
        {
            case 'newsletter_subscriber':
                $sql = "SELECT * FROM newsletter_subscribers WHERE id = '$id' ";
                $newsletter_subscriber = $this->db->query($sql)->row_array();
                if( isset($newsletter_subscriber['id']) )
                {
                    $this->db->update( 'newsletter_subscribers', array('subscription_key' => $subscription_key, 'subscription_key_created_on' => date('Y-m-d G:i:s')), array('id' => $newsletter_subscriber['id']) );
                    
                    $response['rc'] = TRUE;
                    $response['msg'] = '';
                    $response['data'] = $newsletter_subscriber;
                }
                else
                {
                    $response['rc'] = FALSE;
                    $response['msg'] = lang('error');
                    $response['data'] = array();
                }
                break;
            case 'group' :
                $sql = "SELECT * FROM group_has_users WHERE id = '$id' ";
                $group_users = $this->db->query($sql)->row_array();
                if( isset($group_users['id']) )
                {
                    $this->db->update( 'group_has_users', array('subscription_key' => $subscription_key, 'subscription_key_created_on' => date('Y-m-d G:i:s')), array('id' => $group_users['id']) );
                    
                    $response['rc'] = TRUE;
                    $response['msg'] = '';
                    $response['data'] = $group_users;
                }
                else
                {
                    $response['rc'] = FALSE;
                    $response['msg'] = lang('error');
                    $response['data'] = array();
                }
                break;
            default:
                $response['rc'] = FALSE;
                $response['msg'] = lang('error');
                $response['data'] = array();
                break;
        }

        return $response;
    }

    function get_unsubscribed_users_listing()
    {
        $sql = "SELECT * FROM ( SELECT users.id, CONCAT(users.first_name, ' ', users.last_name) as user_name, users.email, users.unsubscribed_on FROM users WHERE is_unsubscribe_to_newsletter =     'yes' 
                        UNION
                        SELECT leads.id, CONCAT(leads.first_name, ' ', leads.last_name) as user_name, leads.email, leads.unsubscribed_on FROM leads WHERE is_unsubscribe_to_newsletter = 'yes'
                        ) as u
                        GROUP BY email;
        ";

        $unsubscribe_users = $this->db->query($sql)->result_array();

        if( ! empty($unsubscribe_users) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $unsubscribe_users;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }

        return $response;
    }

    function add_newsletter_zone( $newsletter_zone_data )
    {
        $result = $this->db->insert( 'newsletter_template_has_zones', $newsletter_zone_data );
        $newsletter_zone_id = $this->db->insert_id();

        if( $result )
        {
            $other_data['newsletter_template_id'] = $newsletter_zone_data['newsletter_template_id'];
            $other_data['zone_name'] = $newsletter_zone_data['zone_name'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_ADDED_NEWLETTER_ZONE,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);

            $response['msg']    = lang('newsletter_zone_created_successfully');
            $response['rc']     = TRUE;
            $response['id']   = $newsletter_zone_id;
        }
        else
        {
            $response['msg']    = lang('error_occurred_while_inserting_data');
            $response['rc']     = FALSE;
            $response['id']   = '';
        }

        return $response;
    }

    function add_newsletter_zone_has_cars( $newsletter_zone_cars_data )
    {
        $this->db->insert_batch( 'newsletter_zone_has_cars', $newsletter_zone_cars_data );

        return TRUE;
    }

    function get_all_newsletter_zones( $newsletter_template_id )
    {
        /*$this->db->select('newsletter_template_has_zones.*, GROUP_CONCAT(newsletter_zone_has_cars.car_id) as zone_cars, car_channels.channel_name');*/
        $this->db->select('newsletter_template_has_zones.*');
        $this->db->order_by('order_id');
        $this->db->where('newsletter_template_id', $newsletter_template_id);
        /*$this->db->join('newsletter_zone_has_cars', 'newsletter_zone_has_cars.newsletter_zone_id = newsletter_template_has_zones.id', 'left');*/
        /*$this->db->join('car_channels', 'car_channels.id = newsletter_template_has_zones.channel_id', 'left');*/
        $this->db->group_by('newsletter_template_has_zones.id');
        $newsletter_zones = $this->db->get('newsletter_template_has_zones')->result_array();

        if( ! empty($newsletter_zones) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $newsletter_zones;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }

        return $response;
    }

    function delete_newsletter_zone( $id, $newsletter_template_id )
    {
        /*$this->db->where( 'newsletter_zone_id', $id );
        $this->db->delete( 'newsletter_zone_has_cars' );*/

        $this->db->where( 'id', $id );
        $zone = $this->db->get( 'newsletter_template_has_zones' )->row_array();

        $this->db->where( 'id', $id );
        $result = $this->db->delete( 'newsletter_template_has_zones' );

        if( $result )
        {
            $other_data['newsletter_template_id'] = $newsletter_template_id;
            $other_data['zone_name'] = $zone['zone_name'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_DELETED_NEWLETTER_ZONE,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);
            $response['msg']    = lang('newsletter_zone_deleted_successfully');
            $response['rc']     = TRUE;
        }
        else
        {
            $response['msg']    = lang('error_occurred_while_deleting_data');
            $response['rc']     = FALSE;
        }

        return $response;
    }

    function get_newsletter_zone( $newsletter_zone_id )
    {
        $newsletter_zone = $this->db->get_where('newsletter_template_has_zones', array( 'id' => $newsletter_zone_id ))->row_array();

        if( ! empty($newsletter_zone) )
        {
            /*$zone_cars = $this->db->get_where( 'newsletter_zone_has_cars', array( 'newsletter_zone_id' => $newsletter_zone_id ) )->result_array();
            $newsletter_zone['cars'] = $zone_cars;*/
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $newsletter_zone;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }

        return $response;
    }

    function delete_newsletter_zone_cars($newsletter_zone_id)
    {
        $this->db->where( 'newsletter_zone_id', $newsletter_zone_id );
        $this->db->delete( 'newsletter_zone_has_cars' );
        return TRUE;
    }

    function update_newsletter_zone($newsletter_zone_id, $newsletter_zone_data)
    {
        $this->db->where( 'id', $newsletter_zone_id );
        $result = $this->db->update( 'newsletter_template_has_zones', $newsletter_zone_data );
        
        if( $result )
        {
            $this->db->where( 'id', $newsletter_zone_id );
            $zone = $this->db->get( 'newsletter_template_has_zones', $newsletter_zone_data )->row_array();

            $other_data['newsletter_template_id'] = $zone['newsletter_template_id'];
            $other_data['zone_name'] = $zone['zone_name'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_UPDATED_NEWLETTER_ZONE,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);

            $response['msg']    = lang('newsletter_zone_updated_successfully');
            $response['rc']     = TRUE;
            $response['data']   = '';
        }
        else
        {
            $response['msg']    = lang('error_occurred_while_updating_data');
            $response['rc']     = FALSE;
            $response['data']   = '';
        }

        return $response;
    }

    function subscribe_to_newsletter( $subscriber_data, $is_unsubscribed = FALSE )
    {
        $user_name = $subscriber_data['user_name'];
        unset($subscriber_data['user_name']);
        if( $is_unsubscribed )
        {
            $this->db->where('id', $subscriber_data['id']);
            $result = $this->db->update('newsletter_subscribers', $subscriber_data);
            $newsletter_subscriber_id = $subscriber_data['id'];
        }
        else
        {
            $result = $this->db->insert('newsletter_subscribers', $subscriber_data);
            $newsletter_subscriber_id = $this->db->insert_id();
        }

        $group_id = NEWSLETTER_GROUP_ID_MAIN_CHANNEL;

        $sql = "SELECT group_has_users.id
                FROM group_has_users
                WHERE ( group_has_users.email = '" .  $subscriber_data['email_id'] . "'
                )
                AND group_has_users.group_id = '". $group_id . "' ";

        $group_user = $this->db->query($sql)->row_array();

        if( ! isset($group_user['id']) )
        {
            $insert_data = array(
                'user_name' => $user_name,
                'group_id' => $group_id, 
                'newsletter_subscriber_id' => $newsletter_subscriber_id,
                'email' => $subscriber_data['email_id'],
                'added_on' => date('Y-m-d G:i:s')
            );
            $this->db->insert('group_has_users', $insert_data);
        }
        else
        {
            $insert_data = array(
                'user_name' => $user_name,
                'group_id' => $group_id, 
                'newsletter_subscriber_id' => $newsletter_subscriber_id,
                'email' => $subscriber_data['email_id'],
                'added_on' => date('Y-m-d G:i:s')
            );
            $this->db->where( 'id', $group_user['id'] );
            $this->db->update('group_has_users', $insert_data);

            $this->db->delete('unsubscribed_group_users', array('group_user_id', $group_user['id']));
        }

        if( $result )
        {
            $response['msg']    = lang('you_are_successfully_subscribe_to_our_newsletter');
            $response['rc']     = TRUE;
            $response['data']     = array( 'id' => $newsletter_subscriber_id );
        }
        else
        {
            $response['msg']    = lang('error_occurred_while_inserting_data');
            $response['rc']     = FALSE;
            $response['data']   = '';
        }

        return $response;
    }

    function get_newsletter_subscriber( $condition = array() )
    {
        $this->db->where($condition);
        $newsletter_subscriber = $this->db->get('newsletter_subscribers')->row_array();

        if( isset($newsletter_subscriber['id']))
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']     = $newsletter_subscriber;
        }
        else
        {
            $response['msg']    = '';
            $response['rc']     = FALSE;
            $response['data']     = $newsletter_subscriber;
        }

        return $response;
    }

    function add_email_stats_batch( $data_array )
    {
        $this->db->insert_batch('newsletter_email_stats',$data_array);
        return TRUE;
    }

    function get_newsletter_sent($newsletter_sent_id)
    {
        $this->db->where('id', $newsletter_sent_id);
        $newsletter_sent = $this->db->get('newsletter_sent')->row_array();

        if(isset($newsletter_sent['id']))
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']     = $newsletter_sent;
        }
        else
        {
            $response['msg']    = '';
            $response['rc']     = FALSE;
            $response['data']     = $newsletter_sent;
        }

        return $response;
    }
}