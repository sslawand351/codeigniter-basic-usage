<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Activity_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_all_activities_count($condition = array(), $limit = 0, $user_id=0)
    {
        $limitCondition = '';
        $user_id_condition = '';

        if( $limit > 0 )
        {
            $limitCondition = ' LIMIT 0, '.$limit;
        }

        if( $user_id != 0)
        {
            $user_id_condition = ' AND activity_by_user_id ='.$user_id;
        }

        $sql = "
            SELECT 
                count(a.id) as activity_count
            FROM 
                activities a
            WHERE
                1=1"
                .$user_id_condition;

        foreach ($condition as $param => $value)
        {
            if($value != "" && is_numeric($value))
            {
                switch ($param) {

                    case 'user_id':
                        $sql .= " AND a.user_id = '".$value."' ";
                        break;

                    case 'activity_type':
                        $sql .= " AND a.activity_type = '".$value."' ";
                        break;

                    default:
                        break;
                }
            }
        }

        $sql .= "
            ORDER BY
                 a.activity_on desc
             $limitCondition
            ";     //echo $sql;exit;   
        $response['data'] = $this->db->query($sql)->result_array();

        $response['msg'] = '';
       
        if(!empty($response['data']))
            $response['rc'] = TRUE;
        else
            $response['rc'] = FALSE;
        //pr($response);exit;
        return $response;
    }

    function get_activities($condition = array(), $limit = 0)
    {
        $limitCondition = '';

        if( $limit > 0 )
        {
            $limitCondition = ' LIMIT 0, '.$limit;
        }

        $sql = "
            SELECT 
                CONCAT(u.first_name, ' ', u.last_name) as user_full_name,
                a.*
            FROM 
                activities a
                LEFT JOIN
                users u ON a.activity_by_user_id = u.id
            WHERE
                1=1
                ";

        foreach ($condition as $param => $value)
        {
            if($value != "" && is_numeric($value))
            {
                switch ($param) {
                   
                    case 'user_id':
                        $sql .= " AND a.user_id = '".$value."' ";
                        break;

                    case 'activity_type':
                        $sql .= " AND a.activity_type = '".$value."' ";
                        break;

                    default:
                        break;
                }
            }
        }

        $sql .= "
            ORDER BY
                 a.activity_on desc
             $limitCondition
            ";     //echo $sql;exit;   
        $response['data'] = $this->db->query($sql)->result_array();

        $response['msg'] = '';
       
        if(!empty($response['data']))
            $response['rc'] = TRUE;
        else
            $response['rc'] = FALSE;
        //pr($response);exit;
        return $response;
    }

    function add_activity($activity_data)
    {
        $this->db->insert('activities',$activity_data);
    }

    function fetch_all_activities($limit, $start, $condition = array(), $user_id=0)
    {
        $user_id_condition = "";
        if( $user_id != 0)
        {
            $user_id_condition = ' AND activity_by_user_id ='.$user_id;
        }

        $sql = "
            SELECT 
                CONCAT(u.first_name, ' ', u.last_name) as user_full_name,
                a.*
            FROM 
                activities a
                LEFT JOIN
                users u ON a.activity_by_user_id = u.id
            WHERE
                1=1"
                .$user_id_condition;

        foreach ($condition as $param => $value)
        {
            if($value != "" && is_numeric($value))
            {
                switch ($param) {

                    case 'user_id':
                        $sql .= " AND a.user_id = '".$value."' ";
                        break;

                    case 'activity_type':
                        $sql .= " AND a.activity_type = '".$value."' ";
                        break;

                    default:
                        break;
                }
            }
        }

        $sql .= "
            ORDER BY
                 a.activity_on desc
            LIMIT
                  ".$start.",".$limit;
      
        $response['data'] = $this->db->query($sql)->result_array();

        $response['msg'] = '';
       
        if(!empty($response['data']))
            $response['rc'] = TRUE;
        else
            $response['rc'] = FALSE;
        
        return $response;
    }

    function get_activity_text($activity_arr)
    {

        if(isset($activity_arr['id']))
        { 
            $activity_text = "";

            if(isset($activity_arr['other_activity_data']) && strlen($activity_arr['other_activity_data']) > 0)
            {
                $other_activity_data = unserialize($activity_arr['other_activity_data']);
            }
            //pr($activity_arr);exit;
            // pr($other_activity_data);exit;
            switch($activity_arr['activity_type'])
            {

                case ACTIVITY_ADMIN_HAS_LOGGED_IN:
                    $activity_text = $activity_arr['user_full_name']." has been logged in";
                    break;
                case ACTIVITY_ADMIN_HAS_LOGGED_OUT:
                    $activity_text = $activity_arr['user_full_name']." has been logged out";
                    break;
                case ACTIVITY_ADMIN_ADDED_USER:
                    $activity_text = "A new user <b>".$other_activity_data['user_name']."</b> has been added by ".$this->user['name'];
                    break;  
                case ACTIVITY_ADMIN_UPDATED_USER:
                    $activity_text = "User <b>".$other_activity_data['user_name']."</b> has been updated by ".$this->user['name'];
                    break;
                case ACTIVITY_ADMIN_DELETED_USER:
                    $activity_text = "User <b>".$other_activity_data['user_name']."</b> has been deleted by ".$this->user['name'];
                    break;
                case ACTIVITY_ADMIN_ADDED_ROLE:
                    $activity_text = $other_activity_data['role_name']." has been added as new role by ".$this->user['name'];
                    break;
                case ACTIVITY_ADMIN_UPDATED_ROLE:
                    $activity_text = "User role <b>".$other_activity_data['user_group']." 's</b> permissions has been updated by ".$this->user['name'];
                    break;
                case ACTIVITY_ADMIN_ADD_BLOG:
                    $activity_text = "A new article <b>".$other_activity_data['blog_name']."</b> has been added by ".$this->user['name'];
                    break;
                case ACTIVITY_ADMIN_UPDATE_BLOG:
                    $activity_text = "Article <b>".$other_activity_data['blog_name']."</b> has been updated by ".$this->user['name'];
                    break;
                case ACTIVITY_ADMIN_DELETE_BLOG:
                    $activity_text = "Article <b>".$other_activity_data['blog_name']."</b> has been deleted by ".$this->user['name'];
                    break;

                case ACTIVITY_ADMIN_ADDED_NEWSLETTER_GROUP:
                    return $activity_arr['user_full_name']." ".lang('has_added_newletter_group').$other_activity_data['group_name'].".";
                    break;

                case ACTIVITY_ADMIN_UPDATED_NEWSLETTER_GROUP:
                    return $activity_arr['user_full_name']." ".lang('has_updated_newletter_group').$other_activity_data['group_name'].".";
                    break;

                case ACTIVITY_ADMIN_DELETED_NEWSLETTER_GROUP:
                    return $activity_arr['user_full_name']." ".lang('has_deleted_newletter_group').$other_activity_data['group_name'].".";
                    break;

                case ACTIVITY_ADMIN_USERS_ADDED_TO_NEWSLETTER_GROUP:
                    return $activity_arr['user_full_name']." ".lang('has_added_users_to_newletter_group').$other_activity_data['group_name'].".";
                    break;

                case ACTIVITY_ADMIN_USERS_REMOVED_FROM_NEWSLETTER_GROUP:
                    return $activity_arr['user_full_name']." ".lang('has_removed_users_from_newletter_group').$other_activity_data['group_name'].".";
                    break;

                case ACTIVITY_ADMIN_ADDED_USERS_TO_NEWSLETTER_GROUP_VIA_CSV:
                    return $activity_arr['user_full_name']." ".lang('has_added_users_to_newletter_group_via_csv').$other_activity_data['group_name'].".";
                    break;

                case ACTIVITY_ADMIN_ADDED_NEWLETTER_ZONE:
                    return $activity_arr['user_full_name']." ".lang('has_added_newletter_zone').$other_activity_data['zone_name'].".";
                    break;

                case ACTIVITY_ADMIN_UPDATED_NEWLETTER_ZONE:
                    return $activity_arr['user_full_name']." ".lang('has_updated_newletter_zone').$other_activity_data['zone_name'].".";
                    break;

                case ACTIVITY_ADMIN_DELETED_NEWLETTER_ZONE:
                    return $activity_arr['user_full_name']." ".lang('has_deleted_newletter_zone').$other_activity_data['zone_name'].".";
                    break;

                default: $comma_flag = FALSE; break;
            }

            return $activity_text;
        }
        else
            return "";
    }
}