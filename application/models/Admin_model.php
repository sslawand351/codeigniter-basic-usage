<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function login($email,$password)
    {
        $query = "SELECT
                         u.*,
                        mfel.code as language
                    FROM 
                        users u
                    LEFT JOIN meta_front_end_languages mfel ON u.language_id = mfel.id
                    WHERE
                        u.email= '".$email."' 
                    AND
                        u.user_type in('admin','master admin','super admin')
                    AND 
                        u.password = '".$password."'";

        $admin = $this->db->query($query)->row_array();

        if(isset($admin['id']))
        {
            $user_data = array(
                'admin_id' => $admin['id'],
                'user_type' =>  $admin['user_type'],
            );
            // pr($user_data);exit;
            $this->set_admin_login_session($user_data);

            $datetime = new DateTime();
            $datetime->modify('+6 months');
            $expiry = $datetime->format('Y-m-d G:i:s');
            
            $pre_token = bin2hex(openssl_random_pseudo_bytes(128));
            $selector = mt_getrandmax().rand();
            $admin_rememberme_cookie = $selector."-".$pre_token;

            $auth_token_data = array(
                'selector' => $selector,
                'token' => hash('sha256', $pre_token),
                'user_id' => $admin['id'],
                'expires' => $expiry
            );

            setcookie('admin_rememberme', $admin_rememberme_cookie, strtotime($expiry), "/");

            $this->db->insert('auth_tokens', $auth_token_data);

            $this->load->model('User_model');


            $is_admin = $this->User_model->check_is_admin_by_user_id($admin['id']);

            if($is_admin['rc'])
            {
                $this->session->set_userdata('is_admin',TRUE);
            }
            else
            {
                $this->session->set_userdata('is_admin',FALSE);
            }

            // ADD ACTIVITY DATA
            $other_data = array();

            $activity_data = array(
                'activity_by_user_id' => $admin['id'],
                'admin_id' => $admin['id'],
                'activity_type' => ACTIVITY_ADMIN_HAS_LOGGED_IN,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s'),
                'user_id'=> $admin['id']
            );
//pr($activity_data);exit;
            $this->Activity_model->add_activity($activity_data);

            $response['rc'] = TRUE;         
            $response['msg'] = lang('successful_login');
        }
        else
        {
            $response['rc'] = FALSE;         
            $response['msg'] = lang('invalid_username_and_password');
        }
        
        return $response;
    }

    function set_admin_login_session($user_data)
    {
        foreach($user_data as $key=>$data)
        {
            $this->session->set_userdata($key,$data);
        }
        
    }

    function update_admin_login($admin_id)
    {
        $ip = $this->input->ip_address();
        $query = "update admin set last_login_ip = ?, last_login_on = ? where id = ?";

        $this->db->query($query,array($ip,time(),$admin_id));
    }
    
    function get_admin_details($id)
    {
        return $this->db->get_where('admin',array('id'=>$id))->row_array();
    }

    function hash_equals($str1, $str2)
    {
        if(strlen($str1) != strlen($str2))
        {
            return false;
        }
        else
        {
            $res = $str1 ^ $str2;
            $ret = 0;
            for($i = strlen($res) - 1; $i >= 0; $i--)
            {
                $ret |= ord($res[$i]);
            }
            return !$ret;
        }
    }

    //Auto login for remember me cookie enabled users
    function auto_login_via_cookie($cookie_parts = array())
    {
        $selector = $cookie_parts[0];
        $pre_token = $cookie_parts[1];
        $token = hash('sha256', $pre_token);

        $auth_token = $this->db->query("select * from auth_tokens where selector = ?",array($selector))->row_array();
        
        $hash_compare = $this->hash_equals( $auth_token['token'] , $token );

        if($hash_compare && strtotime($auth_token['expires']) > time())
        {
            $admin = $this->db->query("select * from users where id = ?",array($auth_token['user_id']))->row_array();
            
            $user_data = array(
                'user_id' => $admin['id'],
                'user_type' => 'admin',
                'ms_365_access_token' => $admin['ms_365_access_token'],
                'ms_365_refresh_token' => $admin['ms_365_refresh_token'],
                'sync_outlook'        => $admin['ms_365_refresh_token']  
            );

            $this->set_admin_login_session($user_data);
            
            $this->load->model('User_model');
            $is_admin = $this->User_model->check_is_admin_by_user_id($admin['id']);

            if($is_admin['rc'])
            {
                $this->session->set_userdata('is_admin',TRUE);
            }
            else
            {
                $this->session->set_userdata('is_admin',FALSE);
            }
        }  
    }

    function is_auto_token_present($user_id)
    {
        $sql = "SELECT * FROM auth_tokens where user_id = ".$user_id;
        $result = $this->db->query($sql)->row_array();
        return $result;
    }
    
    function get_usergroup_id($id,$user_type)
    {
        $sql = "SELECT 
                    ur.*,u.*
                FROM
                    user_roles ur
                INNER JOIN 
                    users u
                WHERE 
                    u.id = ? 
                AND
                     u.user_type = ?
                AND
                    ur.role_name = u.user_type
                GROUP BY
                    ur.role_name";
                   
        $result = $this->db->query($sql,array($id,$user_type))->row_array();

        return $result;
    }    

    function get_users($user_id)
    {
        $sql = "SELECT 
                    a.*
                FROM
                    admin a
                WHERE 
                    a.id= ?";

        $query = $this->db->query($sql,$user_id)->row_array();

        return $query;
    }
}

?>
