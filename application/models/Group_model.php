<?php

class Group_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get_all_groups($limit = array(), $params = array())
	{
		if( ! empty( $limit ) )
        {
            if(isset($limit['limit']) && is_numeric($limit['limit']) && isset($limit['limit_start']) && is_numeric($limit['limit_start']))
                $this->db->limit($limit['limit'],$limit['limit_start']);
        }

        if(!empty($params))
        {
            foreach($params as $column => $value)
            {
                if( is_array($value))
                {
                    $this->db->where_in($column, $value);
                }
                else
                {
                    $this->db->where($column, $value);
                }
            }
        }
        
        $this->db->select('*');
        $this->db->from('groups');
        $this->db->order_by('all_contact','asc');
        $groups = $this->db->get()->result_array();

        if( ! empty($groups) )
        {
        	$response['rc'] = TRUE;
        	$response['msg'] = "";
        	$response['data'] = $groups;
        }
        else
        {
        	$response['rc'] = FALSE;
        	$response['msg'] = lang('data_not_found');
        	$response['data'] = array();

        }

        return $response;
	}

	function add($params)
	{
		$query = $this->db->insert('groups',$params);
		$group_id = $this->db->insert_id();

		if($query)
		{
            $other_data = $params;
            $other_data['group_name'] = $params['name'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_ADDED_NEWSLETTER_GROUP,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);

			$response['rc'] = TRUE;
			$response['data'] = $group_id;
			$response['msg'] = lang('group_added_successfully');
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('error_while_adding_group');
			$response['data'] = array();
		}

		return $response;
	}

	function get_group($id = 0)
	{
		$result = $this->db->get_where( 'groups', array( 'id' => $id ) )->row_array();

		if(!empty($result))
		{
			$response['rc'] = TRUE;
			$response['msg'] = "";
			$response['data'] = $result;
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('data_not_found');
		}

		return $response;
	}

	function update($id = 0, $params)
	{
		$this->db->where('id',$id);
		$query = $this->db->update('groups',$params);

		if($query)
		{
            $other_data = $params;
            $other_data['group_name'] = $params['name'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_UPDATED_NEWSLETTER_GROUP,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);

			$response['rc'] = TRUE;
			$response['msg'] = lang('group_updated_successfully');
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('error_while_updating_group');
		}

		return $response;
	}

	function delete_group($id = 0)
	{
		// Check dependency of group in other tables
		$sql = "SELECT 
					g.*
				FROM 
					group_has_users ghu 
				LEFT JOIN groups g ON g.id = ghu.group_id
				WHERE g.id = '".$id."'
                ";

		$group = $this->db->query($sql)->row_array();
		
		if( empty($group) )
		{
            $group = $this->get_group( $id );
			$this->db->where('id',$id);
            $this->db->where('can_delete', 'yes');
			$query = $this->db->delete('groups');

			if( $this->db->affected_rows() > 0 )
			{
                $other_data['group_name'] = $group['data']['name'];
                $activity_data = array(
                    'activity_by_user_id' => $this->user['id'],
                    'admin_id' => $this->user['id'],
                    'user_id' => $this->user['id'],
                    'activity_type' => ACTIVITY_ADMIN_DELETED_NEWSLETTER_GROUP,
                    'other_activity_data' => serialize($other_data),
                    'activity_on' => date('Y-m-d G:i:s')
                );

                $this->Activity_model->add_activity($activity_data);

				$response['rc'] = TRUE;
				$response['msg'] = lang('group_deleted_successfully');
			}
			else
			{
				$response['rc'] = FALSE;
				$response['msg'] = lang('you_can_not_delete_this_group');
			}
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] =lang('you_can_not_delete_this_group');
		}	
		return $response;
	}
    
    /*function get_users_by_group_id( $group_id = 0 )
    {
        $sql = "SELECT 
                        g.id as group_id,
                        ghu.lead_id,
                        u.first_name,
                        u.last_name,
                        u.email,
                        u.id as user_id
                FROM
                        groups g
                INNER JOIN
                        group_has_users ghu
                ON
                        g.id = ghu.group_id
                LEFT JOIN
                        users u
                ON
                        ghu.user_id = u.id
                WHERE
                        ghu.user_id != 0 AND
                		ghu.lead_id = 0 AND
                        g.id = ".$group_id;
 
        $data['users'] =  $this->db->query($sql)->result_array();
 
 
        $sql = "SELECT 
                        g.id as group_id,
                        l.first_name,
                        l.last_name,
                        l.email,
                        l.id as lead_id,
                        l.client_user_id as user_id
                FROM
                        groups g
                INNER JOIN
                        group_has_users ghu
                ON
                        g.id = ghu.group_id
                RIGHT JOIN
                        leads l
                ON
                        ghu.lead_id = l.id
                WHERE
                        ghu.lead_id != 0 AND
                        g.id = ".$group_id;
 
        $data['leads'] =  $this->db->query($sql)->result_array();
 
        if( ! empty($data) )
        {
            $response['rc'] = TRUE;
            $response['msg'] = "";
            $response['data'] = $data;
        }
 
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = "";
            $response['data'] = array();
        }
        return $response;
         
    }*/

    function get_users_by_group_id( $group_id = 0, $params = array() )
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(Distinct group_has_users.email) as count ";
            $group_by = "";
        }
        else
        {
            $select_statement = " group_has_users.* ";
            $group_by = " GROUP BY group_has_users.email ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }

        $email_condition = "";

        if(isset($params['email']) && trim($params['email'])!="")
        {
            $email = trim($params['email']);
            $email_condition = " AND (group_has_users.email LIKE '%".$email."%')";
        }

        $sql = "SELECT " . $select_statement . "
                FROM group_has_users
                LEFT JOIN unsubscribed_group_users ON group_has_users.id = unsubscribed_group_users.group_user_id
                WHERE group_has_users.group_id = '" .  $group_id . "' 
                AND unsubscribed_group_users.id IS NULL
                " . $email_condition . $group_by . $limit;
        $group_users = $this->db->query($sql)->result_array();

       /* $group_users = $this->db->get_where('group_has_users', array( 'group_has_users.group_id' => $group_id ))->result_array();*/
       
        if( ! empty($group_users) )
        {
            $response['rc'] = TRUE;
            $response['msg'] = '';
            $response['data'] = $group_users;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('no_data_found');
            $response['data'] = array();
        }

        return $response;
    }

    function get_unsubscribed_users_by_group_id($group_id = 0, $params = array())
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(Distinct group_has_users.email) as count ";
            $group_by = "";
        }
        else
        {
            $select_statement = " group_has_users.*, DATE_FORMAT( unsubscribed_group_users.unsubscribed_on, '%e-%m-%Y %T') as unsubscribe_user ";
            $group_by = " GROUP BY group_has_users.email ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }

        $email_condition = "";

        if(isset($params['email']) && trim($params['email'])!="")
        {
            $email = trim($params['email']);
            $email_condition = " AND (group_has_users.email LIKE '%".$email."%')";
        }

        $sql = "SELECT " . $select_statement . "
                FROM group_has_users
                JOIN unsubscribed_group_users ON group_has_users.id = unsubscribed_group_users.group_user_id
                WHERE group_has_users.group_id = '" .  $group_id . "' 
                " . $email_condition . $group_by . $limit;
        $group_users = $this->db->query($sql)->result_array();

        if( ! empty($group_users) )
        {
            $response['rc'] = TRUE;
            $response['msg'] = '';
            $response['data'] = $group_users;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('no_data_found');
            $response['data'] = array();
        }

        return $response;
    }

    function add_users_to_group( $group_id, $data )
    {
    	if( ! empty($data))
    	{
    		$this->db->insert('group_has_users', $data);
    	}
    	
        $group = $this->get_group( $group_id );
        $other_data['group_name'] = $group['data']['name'];
        $activity_data = array(
            'activity_by_user_id' => $this->user['id'],
            'admin_id' => $this->user['id'],
            'user_id' => $this->user['id'],
            'activity_type' => ACTIVITY_ADMIN_USERS_ADDED_TO_NEWSLETTER_GROUP,
            'other_activity_data' => serialize($other_data),
            'activity_on' => date('Y-m-d G:i:s')
        );

        $this->Activity_model->add_activity($activity_data);

		$response['rc'] = TRUE;
		$response['msg'] = lang('group_updated_successfully');

		return $response;
    }

    function delete_user_from_group( $group_user_id )
    {
        $result = $this->db->delete('group_has_users', array( 'id' => $group_user_id ) );

        if( $this->db->affected_rows() > 0 )
        {
            $this->db->delete('unsubscribed_group_users', array( 'group_user_id' => $group_user_id ) );
            
	    	$response['rc'] = TRUE;
			$response['msg'] = lang('user_deleted_from_group_successfully');
	    }
	    else
	    {
	    	$response['rc'] = FALSE;
			$response['msg'] = lang('invalid_data');
		}

		return $response;
    }

    function unsubscribe_user_from_group( $group_user_id )
    {
        $user = $this->db->get_where('group_has_users', array('id' => $group_user_id))->row_array();

        if( isset($user['id']) )
        {
            $unsubscibed_data = array(
                'group_id' => $user['group_id'],
                'group_user_id' => $user['id'],
                'unsubscribed_on' => date('Y-m-d G:i:s')
            );
            $result = $this->db->insert('unsubscribed_group_users', $unsubscibed_data );

            if( $result )
            {
                $response['rc'] = TRUE;
                $response['msg'] = lang('user_unsubscribe_from_group_successfully');
            }
            else
            {
                $response['rc'] = FALSE;
                $response['msg'] = lang('invalid_data');
            }
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('invalid_data');
        }

        return $response;
    }

    function subscribe_user_to_group( $group_user_id )
    {
        $user = $this->db->get_where('group_has_users', array('id' => $group_user_id))->row_array();

        if( isset($user['id']) )
        {
            $this->db->delete('unsubscribed_group_users', array( 'group_user_id' => $group_user_id ) );
            
            $response['rc'] = TRUE;
            $response['msg'] = lang('user_subscribe_to_group_successfully');
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('invalid_data');
        }

        return $response;
    }

    function get_users_for_newsletter($group_ids, $user_ids)
    {
    	$user_group_condition = "";

    	if( $group_ids != "" )
    	{
    		$user_group_condition = " OR users.id IN ( SELECT ghu.user_id FROM group_has_users ghu JOIN groups g ON g.id = ghu.group_id WHERE g.id IN ( ".$group_ids." ) ";
    		$lead_group_condition = " OR leads.id IN ( SELECT ghu.lead_id FROM group_has_users ghu JOIN groups g ON g.id = ghu.group_id WHERE g.id IN ( ".$group_ids." ) ";
    	}
    	else
    	{
    		$group_ids = 0;
    	}

    	if( $user_ids != "" )
    	{
    		//$lead_condition = " leads.id IN ( ".$lead_ids." ) ";
    	}
    	else
    	{
    		$user_ids = 0;
    	}

        $sql = "SELECT * FROM (SELECT 
                                    group_has_users.email COLLATE utf8_general_ci as email, 
                                    group_has_users.user_name COLLATE utf8_general_ci as user_name,
                                    group_has_users.id as u_id, 
                                    group_has_users.subscription_key, 
                                    'group' as type, 
                                    group_has_users.subscription_key_created_on 
                                FROM group_has_users
                                LEFT JOIN unsubscribed_group_users ON unsubscribed_group_users.group_user_id = group_has_users.id
                                WHERE group_has_users.group_id IN ( " . $group_ids . " )
                                AND unsubscribed_group_users.id IS NULL
                                UNION
                                SELECT 
                                    users.email, 
                                    CONCAT( users.first_name, ' ', users.last_name ) as user_name,
                                    users.id as u_id, 
                                    '' as subscription_key, 
                                    'user' as type, 
                                    '' as subscription_key_created_on
                                FROM users
                                WHERE users.id IN ( ".$user_ids." )
                                ) as u
                GROUP BY u.email";

    	$users = $this->db->query($sql)->result_array();

    	if( !empty($users) )
		{
			$response['rc'] = TRUE;
			$response['msg'] = '';
			$response['data'] = $users;
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = '';
			$response['data'] = array();
		}

		return $response;
    }

    function get_all_groups_has_users( $params = array() )
    {
        if(!empty($params))
        {
            foreach($params as $column => $value)
            {
                if( is_array($value))
                {
                    $this->db->where_in($column, $value);
                }
                else
                {
                    $this->db->where($column, $value);
                }
            }
        }

    	$this->db->select('groups.*');
        $this->db->from('groups');
        $this->db->join('group_has_users', 'groups.id = group_has_users.group_id');
        /*$this->db->join('unsubscribed_group_users', 'unsubscribed_group_users.id = group_has_users.', 'left');*/
        $this->db->group_by('groups.id');
        $groups = $this->db->get()->result_array();

        if( ! empty($groups) )
        {
        	$response['rc'] = TRUE;
        	$response['msg'] = "";
        	$response['data'] = $groups;
        }
        else
        {
        	$response['rc'] = FALSE;
        	$response['msg'] = lang('data_not_found');
        	$response['data'] = array();

        }

        return $response;
    }

    function get_group_by_name( $name )
    {
        $group = $this->db->get_where('groups', array('name' => $name))->row_array();

        if(!empty($group))
        {
            $response['rc'] = TRUE;
            $response['msg'] = "";
            $response['data'] = $group;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('data_not_found');
            $response['data'] = array();
        }

        return $response;
    }

    function get_users( $group_id )
    {
        $sql = "SELECT * FROM group_has_users WHERE group_id = '" . $group_id . "'";
        $users = $this->db->query($sql)->result_array();

        if( ! empty($users) )
        {
            $response['rc'] = TRUE;
            $response['msg'] = '';
            $response['data'] = $users;
        }
        else
        {
            $rewsponse['rc'] = FASLE;
            $response['msg'] = '';
            $response['data'] = array();
        }

        return $response;
    }

    function check_user_exists_in_group( $group_id, $email )
    {
        $this->db->where( 'group_id', $group_id );
        $this->db->where( 'email', $email );
        $user = $this->db->get( 'group_has_users' )->row_array();

        if( isset($user['id']) )
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function insert_users_to_group( $group_id, $group_users )
    {
        $sql = " INSERT IGNORE INTO group_has_users (group_id, user_name, email, added_on, added_by) VALUES";
        $count = 0;
       
        $i=1;
        foreach ($group_users as $key => $u) 
        {
            $sql .= "  ('". $group_id ."', '". $u['user_name'] ."', '". $u['email'] ."', '". $u['added_on'] ."', '". $u['added_by'] ."' ),";
            $i++;
        }
        $sql = rtrim($sql,',');
        $this->db->query($sql);

        $group = $this->get_group( $group_id );
        $other_data['group_name'] = $group['data']['name'];
        $activity_data = array(
            'activity_by_user_id' => $group_users[0]['added_by'],
            'admin_id' => $group_users[0]['added_by'],
            'user_id' => $group_users[0]['added_by'],
            'activity_type' => ACTIVITY_ADMIN_ADDED_USERS_TO_NEWSLETTER_GROUP_VIA_CSV,
            'other_activity_data' => serialize($other_data),
            'activity_on' => date('Y-m-d G:i:s')
        );

        $this->Activity_model->add_activity($activity_data);

        $response['rc'] = TRUE;
        $response['msg'] = lang('group_updated_successfully');
    }

    function load_data_infile($group_id, $user_id)
    {
        $dir = FCPATH . "uploads/tmp_csv/" . $user_id . "/";

        $file = $dir . 'user.csv';
        $file = str_replace('\\', '/', $file);
        $sql = "LOAD DATA LOCAL INFILE '" . $file . "' 
                INTO TABLE group_has_users 
                FIELDS TERMINATED BY ';' 
                LINES TERMINATED BY '\r\n' 
                IGNORE 1 LINES 
                (@col1,@col2) 
                set 
                    group_id = IF(@col2 REGEXP '[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}', '" . $group_id . "', 0),
                    user_name = @col1,
                    email = IF(@col2 REGEXP '[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}', @col2,''),
                    added_on = '" . date('Y-m-d G:i:s') . "',
                    added_by = '" . $user_id . "' ";
        $this->db->query($sql);

        $this->db->delete('group_has_users', array('group_id' => 0));

        $group = $this->get_group( $group_id );
        $other_data['group_name'] = $group['data']['name'];
        $activity_data = array(
            'activity_by_user_id' => $user_id,
            'admin_id' => $user_id,
            'user_id' => $user_id,
            'activity_type' => ACTIVITY_ADMIN_ADDED_USERS_TO_NEWSLETTER_GROUP_VIA_CSV,
            'other_activity_data' => serialize($other_data),
            'activity_on' => date('Y-m-d G:i:s')
        );

        $this->Activity_model->add_activity($activity_data);

        $response['rc'] = TRUE;
        $response['msg'] = lang('group_updated_successfully');
    }

    function get_user_group_info_by_email( $email )
    {
       $sql = "SELECT 
                    ghu.*,GROUP_CONCAT(ghu.group_id) as user_groups
                FROM
                    group_has_users ghu
                WHERE 
                    ghu.email = '".$email."'
                GROUP BY 
                    ghu.email";
                    //echo $sql;exit;
        $result = $this->db->query($sql)->row_array();
        return $result;
    }

    function delete_user_from_group_by_email($email, $user_groups)
    {
        $sql = "DELETE FROM 
                    group_has_users
                WHERE
                    email = '".$email."'
                    ";
        $result = $this->db->query($sql);

        if( $this->db->affected_rows() > 0 )
        {
            if(! in_array(NEWSLETTER_GROUP_ID_MAIN_CHANNEL, $user_groups))
            {
                $this->db->delete('newsletter_subscribers', array('email_id' => $email));
            }

            $response['rc'] = TRUE;
            $response['msg'] = lang('user_deleted_from_group_successfully');
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('invalid_data');
        }

        return $response;
    }

    function add_user_to_all_contact_group($email, $name = '')
    {
        $response = "";
        
        $all_contact_group = $this->get_group_with_all_contact();

        $sql2 = "SELECT * FROM group_has_users where email='".$email."' AND group_id = '".$all_contact_group['id']."'";
        $already_exists = $this->db->query($sql2)->row_array();

        if( ! isset($already_exists['id']))
        {
            $user_data = array(
                'user_name' => $name,
                'email' => $email,
                'group_id' =>  $all_contact_group['id'],
                'added_on' => date('Y-m-d G:i:s'),
            );
            $response = $this->db->insert( 'group_has_users', $user_data );
        }
        return $response;
    }
}