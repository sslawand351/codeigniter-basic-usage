<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_model extends CI_model {

    function add_email_stats($email_data)
    {
        $result = $this->db->insert('email_stats', $email_data);
        $last_insert_id = $this->db->insert_id();
        return $last_insert_id;     
    }

    function add_email_stats_batch( $data_array )
    {
        $this->db->insert_batch('email_stats',$data_array);
        return TRUE;
    }

    function fetch_queued_emails( $vendor_type = EMAIL_VENDOR_MANDRILL )
    {
        $sql = "SELECT email_stats.* 
                FROM email_stats
                WHERE 
                    newsletter_sent_id = 0
                    AND status = 'email_added' 
                    AND vendor_type = '" . $vendor_type . "' 
                LIMIT 5";
        $result = $this->db->query($sql)->result_array();

        if(!empty($result))
        {
            $response['rc'] = TRUE;
            $response['msg'] = '';
            $response['data'] = $result;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = '';
            $response['data'] = array();
        }

        return $response;
    }

    function fetch_queued_newsletter_emails( $vendor_type = EMAIL_VENDOR_MANDRILL )
    {
        $sql = "SELECT newsletter_email_stats.newsletter_sent_id, newsletter_sent.html
                FROM newsletter_email_stats
                JOIN newsletter_sent ON newsletter_sent.id = newsletter_email_stats.newsletter_sent_id
                WHERE newsletter_email_stats.status = 'email_added' 
                AND newsletter_email_stats.vendor_type = '" . $vendor_type . "' 
                LIMIT 1";

        $newsletter_sent = $this->db->query($sql)->row_array();

        if( isset($newsletter_sent['newsletter_sent_id']))
        {
            $sql = "SELECT 
                        newsletter_email_stats.* 
                    FROM newsletter_email_stats
                    WHERE status='email_added' 
                    AND vendor_type = '" . $vendor_type . "' 
                    AND newsletter_sent_id = '" . $newsletter_sent['newsletter_sent_id'] . "'
                    LIMIT 500";

            $result = $this->db->query($sql)->result_array();

            if( ! empty($result))
            {
                $response['rc'] = TRUE;
                $response['msg'] = '';
                $response['data'] = $result;
                $response['body_html'] = $newsletter_sent['html'];
            }
            else
            {
                $response['rc'] = FALSE;
                $response['msg'] = '';
                $response['data'] = array();
            }
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = '';
            $response['data'] = array();
        }

        return $response;
    }

    function update_queued_emails($mail_id, $mandril_hash_id, $status, $sent_on, $vendor = EMAIL_VENDOR_MANDRILL )
    {   
        $data = array(
            'hash' => $mandril_hash_id,
            'status' => $status,
            'sent_on' => date('Y-m-d G:i:s'),
            'vendor_type' => $vendor
        );

        $this->db->where('id',$mail_id);
        $result = $this->db->update('email_stats',$data);
    }

    function update_queued_newsletter_emails($newsletter_sent_id, $sent_email_stats)
    {
        $sql = "UPDATE newsletter_email_stats
                SET 
                    newsletter_email_stats.status = '" . $sent_email_stats['status'] . "',
                    newsletter_email_stats.hash = '" . $sent_email_stats['hash'] . "',
                    newsletter_email_stats.sent_on = '" . date('Y-m-d G:i:s') . "'
                
                WHERE 
                    to_email = '" . $sent_email_stats['to_email'] . "'
                    AND newsletter_sent_id = '" . $newsletter_sent_id . "'

                ";
        $result = $this->db->query($sql);
    }

    function get_all_email_templates($limit = array())
    {
        if( ! empty($limit))
        {
            if(isset($limit['limit']) && is_numeric($limit['limit']) && isset($limit['limit_start']) && is_numeric($limit['limit_start']))
                $this->db->limit($limit['limit'],$limit['limit_start']);
        }

        $email_templates = $this->db->get('email_templates')->result_array();

        if( ! empty($email_templates) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $email_templates;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }

    function add_email_template($email_template_data)
    {
        $add_result = $this->db->insert('email_templates', $email_template_data);

        if($add_result)
        {
            $email_template_id = $this->db->insert_id();
            $response['msg']    = lang('email_template_created_successfully');
            $response['rc']     = TRUE;
            $response['data']   = array('id' => $email_template_id);
        }
        else
        {
            $response['msg']    = lang('error_occured_while_inserting_data');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }

        return $response;
    }

    function update_email_template($id, $email_template_data)
    {
        $this->db->where('id', $id);
        $update_result = $this->db->update('email_templates', $email_template_data);

        if($update_result)
        {
            $response['msg']    = lang('email_template_updated_successfully');
            $response['rc']     = TRUE;
        }
        else
        {
            $response['msg']    = lang('error_occured_while_updating_data');
            $response['rc']     = FALSE;
        }

        return $response;
    }

    function add_header_image($email_template_id, $image)
    {
        if (isset($image['name']) && $image['name'] != "")
        {
            $dir = FCPATH . "uploads/email_module/templates/images/header/" . $email_template_id . "/";

            if(!is_dir($dir))
            {   
                @mkdir($dir, 0777,true);
            }

            $hash = md5(microtime().rand()).".jpg";

            if(move_uploaded_file($image['tmp_name'], $dir.$hash))
            {
                /*copy($dir.$hash, $dir.$images['header_background']['name']);
               
                //Create Image thumbnail
                createThumbnail($dir.$hash,$dir.'large_'.$hash,IMAGE_HEIGHT_LARGE,IMAGE_WIDTH_LARGE);
                createThumbnail($dir.$hash,$dir.'medium_'.$hash,IMAGE_HEIGHT_MEDIUM,IMAGE_WIDTH_MEDIUM);
                createThumbnail($dir.$hash,$dir.'small_'.$hash,IMAGE_HEIGHT_SMALL,IMAGE_WIDTH_SMALL);*/
                $where = array('id' => $email_template_id);
                $update_data = array('header_image' => $hash);
                $this->db->update('email_templates', $update_data, $where);
            }
        }
    }

    function add_footer_image($email_template_id, $image)
    {
        if (isset($image['name']) && $image['name'] != "")
        {
            $dir = FCPATH . "uploads/email_module/templates/images/footer/" . $email_template_id . "/";

            if(!is_dir($dir))
            {
                @mkdir($dir, 0777,true);
            }

            $hash = md5(microtime().rand()).".jpg";

            if(move_uploaded_file($image['tmp_name'], $dir.$hash))
            {
                /*copy($dir.$hash,$dir.$images['car_images']['name'][$key]);
               
                //Create Image thumbnail
                createThumbnail($dir.$hash,$dir.'large_'.$hash,IMAGE_HEIGHT_LARGE,IMAGE_WIDTH_LARGE);
                createThumbnail($dir.$hash,$dir.'medium_'.$hash,IMAGE_HEIGHT_MEDIUM,IMAGE_WIDTH_MEDIUM);
                createThumbnail($dir.$hash,$dir.'small_'.$hash,IMAGE_HEIGHT_SMALL,IMAGE_WIDTH_SMALL);*/
                
                $where = array('id' => $email_template_id);
                $update_data = array('footer_image' => $hash);
                $this->db->update('email_templates', $update_data, $where);
            }
        }
    }

    function get_email_template($id = 0, $where = array())
    {
        $condition = "";
        $id = (int) $id;
        if( $id != 0 && $id != '')
        {
            $condition .= " AND et.id = '" .$id. "' ";
        }

        if( ! empty($where))
        {
            foreach ($where as $k => $v)
            {
                $v = trim($v);

                switch ($k)
                {
                    /*case 'email':
                        $condition .= " AND c.email LIKE '" .$v. "' ";
                        break;*/
                    
                    default:
                        break;
                }
            }
        }

        $sql = "
                SELECT
                    et.*

                FROM 
                    email_templates et
  
                WHERE
                    1=1
                    " . $condition . "

                ORDER BY et.id DESC
        ";
        
        $email_template = $this->db->query($sql)->row_array();
        
        if(isset($email_template))
        {
            $response['rc']     = TRUE;
            $response['data']   = $email_template;
            $response['msg']    = '';
        }
        else
        {
            $response['rc']     = FALSE;
            $response['data']   = array();
            $response['msg']    = sprintf( lang('no_data_found'), 'email_templates' );
        }

        return $response;
    }

    function delete_email_template($email_template_id)
    {
        $email_template = $this->Email_model->get_email_template($email_template_id);

        if($email_template['rc'])
        {
            $this->db->delete('email_templates', array('id' => $email_template_id));
            $dir = FCPATH . 'uploads/email_module/templates/images/';
            $file_path = $dir . 'header/' . $email_template['data']['id'] . '/' . $email_template['data']['header_image'];

            if(file_exists($file_path) && $email_template['data']['header_image'] != '')
            {
                unlink($file_path);
                rmdir($dir . 'header/' . $email_template['data']['id']);
            }

            $file_path = $dir . 'header/' . $email_template['data']['id'] . '/' . $email_template['data']['footer_image'];

            if(file_exists($file_path) && $email_template['data']['footer_image'] != '')
            {
                unlink($file_path);
                rmdir($dir . 'footer/' . $email_template['data']['id']);
            }

            $response['rc']     = TRUE;
            $response['msg']    = lang('email_template_deleted_successfully');
        }
        else
        {
            $response['rc']     = TRUE;
            $response['msg']    = lang('invalid_data');
        }

        return $response;
    }

    function add_email($email_data)
    {
        $add_result = $this->db->insert('form_has_emails', $email_data);

        if($add_result)
        {
            $email_id = $this->db->insert_id();

            $response['rc'] = TRUE;
            $response['msg'] = lang('email_added_successfully');
            $response['data'] = array('id' => $email_id);
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('error_occured_while_inserting_data');
            $response['data'] = array();
        }

        return $response;
    }

    function get_all_emails($params = array(), $count_flag = FALSE, $limit = array())
    {
        if( $count_flag )
        {
            $select_statement = " count(e.id) as count ";
            $group_by = "";         
        }
        else
        {
            $select_statement = " 
                                e.*,
                                et.header_image,
                                et.footer_image,
                                et.body_color,
                                et.content_color
                                ";
            $group_by = " GROUP BY e.id ";
        }

        $limit_condition = "";

        if( isset($limit['start']) && isset($limit['limit']) )
        {
            $limit_condition = " LIMIT " . $limit['start'] . ", " . $limit['limit'] . " ";
        }

        $condition = "";

        if( ! empty($params))
        {
            foreach ($params as $k => $v)
            {
                $v = is_array($v) ? $v : trim($v);

                switch ($k)
                {
                    case 'form_id':
                        $condition .= " AND e.form_id = '" .$v. "' ";
                        break;

                    case 'form_type':
                        $condition .= " AND e.form_type = '" .$v. "' ";
                        break;

                    case 'form_name':
                        $condition .= " AND e.form_name = '" .$v. "' ";
                        break;

                    default:
                        break;
                }
            }
        }

        $sql = "
                SELECT
                    " . $select_statement . "
                FROM 
                    form_has_emails e
                    JOIN email_templates et ON e.email_template_id = et.id

                WHERE
                    1=1
                    " .$condition. " 
                ORDER BY e.added_on DESC, e.id DESC
                " .$limit_condition. "
        ";

        $query = $this->db->query($sql);
        $emails = $count_flag ? $query->row_array() : $query->result_array(); 
        
        if(count($emails) > 0)
        {
            $response['rc']     = TRUE;
            $response['data']   = $emails;
            $response['msg']    = '';
        }
        else
        {
            $response['rc']     = FALSE;
            $response['data']   = array();
            $response['msg']    = sprintf( lang('no_data_found'), 'emails' );
        }

        return $response;
    }

    function get_email($id = 0, $where = array())
    {
        $condition = "";

        if( $id != 0 && $id != '')
        {
            $condition .= " AND e.id = '" .$id. "' ";
        }

        if( ! empty($where))
        {
            foreach ($where as $k => $v)
            {
                $v = trim($v);

                switch ($k)
                {
                    case 'form_id':
                        $condition .= " AND e.form_id = '" .$v. "' ";
                        break;
                    
                    default:
                        break;
                }
            }
        }

        $sql = "
                SELECT
                    e.*,
                    et.header_image,
                    et.footer_image,
                    et.body_color,
                    et.content_color

                FROM 
                    form_has_emails e
                    JOIN email_templates et ON e.email_template_id = et.id
  
                WHERE
                    1=1
                    " . $condition . "

                ORDER BY e.id DESC
        ";
        
        $email = $this->db->query($sql)->row_array();
        
        if(isset($email['id']) && (($id != 0 && $id != '') || ! empty($where)) )
        {
            $response['rc']     = TRUE;
            $response['data']   = $email;
            $response['msg']    = '';
        }
        else
        {
            $response['rc']     = FALSE;
            $response['data']   = array();
            $response['msg']    = sprintf( lang('no_data_found'), 'email' );
        }

        return $response;
    }

    function update_email($id, $email_data)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('form_has_emails', $email_data);

        if($result)
        {
            $response['rc']     = TRUE;
            $response['msg']    = lang('email_updated_successfully');
        }
        else
        {
            $response['rc']     = FALSE;
            $response['msg']    = lang('error_occured_while_updating_data');
        }

        return $response;
    }

    function delete_email($email_id)
    {
        $email = $this->get_email($email_id);

        if($email['rc'])
        {
            $this->db->delete('form_has_emails', array('id' => $email_id));

            $response['rc']     = TRUE;
            $response['msg']    = lang('email_deleted_successfully');
        }
        else
        {
            $response['rc']     = TRUE;
            $response['msg']    = lang('invalid_data');
        }

        return $response;
    }
}