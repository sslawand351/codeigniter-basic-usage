<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_model extends CI_model {

	function __construct()
	{
		parent::__construct();
	}

	function get_all_forms()
	{
		$forms = $this->db->get('forms')->result_array();

		if( ! empty($forms) )
		{
			$response['rc'] = TRUE;
			$response['msg'] = '';
			$response['data'] = $forms;
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('no_data_found');
			$response['data'] = array();
		}

		return $response;
	}

	function add_form($array)
	{
		$form_data = array(
			'title' => $array['form'][0], 
			'website_id' => $array['form'][1], 
			'added_on' => date('Y-m-d G:i:s'),
			'added_by' => $this->user['id'],
			'modified_on' => date('Y-m-d G:i:s')
		);

		if($this->db->insert('forms', $form_data ))
        {
            $form_id = $this->db->insert_id();
             
            if(isset($array['form'][2]))
            {
                foreach($array['form'][2] as $fields)
                {
                	$form_fields = array(
                		'field_type' => $fields[0], 
                        'label' => trim($fields[1]),
                        'shortcode' => $this->clean(trim(strtolower($fields[1]))),
                        'is_required' => $fields[2], 
                        'form_id'=> $form_id
                	);

                    $this->db->insert('form_has_fields', $form_fields);
                    $field_id = $this->db->insert_id();

                    if(isset($fields[4]) && !is_array($fields[4]))
                    {
                        if($fields[4] == "Yes")
                        {
                            $this->db->where("id",$field_id);
                            $this->db->set("is_multiple" , $fields[4]);
                            $this->db->update("form_has_fields");
                        }
                    }

                    if(isset($fields[3]) && is_array($fields[3]))
                    {
                        foreach($fields[3] as $options)
                        {                        
                            $this->db->insert('field_has_options', array('option_value' => $options, 'field_id'=> $field_id));
                        }                                   
                    }
                    else
                    {
                        //No Options
                    }
                }
                
            }                        
     
    		$response['rc'] = TRUE;
    		$response['msg'] = lang('form_created_successfully');
        }
        else
        {
        	$response['rc'] = TRUE;
    		$response['msg'] = lang('error');
        }

        return $response;
	}

	function update_form( $form_id, $form_array)
	{
		// Update form name
        $this->db->where('id',$form_id);
        $this->db->set('title',$form_array['form'][0]);
        $this->db->update('forms');

        $old_form_fields_temp_array = $this->get_form_fields( $form_id );
        $old_form_fields_temp_array = $old_form_fields_temp_array['rc'] ? $old_form_fields_temp_array['data'] : array(); 
        /*$this->db->select('id');  
        $old_form_fields_temp_array = $this->db->get_where("form_has_fields", array("form_id" => $form_id))->result_array();*/
        
        $old_form_fields = array();
        $new_form_fields = array();
        
        $old_field_options = array();
        $new_field_options = array();
        
        foreach($old_form_fields_temp_array as $old_form_fields_temp_value)
        {            
            $old_form_fields[] = $old_form_fields_temp_value['field_id'];
        }

        if(isset($form_array['form'][3])){

        	$count = 1;
            foreach($form_array['form'][3] as $fields)
            { 
                if(isset($fields[3]['field_id']) && $fields[3]['field_id'] != "undefined" && is_array($fields[3]))
                {                    
                    $field_id = $fields[3]['field_id'];              
                    
                    $new_form_fields[] = $field_id;

                    $form_fields = array(
                		'field_type' => $fields[0], 
                        'label' => trim($fields[1]),
                        'shortcode' => $this->clean(trim(strtolower($fields[1]))),
                        'is_required' => $fields[2],
                        'order' => $count++
                	);
                   
                    $this->db->update("form_has_fields", $form_fields, array("id" => $field_id));                                    
                }
                else
                {
                    if(isset($fields[3]) && $fields[3] == "Yes") {
                        $isMultiple = "Yes";
                    } else {
                        $isMultiple = "";
                    }

                    $form_fields = array(
                		'field_type' => $fields[0], 
                        'label' => trim($fields[1]),
                        'shortcode' => $this->clean(trim(strtolower($fields[1]))),
                        'is_required' => $fields[2], 
                        'form_id'=> $form_id,
                        'is_multiple' => $isMultiple,
                        'order' => $count++
                	);
                    
                    $result = $this->db->insert('form_has_fields', $form_fields);
                }

                if(isset($fields[4]) && is_array($fields[4]))
                {
                    $new_field_id = $this->db->insert_id();  
                    
                    foreach($fields[4] as $options)
                    {
                        if(isset($options['option_id']) && $options['option_id'] != "undefined")
                        {
                            $new_field_options[] = $options['option_id'];
                          
                            $this->db->update("field_has_options", array('option_value' => $options['option_value']), array("id" => $options['option_id']));                                                                                                                          
                        }
                        else
                        {
                            $field_ids  = isset($fields[3]['field_id']) && $fields[3]['field_id'] != "undefined" ? $fields[3]['field_id'] : $new_field_id;
                            $this->db->insert('field_has_options', array('option_value' => $options['option_value'], 'field_id '=> $field_ids));                   
                            $new_option_id = $this->db->insert_id();
                            $new_field_options[] = $new_option_id;
                        }
                    }                                   
                }
                else if(isset($fields[4]) && !is_array($fields[4])){
                    
                    if(isset($fields[3]['field_id']) && $fields[3]['field_id'] != "undefined")
                    {                    
                        $field_id = $fields[3]['field_id'];              
                        if($fields[4] == "Yes"){
                            $isMultiple = "Yes";
                        }else{
                            $isMultiple = "";
                        }
                        $new_form_fields[] = $field_id;

                        $this->db->update("form_has_fields", array('is_multiple' => $isMultiple), array("id" => $field_id));                                    
                    }
                }
                else
                {
                    //No Options
                }
            }
            
        }
        foreach($old_form_fields as $old_form_field)
        {                        
            $this->db->select('id');  
        
            $old_field_options_temp_array = $this->db->get_where("field_has_options", array("field_id" => $old_form_field))->result_array();            

            foreach($old_field_options_temp_array as $old_field_options_temp_value)
            {            
                $old_field_options[] = $old_field_options_temp_value['id'];
            }            
        }     
                
        $deleted_fields = array_diff($old_form_fields, $new_form_fields);
        $deleted_field_options = array_diff($old_field_options, $new_field_options);

        if(isset($deleted_fields))
        {
            foreach($deleted_fields as $deleted_field)
            {
               /*$this->db->query("DELETE 
                                 FROM form_has_fields                         
                                 WHERE id = ".$deleted_field);*/ 
                
                $this->db->update("form_has_fields", array('soft_delete' => 1), array("id" => $deleted_field));                                                                                                                          
            }                   
        }
        if($deleted_field_options)
        {
            foreach($deleted_field_options as $deleted_field_option)
            {
		        /*$this->db->query("DELETE 
		                          FROM field_has_options                         
		                          WHERE id = ".$deleted_field_option);*/ 
                
                $this->db->update("field_has_options", array('soft_delete' => 1), array("id" => $deleted_field_option));                                                                                                                          
            }            
        }

        $response['rc'] = TRUE;
    	$response['msg'] = lang('form_updated_successfully');  
        return $response;
	}

	function delete_form( $id )
	{
		$form = $this->get_form( $id );

		if( $form['rc'] )
		{
			$this->delete_form_field_options( $id );
			$this->delete_form_field_values( $id );
			$this->delete_form_fields( $id );		
			$this->delete_form_configurations( $id );
			
			$this->db->where('id', $id);
			$this->db->delete('forms');

			$response['rc'] = TRUE;
			$response['msg'] = lang('form_deleted_successfully');
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('invalid_data');
		}

		return $response;
	}

	function add_form_fields( $form_id, $form_field_data )
	{
		
	}

	function get_form( $id )
	{
		$form = $this->db->get_where( 'forms', array('id' => $id) )->row_array();

		if( isset($form['id']) )
		{
			$response['rc'] = TRUE;
			$response['msg'] = '';
			$response['data'] = $form;
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('no_data_found');
			$response['data'] = array();
		}

		return $response;
	}

	function delete_form_field_options( $form_id )
	{
		$sql = "DELETE o 
				FROM field_has_options as o
				JOIN form_has_fields ON form_has_fields.id = o.field_id
				WHERE form_has_fields.form_id = '".$form_id."'";

		$this->db->query($sql);
		/*$this->db->where( 'form_has_fields.form_id', $form_id );
		$this->db->join('form_has_fields', '');
		$this->db->from('field_has_options as o');
		$this->db->delete('o');*/
	}

	function delete_form_field_values( $form_id )
	{
		$this->db->where( 'form_id', $form_id );
		$this->db->delete('form_fields_have_values');
	}

	function delete_form_fields( $form_id )
	{
		$this->db->where( 'form_id', $form_id );
		$this->db->delete('form_has_fields');
	}

	function delete_form_configurations( $form_id )
	{
		$this->db->where( 'form_id', $form_id );
		$this->db->delete('form_configuration');
	}

	function get_form_fields( $form_id )
	{
		$this->db->order_by( 'form_has_fields.order');
		$this->db->select( 'form_has_fields.id as field_id, form_has_fields.*, forms.*');
		$this->db->join('forms', 'form_has_fields.form_id = forms.id');
		$this->db->group_by('form_has_fields.id');
		$form_fields = $this->db->get_where( 'form_has_fields', array('form_has_fields.form_id' => $form_id, 'form_has_fields.soft_delete' => 0) )->result_array();

		if( ! empty($form_fields) )
		{
			$response['rc'] = TRUE;
			$response['msg'] = '';
			$response['data'] = $form_fields;
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('no_data_found');
			$response['data'] = array();
		}

		return $response;
	}

	function get_form_field_options( $field_id )
	{
		$field_options = $this->db->get_where( 'field_has_options', array('field_has_options.field_id' => $field_id, 'field_has_options.soft_delete' => 0) )->result_array();

		if( ! empty($field_options) )
		{
			$response['rc'] = TRUE;
			$response['msg'] = '';
			$response['data'] = $field_options;
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('no_data_found');
			$response['data'] = array();
		}

		return $response;
	}

	function get_form_configuration( $form_id )
	{
		$form_configuration = $this->db->get_where('form_configuration', array('form_id' => $form_id ) )->row_array();

		if( ! empty($form_configuration) )
		{
			$response['rc'] = TRUE;
			$response['msg'] = '';
			$response['data'] = $form_configuration;
		}
		else
		{
			$response['rc'] = FALSE;
			$response['msg'] = lang('no_data_found');
			$response['data'] = array();
		}

		return $response;
	}

	function add_form_configuration($configArray)
    {
        $form_configuration = $this->get_form_configuration( $configArray['form_id'] );
        
        if($form_configuration['rc'])
        {
            $data = "updated";
            
            $this->db->where('form_id', $configArray['form_id']);
            $this->db->update('form_configuration', $configArray);            
        }
        else
        {
            $this->db->insert('form_configuration', $configArray);
            $data = "added";
        }
        
        return $data;
    }

    function clean($string)
    {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

    function add_form_data($form_data)
    {
        $result = $this->db->insert('frontend_forms', $form_data);

        if($result)
        {
            $insert_id = $this->db->insert_id();

            $response['rc'] = TRUE;
            $response['msg'] = lang('frontend_form_added_successfully');
            $response['data'] = array('id' => $insert_id);
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('error_occured_while_inserting_data');
            $response['data'] = '';
        }

        return $response;
    }

    function get_frontend_forms($params = array(), $count_flag = FALSE, $limit = array())
    {
        if( $count_flag )
        {
            $select_statement = " count(f.id) as count ";
            $group_by = "";         
        }
        else
        {
            $select_statement = " 
                                f.*,
                                forms.title ";
            $group_by = " GROUP BY f.id ";
        }

        $limit_condition = "";

        if( isset($limit['start']) && isset($limit['limit']) )
        {
            $limit_condition = " LIMIT " . $limit['start'] . ", " . $limit['limit'] . " ";
        }

        $condition = "";

        $sql = "
                SELECT
                    " . $select_statement . "
                FROM 
                    frontend_forms f
                    JOIN forms ON f.form_id = forms.id

                WHERE
                    1=1
                    " .$condition. " 
                ORDER BY f.added_on DESC, f.id DESC
                " .$limit_condition. "
        ";

        $query = $this->db->query($sql);
        $form_data = $count_flag ? $query->row_array() : $query->result_array();

        if(count($form_data) > 0)
        {
            $response['rc'] = TRUE;
            $response['msg'] = '';
            $response['data'] = $form_data;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('no_data_found');
            $response['data'] = array();
        }

        return $response;
    }

    function get_frontend_forms_by_form_id($params = array(), $count_flag = FALSE, $limit = array())
    {
        if( $count_flag )
        {
            $select_statement = " count(f.id) as count ";
            $group_by = "";         
        }
        else
        {
            $select_statement = " 
                                f.*,
                                forms.title ";
            $group_by = " GROUP BY f.id ";
        }

        $limit_condition = "";

        if( isset($limit['start']) && isset($limit['limit']) )
        {
            $limit_condition = " LIMIT " . $limit['start'] . ", " . $limit['limit'] . " ";
        }

        $condition = "";

        $sql = "
                SELECT
                    " . $select_statement . "
                FROM 
                    frontend_forms f
                    JOIN forms ON f.form_id = forms.id
                WHERE
                    1=1
                AND
                    f.form_id ='".$params['form_id']."'
                AND
                    f.id = '".$params['id']."'
                    " .$condition. " 
                ORDER BY f.added_on DESC, f.id DESC
                " .$limit_condition. "
        ";

        $query = $this->db->query($sql);

        $form_data = $count_flag ? $query->row_array() : $query->result_array();
        if(count($form_data) > 0)
        {
            $response['rc'] = TRUE;
            $response['msg'] = '';
            $response['data'] = $form_data;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('no_data_found');
            $response['data'] = array();
        }

        return $response;
    }
}