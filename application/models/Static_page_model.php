<?php
class Static_page_model extends CI_model
{

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to add pages 
     * @param  
     * @return NULL
     */

    function add($page_data)
    {
 
        $result = $this->db->insert('static_pages',$page_data);
        $page_id = $this->db->insert_id();

        if($result)
        {
            $response['rc'] = TRUE;
            $response['data'] = $page_id;
            $response['msg'] = "pages found";
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = "No pages found";
        }
        return $response;

    }
    
    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to add pages by language id 
     * @param  
     * @return NULL
     */

    function add_pages_by_language($page_data)
    {
    
        $result = $this->db->insert_batch('static_pages_by_language',$page_data);
        
        if( $result )
        {
            $response["rc"] = TRUE;
            $response["msg"] = lang('page_successfully_added');
        }
        else
        {
            $response["rc"] = FALSE;
            $response["msg"] = lang('error_adding_page');
        }

        return $response;
    }

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to get all pages 
     * @param  
     * @return NULL
     */
  

    function get_all_pages($limit = "", $limit_start="", $language_id='')
    {
        $limit_condition = "";
        if($limit != '')
        {
            $limit_condition = " limit " . $limit_start . ',' . $limit;
        }

       
        $query = "SELECT
                    *, sp.id as sp_id
                FROM
                    static_pages sp               
            
                INNER JOIN
                      static_pages_by_language spl
                ON
                   sp.id = spl.page_id
                
                AND
                        spl.page_title!=''  
                GROUP BY
                sp.id
                        ".$limit_condition."

                ";

                 
        $result = $this->db->query($query)->result_array();
      
        if(!empty($result))
        {
            $response['rc'] = TRUE;
            $response['data'] = $result;
            $response['msg'] = "pages found";
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = "No pages found";
            $response['data'] = array();
        }
        return $response;
    }


    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to check the occurence of page url
     * @param  
     * @return NULL
     */

    function check_url_occurrence($title)
    {
        $url = $title;
        if(!empty($url))
        {
            $query = "SELECT 
                                *
                        FROM
                                static_pages_by_language
                        WHERE
                                page_title = '".$url."'
                    ORDER BY
                                added_on
                        DESC

                        ";
            $result = $this->db->query($query)->row_array();

            if(!empty($result))
            {
                if($result['url_occurrence']=="0")
                {
                    $array["url"] = $url."-1"; 
                    $array["url_occurrence"] = 1; 
                }
                else
                {
                    $occurence = $result['url_occurrence'] + 1;
                    $array["url"] = $url."-".$occurence; 
                    $array["url_occurrence"] = $occurence; 
                }
            }
            else
            {
                $array["url"]               = $url;
                $array["url_occurrence"]    = "";
            }

            return $array;
        }
    }


    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to get languages to add pages by lang id
     * @param  
     * @return NULL
     */

    function get_languages()
    {
        $query = "SELECT 
                        *
                    FROM
                        meta_front_end_languages
                ";
        $result = $this->db->query($query)->result_array();

        if(!empty($result))
        {
            $response['rc'] = TRUE;
            $response['msg'] = lang('languages_found');
            $response['data'] = $result;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('no_languages_found');
        }

        return $response;
    }

    function get_page_categories()
    {
        return $this->db->get('page_has_categories')->result_array();
    }

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to get pages by lang id
     * @param  
     * @return NULL
     */

    function get_page($id, $language_id = "")
    {
        $lang_condition = "";

        if( $language_id != "" )
        {
            $lang_condition = "AND al.language_id = '" . $language_id . "' ";
        }

        $query = "SELECT
                        *, sp.id as page_id
                    FROM
                        static_pages sp
                INNER JOIN
                        static_pages_by_language spl
                    ON
                        sp.id = spl.page_id
                WHERE 
                    sp.id = '".$id."'
                    ".$lang_condition;

        if($language_id!="")
            $data = $this->db->query($query)->row_array();

        else
            $data = $this->db->query($query)->result_array();
        

        if(!empty($data ))
        {
            if($language_id == "")
            {
                foreach ($data as $key=>$value) {
                    
                    $this->db->select('language');
                    $language_name = $this->db->get_where("meta_front_end_languages",array('id' => $value['language_id']))->row_array();
                    if(!empty($language_name))
                        $data[$key]['language_name'] = $language_name['language'];
                    else
                        $data[$key]['language_name'] = "";
                }
            }


            $response["rc"] = TRUE;
            $response["data"] = $data;
            $response["msg"] = "";
        }
        else
        {
            $response["rc"] = FALSE;
            $response["data"] = array();
            $response["msg"] = lang('invalid_data');
        }
        
        return $response;
    }

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to update existing pages
     * @param  
     * @return NULL
     */

    function update($id, $params, $param1)
    {
      
        if($this->db->update('static_pages', $params, array('id' => $id)))
        {
            $this->db->where('page_id', $id);
            $this->db->update_batch('static_pages_by_language',$param1, 'language_id'); 

            $response["rc"] = TRUE;
            $response["msg"] = lang('page_successfully_updated');
        }
        else
        {
            $response["rc"] = FALSE;
            $response["msg"] = lang('error_updating_page');
        }

        return $response;
    }

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to check unique url of pages
     * @param  
     * @return NULL
     */

    function check_unique_url($id,$language_id,$url)
    {
        $query = "SELECT
                        *
                    FROM
                        static_pages sp
                    INNER JOIN
                        static_pages_by_language spl
                    ON
                        sp.id = spl.page_id
                    AND
                        spl.language_id
                    WHERE
                        sp.id != '".$id."'
                    AND
                        spl.url = '".$url."'";
        $result = $this->db->query($query)->row_array();
                                
        return $result;

    }

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  function to delete pages
     * @param  
     * @return NULL
     */

    function delete($id, $blog_details=array())
    {
        $this->db->where('page_id', $id);
        $this->db->delete('static_pages_by_language');

       
        if( $this->db->delete('static_pages', array('id' => $id)) )
        {
            $response["rc"] = TRUE;
            $response["msg"] = lang('page_successfully_deleted');
        }
        else
        {
            $response["rc"] = FALSE;
            $response["msg"] = lang('error_deleting_page');
        }
        
        return $response;
    }

    function change_status( $id, $params)
    {  
        if( $this->db->update('static_pages', $params, array('id' => $id)) )
        {
            $response["rc"] = TRUE;
            $response["msg"] = $params['is_offline'] == 0 ? lang('static_page_is_successfully_published') : lang('static_page_is_successfully_unpublished');
        }
        else
        {
            $response["rc"] = FALSE;
            $response["msg"] = lang('there_was_some_error');
        }
        
        return $response;
    }

    function get_static_page_by_url($url, $language_id =0)
    {
        $lang_condition = "";
        if($language_id > 0)
        {
            $lang_condition = " AND spl.language_id = '" .$language_id. "'";
        }
        
        $query = "SELECT 
                        *
                    FROM
                        static_pages_by_language spl
                    LEFT JOIN 
                        static_pages sp
                    ON 
                        spl.page_id = sp.id
                    WHERE
                        sp.is_offline = '1' AND
                        spl.url = '".$url."'"
                        .$lang_condition;
        //echo $query; exit;
        $result = $this->db->query($query)->row_array();
        
        if(!empty($result))
        {
            $response['rc'] = TRUE;
            $response['data'] = $result;
            $response['msg'] = "pages found";
        }
        else
        {
            $response['rc'] = FALSE;
            $response['data'] = $result;
            $response['msg'] = "No Content found";
            $response['data'] = '';
        }
        return $response;
    }
}