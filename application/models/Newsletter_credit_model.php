<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_credit_model extends CI_model{   

    function get_all_credits()
    {
        $credits = $this->db->get_where("user_has_newsletter_credits")->row_array();
        
        if( isset($credits['id']) )
        {
            $response['msg']    = '';
            $response['rc']     = TRUE;
            $response['data']   = $credits;
        }
        else
        {
            $response['msg']    = lang('no_data_found');
            $response['rc']     = FALSE;
            $response['data']   = array();
        }
        
        return $response;
    }
    
    function purchase($array, $user_id)
    {        
        $previous_credits = $this->get_all_credits();
        $previous_credits_available = $previous_credits['data'];
        
        if(isset($previous_credits_available['credits_available']))
        {
            if( ! empty($previous_credits_available['credits_available']))
            {
                $new_credits_available['credits_available'] = $array['number_of_credits'] + $previous_credits_available['credits_available'];

                $credits_data = array(
                    'user_id' => $user_id,
                    'credits_amount' => $array['number_of_credits'],
                    'credits_available' => $new_credits_available['credits_available'],
                    'purchase_date' => date('Y-m-d G:i:s')
                );
                $this->db->insert("newsletter_credits_purchase_history", $credits_data);
                return $this->db->update("user_has_newsletter_credits", $new_credits_available, array('id' => $previous_credits_available['id']));                        
            }
            else
            {
                $new_credits_available['credits_available'] = $array['number_of_credits'] + $previous_credits_available['credits_available'];

                $credits_data = array(
                    'user_id' => $user_id,
                    'credits_amount' => $array['number_of_credits'],
                    'credits_available' => $new_credits_available['credits_available'],
                    'purchase_date' => date('Y-m-d G:i:s')
                );

                $this->db->insert("newsletter_credits_purchase_history", $credits_data);
                return $this->db->update("user_has_newsletter_credits", $new_credits_available, array('id' => $previous_credits_available['id']));                
            }
        }
        else
        {
            $credits_data = array(
                'user_id' => $user_id,
                'credits_amount' => $array['number_of_credits'],
                'credits_available' => $array['number_of_credits'],
                'purchase_date' => date('Y-m-d G:i:s')
            );

            $this->db->insert("newsletter_credits_purchase_history", $credits_data);

            $credits_data = array(
                'user_id' => $user_id,
                'credits_available' => $array['number_of_credits']
            );
            return $this->db->insert("user_has_newsletter_credits", $credits_data);
        }    
    }
    
    function get_credits_purchase_history($user_id)
    {
        $this->db->order_by("purchase_date", "desc"); 
        return $this->db->get_where("newsletter_credits_purchase_history", array("user_id" => $user_id))->result_array();
    }
    
    function get_credits_consumed_history($user_id)
    {
        $this->db->order_by("consumed_date", "desc"); 
        return $this->db->get_where("newsletter_credits_consumed_history", array("user_id" => $user_id))->result_array();
    }
       
    function get_all_credits_log_by_customers($reseller_user_id)
    {
        $reseller_id = $this->db->get_where("resellers", array("user_id" => $reseller_user_id))->row_array();        
        $customer_user_id = $this->db->query("SELECT user_id FROM customers WHERE reseller_id = ".$reseller_id['id'])->result_array();
        
        if(!empty($customer_user_id))
        {
            $customer_user_ids = "";
        
            foreach($customer_user_id as $customer)
            {
                $customer_user_ids .= $customer['user_id'].",";
            }
            $customer_user_ids = rtrim($customer_user_ids,",");
           
            $tempArray = array();

            $tempArray = $this->db->query("SELECT * FROM (
                                            SELECT cp.user_id, cp.credits_amount, cp.purchase_date AS transaction_date, cp.credits_available, 1 AS type, u.username, u.firstname, u.lastname FROM credits_purchase_history cp LEFT JOIN users u ON u.id = cp.user_id WHERE cp.user_id IN(".$customer_user_ids.")
                                            UNION
                                            SELECT cc.user_id, cc.credits_amount, cc.consumed_date AS transaction_date, cc.credits_available, 2 AS type, u.username, u.firstname, u.lastname FROM credits_consumed_history cc LEFT JOIN users u ON u.id = cc.user_id WHERE cc.user_id IN(".$customer_user_ids."))a ORDER BY transaction_date desc")->result_array();
        }    
        return $tempArray;
    }
    
    function free_credits_per_month()
    {
        $customer_credits = $this->db->query("SELECT * FROM user_has_newsletter_credits")->row_array();

        if( isset($customer_credits['id']) )
        {
            $master_admin = $this->db->get_where('users', array('user_type' => 'master admin'))->row_array();

            if($customer_credits['last_updated_on'] == "0000-00-00 00:00:00")
            {
                $credits['free_credits_available'] = $customer_credits['free_credits_available'] + NEWSLETTER_FREE_CREDITS;
                $credits['last_updated_on'] = date('Y-m-d G:i:s', time());
            
                $this->db->update("user_has_newsletter_credits", $credits, array('id' => $customer_credits['id']));
            
                $credits_data = array(
                    'user_id' => $master_admin['id'],
                    'credits_amount' => NEWSLETTER_FREE_CREDITS,
                    'credits_available' => $credits['free_credits_available'],
                    'purchase_date' => date('Y-m-d G:i:s'),
                    'is_free_credits' => 'yes'
                );

                $this->db->insert("newsletter_credits_purchase_history", $credits_data);
            
                echo "Free " . NEWSLETTER_FREE_CREDITS . " credits updated for first time.";                    
                echo "<br/>";
            }
            else
            {
                if(date("Y-m-d") >= date("Y-m-d", strtotime('+1 month', strtotime($customer_credits['last_updated_on']))))
                {
                    $credits['free_credits_available'] = $customer_credits['free_credits_available'] + NEWSLETTER_FREE_CREDITS;
                    $credits['last_updated_on'] = date('Y-m-d G:i:s', time());

                    $this->db->update("user_has_newsletter_credits", $credits, array('id' => $customer_credits['id']));
                    
                    $credits_data = array(
                        'user_id' => $master_admin['id'],
                        'credits_amount' => NEWSLETTER_FREE_CREDITS,
                        'credits_available' => $credits['free_credits_available'],
                        'purchase_date' => date('Y-m-d G:i:s'),
                        'is_free_credits' => 'yes'
                    );

                    $this->db->insert("newsletter_credits_purchase_history", $credits_data);
                    
                    echo "Free " . NEWSLETTER_FREE_CREDITS . " credits updated.";
                    echo "<br/>";
                }
                else
                {
                    echo "No credits updated.";
                    echo "<br/>";
                }
            }
        }
    }

    function get_credits_purchased_history_by_user_id( $user_id = 0, $params = array() )
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(newsletter_credits_purchase_history.id) as count ";
            $group_by = "";
        }
        else
        {
            $select_statement = " newsletter_credits_purchase_history.*,
                                  CONCAT( users.first_name, ' ', users.last_name ) as username,
                                  users.user_type ";
            $group_by = " GROUP BY newsletter_credits_purchase_history.id ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }

        /*WHERE newsletter_credits_purchase_history.user_id = '" .  $user_id . "'*/

        $sql = "SELECT " . $select_statement . "
                FROM newsletter_credits_purchase_history
                JOIN users ON newsletter_credits_purchase_history.user_id = users.id "
                . $group_by . $limit;
        $credits_purchase = $this->db->query($sql)->result_array();

        if( ! empty($credits_purchase) )
        {
            $response['rc'] = TRUE;
            $response['msg'] = '';
            $response['data'] = $credits_purchase;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('no_data_found');
            $response['data'] = array();
        }

        return $response;
    }

    function get_credits_consumed_history_by_user_id($user_id = 0, $params = array())
    {
        if( isset($params['count_flag']) && $params['count_flag'] )
        {
            $select_statement = " count(newsletter_credits_consumed_history.id) as count ";
            $group_by = "";
        }
        else
        {
            $select_statement = " newsletter_credits_consumed_history.*,
                                  CONCAT( users.first_name, ' ', users.last_name ) as username ";
            $group_by = " GROUP BY newsletter_credits_consumed_history.id ";
        }

        $limit = "";

        if(isset($params['limit_start']) && isset($params['limit']))
        {   
            $limit = " LIMIT ".$params['limit_start'].", ".$params['limit'];
        }
        /*WHERE newsletter_credits_consumed_history.user_id = '" .  $user_id . "' */
        $sql = "SELECT " . $select_statement . "
                FROM newsletter_credits_consumed_history
                JOIN users ON newsletter_credits_consumed_history.user_id = users.id
                
                " . $group_by . $limit;
        $credits_consumed = $this->db->query($sql)->result_array();

        if( ! empty($credits_consumed) )
        {
            $response['rc'] = TRUE;
            $response['msg'] = '';
            $response['data'] = $credits_consumed;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('no_data_found');
            $response['data'] = array();
        }

        return $response;
    }

    function update_newsletter_credits_consumed($credits_consumed, $credits_update)
    {
        $credits = $this->get_all_credits();

        if($credits['rc'])
        {
            $this->db->insert('newsletter_credits_consumed_history', $credits_consumed);
            $this->db->update('user_has_newsletter_credits', $credits_update, array('id' => $credits['data']['id']));
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}

?>
