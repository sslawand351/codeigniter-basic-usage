<?php
class Article_model extends CI_model
{
    /*
        function to add articles
    */
    function add( $blog_data, $images )
    {
        $result = $this->db->insert('articles',$blog_data);
        $blog_id = $this->db->insert_id();

        if($result)
        {
            if( isset($images['image']['name']) && $images['image']['name'] != "" )
            {
                $dir = FCPATH . "uploads/img/articles/" . $blog_id . "/";

                if(!is_dir($dir))
                {
                    @mkdir($dir, 0777,true);
                }

                $image = $images['image']['tmp_name'];

                $hash = md5(microtime().rand()).".jpg";

                if(move_uploaded_file($image, $dir.$hash))
                {
                    copy($dir.$hash,$dir.$images['image']['name']);
                   
                    //Create Image thumbnail
                    createThumbnail($dir.$hash,$dir.'large_'.$hash,IMAGE_HEIGHT_LARGE,IMAGE_WIDTH_LARGE);
                    createThumbnail($dir.$hash,$dir.'medium_'.$hash,IMAGE_HEIGHT_MEDIUM,IMAGE_WIDTH_MEDIUM);
                    createThumbnail($dir.$hash,$dir.'small_'.$hash,IMAGE_HEIGHT_SMALL,IMAGE_WIDTH_SMALL);
                    
                    $image_details = array(
                        "image_name" => $images['image']['name'],
                        "image_hash_name" => $hash
                    );
                    $this->db->where('id', $blog_id);
                    $this->db->update('articles', $image_details);
                }
            }

            if(isset($images['thumb']['name']) && $images['thumb']['name'] != "")
            {
                $dir = FCPATH . "uploads/img/articles/" . $blog_id . "/";

                 if(!is_dir($dir))
                {
                    @mkdir($dir, 0777,true);
                }

                $thumb = $images['thumb']['tmp_name'];

                $hash = md5(microtime().rand()).".jpg";

                if(move_uploaded_file($thumb, $dir.$hash))
                {
                    copy($dir.$hash,$dir.$images['thumb']['name']);
                   
                    //Create Image thumbnail
                    createThumbnail($dir.$hash,$dir.'thumb_large_'.$hash,IMAGE_HEIGHT_LARGE,IMAGE_WIDTH_LARGE);
                    createThumbnail($dir.$hash,$dir.'thumb_medium_'.$hash,IMAGE_HEIGHT_MEDIUM,IMAGE_WIDTH_MEDIUM);
                    createThumbnail($dir.$hash,$dir.'thumb_small_'.$hash,IMAGE_HEIGHT_SMALL,IMAGE_WIDTH_SMALL);
                    $thumb_details = array(
                        "thumb_image_name" => $images['thumb']['name'],
                        "thumb_hash_name" => $hash
                    );
                    $this->db->where('id', $blog_id);
                    $this->db->update('articles', $thumb_details);
                }
            }
            $other_data['blog_name'] = $blog_data['title'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_ADD_BLOG,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);

            $response['id'] = $blog_id;
            $response["rc"] = TRUE;
            $response["msg"] = lang('article_successfully_added');
        }
        else
        {
            $response["rc"] = FALSE;
            $response["msg"] = lang('error_adding_article');
        }

        return $response;
    }
        
    function add_articles_by_language($blog_data)
    {
        $result = $this->db->insert_batch('articles_by_language', $blog_data);

        if( $result )
        {
            $response["rc"] = TRUE;
            $response["msg"] = lang('article_successfully_added');
        }
        else
        {
            $response["rc"] = FALSE;
            $response["msg"] = lang('error_adding_article');
        }

        return $response;
    }

    function get_all_blogs($limit = "", $limit_start="", $language_id='')
    {

        if(is_numeric($limit) && $limit != "" && is_numeric($limit_start) && ($limit_start != "" || $limit_start == 0))
        {
            $this->db->limit($limit,$limit_start);
        }
        $lang_condition='';

        if(!empty($language_id))
        {
             $lang_condition = "WHERE  al.language_id = '".$language_id."'";
        }
       
        $query = "SELECT
                    *, a.id as a_id 
                FROM
                    articles  a
            INNER JOIN
                    articles_by_language al
                ON
                    a.id = al.article_id
                    ".$lang_condition."
                AND
                        al.title!=''
                GROUP BY
                        al.article_id
                Order by 
                        a.id
                 DESC";

        $result = $this->db->query($query)->result_array();
        if(!empty($result))
        {
            $response['rc'] = TRUE;
            $response['data'] = $result;
            $response['msg'] = "articles found";
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = "No articles found";
        }
        return $response;
    }

    function check_url_occurrence($title)
    {
        $url = $title;

        if(!empty($url))
        {
            $query = "SELECT 
                            *
                    FROM
                            articles_by_language
                    WHERE
                            title = '".$url."'
                    ";
        $result = $this->db->query($query)->row_array();

        if(!empty($result))
        {
            if($result['url_occurrence']=="0")
            {
                $array["url"] = $url."-1"; 
                $array["url_occurrence"] = 1; 
            }
            else
            {
                $occurence = $result['url_occurrence'] + 1;
                $array["url"] = $url."-".$occurence; 
                $array["url_occurrence"] = $occurence; 
            }
        }
        else
        {
            $array["url"]               = $url;
            $array["url_occurrence"]    = "";
        }

        return $array;
        }
    }

    function get_article_by_url($url)
    {
        $query = "SELECT 
                        a.*,al.*
                  FROM
                        articles a
                  INNER JOIN 
                        articles_by_language al
                  WHERE
                        al.url = '".$url."'
                  AND
                        a.id = al.article_id";
        
        $response['data'] = $this->db->query($query)->row_array();

        if( !empty($response['data']) )
        {
            $response['rc'] = TRUE;
            $response['msg'] = 'Data found';
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = 'Data not found';
        }
    
        return $response;
    
    }
    function get_article_by_id($id)
    {
        $query = "SELECT 
                        *
                  FROM
                        articles
                  WHERE
                        id = '".$id."'
                 ";
        
        $response['data'] = $this->db->query($query)->row_array();

        if( !empty($response['data']) )
        {
            $response['rc'] = TRUE;
            $response['msg'] = 'Data found';
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = 'Data not found';
        }
    
        return $response;
    
    }
    function get_recent_articles()
    {
        $this->db->order_by("id","DESC");
        $result = $this->db->get("articles",3,0)->result_array();
        if(!empty($result))
        {
            $response['rc'] = TRUE;
            $response['data'] = $result;
            $response['msg'] = 'Recent 3 articles';
        }
        else
        {
            $response['rc'] = FALSE;
            $response['data'] = '';
            $response['msg'] = 'No articles found';
        }
        return $response;
    }
    
    function edit($id)
    {
        $query = "SELECT
                        *
                    FROM
                            articles
                    WHERE
                            id=$id
                ";
        $result = $this->db->query($query)->row_array();

        if(!empty($result))
        {
            $response['rc'] = TRUE;
            $response['data'] = $result;
            $response['msg'] = "Data found";
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = "Data not found";
        }
        return $response;
    }

    function update($id, $params, $param1, $images)
    {
      // pr($param1);pr($params);pr($id);exit;
        
        if($this->db->update('articles', $params, array('id' => $id)))
        {
            $this->db->where('article_id', $id);
            $this->db->update_batch('articles_by_language', $param1, 'language_id'); 

            if( isset($images['image']['name']) && $images['image']['name'] != "" )
            {
                $dir = FCPATH . "uploads/img/articles/" . $id . "/";

                if(!is_dir($dir))
                {
                    @mkdir($dir, 0777,true);
                }

                $image = $images['image']['tmp_name'];

                $hash = md5(microtime().rand()).".jpg";

                if(move_uploaded_file($image, $dir.$hash))
                {
                    copy($dir.$hash,$dir.$images['image']['name']);
                   
                    //Create Image thumbnail
                    createThumbnail($dir.$hash,$dir.'large_'.$hash,IMAGE_HEIGHT_LARGE,IMAGE_WIDTH_LARGE);
                    createThumbnail($dir.$hash,$dir.'medium_'.$hash,IMAGE_HEIGHT_MEDIUM,IMAGE_WIDTH_MEDIUM);
                    createThumbnail($dir.$hash,$dir.'small_'.$hash,IMAGE_HEIGHT_SMALL,IMAGE_WIDTH_SMALL);
                    
                    $image_details = array(
                        "image_name" => $images['image']['name'],
                        "image_hash_name" => $hash
                    );
                    $this->db->where('id', $id);
                    $this->db->update('articles', $image_details);
                }
            }
            if( isset($images['thumb']['name']) && $images['thumb']['name'] != "" )
            {
                $dir = FCPATH . "uploads/img/articles/" . $id . "/";

                if(!is_dir($dir))
                {
                    @mkdir($dir, 0777,true);
                }

                $thumb = $images['thumb']['tmp_name'];

                $hash = md5(microtime().rand()).".jpg";

                if(move_uploaded_file($thumb, $dir.$hash))
                {
                    copy($dir.$hash,$dir.$images['thumb']['name']);
                   
                    //Create Image thumbnail
                    createThumbnail($dir.$hash,$dir.'thumb_large_'.$hash,IMAGE_HEIGHT_LARGE,IMAGE_WIDTH_LARGE);
                    createThumbnail($dir.$hash,$dir.'thumb_medium_'.$hash,IMAGE_HEIGHT_MEDIUM,IMAGE_WIDTH_MEDIUM);
                    createThumbnail($dir.$hash,$dir.'thumb_small_'.$hash,IMAGE_HEIGHT_SMALL,IMAGE_WIDTH_SMALL);
                    
                    $thumb_details = array(
                        "thumb_image_name" => $images['thumb']['name'],
                        "thumb_hash_name" => $hash
                    );
                    $this->db->where('id', $id);
                    $this->db->update('articles', $thumb_details);
                }
            }

            $other_data['blog_name'] = $params['title'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_UPDATE_BLOG,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);
            $response["rc"] = TRUE;
            $response["msg"] = lang('article_successfully_updated');
        }
        else
        {
            $response["rc"] = FALSE;
            $response["msg"] = lang('error_updating_article');
        }

        return $response;
    }

    function delete($id, $blog_details=array())
    {
        $this->db->where('article_id', $id);
        $this->db->delete('articles_by_language');

        $this->delete_image( $id );

        if( $this->db->delete('articles', array('id' => $id)) )
        {   
            $other_data['blog_name'] = $blog_details['data']['title'];
            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_DELETE_BLOG,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);

            $response["rc"] = TRUE;
            $response["msg"] = lang('article_successfully_deleted');
        }
        else
        {
            $response["rc"] = FALSE;
            $response["msg"] = lang('error_deleting_article');
        }
        
        return $response;
    }

    function delete_image( $id )
    {
        $blog = $this->db->get_where('articles', array('id' => $id) )->row_array();

        if( ! empty($blog) )
        {
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/".$blog['image_name'];
            $this->delete_folder($delete_folder_path);
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/".$blog['image_hash_name'];
            $this->delete_folder($delete_folder_path);
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/small_".$blog['image_hash_name'];
            $this->delete_folder($delete_folder_path);
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/medium_".$blog['image_hash_name'];
            $this->delete_folder($delete_folder_path);
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/large_".$blog['image_hash_name'];
            $this->delete_folder($delete_folder_path);

            $blog_data = array(
                'image_name' => '',
                'image_hash_name' => ''
            );

            $this->db->where('id', $id);
            $this->db->update('articles', $blog_data );

            //pr($response); exit;
            $response['rc'] = TRUE;
            $response['msg'] = lang('image_deleted_successfully');
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('invalid_id');
        }

        return $response;
    }

    function delete_thumbnail( $id )
    {
        $blog = $this->db->get_where('articles', array('id' => $id) )->row_array();

        if( ! empty($blog) )
        {
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/".$blog['thumb_image_name'];
            $this->delete_folder($delete_folder_path);
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/".$blog['thumb_hash_name'];
            $this->delete_folder($delete_folder_path);
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/thumb_small_".$blog['thumb_hash_name'];
            $this->delete_folder($delete_folder_path);
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/thumb_medium_".$blog['thumb_hash_name'];
            $this->delete_folder($delete_folder_path);
            $delete_folder_path = FCPATH . "uploads/img/articles/".$id."/thumb_large_".$blog['thumb_hash_name'];
            $this->delete_folder($delete_folder_path);

            $blog_data = array(
                'thumb_image_name' => '',
                'thumb_hash_name' => ''
            );

            $this->db->where('id', $id);
            $this->db->update('articles', $blog_data );

            $response['rc'] = TRUE;
            $response['msg'] = lang('thumbnail_deleted_successfully');
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('invalid_id');
        }

        return $response;
    }

    function delete_folder($path)
    {
        if (is_dir($path) === true)
        {
            $files = array_diff(scandir($path), array('.', '..'));

            foreach ($files as $file)
            {
                $this->delete_folder(realpath($path) . '/' . $file);
            }

            return rmdir($path);
        }

        else if (is_file($path) === true)
        {
            return unlink($path);
        }

        return false;
    }   

    function get_all_articles($table,$limit_start,$limit_end)
    {
        // $this->db->order_by("added_on DESC");
        //$response['data'] = $this->db->get($table,$limit_end,$limit_start)->result_array();
        $response['data'] = $this->db->query(" SELECT 
                                                    articles.* 
                                                FROM 
                                                    articles
                                                
                                                ORDER BY sort_id ASC
                                                LIMIT ".$limit_start.", ".$limit_end."
                                            ")->result_array();

         if( !empty($response['data']) )
         {
            $response['rc'] = TRUE;
        }
         else
         {
            $response['rc'] = FALSE;
    
            $response['msg'] = '';
        }
    
            return $response;
    }

    function get_blog($id, $language_id = "")
    {
        $lang_condition = "";

        if( $language_id != "" )
        {
            $lang_condition = "AND al.language_id = '" . $language_id . "' ";
        }

        $query = "SELECT
                        *, a.id as blog_id
                    FROM
                        articles a
                INNER JOIN
                        articles_by_language al
                    ON
                        a.id = al.article_id
                WHERE 
                    a.id = '".$id."'
                    ".$lang_condition;

        if($language_id!="")
            $data = $this->db->query($query)->row_array();

        else
            $data = $this->db->query($query)->result_array();
        

        if(!empty($data ))
        {
            if($language_id == "")
            {
                foreach ($data as $key=>$value) {
                # code...
                    $this->db->select('language');
                    $language_name = $this->db->get_where("meta_front_end_languages",array('id' => $value['language_id']))->row_array();
                    if(!empty($language_name))
                        $data[$key]['language_name'] = $language_name['language'];
                    else
                        $data[$key]['language_name'] = "";
                }
            }


            $response["rc"] = TRUE;
            $response["data"] = $data;
            $response["msg"] = "";
        }
        else
        {
            $response["rc"] = FALSE;
            $response["data"] = array();
            $response["msg"] = lang('invalid_data');
        }
        
        return $response;
    }

    function get_total_articles()
    {       
        return $this->db->get("articles")->num_rows();
    }

    function get_total_categories()
    {       
        return $this->db->get("categories")->num_rows();
    }

    function check_unique_url($url,$id)
    {
        $query = "SELECT
                        url
                    FROM
                        articles_by_language
                    WHERE
                        url = ?
                    AND
                        id = ?";
        $result = $this->db->query($query,array($url,$id))->row_array();
                                
        return $result;

    }

    function get_languages()
    {
        $query = "SELECT 
                        *
                    FROM
                        meta_front_end_languages
                ";
        $result = $this->db->query($query)->result_array();

        if(!empty($result))
        {
            $response['rc'] = TRUE;
            $response['msg'] = lang('languages_found');
            $response['data'] = $result;
        }
        else
        {
            $response['rc'] = FALSE;
            $response['msg'] = lang('no_languages_found');
        }

        return $response;
    }
}
?>