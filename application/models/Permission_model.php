<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Permission_model extends CI_Model
{
	function get_all()
	{
		$this->db->select('*');
		$this->db->from('available_permissions');
		$result = $this->db->get()->result_array();

		if(count($result) > 0)
		{
			$response['rc'] 	= TRUE;
			$response['data'] 	= $result;
		}
		else
		{
			$response['rc'] 	= FALSE;
			$response['msg']	= lang('no_permissions_found');
		}

		return $response;
	}

	function add_extra_permission_by_user_id($role_id,$permission_id,$user_id,$added_by)
	{

		// First check if the extra permission exists
		$this->db->select('id');
		$this->db->from('user_has_extra_permissions');
		$this->db->where('user_id',$user_id);
		$this->db->where('permission_id',$permission_id);
		$this->db->where('role_id',$role_id);

		$result = $this->db->get()->row_array();

		if(isset($result['id']))
		{
			// if  record exists update the record
			$permission_data = array(
				'user_id' 		=> $user_id,
				'permission_id' => $permission_id,
				'role_id'		=> $role_id,
				'status'		=> 'added',
				'added_on' 		=> time(),
				'added_by' 		=> $added_by
			);

			$this->db->where('id', $result['id']);

			if($this->db->update('user_has_extra_permissions', $permission_data))
			{
				$response['rc'] 	= TRUE;
				$response['data'] 	= '';
				$response['msg']	= lang('extra_permissions_added_successfully');
			}
			else
			{
				$response['rc'] 	= FALSE;
				$response['data'] 	= '';
				$response['msg']	= lang('there_was_some_error');
			}
		}
		else
		{
			// if no record exists add a new record
			$permission_data = array(
				'user_id' 		=> $user_id,
				'permission_id' => $permission_id,
				'role_id'		=> $role_id,
				'status'		=> 'added',
				'added_on' 		=> time(),
				'added_by' 		=> $added_by
			);

			if($this->db->insert('user_has_extra_permissions',$permission_data))
			{
				$response['rc'] 	= TRUE;
				$response['data'] 	= '';
				$response['msg']	= lang('extra_permissions_added_successfully');
			}
			else
			{
				$response['rc'] 	= FALSE;
				$response['data'] 	= '';
				$response['msg']	= lang('there_was_some_error');
			}

		}

		return $response;
	}

	function remove_extra_permission_by_user_id($role_id,$permission_id,$user_id,$added_by)
	{
		// First check if the extra permission exists
		$this->db->select('id');
		$this->db->from('user_has_extra_permissions');
		$this->db->where('user_id',$user_id);
		$this->db->where('permission_id',$permission_id);
		$this->db->where('role_id',$role_id);

		$result = $this->db->get()->row_array();

		if(isset($result['id']))
		{
			$permission_data = array(
				'user_id' 		=> $user_id,
				'permission_id' => $permission_id,
				'role_id'		=> $role_id,
				'status'		=> 'removed',
				'added_on' 		=> time(),
				'added_by' 		=> $added_by
			);

			$this->db->where('id', $result['id']);

			if($this->db->update('user_has_extra_permissions', $permission_data))
			{
				$response['rc'] 	= TRUE;
				$response['data'] 	= '';
				$response['msg']	= lang('extra_permissions_removed_successfully');
			}
			else
			{
				$response['rc'] 	= FALSE;
				$response['data'] 	= '';
				$response['msg']	= lang('there_was_some_error');
			}
		}
		else
		{
			// if no record exists add a new record
			$permission_data = array(
				'user_id' 		=> $user_id,
				'permission_id' => $permission_id,
				'role_id'		=> $role_id,
				'status'		=> 'removed',
				'added_on' 		=> time(),
				'added_by' 		=> $added_by
			);

			if($this->db->insert('user_has_extra_permissions',$permission_data))
			{

				

				$response['rc'] 	= TRUE;
				$response['data'] 	= '';
				$response['msg']	= lang('extra_permissions_removed_successfully');
			}
			else
			{
				$response['rc'] 	= FALSE;
				$response['data'] 	= '';
				$response['msg']	= lang('there_was_some_error');
			}
		}

		// Now check if the permission is in any other roles remove from that also
		$other_roles = $this->get_role_perimssions_by_user_id_permission_id($user_id,$permission_id);

		// If so remove the permissions from that user role
		if($other_roles['rc'])
		{
			foreach ($other_roles['data'] as $key => $value) {
				if($value['permission_id'] != $permission_id || $value['role_id'] != $role_id)
				{
					$permission_data = array(
						'user_id' 		=> $user_id,
						'permission_id' => $value['permission_id'],
						'role_id'		=> $value['role_id'],
						'status'		=> 'removed',
						'added_on' 		=> time(),
						'added_by' 		=> $added_by
					);

					$this->db->insert('user_has_extra_permissions',$permission_data);
				}

			}
		}
		

		return $response;
	}

	function get_extra_permission_by_user_id($user_id)
	{
		$this->db->select('*');
		$this->db->from('user_has_extra_permissions');
		$this->db->where('user_id',$user_id);

		$result = $this->db->get()->result_array();

		if(count($result) > 0)
		{
			$response['rc'] 	= TRUE;
			$response['data']	= $result;
			$response['msg']	= '';
		}
		else
		{
			$response['rc'] 	= FALSE;
			$response['data']	= '';
			$response['msg']	= '';
		}

		return $response;
	}

	function get_extra_permission_by_role_id($role_id, $user_id)
	{
		$this->db->select('*');
		$this->db->from('user_has_extra_permissions');
		$this->db->where('user_id',$user_id);
		$this->db->where('role_id',$role_id);
		
		$result = $this->db->get()->result_array();

		if(count($result) > 0)
		{
			$response['rc'] 	= TRUE;
			$response['data']	= $result;
			$response['msg']	= '';
		}
		else
		{
			$response['rc'] 	= FALSE;
			$response['data']	= '';
			$response['msg']	= '';
		}

		return $response;
	}

	function check_permission_by_slug($slug = '')
	{
		if($slug!='')
		{
			$count = 0;

			$user_id = $this->user['id'];




			// Get the permission id from slug
			$this->db->select('id');
			$this->db->from('available_permissions');
			$this->db->where('permission_code',$slug);

			$permission = $this->db->get()->row_array();

			if(isset($permission['id']))
			{
				// check if for role
				$this->db->select('user_id,permission_id,usergroup_has_permissions.usergroup_id');
				$this->db->from('usergroup_has_permissions');
				$this->db->join('user_has_roles','user_has_roles.role_id = usergroup_has_permissions.usergroup_id','inner');
				$this->db->where('usergroup_has_permissions.permission_id',$permission['id']);
				$this->db->where('user_has_roles.user_id',$user_id);

				$result = $this->db->get()->result_array();
				foreach ($result as $key => $value) {

					$data_array = array(
						'user_id' => $value['user_id'],
						'permission_id' => $value['permission_id'],
						'role_id' => $value['role_id'],
						'status' => 'added'
					);

					$check_extra_permission = $this->db->get_where('user_has_extra_permissions',$data_array)->row_array();

					if(isset($check_extra_permission['id']))
					{
						$count = $count + 1;
					}
				}
				// Now check in extra permissions
				$this->db->select('*');
				$this->db->from('user_has_extra_permissions');
				$this->db->where('status','added');
				$this->db->where('permission_id',$permission['id']);
				$this->db->where('user_id',$user_id);


				$count = $count+$this->db->count_all_results();

				if($count > 0)
				{
					$response['rc'] 	= TRUE;
					$response['data'] 	= '';
					$response['msg'] 	= lang('permission_granted');
				}
				else
				{
					$response['rc'] 	= FALSE;
					$response['data'] 	= '';
					$response['msg'] 	= lang('access_denied');
				}

			}
			else
			{
				$response['rc'] 	= FALSE;
				$response['data'] 	= '';
				$response['msg'] 	= lang('no_such_permission');
			}
		}
		else
		{
			$response['rc'] = FALSE;
			$response['data'] = '';
			$response['msg'] = lang('invalid_permission');
		}

		return $response;
	}


    /**
     * @author Prathamesh Mhatre <prathameshm@codebox.in>
     * @desc   Delete the extra permission from the system
     * @param  $role_id 		int 		id of the role
     * @param  $permission_id 	int 		id of the permission
     * @param  $user_id 		int 		if of the logged in user	
     * @return Boolean
     */
	function delete_extra_permission($role_id,$permission_id,$user_id)
	{
		 
		 if(is_array($permission_id))
		 {	
		 	// Get all permission according to role id
			$permissions = $this->get_permissions_by_role_id($role_id);

			foreach ($permission_id as $key => $value) {

				
				$this->db->where('permission_id', $value);
				$this->db->where('user_id', $user_id);
				$this->db->delete('user_has_extra_permissions');

				// Now check if any other role with the same permission exists for the user
				$other_roles = $this->get_role_perimssions_by_user_id_permission_id($user_id,$value);


				// If so remove the permissions from that user role
				if($other_roles['rc'])
				{
					foreach ($other_roles['data'] as $key => $value) {
						if($value['permission_id'] != $value || $value['role_id'] != $role_id)
						{
							$permission_data = array(
								'user_id' 		=> $user_id,
								'permission_id' => $value['permission_id'],
								'role_id'		=> $value['role_id'],
							);

							$this->db->delete('user_has_extra_permissions',$permission_data);
						}

					}
				}
			}

			if($permissions['rc'])
			{
				$assigned_permissions = array();

				foreach ($permissions['data'] as $key => $value) {
					$assigned_permissions[] = $value['permission_id'];
				}

				$remaining_permissions = array_merge(array_diff($permission_id, $assigned_permissions), array_diff($assigned_permissions, $permission_id));
				
				// Check if there are any record for this user
				

				foreach ($remaining_permissions as $key => $value) {
					$check_record = $this->db->get_where('user_has_extra_permissions',array('user_id' => $user_id,'permission_id' => $value))->row_array();
					if(isset($check_record['id']))
					{
						$check_permission = $this->check_permission_assigned_to_other_roles_removed($role_id,$user_id,$value);
					
						if($check_permission['rc'])
						{
							$permission_data = array(
								'user_id' 		=> $user_id,
								'permission_id' => $value,
								'role_id'		=> $role_id,
								'status'		=> 'removed',
								'added_on' 		=> time(),
							);

							$this->db->insert('user_has_extra_permissions',$permission_data);
						}
					}
					else
					{
						$permission_data = array(
								'user_id' 		=> $user_id,
								'permission_id' => $value,
								'role_id'		=> $role_id,
								'status'		=> 'removed',
								'added_on' 		=> time(),
							);

						$this->db->insert('user_has_extra_permissions',$permission_data);
					}
					
				}
			}


			
			$response['rc'] 	= TRUE;
			$response['data'] 	= '';
			$response['msg']	= lang('permission_delete_successfully');

		 }
		 else
		 {
		 	$permission_data = array(
				'user_id' 		=> $user_id,
				'permission_id' => $permission_id,
				'role_id'		=> $role_id,
			);

			if($this->db->delete('user_has_extra_permissions',$permission_data))
			{
				$response['rc'] 	= TRUE;
				$response['data'] 	= '';
				$response['msg']	= lang('permission_delete_successfully');
			}
			else
			{
				$response['rc'] 	= FALSE;
				$response['data'] 	= '';
				$response['msg']	= lang('permission_delete_successfully');
			}
		 }

		return $response;
	}

	 /**
     * @author Prathamesh Mhatre <prathameshm@codebox.in>
     * @desc   Get permisson assigned to user
     * @param  $user_id 		int 		if of the logged in user	
     * @return Array
     */
    function get_permissions_by_user_id($user_id)
    {
    	$sorted_permission_array = array();
    	$this->db->select('available_permissions.id,available_permissions.permission_code');
    	$this->db->from('available_permissions');
    	$this->db->join('usergroup_has_permissions','usergroup_has_permissions.permission_id = available_permissions.id','inner');
    	$this->db->join('user_has_roles','user_has_roles.role_id = usergroup_has_permissions.usergroup_id','inner');
    	$this->db->where('user_has_roles.user_id',$user_id);

    	$permissions = $this->db->get()->result_array();

    	if(count($permissions) > 0)
    	{
    		// Make the permission as the index of the array
    		foreach ($permissions as $key => $value) {
    			$sorted_permission_array[$value['id']] = $value;
    		}

    		// Now check if any of the existing permssion is removed or added 
    		$this->db->select('user_has_extra_permissions.status,available_permissions.*,user_has_extra_permissions.role_id');
    		$this->db->from('available_permissions');
    		$this->db->join('user_has_extra_permissions','user_has_extra_permissions.permission_id = available_permissions.id','inner');
    		$this->db->where('user_has_extra_permissions.user_id',$user_id);
    		$extra_permissions = $this->db->get()->result_array();


    		if(count($extra_permissions) > 0)
    		{
    			foreach ($extra_permissions as $key => $value) {

    				if($value['status'] == 'removed')
    				{
    					unset($sorted_permission_array[$value['id']]);
    				}
    				else
    				{
    					
    					$sorted_permission_array[$value['id']] = $value;
    				}

    			}
    		}

    		if(count($sorted_permission_array) > 0)
    		{
    			$response['rc'] 	= TRUE;
    			$response['msg'] 	= '';
    			$response['data'] 	= $sorted_permission_array;
    		}
    		else
    		{
    			$response['rc'] 	= FALSE;
    			$response['msg'] 	= lang('no_permission_found');
    			$response['data'] 	= '';
    		}
    		
    	}
    	else
    	{

    		// Now check if any of the existing permssion is removed or added 
    		$this->db->select('user_has_extra_permissions.status,available_permissions.*');
    		$this->db->from('user_has_extra_permissions');
    		$this->db->join('available_permissions','user_has_extra_permissions.permission_id = available_permissions.id','inner');
    		$this->db->where('user_has_extra_permissions.user_id',$user_id);
    		$extra_permissions = $this->db->get()->result_array();
    		
    		if(count($extra_permissions) > 0)
    		{
    			foreach ($extra_permissions as $key => $value) {

    				if($value['status'] == 'removed')
    				{
    					unset($sorted_permission_array[$value['id']]);
    				}
    				else
    				{
    					
    					$sorted_permission_array[$value['id']] = $value;
    				}

    			}
    		}

    		if(count($sorted_permission_array) > 0)
    		{
    			$response['rc'] 	= TRUE;
    			$response['msg'] 	= '';
    			$response['data'] 	= $sorted_permission_array;
    		}
    		else
    		{
    			$response['rc'] 	= FALSE;
    			$response['msg'] 	= lang('no_permission_found');
    			$response['data'] 	= '';
    		}
    	}

    	return $response;
    }

    function get_role_perimssions_by_user_id_permission_id($user_id,$permission_id)
    {
    	$this->db->select('user_has_roles.user_id,user_has_roles.role_id,usergroup_has_permissions.permission_id');
    	$this->db->from('user_has_roles');
    	$this->db->join('usergroup_has_permissions','usergroup_has_permissions.usergroup_id = user_has_roles.role_id','inner');
    	$this->db->where('usergroup_has_permissions.permission_id',$permission_id);
    	$this->db->where('user_has_roles.user_id',$user_id);
    	$this->db->group_by('user_has_roles.role_id');

    	$result = $this->db->get()->result_array();
    	if(count($result) > 0)
    	{
    		$response['rc'] = TRUE;
    		$response['msg'] = '';
    		$response['data'] = $result;
    	}
    	else
    	{
    		$response['rc'] = FALSE;
    		$response['msg'] = '';
    		$response['data'] = '';
    	}

    	return $response;

    }

    function get_permissions_by_role_id($role_id)
    {
    	$this->db->select('usergroup_has_permissions.permission_id');
    	$this->db->from('usergroup_has_permissions');
    	$this->db->where('usergroup_has_permissions.usergroup_id',$role_id);

    	$permissions = $this->db->get()->result_array();

    	if(count($permissions) > 0)
    	{
    		$response['rc'] = TRUE;
    		$response['data'] = $permissions;
    		$response['msg'] = '';
    	}
    	else
    	{
    		$response['rc'] = FALSE;
    		$response['data'] = '';
    		$response['msg'] = '';
    	}

    	return $response;
    }

	 /**
     * @author Prathamesh Mhatre <prathameshm@codebox.in>
     * @desc   Check if the permission is removed from other roles
     * @param  $user_id 		int 		id of the logged in user
     * @param  $role_id 		int 		id of the role
     * @param  $permission_id 	int 		id of the permission	
     * @return Boolean
     */
    function check_permission_assigned_to_other_roles_removed($role_id,$user_id,$permission_id)
    {
    	$users_valid_permissions = array();
    	// First get all the roles other than give
    	
    	$this->db->select('user_roles.id');
    	$this->db->from('user_roles');
    	$this->db->join('user_has_roles','user_has_roles.role_id = user_roles.id','INNER');
    	$this->db->where('user_has_roles.user_id',$user_id);
    	$this->db->where('user_has_roles.role_id !=',$role_id);

    	$roles_assigned = $this->db->get()->result_array();

    	if(count($roles_assigned) > 0)
    	{
    		// CHeck if the role and permission is removed or not
    		foreach ($roles_assigned as $key => $value) {
    			
    			$this->db->select('user_has_extra_permissions.id');
	    		$this->db->from('user_has_extra_permissions');
	    		$this->db->where('role_id',$value['id']);
	    		$this->db->where('user_id',$user_id);
	    		$this->db->where('permission_id',$permission_id);

	    		$result = $this->db->get()->row_array();

	    		if(isset($result['id']))
	    		{
	    			$users_valid_permissions[] = $result;
	    		}
    		}
    		
    	}


    	if(count($users_valid_permissions) > 0)
    	{
    		$response['rc'] = TRUE;
    		$response['msg'] = '';
    		$response['data'] = '';
    	}
    	else
    	{
    		$response['rc'] = FALSE;
    		$response['msg'] = '';
    		$response['data'] = '';
    	}
    	
    	return $response;
    }
}