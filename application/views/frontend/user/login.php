<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link href="<?php echo base_url()?>resources/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <style>
        
        .panel-body
        {  
            color:#7B7070;
        }
        .panel-default
        {
            width:400px;
            min-height:300px;
            border-top: 5px solid #7B7070;
            margin:auto;
            overflow:hidden;
            position: relative;
            margin-top:6%;
        }

        .btn-red
        {
            padding: 8px 18px;
            font-size: 13px;
            text-transform: uppercase;
        	background-color: #7B7070;
        	color: #FFFFFF;
        }
        h3
        {
            padding-bottom: 10px;
            color:#7B7070;
            font-weight: bold;
        }
        .bg-success
        {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
            font-size: 13px;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2);
        }
        .bg-danger
        {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #a94442;
            background-color:#f2dede;
            border-color: #ebccd1;
            font-size: 13px;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2);
        }
        .link
        {
          clear:both;
          margin-top: 20px;
        }
        #login-btn
        {
            z-index: 1;
        }
        .link a
        {
            color: #7B7070 !important;
        }
        .links_login{
            padding:0;
        }
        @media (max-width: 440px) {
          .panel-default {
              width: 100%;
          }
        }
        @media (max-width: 767px) {
          .bear-logo{
            width: 33%;
            position: absolute;
            opacity: 0.3;
            right: -11px;
            bottom: -28px;
          }
        }
        @media (min-width: 767px) {
          .bear-logo{
            width: 82%;
            position: absolute;
            top: -60px;
            opacity: 0.3;
            right: -11px;
          }
        }
    </style>

</head>
<body>
	<div class="listing-box-wrap">
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Login</h3>
                    <?php  if($this->session->flashdata('success')) { ?>
                    <div class="bg-success">
                        <?php echo $this->session->flashdata('success'); ?> 
                    </div>
                    <?php } ?>
                    <?php  if($this->session->flashdata('error')) { ?>
                    <div class="bg-danger">
                         <?php echo $this->session->flashdata('error'); ?> 
                    </div>
                    <?php } ?>
                    <form id="" method="post" action="<?php echo site_url('user/login')?>" enctype="multipart/form-data">
                        <div class="row"> 
                            <div class="form-group col-xs-12 <?php echo form_error("email") ? "has-error" : "" ?>">
                                <label class="control-label">Email</label>
                                <input type="text" class="form-control" name="email" value="">
                                <span class="text-danger"><?php echo form_error("email") ?></span>
                            </div>
                            <div class="form-group col-xs-12 <?php echo form_error("password") ? "has-error" : "" ?>">
                                <label class="control-label">Password</label>
                                <input type="password" name="password" class="form-control">
                                 <span class="text-danger"><?php echo form_error("password") ?></span>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="form-group col-sm-12 col-md-12 col-xs-12">
                                <button class="btn-red" type="submit" >Login</button>
                            </div>
                            <div class="form-group col-sm-12 col-md-12 col-xs-12">
                               <a class="register-social-button-facebook btn-red" href="<?php echo site_url('social/facebook'); ?>"><i class="fa fa-facebook-square"></i>login via Facebook</a>
                            </div>
                            <div class='col-md-8 link col-sm-8 col-xs-12'>
                                <div class="form-group">
                                    <span class="col-xs-12 links_login">
                                        <label class="control-label">New User? </label>
                                        <a href="<?php echo site_url("user/register");?>"> Register here</a>
                                    </span>
                                    <span class="col-xs-12 links_login">
                                        <label class="control-label">Forgot password? </label>
                                        <a href="<?php echo site_url("user/forgot_password");?>" > Click here</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
</body>
</html>