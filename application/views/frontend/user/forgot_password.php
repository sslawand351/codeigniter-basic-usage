    <style>
        .btn-red-normal
        {
            padding: 8px 18px;
            font-size: 13px;
            text-transform: uppercase;
        }
        .panel-default
        {
            width:400px;
            border-top: 5px solid #7B7070 !important;
            margin:auto;
            overflow:hidden;
            position: relative;
        }

        .btn-red
        {
            background-color: #7B7070 !important;
            color: #FFFFFF;
        }
        .panel-body
        {
            color:#7B7070 !important;
        }
        h3
        {
           color:#7B7070 !important;
            font-weight: bold;
        }
        a
        {
            color:#7B7070 !important;
        }
        .bg-success
        {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
            font-size: 13px;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2);
        }
        .bg-danger
        {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #a94442;
            background-color:#f2dede;
            border-color: #ebccd1;
            font-size: 13px;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2);
        }
        .link
        {
          clear:both;
          margin-top: 20px;
        }
        @media (max-width: 440px) {
          .panel-default {
              width: 100%;
          }
        }
        @media (max-width: 767px) {
          .bear-logo{
            width: 18%;
            position: absolute;
            opacity: 0.3;
            right: -6px;
            bottom: -21px;
          }
        }
        @media (min-width: 767px) {
          .bear-logo{
            width: 54%;
            position: absolute;
            top: -27px;
            opacity: 0.3;
            right: -23px;
          }
        }
    </style>
    <div class="listing-box-wrap">
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Forgot password</h3>
                    <?php  if($this->session->flashdata('success')) { ?>
                    <div class="bg-success">
                        <?php echo $this->session->flashdata('success'); ?> 
                    </div>
                    <?php } ?>
                    <?php  if($this->session->flashdata('error')) { ?>
                    <div class="bg-danger">
                        <?php echo $this->session->flashdata('error'); ?> 
                    </div>
                    <?php } ?>

                    <form id="" method="post" action="" enctype="multipart/form-data">
                       <div class="form-group <?php echo form_error("email") ? "has-error" : "" ?>">
                            <label class="control-label">Email</label>
                            <input class="form-control" type="text" name="email" value="<?php echo $this->input->post('email'); ?>" style="margin-bottom: 0;">
                            <span class="text-danger"><?php echo form_error("email") ?></span>
                        </div>
                    
                        <div class="row"> 
                            <div class="form-group col-sm-8 col-md-8">
                                <button class="btn-red btn-red-normal" type="submit" >SUBMIT</button>
                            </div>
                            
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>