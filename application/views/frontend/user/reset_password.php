<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
  <tr><td height="10"></td></tr>
  <tr>
    <td style="width:1%"></td>
    <td>
      <p>HII,
        <br><br>
        <span>
         Thank you for contacting us. Please click on the link below to reset your password. The link is available for <b>one time use</b> and will <b>expire in 24 hrs</b>.
        </span>
        <br><br>
        <span style="word-break:break-all;">
        <?php echo $reset_link; ?>
        </span>
        <br>
      </p>
    </td>
    <td style="width:1%"></td>
  </tr>
    <tr>
      <td style="width:1%"></td>
      <td><br>With kind regards,<br>TEST</td>
      <td style="width:1%"></td>
    </tr>
</table>