<!DOCTYPE html>
<html>
<head>
<title>User|register</title>
<link href="<?php echo base_url()?>resources/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <style>
       .btn-red-normal
        {
            padding: 8px 18px;
            font-size: 13px;
            text-transform: uppercase;
            width:100px;
        }
        
        .btn-red
        {
            background-color: #7B7070 !important;
            color: #FFFFFF;
        }
        .panel-default
        {
            width:400px;
            min-height:545px;
            border-top: 5px solid #7B7070 !important;
            margin:auto;
            overflow:hidden;
            position: relative;
        }
        .panel-body
        {
            color:#7B7070 !important;
        }
        h3
        {
           color:#7B7070 !important;
            font-weight: bold;
        }
        a
        {
            color:#7B7070 !important;
        }
        .bg-success
        {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
            font-size: 13px;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2);
        }
        .bg-danger
        {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #a94442;
            background-color:#f2dede;
            border-color: #ebccd1;
            font-size: 13px;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2);
        }
        .link
        {
          clear:both;
          margin-top: 20px;
        }

        @media (max-width: 440px) {
          .panel-default {
              width: 100%;
          }
        }

         @media (max-width: 337px) {
          .bear-logo {
            width: 26%;
            position: absolute;
            bottom: -40px;
            opacity: 0.3;
            right: -13px;
            }
        }

        @media (min-width: 337px) and (max-width: 436px){
          .bear-logo {
            width: 24%;
            position: absolute;
            bottom: -43px;
            opacity: 0.3;
            right: -7px;
            }
        }
        @media (min-width: 436px) and (max-width: 767px) {
          .bear-logo{
            width: 23%;
            position: absolute;
            top: -94px;
            opacity: 0.3;
            right: -6px;
          }
        }
        @media (min-width: 767px) {
          .bear-logo{
            width: 89%;
            position: absolute;
            bottom: -83px;
            opacity: 0.3;
            right: -26px;
          }
        }
    </style>
</head>
<body>
    <div class="listing-box-wrap">
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Register</h3>
                    <?php  if($this->session->flashdata('success')) { ?>
                    <div class="bg-success">
                        <?php echo $this->session->flashdata('success'); ?> 
                    </div>
                    <?php } ?>
                    <?php  if($this->session->flashdata('error')) { ?>
                    <div class="bg-danger">
                        <?php echo $this->session->flashdata('error'); ?> 
                    </div>
                    <?php } ?>

                    <form id="" method="post" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-12 <?php echo form_error("first_name") ? "has-error" : "" ?>">
                                <label class="control-label">First Name</label>
                                <input class="form-control" type="text" name="first_name" value="<?php echo $this->input->post('first_name'); ?>" style="margin-bottom: 0;">
                                <span class="text-danger"><?php echo form_error("first_name") ?></span>
                            </div>
                            <div class="form-group col-xs-12 <?php echo form_error("last_name") ? "has-error" : "" ?>">
                                <label class="control-label">Last Name</label>
                                <input class="form-control" type="text" name="last_name" value="<?php echo $this->input->post('last_name'); ?>" style="margin-bottom: 0;">
                                <span class="text-danger"><?php echo form_error("last_name") ?></span>
                            </div>
                           
                            <div class="form-group  col-xs-12 <?php echo form_error("email") ? "has-error" : "" ?>">
                                <label class="control-label">Email</label>
                                <input class="form-control" type="text" name="email" value="<?php echo $this->input->post('email'); ?>" style="margin-bottom: 0;">
                                <span class="text-danger"><?php echo form_error("email") ?></span>
                            </div>
                            <div class="form-group col-xs-12 <?php echo form_error("password") ? "has-error" : "" ?>">
                                <label class="control-label">Password</label>
                                <input class="form-control" type="password" name="password" style="margin-bottom: 0;">
                                <span class="text-danger"><?php echo form_error("password") ?></span>
                            </div>
                            <div class="form-group col-xs-12 <?php echo form_error("passconf") ? "has-error" : "" ?>">
                                <label class="control-label">Confirm password</label>
                                <input class="form-control" type="password" name="passconf" style="margin-bottom: 0;">
                                <span class="text-danger"><?php echo form_error("passconf") ?></span>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="form-group col-sm-12 col-md-12 col-xs-12">
                                 <button class="btn-red btn-red-normal" type="submit" >Register</button>
                            </div>
                            <div class='col-md-8 link col-sm-8'>
                                <div class="form-group">
                                    <label class="control-label">Already Registered?</label class="control-label">
                                    <a href="<?php echo site_url('user/login');?>">Login here!</a>
                                    <div></div>
                                </div>
                            </div>  
                        </div>
                    </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>