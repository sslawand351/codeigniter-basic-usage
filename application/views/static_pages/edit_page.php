<?php echo Assets::js("admin/ckeditor/ckeditor.js"); ?>
<?php echo Assets::js("admin/js/plugins/bootstrap-filestyle/src/bootstrap-filestyle.js"); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/admin/ckeditor/config.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/admin/ckeditor/adapters/jquery.js"></script>

  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
    <!-- main header -->
    <div class="bg-light lter b-b wrapper-md">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <h1 class="m-n font-thin h3 text-black"><?php echo lang('edit_page') ?></h1>
            </div>
        </div>
    </div>
    <!-- / main header -->
    <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
        <div class="row">
            <?php  if($this->session->flashdata('success_msg')) { ?>
                  <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   <?php echo $this->session->flashdata('success_msg'); ?>
                  </div>
            <?php } ?>
            <?php  if($this->session->flashdata('error_msg')) { ?>            
                  <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $this->session->flashdata('error_msg'); ?>
                  </div>
            <?php } ?>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo lang('edit_page') ?>
            </div>
            <div class="panel-body">
                <form role="form" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                     <ul id="myTabs" class="nav nav-tabs" role="tablist">
                        <?php if(isset( $page ) && !empty($page)) { ?>

                        <?php foreach($page as $value) {  if($value['language_name']!='german') {?>
                        <li role="presentation" class="<?php if($value['language_name'] == 'english') { echo 'active';} ?> <?php if(form_error('data['.$value['language_id'].'][title]') || form_error('data['.$value['language_id'].'][url]') || form_error('data['.$value['language_id'].'][body]') ) { echo 'validation_error';}?>"><a href="#<?php echo $value['language_name']; ?>" id="<?php echo $value['language_name']; ?>-tab" role="tab" data-toggle="tab" aria-controls="<?php echo $value['language_name'];?>" aria-expanded="true"><?php echo ucfirst($value['language_name']); ?></a></li>
                            <?php 
                            }}
                        ?>
                     </ul>
                     <div id="myTabContent" class="tab-content">

                    <?php foreach($page as $value) {?>
                      <div role="tabpanel" class="tab-pane fade <?php if($value['language_name'] == 'english') { echo 'in active';} ?> " id="<?php echo $value['language_name'];?>" aria-labelledBy="<?php echo $value['language_name'];?>-tab">
                    
                    <div class="row">
                       <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('page_name') ?><?php if($value['language_name'] != 'german'){?><span class="text-danger"> *</span><?php } else {?><span></span><?php }?> :</label>
                                <input type="text" class="form-control" name="data[<?php echo $value['language_id']; ?>][page_name]" value="<?php echo ($this->input->post('data['.$value['language_id'].'][page_name]')) ?  $this->input->post('data['.$value['language_id'].'][page_name]') : $value['page_name'] ;?>"/>
                                <?php if(form_error('data['.$value["language_id"].'][page_name]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][page_name]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                       <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('url') ?><?php if($value['language_name'] != 'german'){?><span class="text-danger"> *</span><?php } else {?><span></span><?php }?> :</label>
                                <input type="text" class="form-control" name="data[<?php echo $value['language_id']; ?>][url]" value="<?php echo ($this->input->post('data['.$value['language_id'].'][url]')) ?  $this->input->post('data['.$value['language_id'].'][url]') : $value['url'] ;?>"/>
                                <?php if(form_error('data['.$value["language_id"].'][url]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][url]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('body') ?><?php if($value['language_name'] != 'german'){?><span class="text-danger"> *</span><?php } else {?><span></span><?php }?> :</label>
                                <textarea id="editor<?php echo $value['language_id']; ?>" cols="50" rows="5" class ="form-control" name="data[<?php echo $value['language_id']; ?>][body]" > <?php echo ($this->input->post('data['.$value['language_id'].'][body]')) ? $this->input->post('data['.$value['language_id'].'][body]') : $value['body']; ?></textarea>
                                <script type="text/javascript">
                                  CKEDITOR.replace( 'editor<?php echo $value["language_id"]; ?>' );
                                  CKEDITOR.edit            
                                </script>
                                <?php if(form_error('data['.$value['language_id'].'][body]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value['language_id'].'][body]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('page_title')?> <?php if($value['language_name'] != 'german'){?><span class="text-danger"> *</span><?php } else {?><span></span><?php }?> :</label>
                                <input type="text" class="form-control" name="data[<?php echo $value['language_id']; ?>][page_title]" value="<?php echo ($this->input->post('data['.$value['language_id'].'][page_title]')) ?  $this->input->post('data['.$value['language_id'].'][page_title]') : $value['page_title'] ;?>"/>
                                <?php if(form_error('data['.$value["language_id"].'][page_title]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][page_title]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                       
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('meta_description') ?><?php if($value['language_name'] != 'german'){?><span class="text-danger"> *</span><?php } else {?><span></span><?php }?> :</label>
                                <textarea class="form-control" name="data[<?php echo $value['language_id']; ?>][meta_description]"><?php echo ($this->input->post('data['.$value['language_id'].'][meta_description]')) ?  $this->input->post('data['.$value['language_id'].'][meta_description]') : $value['meta_description'] ;?></textarea>
                                <?php if(form_error('data['.$value["language_id"].'][meta_description]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][meta_description]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
        
                       
                </div>
                    <?php }?>

                    <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('update') ?></button>
                    <?php
                    }
                    else
                    {?>
                        <div class="row">
                           <div class="alert alert-danger">
                                <strong><?php echo lang('no_data_found') ?>!</strong>
                            </div>
                        </div>  
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
    </div>



</div>

</div>


<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
.chosen-container{
    width:100%!important;
}
.text-danger
{
    display: inline-block;
}
</style>
<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
 }
 .language{
    margin-left:16px!important;
}
</style>
