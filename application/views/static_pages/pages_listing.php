<?php echo Assets::js("admin/js/custom/blog_listing.js"); ?>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('pages') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('success_msg')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('success_msg'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error_msg')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					 <?php echo $this->session->flashdata('error_msg'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
				
						<a class="btn btn-primary btn-xs pull-right" href="<?php echo site_url('admin/static_page/add');?>"  ><?php echo lang('add_pages') ?></a>
				
					
					<?php echo lang('pages_listing') ?>
				</div>
				<div class="page-table table-responsive">	
					<table class="tftable table table-striped m-b-none">
					<tr>
						<th style="width:20%;text-align: 20%;"><?php echo lang('page_name')?></th>
						
						<th style="width:30%;text-align: 20%;padding-left: 30px;"><?php echo lang('url')?></th>
						
						<th style="width:30%;text-align: 20%;padding-left: 50px;"><?php echo lang('added_on')?></th>

						<th style="padding-left: 30px;"><?php echo lang('action')?></th>

					</tr>
					 
					
						<?php 
			                    if(($pages)){ 
			                   
			                        $count = 1;

							foreach ($pages as $value){ ?>
							
						        <tr class="row1" data-href=''>
						            <td class= " blog-img">
						        
						                    <div class="blog-para">
						                        <?php echo '<p style="width:20%;text-align:center;">'.$value['page_name'].'</p>' ?>
						                    </div>
						            </td>
						            
						            <td class="blog-content">
						                    
						                    <div class="blog-para">
						                        <?php echo '<p style="width:20%;text-align:center;">'.$value['url'].'</p>'?>
						                    </div>
						            </td>
						            
						            <td class= "added_on col-md-2 col-sm-6 col-xs-12">
						                <?php echo '<p style="width:45%;text-align:center;">'.date('d-m-Y g:i:s',strtotime($value['added_on'])).'</p>'?>
						            </td>

						            <td class="blog-action col-md-2 col-sm-6 col-xs-12">
			          
							
									<a class="btn btn-xs btn-info" href="<?php echo site_url('admin/static_page/edit/'.$value['page_id']);?>"><?php echo lang('edit')?></a>
								
								 
									<a class="btn btn-xs btn-danger delete_page" href="#delete_page_popup" data-toggle="modal" data-page-id="<?php echo $value['page_id']; ?>"><?php echo lang('delete')?></a>

									<?php if( $value['is_offline'] == FALSE ){ ?>
										<a class="btn btn-xs btn-danger" href="<?php echo site_url('admin/static_page/change_status/offline/'.$value['page_id']);?>" ><?php echo lang('offline')?></a>
									<?php }else{  ?>
									<a class="btn btn-xs btn-success" href="<?php echo site_url('admin/static_page/change_status/online/'.$value['page_id']);?>" ><?php echo lang('online')?></a>
									<?php }?>
				            </td>
						            
						          </tr>
						    
					 	</div>   
					    <?php } $count++;} else{?>
							<div>
								<tr>
									<td><b><?php echo lang('no_pages_added_yet') ?></b></td>
								</tr>
							</div>				
						<?php }?>			
        		 </table>
        		 <footer class="panel-footer pagination_links">
			      		<div class="row">
							<div class="col-sm-12 text-right text-center-xs">
								<?php echo $links; ?>
							</div>
						</div>
					</footer>
			</div>
		</div>
	</div> 

	<div id="delete_page_popup" class="modal fade" data-backdrop="true" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 style="margin:0px;"><?php echo lang('delete_blog') ?></h3>
				</div>
				<form id="delete_page_form" action="" method="post">
				    <div class="modal-body">
			    		<input type="hidden" class="page_id" name="page_id" value="">
			    		<div class="row">
			    			<div class="col-md-12">
			    			<?php echo lang('are_you_sure_you_want_to_delete_this_page') ?> ?
			    			</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="delete_page_btn btn btn-default"><?php echo lang('ok') ?></button>
						<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
					</div>
		    	</form>
			</div>
		</div>
	</div>
	
			
</div>
<script>
		$(document).on("click", ".delete_page", function(e){
        var page_id = $(this).attr('data-page-id');
        var link = site_url('admin/static_page/delete/' + page_id);

        $('#delete_page_form').attr('action',link);
        $(".page_id").val(page_id);
        
    });

    $(".delete_page_btn").on("click",function(){
        var form = $("#delete_page_form");
        form.submit();
    });
    
</script>

<style>
	#article
	{
		height: 100px!important;
	}

	.page-table
	{
		border-bottom: 1px solid #EAEAEA;
	}
	.added_on
	{
		top: 25px!important;
	}
	.tftable th
	{
		/*text-align: center;*/
		border-bottom: 1px solid #ddd;
	}
	b
	{
		text-align: center;
	}
	.blog-action
	{
		top: 25px!important;
	}
</style>