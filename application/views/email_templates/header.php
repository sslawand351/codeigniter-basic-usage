<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

    <head>        

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        

        <title><?php echo isset($email_title) ? $email_title : WEBSITE_NAME . ' Email'; ?></title>        

        <meta name="description" content="" />        

        <meta name="keywords" content="" />        

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <style type="text/css">

            /* Override styles in certain mail clients and force our styles and settings */

            body {min-width: 100% !important; margin: 0; padding: 0; -webkit-text-size-adjust: none;} 

            table td {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;} 

            table {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;} 

            td {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;} 

            img {border-collapse: collapse; border: none; margin: 0; padding: 0;} 

            a {text-decoration: none !important;} 

            p, h1, h2, h3, h4, h5, h6 {line-height: 20px; margin: 0px; padding: 0px;} 
            
            h1, h2, h3, h4, h5, h6 {color:#D32812;}

            ul {margin: 0px; padding: 0px; list-style-position: inside;} 

            li {margin: 0px; padding: 0px; list-style-position: inside;}                          

         

            td[class=contentholdingtable] {min-width: 600px; width: 600px; max-width: 600px !important;} 

            /* Responsivnes settings */

            

            @media only screen and (min-width: 676px) { 
                
                img[class=img-600-mobile] {width:125px; height:91px;}
            }
            @media only screen and (max-width: 675px) { 

                body {min-width: 100% !important;}

                td[class=header] {padding-left: 5% !important; padding-right: 5% !important;}

                td[class=main-content] {padding-left: 5% !important; padding-right: 5% !important;}

                /*td[class=contentholdingtable] {padding-left: 5% !important; padding-right: 5% !important;} */
                
                td[class=footer] {padding-left: 5% !important; padding-right: 5% !important;}
                
                td[class=contentholdingtable] {min-width: 0px !important; width: 90% !important;} 

                td[class=spacercolDELformobile] {display: none !important;} 
                
                td[class=title-text] {padding-left: 5% !important; padding-right: 5% !important;} 

                td[class=img-div] {padding-right: 5% !important; text-align:center !important; padding-left: 5% !important; } 
                td[class=splitedcontenttableinset] {width: 96% !important; float: left !important; padding: 8px !important}
                td[class=splited2contenttableinset] {width: 100% !important; float: left !important; padding:0px !important; padding-top:3px !important;}
                 

                td[class=splited3contenttableinset] {width: 100% !important; float: left !important;  padding: 0px 20px 10px 0px !important;} 
                td[class=splited3contenttableinset2] {width: 100% !important; float: left !important;  padding: 0px 0px 10px 18px !important;} 
                td[class=splited3contenttableinset2price] {width: 100% !important; float: left !important;  padding: 0px 0px 10px 18px !important;} 
                
                img[class=img-600] {width: 100% !important; height: auto !important;}

                img[class=img-580] {width: 90% !important; height: auto !important;}           

                img[class=img-285] {width: 90% !important; height: auto !important;} 

                img[class=img-185] {width: 90% !important; height: auto !important;}

                img[class=img-200] {width: 90% !important; height: auto !important;}                

                p img {max-width: 100% !important; height: auto !important;} 

                 img[class=img-600-mobile] {width: 88% !important; height: auto !important;}

                hr[class=hr-line] {width: 90% !important; height: auto !important;}

            }

            

            @media only screen and (max-width: 430px) {                

                td[class=view-online-link] {width: 185px !important;}

            }

                     

        </style>    

    </head> 

    <body bgcolor="white" style="background-color: white;">                                     

                         

        

        <!--START main table-->         

        <table bgcolor="white" width="100%" border="0" cellpadding="0" cellspacing="0" style="background: white; padding: 0px; margin: 0px; border-collapse: collapse;">             

            <!--START Header image and view online link-->             

            <tr>                 

                <td class="main-content" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">                     

                    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">                         

                        <tr>                             

                            <td class="contentholdingtable" width="600" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">                                 

                                <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 

                                    <!--START view online link--> 

                                    <tr> 

                                        <td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 

                                            <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 

                                                <tr> 

                                                    <!--START CONTENT column--> 

                                                    <td class="fullwidthcontenttableinset" bgcolor="white" width="600" align="center" valign="top" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 

                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 

                                                            <tr> 

                                                                <td align="center" style="font-size: 0px; line-height: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 

                                                                    <!--Table with border--> 

                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 

                                                                        <tr> 

                                                                            <td align="left" valign="middle" style="font-size: 0px; line-height: 0px; padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 

                                                                                <!--Table with navi links--> 

                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 

                                                                                    <tr> 

                                                                                        <td class="view-online-link" align="center" valign="middle" style="color: #FF4704; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; line-height: 12px; padding: 0px; margin: 0px; border-collapse: collapse;"> 

                                                                                            <!--View online link-->

                                                                                                                                                                                          

                                                                                        </td> 

                                                                                    </tr> 

                                                                                </table> 

                                                                            </td> 

                                                                        </tr> 

                                                                    </table> 

                                                                </td> 

                                                            </tr> 

                                                        </table> 

                                                    </td> 

                                                    <!--END CONTENT column--> 

                                                </tr> 

                                            </table> 

                                        </td> 

                                    </tr> 

                                    <!--END view online link--> 

                                    <!--START header image--> 

                                    <tr> 

                                        <td align="center" bgcolor="#ffffff" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 

                                            <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 

                                                <tr> 

                                                    <td class="fullwidthcontenttableinset" width="600" align="center" style="font-size: 0px; line-height: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse; max-width: 600px; max-height: 145px;"> 
                                                        <!--Logo 276 goes here--><img class="img-600" src="<?php echo base_url("resources/newsletter/img/email-header.png") ?>" width="600" alt="header" border="no" style="margin: 0px; padding: 0px; border: none; max-width: 600px; max-height: 145px;"/> 
                                                    </td> 
                                                </tr> 

                                            </table> 

                                        </td> 

                                    </tr> 

                                    <!--END header image-->                                     

                                </table> 

                            </td> 

                        </tr> 

                    </table> 

                </td> 

            </tr> 

            <!--END Header image and view online link-->

            <!--START newsletter content-->             
            <tr> 
                <td class="main-content" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">

<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                        <tr> 
                            <td class="contentholdingtable" bgcolor="#ffffff" width="600" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                    <!--START content holding table row--> 
                                    <tr> 
                                        <td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                          
                   
                                        
                                            <table class="mainZone zone-100" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                <tbody><tr>
                                                    <!--START CONTENT column-->
                                                                                                                <td class="fullwidthcontenttableinset" width="600" align="center" valign="top" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                    
                                                                                                                                                
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">                                                                             
                                                                            <tbody>
                                                                            <tr> 
                                                                                <td class="title-text" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 20px; padding-top: 0px; padding-right: 10px; padding-bottom: 0px; padding-left: 10px; margin: 0px; border-collapse: collapse;">