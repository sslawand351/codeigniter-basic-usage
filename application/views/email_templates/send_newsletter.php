<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
  <tr><td height="10"></td></tr>
  
  <?php if( ! empty($newsletter_zones) ) { ?>
  <?php foreach($newsletter_zones as $nz_key => $nz ) { ?>
  <tr>
  <td style="width:1%"></td>
  <td>
        <?php 
            /*if( $nz['zone_type'] != 4 )
            {
                $car_ids = explode(',', $nz['zone_cars']);

                if( ! empty($car_ids) )
                {
                    $site_url = '';

        ?>
            <table class="mainZone zone-33-33-33" align="left" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px 0px; border-collapse: collapse;"> 
            <tr>
                <?php
                        $layout = $nz['zone_type'];

                        $count = count($car_ids);
                        if( $layout == 1 )
                        {
                            $class = "splited3contenttableinset";
                            $width = "200";
                            $count_div = 3;
                        }
                        else if( $layout == 2 )
                        {
                            $class = "splited2contenttableinset";
                            $width = "285";
                            $count_div = 2;
                        }
                        else
                        {
                            $class = "splited3contenttableinset";
                            $width = "185";
                            $count_div = 3;
                        }
                        $key = 0;
                        foreach( $car_ids as $c )
                        {
                            if( isset($zone_cars[$c]) )
                            {
                                $car = $zone_cars[$c];
                            
                                $flag = FALSE;
                ?>

                            
                            <!-- CAR BOX -->
                            <td class="<?php echo $class; ?>" width="<?php echo $width; ?>" align="center" valign="top" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 

                        
                                    <table class="image-container" width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">                                                                                                                                                                                                                                             
                                        <tr> 
                                            <td align="center" height="10" style="padding: 0px; margin: 0px; border-collapse: collapse;"></td> 
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
                                                <?php $car_type = $car['car_subtitle'] != "" ? str_replace(" ", "-", $car['car_subtitle']) : ""; ?>
                                                <?php $car_type = $car_type != "" ? str_replace("/", "", $car_type) : ""; ?>
                                                <?php $car_model = $car['model'] != "" ? str_replace(" ", "-", $car['model']) : ""; ?>
                                                <?php $car_name =  $car['brand']."-".$car_model."-".$car_type; 
                                                      $car_name = urlencode($car_name);
                                                ?>
                                                    <a href="<?php echo $site_url.$language_code.'/'.$car['id']."/".$car_name; ?>" target="_blank">
                                                        <img class="img-<?php echo $width; ?>" src="<?php echo get_car_image($car, 'medium'); ?>" width="<?php echo $width; ?>" alt="" border="no" style="margin: 0px; padding: 0px; display: block; border: none;"/>                                                                                                    
                                                    </a>
                                            </td>
                                        </tr>        
                                    </table>
                            <?php 
                                    if( $layout == 1 )
                                    {
                            ?>
                                </td>                                                                                                     <!--SPACER COLUMN do not delete-->
                            <td class="spacercolDELformobile" width="10" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">&nbsp;</td>                               
                                <td class="splited3contenttableinset2" width="380" align="center" valign="top" style="padding: 0px; margin: 0px; border-collapse: collapse;padding-bottom: 20px;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="color:black !important; padding: 0px; margin: 0px; border-collapse: collapse;">                                                                             
                                        <tr> 
                                            <td align="center" height="10" style="padding: 0px; margin: 0px; border-collapse: collapse;"></td> 
                                        </tr>
                                        <tr> 
                                            <td class="title-text" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: normal; line-height: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                                <p><strong><font color="#BD1812"><?php echo $car['brand']." ".$car['model'];?></font></strong><br />
                                                <span style="font-size: 12px; color: #555555; font-weight: 600;"><?php echo $car['car_subtitle'];?></span><br />
                                                <br />
                                            </td>
                                        </tr>
                                    </table>

                                    <table style="width:100%;text-align: left; line-height: 12px;font-variant: inherit;font-size: 13px;">
                                        <tr>
                                            <td width="50%">
                                                <img alt="" src="<?php echo $site_url.'resources/b2/img/icons/km.jpg'; ?>" style="width: 15px; height: 13px;" />&nbsp;<?php echo $car['mileage'];?><span style="line-height: 20.8px;"> km</span><br />
                                            </td>
                                            <td width="50%">
                                                <span style="font-size: 16px;"><span style="color: rgb(128, 128, 128);"><span style="line-height: 20.8px;">&euro;&nbsp;<?php echo $car['inclusive_selling_cost']; ?> of</span></span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50%">
                                        <?php 
                                            $date   = $car['registration_date'];
                                            $day    = date('d',strtotime($car['registration_date']));
                                            $month  = date('m',strtotime($car['registration_date']));
                                            $year   = date('Y',strtotime($car['registration_date']));
                                            $month_name = date('F',strtotime($car['registration_date']));

                                        if( checkdate( $month, $day, $year) && $car['registration_date'] != '0000-00-00' ) { ?>
                                                <img alt="" src="<?php echo $site_url.'resources/b2/img/icons/calendar.jpg'; ?>" style="width: 15px; height: 13px;  <?php if( ! ( checkdate( $month, $day, $year ) && $car['registration_date'] != '0000-00-00')) { ?> visibility: hidden; <?php } ?>"  />&nbsp;<span style="line-height: 20.8px;"><?php echo lang($month_name)." ".$year; ?><span style="line-height: 20.8px;">/2011</span>
                                                <?php } ?>
                                                <br />
                                                <br />
                                            </td>
                                            <td width="50%">
                                                <span style="color: #BD1812;"><span style="line-height: 20.8px;">
                                                   
                                                    <?php if (isset($car['monthly_payment'])):?>
                                                        &euro;&nbsp;<?php echo format_currency(floor($car['monthly_payment'])); ?>&nbsp;/&nbsp;mnd
                                                    <?php else:?>
                                                        N/a
                                                    <?php endif;?>
                                                </span></span></span><br />
                                                &nbsp;<br />
                                                &nbsp;</p>
                                            </td> 
                                        </tr>
                                    </table>
                            <?php
                                    }
                                    else
                                    {  
                            ?>                                                                         
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="color:black !important; padding: 0px; margin: 0px; border-collapse: collapse;">                                                                             
                                        <tr> 
                                            <td align="center" height="10" style="padding: 0px; margin: 0px; border-collapse: collapse;"></td> 
                                        </tr>
                                        <tr> 
                                            <td class="title-text" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: normal; line-height: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                                <p><strong><font color="#BD1812"><?php echo $car['brand']." ".$car['model'];?></font></strong><br />
                                                <span style="font-size: 12px; color:#555555;font-weight: 600;"><?php echo $car['car_subtitle'];?></span><br />
                                                <br />
                                                <img alt="" src="<?php echo $site_url.'resources/b2/img/icons/km.jpg'; ?>" style="width: 15px; height: 13px;" />&nbsp;<span style="line-height: 20.8px;font-size: 14px;color: rgb(128, 128, 128);"><?php echo $car['mileage'];?> km</span>
                                                <br>
                                                <?php

                                                    $date   = $car['registration_date'];
                                                    $day    = date('d',strtotime($car['registration_date']));
                                                    $month  = date('m',strtotime($car['registration_date']));
                                                    $year   = date('Y',strtotime($car['registration_date']));
                                                    $month_name = date('F',strtotime($car['registration_date']));

                                                 if( checkdate( $month, $day, $year) && $car['registration_date'] != '0000-00-00' ) { ?>
                                                <img alt="" src="<?php echo $site_url.'resources/b2/img/icons/calendar.jpg'; ?>" style="width: 15px; height: 13px;  <?php if(!( checkdate( $month, $day, $year) && $car['registration_date'] != '0000-00-00') ) { ?> visibility: hidden; <?php } ?>"  />&nbsp;<span style="line-height: 20.8px;font-size: 14px;color: rgb(128, 128, 128);"><?php echo lang($month_name)." ".$year; ?>/2011</span>
                                                <?php } ?>
                                                <br />
                                                <br />
                                                <span style="font-size: 16px;"><span style="color: rgb(128, 128, 128);"><span style="line-height: 20.8px;">&euro;&nbsp;<?php echo $car['inclusive_selling_cost']; ?> of</span></span><br />
                                                <span style="color: rgb(178, 34, 34);"><span style="line-height: 20.8px;">
                                                   
                                                    <?php if (isset($car['monthly_payment'])):?>
                                                        &euro;&nbsp;<?php echo format_currency(floor($car['monthly_payment'])); ?>&nbsp;/&nbsp;mnd
                                                    <?php else:?>
                                                        N/a
                                                    <?php endif;?>
                                                </span></span></span><br />
                                                &nbsp;<br />
                                                &nbsp;</p>
                                            </td> 
                                        </tr>
                                    </table>
                            <?php } ?>                                                              
                            </td>              
                            <!--SPACER COLUMN do not delete-->
                            <td class="spacercolDELformobile" width="10" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">&nbsp;</td>                                                                                                       
                                                                                                                
                            <!-- CAR BOX ENDS -->

                            <?php if(($key+1)%$layout == 0)
                            { $flag = TRUE;
                                ?>
                                     </tr> 
                                        <!-- </table>
                                    <table class="mainZone zone-33-33-33" align="left" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px 20px; border-collapse: collapse;">  -->
                                            <tr>
                           <?php }
                           else
                           {
                                $flag = FALSE;
                           }
                           $key++;

                            }
                        }
                            ?>
                                     </tr> 
                                        </table>
                        <?php

                }
        }
        else{*/ ?>
        <span>
           <?php echo $nz['zone_body']; ?>
        </span>
    <?php /*}*/ ?>
    </td>
    <td style="width:1%"></td>
  </tr>
    <?php } ?>
<?php } ?>
</table>
<!-- END of Send Newsletter Template -->
