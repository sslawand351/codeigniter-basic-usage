<?php

$this->load->view("email_templates/header");

if(isset($_view) && $_view != "")
{
	$this->load->view($_view);
}

$this->load->view("email_templates/footer");