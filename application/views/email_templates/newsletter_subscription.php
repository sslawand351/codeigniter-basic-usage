<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
  <tr><td height="10"></td></tr>
  <tr>
    <td style="width:1%"></td>
    <td>
      <p><?php echo lang("hi") ?>,
        <br><br>
        <span>
          <?php echo lang("newsletter_subscription_thank_you_msg") ?>
        </span>
        <br><br>
        <span>
          <?php echo lang("newsletter_unsubscription_msg") ?>
        </span>
        <br><br>
        <a href="<?php echo $cancel_subscription_link; ?>" style="color:#fff; background: #ccc; padding:10px;border-radius: 5px;display: inline-block;"><?php echo lang('unsubscribe_me_please'); ?></a>
        <br>
      </p>
    </td>
    <td style="width:1%"></td>
  </tr>
    <tr>
      <td style="width:1%"></td>
      <td><br><?php echo lang("with_kind_regards") ?><br>Belgoptic</td>
      <td style="width:1%"></td>
    </tr>
</table>