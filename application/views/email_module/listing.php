<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('email') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				
				<div class="alert alert-success" style="<?php if(!$this->session->flashdata('success')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="success-msg"><?php echo $this->session->flashdata('success'); ?></span>
				</div>

				<div class="alert alert-danger" style="<?php if(!$this->session->flashdata('error')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span><?php echo $this->session->flashdata('error'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('admin/email/add'); ?>" data-toggle="modal" class="btn btn-primary btn-xs pull-right open-send-newsletter"><?php echo lang('add_email') ?></a>
					
					<?php echo lang('email_listing') ?>
				</div>
				<div class="table-container">
					<div class="table-responsive">		
						<table class="tftable table table-striped m-b-none">
							<tr>
								<th><?php echo lang('name') ?></th>
								<th><?php echo lang('subject') ?></th>
								<th><?php echo lang('created_on') ?></th>
								<th colspan="2"><?php echo lang('action') ?></th>
							</tr>
							<?php if(count($emails) > 0) { ?>
							<?php
								foreach($emails as $value)
								{
							?>
							<tr>
								<td><?php echo $value['title']; ?></td>
								<td><?php echo $value['subject']; ?></td>
								<td><?php echo date('d-m-Y G:i:s', strtotime($value['added_on']) ); ?></td>
								<td>
									<a href="<?php echo site_url('admin/email/edit/' . $value['id']); ?>" class="btn btn-info btn-xs" data-email-id="<?php echo $value['id']; ?>"><?php echo lang('edit') ?></a>
									<a href="#delete-email-popup" data-toggle="modal" data-email-id="<?php echo $value['id']; ?>" class="delete-email btn btn-xs btn-danger"><?php echo lang('delete') ?></a>
								</td>
							</tr>
							<?php } ?>
							<?php } else { ?>
								<tr>
									<td colspan="6" style="text-align:center;"><?php echo lang('no_data_found') ?> </td>
								</tr>
							<?php } ?>
						</table>
					</div>
					
				</div>
				<?php 
					// load table view
					//$this->load->view('email_module/templates/_table');
				?>
				<footer class="panel-footer">
			  		<div class="row">
						<div class="col-sm-12 text-right text-center-xs">
							<?php echo $links; ?>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
</div>

<!-- delete-email-popup Modal -->
<div id="delete-email-popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('delete_email') ?></h3>
			</div>
			<form id="form-delete-email" action="<?php echo site_url('admin/email/delete_email'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="email_id" name="email_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    			<?php echo lang('are_you_sure_you_want_to_delete_this_email') ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="delete-email-btn btn btn-default"><?php echo lang('ok') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of delete-email-popup Modal -->

<script>
$(document).ready(function(){
	$('.delete-email').click(function(){
		var _this = $(this);
		var email_id = _this.attr('data-email-id');
		$('#delete-email-popup').find('input[name="email_id"]').val(email_id);
	});
});
</script>