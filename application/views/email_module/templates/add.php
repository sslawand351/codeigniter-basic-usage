<?php echo Assets::js('frontend/js/farbtastic.js'); ?>
<?php echo Assets::css('frontend/css/farbtastic.css'); ?>
<style>
.colorwell {
	text-align: center;
}
</style>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">
		<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('email') ?></h1>
				</div>
			</div>
		</div><!-- / main header -->
		
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo lang('create_email_template') ?>
				</div>
				<div class="panel-body">
					<form action="<?php echo site_url('admin/email/create_template')?>" id="edit_form" method="post" enctype="multipart/form-data">
					<div class="wrapper" style="padding-left:0px">
					    <div class="row">
							<div class="col-sm-3">
								<label><?php echo lang('title') ?><span class="text-danger"> *</span> :</label>
							</div>
							<div class="col-sm-5">
							    <div class="form-group">
							        
							        <input type="text" name="template_name" value="<?php echo $this->input->post('template_name'); ?>" class="form-control">
									<?php if(form_error('template_name')) { ?>
										<div class="text-danger"><?php echo form_error('template_name'); ?></div>
									<?php } ?>
							    </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<label><?php echo lang('upload_header_background') ?> :</label>
							</div>
							<div class="col-sm-5">
							    <div class="form-group">
							        
							        <input type="file" name="header_background" value="<?php echo $this->input->post('header_background'); ?>" class="form-control">
									<?php if(form_error('header_background')) { ?>
										<div class="text-danger"><?php echo form_error('header_background'); ?></div>
									<?php } ?>
							    </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<label><?php echo lang('upload_footer_background') ?> :</label>
							</div>
							<div class="col-sm-5">
							    <div class="form-group">
							        
							        <input type="file" name="footer_background" value="<?php echo $this->input->post('footer_background'); ?>" class="form-control">
									<?php if(form_error('footer_background')) { ?>
										<div class="text-danger"><?php echo form_error('footer_background'); ?></div>
									<?php } ?>
							    </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<label><?php echo lang('body_background_color') ?> :</label>
							</div>
							<div class="col-sm-5">
							    <div class="form-group">
							        
							        <input type="text" name="body_background_color" value="<?php echo $this->input->post('body_background_color') ? $this->input->post('body_background_color') : '#ffffff'; ?>" class="form-control colorwell">
									<?php if(form_error('body_background_color')) { ?>
										<div class="text-danger"><?php echo form_error('body_background_color'); ?></div>
									<?php } ?>
							    </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<label><?php echo lang('content_background_color') ?> :</label>
							</div>
							<div class="col-sm-5">
							    <div class="form-group">
							        
							        <input type="text" name="content_background_color" value="<?php echo $this->input->post('content_background_color') ? $this->input->post('content_background_color') : '#ffffff'; ?>" class="form-control colorwell">
									<?php if(form_error('content_background_color')) { ?>
										<div class="text-danger"><?php echo form_error('content_background_color'); ?></div>
									<?php } ?>
							    </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								
							</div>
							<div class="col-sm-5">
							    <div class="field-box">
			                       	<label class="span3"></label>
			                       	<div class="span3" id="picker"></div>
			                   	</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn btn-sm btn-primary"><?php echo lang('save'); ?></button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#demo').hide();
        var f = $.farbtastic('#picker');
        var p = $('#picker').css('opacity', 1);
        var selected;
        $('.colorwell')
        .each(function () { f.linkTo(this); $(this).css('opacity', 0.75); })
        .focus(function() {
            if (selected) {
                $(selected).css('opacity', 0.75).removeClass('colorwell-selected');
            }
            f.linkTo(this);
            p.css('opacity', 1);
            $(selected = this).css('opacity', 1).addClass('colorwell-selected');
        });
  });
</script>