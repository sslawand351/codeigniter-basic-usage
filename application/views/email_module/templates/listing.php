<?php echo Assets::js("admin/js/jquery.bootstrap.wizard.js"); ?>
<?php echo Assets::css("admin/css/bootstrap-chosen.css"); ?>
<?php echo Assets::js("admin/ckeditor/ckeditor.js"); ?>
<?php echo Assets::js("admin/ckeditor/samples/js/sample.js"); ?>
<?php echo Assets::js("admin/js/custom/newsletter_template.js"); ?>
<?php echo Assets::css("admin/css/custom/newsletter_template_listing.css"); ?>
<style type="text/css">
.chosen-container {
    width: 100%!important;
}
.modal-header h3
{
	color: #58666e !important;
}
td.splitedcontenttableinset {
    padding: 10px;
}
</style>

<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('email') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				
				<div class="alert alert-success" style="<?php if(!$this->session->flashdata('success')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="success-msg"><?php echo $this->session->flashdata('success'); ?></span>
				</div>

				<div class="alert alert-danger" style="<?php if(!$this->session->flashdata('error')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span><?php echo $this->session->flashdata('error'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('admin/email/create_template'); ?>" data-toggle="modal" class="btn btn-primary btn-xs pull-right open-send-newsletter"><?php echo lang('create_email_template') ?></a>
					
					<?php echo lang('email_templates') ?>
				</div>
				<?php 
					// load table view
					$this->load->view('email_module/templates/_table');
				?>
				<footer class="panel-footer">
			  		<div class="row">
						<div class="col-sm-12 text-right text-center-xs">
							<?php echo $links; ?>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
</div>

<!-- delete-email-template-popup Modal -->
<div id="delete-email-template-popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('delete_email_template') ?></h3>
			</div>
			<form id="form-delete-email-template" action="<?php echo site_url('admin/email/delete_email_template'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="email_template_id" name="email_template_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    			<?php echo lang('are_you_sure_you_want_to_delete_this_email_template') ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="delete-email-template-btn btn btn-default"><?php echo lang('ok') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of delete-newsletter-template-popup Modal -->

<script>
$(document).ready(function(){
	$('.delete-email-template').click(function(){
		var _this = $(this);
		var email_template_id = _this.attr('data-email-template-id');
		$('#delete-email-template-popup').find('input[name="email_template_id"]').val(email_template_id);
	});

	/*$('.delete-email-template-btn').click(function(){
		var _this = $(this);
		var email_template_id = $('#delete-email-template-popup').find('input[name="email_template_id"]').val();
	});*/
});
</script>