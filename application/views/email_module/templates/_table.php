<div class="table-container">
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th><?php echo lang('name') ?></th>
				<th><?php echo lang('created_on') ?></th>
				<th colspan="2"><?php echo lang('action') ?></th>
			</tr>
			<?php if(is_array($email_templates) && count($email_templates) > 0) { ?>
			<?php
				foreach($email_templates as $v)
				{
			?>
			<tr>
				<td><?php echo $v['title']; ?></td>
				<td><?php echo date('d-m-Y G:i:s', strtotime($v['added_on']) ); ?></td>
				<td>
					<!--<a href="#viewEmailTemplate" data-toggle="modal" class="btn btn-success btn-xs view-email-template" data-email-template-id="<?php //echo $v['id'];?>" style="margin-right: 5px;"><?php echo lang('view') ?></a>-->
					<a href="<?php echo site_url('admin/email/edit_template/'.$v['id']); ?>" data-toggle="modal" class="btn btn-info btn-xs" data-email-template-id="<?php echo $v['id']; ?>"><?php echo lang('edit') ?></a>
					<a href="#delete-email-template-popup" data-toggle="modal" data-email-template-id="<?php echo $v['id']; ?>" class="delete-email-template btn btn-xs btn-danger"><?php echo lang('delete') ?></a>
					
					<!--<a href="#send-test-email-popup" data-toggle="modal" data-email-template-id="<?php //echo $v['id']; ?>" class="send-test-email btn btn-primary btn-xs"><?php //echo lang('send_preview') ?></a>-->
				</td>
			</tr>
			<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_data_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>