<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
        <title><?php echo isset($email['subject']) ? $email['subject'] : WEBSITE_NAME; ?></title>        
        <meta name="description" content="" />        
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />        
        <style type="text/css">
            /* Override styles in certain mail clients and force our styles and settings */
            body {min-width: 100% !important; margin: 0; padding: 0; -webkit-text-size-adjust: none;} 
            table td {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;} 
            table {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;} 
            td {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;} 
            img {border-collapse: collapse; border: none; margin: 0; padding: 0;} 
            a {text-decoration: none !important;} 
            p, h1, h2, h3, h4, h5, h6 {line-height: 20px; margin: 0px; padding: 0px;} 
            ul {margin: 0px; padding: 0px; list-style-position: inside;} 
            li {margin: 0px; padding: 0px; list-style-position: inside;}                          
         
            td[class=contentholdingtable] {min-width: 600px; width: 600px; max-width: 600px !important;} 
            /* Responsivnes settings */
            
            @media only screen and (max-width: 675px) { 
                body {min-width: 100% !important;}
                td[class=header] {padding-left: 5% !important; padding-right: 5% !important;}
                td[class=main-content] {padding-left: 5% !important; padding-right: 5% !important;}
                td[class=contentholdingtable] {padding-left: 5% !important; padding-right: 5% !important;}
                td[class=footer] {padding-left: 5% !important; padding-right: 5% !important;}
                td[class=contentholdingtable] {min-width: 0px !important; width: 90% !important;} 

                td[class=spacercolDELformobile] {display: none !important;} 

                td[class=title-text] {padding-left: 5% !important; padding-right: 5% !important;} 

                td[class=fullwidthcontenttableinset] {width: 100% !important;} 
                td[class=splited2contenttableinset] {width: 100% !important; float: left !important;} 
                td[class=splited3contenttableinset] {width: 100% !important; float: left !important;} 

                img[class=img-600] {width: 100% !important; height: auto !important;}
                img[class=img-580] {width: 90% !important; height: auto !important;}           
                img[class=img-285] {width: 90% !important; height: auto !important;} 
                img[class=img-185] {width: 90% !important; height: auto !important;}                
                p img {max-width: 100% !important; height: auto !important;} 
                
                hr[class=hr-line] {width: 90% !important; height: auto !important;}
            }
            
            @media only screen and (max-width: 430px) {                
                td[class=view-online-link] {width: 185px !important;}
            }
                     
        </style>    
    </head>	
	<body bgcolor="<?php echo $email['body_color'];?>" style="background-color: <?php echo $email['body_color'];?>">		        	                   
        <?php    
            $max_header_width = "";
            $max_header_height = "";
            $header_url_image = "";
            $footer_url_image = "";
            $max_footer_width = "";
            $max_footer_height = "";

			if($email['header_image'] != '')
			{
				$dir = FCPATH . 'uploads/email_module/templates/images/header/' . $email['email_template_id'];

				if(file_exists($dir . '/' . $email['header_image']))
				{
					$header_url_image = site_url('uploads/email_module/templates/images/header/' . $email['email_template_id'] . '/' . $email['header_image']);

					list($max_header_width, $max_header_height) = getimagesize($header_url_image);
				}
			}

			if($email['footer_image'] != '')
			{
				$dir = FCPATH . 'uploads/email_module/templates/images/footer/' . $email['email_template_id'];

				if(file_exists($dir . '/' . $email['footer_image']))
				{
					$footer_url_image = site_url('uploads/email_module/templates/images/footer/' . $email['email_template_id'] . '/' . $email['footer_image']);

					list($max_footer_width, $max_footer_height) = getimagesize($footer_url_image);
				}
			}
        ?>
<?php
	$email_body = $email['email_body'];

    if($email['form_type'] == FORM_TYPE_PREDEFINED_EMAIL_TEMPLATE)
    {
        // build shortcode for predefined email template
        $predefined_shortcodes = array(
            'registration-email-to-admin-NL' => array(
                array(
                    'label' => 'name',
                    'shortcode' => 'name'
                ),
                array(
                    'label' => 'email',
                    'shortcode' => 'email'
                )
            ),
            'registration-email-to-admin-FR' => array(
                array(
                    'label' => 'name',
                    'shortcode' => 'name'
                ),
                array(
                    'label' => 'email',
                    'shortcode' => 'email'
                )
            ),
            'registration-email-to-user-NL' => array(
                array(
                    'label' => 'login url',
                    'shortcode' => 'login-url'
                ),
                 array(
                    'label' => 'name',
                    'shortcode' => 'name'
                ),

            ),
            'registration-email-to-user-FR' => array(
                array(
                    'label' => 'login url',
                    'shortcode' => 'login-url'
                ),
                 array(
                    'label' => 'name',
                    'shortcode' => 'name'
                ),

            ),
            'reset-password-email-to-user-NL' => array(
                array(
                    'label' => 'reset password link',
                    'shortcode' => 'reset_password_link'
                ),
                array(
                    'label' => 'user name',
                    'shortcode' => 'user_name'
                ),
                array(
                    'label' => 'user email',
                    'shortcode' => 'user_email'
                )
            ),
            'reset-password-email-to-user-FR' => array(
                array(
                    'label' => 'reset password link',
                    'shortcode' => 'reset_password_link'
                ),
                array(
                    'label' => 'user name',
                    'shortcode' => 'user_name'
                ),
                array(
                    'label' => 'user email',
                    'shortcode' => 'user_email'
                )
            ),
            'credit-purchase-email-to-admin-NL'=>array(
               array(
                    'label' => 'first name',
                    'shortcode' => 'first_name'
                ),
                array(
                    'label' => 'last name',
                    'shortcode' => 'last_name'
                ),
                array(
                    'label' => 'number of credits',
                    'shortcode' => 'number_of_credits'
                ),
                array(
                    'label' => 'submited on',
                    'shortcode' => 'submited_on'
                )
            ),
            'credit-purchase-email-to-admin-FR'=>array(
               array(
                    'label' => 'first name',
                    'shortcode' => 'first_name'
                ),
                array(
                    'label' => 'last name',
                    'shortcode' => 'last_name'
                ),
                array(
                    'label' => 'number of credits',
                    'shortcode' => 'number_of_credits'
                ),
                array(
                    'label' => 'submited on',
                    'shortcode' => 'submited_on'
                )
            ),
            'newsletter-subscription-email-to-user-NL'=>array(
                array(
                    'label' => 'cancel subscription link',
                    'shortcode' => 'cancel_subscription_link'
                )
            ),
            'newsletter-subscription-email-to-user-FR'=>array(
                array(
                    'label' => 'cancel subscription link',
                    'shortcode' => 'cancel_subscription_link'
                )
            )
        );

        if(isset($predefined_shortcodes[$email['form_name']]))
        {
            $form_fields = $predefined_shortcodes[$email['form_name']];
        }
    }

	if(is_array($form_fields) && count($form_fields) > 0)
    {
        foreach($form_fields as $f_key => $f )
        {
            $name = str_replace('-', '_', $f['shortcode']);
            
            if(isset($form_data[$name]))
            {
                $value = is_array($form_data[$name]) ? implode(', ', $form_data[$name]) : $form_data[$name]; 

                $email_body = str_replace('{' .$f['shortcode']. '}', $value, $email_body);
            }
        }
    }
?>
        
        <!--START main table-->         
        <table bgcolor="<?php echo $email['body_color'];?>" width="100%" border="0" cellpadding="0" cellspacing="0" style="background: <?php echo $email['body_color'];?>; padding: 0px; margin: 0px; border-collapse: collapse;">             
            <!--START Header image and view online link-->             
            <tr>                 
                <td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">                     
                    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">                         
                        <tr>                             
                            <td class="contentholdingtable" width="600" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">                                 
                                <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                    <!--START view online link--> 
                                    <tr> 
                                        <td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                            <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                <tr> 
                                                    <!--START CONTENT column--> 
                                                    <td class="fullwidthcontenttableinset" bgcolor="<?php echo $email['body_color'];?>" width="600" align="center" valign="top" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                            <tr> 
                                                                <td align="center" style="font-size: 0px; line-height: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                                                    <!--Table with border--> 
                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                                        <tr> 
                                                                            <td align="left" valign="middle" style="font-size: 0px; line-height: 0px; padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                                                                <!--Table with navi links--> 
                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                                                    <tr> 
                                                                                        <td class="view-online-link" align="center" valign="middle" style="color: #FF4704; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; line-height: 12px; padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                                                            <!--View online link-->
                                                                                            <!--dot--><!-- &#8226; -->
                                                                                            <!-- <span style="padding-right: 2px;">
                                                                                                <a href="<?php //echo isset($preview) ? 'javascript:void(0)' : '{view-online}'?>" title="Bekijk email..." target="_blank" style="color: #9FA0A4; text-decoration: none;">View this email in your browser | Bekijk deze email in uw browser</a>
                                                                                            </span>  -->                                                                                                                                                                                                                                                                                      
                                                                                            <!--dot--><!-- &#8226; -->                                                                                              
                                                                                        </td> 
                                                                                    </tr> 
                                                                                </table> 
                                                                            </td> 
                                                                        </tr> 
                                                                    </table> 
                                                                </td> 
                                                            </tr> 
                                                        </table> 
                                                    </td> 
                                                    <!--END CONTENT column--> 
                                                </tr> 
                                            </table> 
                                        </td> 
                                    </tr> 
                                    <!--END view online link--> 
                                    <!--START header image--> 
                                    <tr> 
                                        <td align="center" bgcolor="<?php echo $email['content_color'];?>" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                            <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                <tr> 
                                                    <td class="fullwidthcontenttableinset" width="600" align="center" style="font-size: 0px; line-height: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse; max-width: 600px; max-height: <?php echo $max_header_height.'px;';?>"> 
                                                        <!--Logo 276 goes here--><img class="img-600" src="<?php if(isset($header_url_image)){ echo $header_url_image;} ?>" width="600" alt="header" border="no" style="margin: 0px; padding: 0px; border: none; max-width: 600px; max-height: <?php echo $max_header_height.'px;';?>"/> 
                                                    </td> 
                                                </tr> 
                                            </table> 
                                        </td> 
                                    </tr> 
                                    <!--END header image-->                                     
                                </table> 
                            </td> 
                        </tr> 
                    </table> 
                </td> 
            </tr> 
            <!--END Header image and view online link-->
            <!--START newsletter content-->             
            <tr> 
                <td class="main-content" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                        <tr> 
                            <td class="contentholdingtable" bgcolor="<?php echo $email['content_color'];?>" width="600" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                    <!--START content holding table row--> 
                                    <tr> 
                                        <td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
                                        <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
                                          <tr><td height="10"></td></tr>
                                          <tr>
                                            <td style="width:2%"></td>
                                            <td style="word-wrap: break-word;">
                                            <?php echo $email_body; ?>
                                            </td>
                                            <td style="width:2%"></td>
                                          </tr>
                                        </table>
                                        </td> 
                                    </tr> 
                                    <!--END content holding table row--> 
                                </table> 
                            </td> 
                        </tr> 
                    </table> 
                </td> 
            </tr>                             
            <!--END newsletter content--> 
            <!--START Footer and unsubscribe link--> 
            <tr> 
                <td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                        <tr> 
                            <td class="contentholdingtable" width="600" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                    <!--START content holding table row--> 
                                    <tr> 
                                        <td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                            <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                <tr> 
                                                    <!--START CONTENT column--> 
                                                    <td class="fullwidthcontenttableinset" width="600" align="center" valign="top" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                            <!--START footer image--> 
                                                            <tr> 
                                                                <td bgcolor="<?php echo $email['content_color'];?>" align="center" height="10" style="padding: 0px; margin: 0px; border-collapse: collapse;"></td> 
                                                            </tr>
                                                            <tr> 
                                                                <td align="center" bgcolor="<?php echo $email['content_color'];?>" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                                                    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;"> 
                                                                        <tr> 
                                                                            <td class="fullwidthcontenttableinset" width="600" align="center" style="font-size: 0px; line-height: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse; max-width: 600px; max-height: <?php echo $max_footer_height.'px;';?>"> 
                                                                                <!--Logo 276 goes here--><img class="img-600" src="<?php if(isset($footer_url_image)){ echo $footer_url_image;}?>" width="600" alt="footer" border="no" style="margin: 0px; padding: 0px; border: none; max-width: 600px; max-height: <?php echo $max_footer_height.'px;';?>"/> 
                                                                            </td> 
                                                                        </tr> 
                                                                    </table> 
                                                                </td> 
                                                            </tr> 
                                                            <!--END footer image-->
                                                            <!--Unsubscribe link--> 
                                                            <tr> 
                                                                <td align="center" style="color: #A09FA5; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; line-height: 12px; padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px; margin: 0px; border-collapse: collapse;"> 
                                                                    <!--Unsubscribe link-->
                                                                    </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>