<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript" src="http://cdn.ckeditor.com/4.5.8/standard-all/ckeditor.js"></script>
<?php echo Assets::js("admin/js/jquery.bootstrap.wizard.js"); ?>
<?php echo Assets::js("admin/ckeditor/config.js"); ?>
<?php echo Assets::js("admin/ckeditor/adapters/jquery.js"); ?>

<style>
.chosen-container {
	width: 100% !important;
}
</style>
<?php
	$predefined_email_templates = array(
		'registration-email-to-admin-NL' => lang('registration_email_to_admin_NL'),
		'registration-email-to-admin-FR' => lang('registration_email_to_admin_FR'),
		'registration-email-to-user-NL' => lang('registration_email_to_user_NL'),
		'registration-email-to-user-FR' => lang('registration_email_to_user_FR'),
		'reset-password-email-to-user-NL' => lang('reset_password_email_to_user_NL'),
		'reset-password-email-to-user-FR' => lang('reset_password_email_to_user_FR'),
		'credit-purchase-email-to-admin-FR'=> lang('credit_purchase_email_to_admin_FR'),
		'credit-purchase-email-to-admin-NL'=> lang('credit_purchase_email_to_admin_NL'),
		'newsletter-subscription-email-to-user-NL'=> lang('newsletter_subscription_email_to_user_NL'),
		'newsletter-subscription-email-to-user-FR'=> lang('newsletter_subscription_email_to_user_FR')
	);
?>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">
		<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('email') ?></h1>
				</div>
			</div>
		</div><!-- / main header -->
		
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo lang('edit_email') ?>
				</div>
				<div class="panel-body">
					<form action="<?php echo site_url('admin/email/edit/'. $email['id'])?>" id="form-edit-email" method="post" enctype="multipart/form-data">
					<div class="wrapper" style="padding-left:0px">
					<div id="rootwizard">
						<div class="navbar">
							<div class="navbar-inner">
								<ul>
									<li><a href="#tab1" data-toggle="tab"><?php echo lang('select_templates') ?></a></li>
									<li><a href="#tab2" data-toggle="tab"><?php echo lang('email_body') ?></a></li>
									<li><a href="#tab3" data-toggle="tab"><?php echo lang('save') ?></a></li>
								</ul>
							</div>
						</div>
						
						<div id="bar" class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
						</div>

						<div class="tab-content">
							<div class="tab-pane" id="tab1">
								<div class="control-group">
									<div class="controls">
										<div class="row">
											<div class="col-sm-3">
												<label><?php echo lang('title') ?><span class="text-danger"> *</span> :</label>
											</div>
											<div class="col-sm-5">
											    <div class="form-group">
											        <input type="text" name="email_name" value="<?php echo $this->input->post('email_name') ? $this->input->post('email_name') : $email['title']; ?>" class="form-control">
													<?php if(form_error('email_name')) { ?>
														<div class="text-danger"><?php echo form_error('email_name'); ?></div>
													<?php } ?>
											    </div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label><?php echo lang('email_template') ?> :</label>
											</div>
											<div class="col-sm-5">
											    <div class="form-group">
											        <select ui-jq="chosen" name="email_template_id">
											        	<option value=""><?php echo lang('please_select'); ?></option>
											        	<?php

											        	if( ! empty($email_templates))
											        	{
											        		$email_template_id = $this->input->post('email_template_id') ? $this->input->post('email_template_id') : $email['email_template_id'];

											        		foreach ($email_templates as $et) {
											        			$selected = $email_template_id == $et['id'] ? 'selected' : '';
											        	?>
											        	<option value="<?php echo $et['id']; ?>" <?php echo $selected; ?>><?php echo $et['title']; ?></option>
											        	<?php
											        		}
											        	}
											        	?>
											        </select>
											        <span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
											    </div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label><?php echo lang('form') ?> :</label>
											</div>
											<div class="col-sm-5">
											    <div class="form-group">
											        <select ui-jq="chosen" name="form_id">
											        	<option value=""><?php echo lang('please_select'); ?></option>
											        	<?php

											        	if(is_array($predefined_email_templates) && count($predefined_email_templates) > 0)
											        	{
											        		$form_id = $this->input->post('form_id') ? $this->input->post('form_id') : $email['form_name'];
											        	?>
											        	<optgroup label="Predefined Email Templates and Forms">
											        	<?php
											        		foreach ($predefined_email_templates as $e_key => $e) {
											        			$selected = $form_id == $e_key ? 'selected' : '';
											        	?>
											        	<option value="<?php echo $e_key; ?>" data-form-type="predefined-email-template" <?php echo $selected; ?>><?php echo $e; ?></option>
											        	<?php
											        		}
											        	?>
											        	</optgroup>
											        	<?php
											        		}
											        	?>
											        	<?php

											        	if(is_array($forms) && count($forms) > 0)
											        	{
											        	?>
											        	<optgroup label="Custom forms">
											        	<?php
											        		$form_id = $this->input->post('form_id') ? $this->input->post('form_id') : $email['form_id'];

											        		foreach ($forms as $f) {
											        			$selected = $form_id == $f['id'] ? 'selected' : '';
											        	?>
											        	<option value="<?php echo $f['id']; ?>" <?php echo $selected; ?>><?php echo $f['title']; ?></option>
											        	<?php
											        		}
											        	}
											        	?>
											        </select>
											        <span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
											    </div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab2">
								<div class="control-group">
									<div class="controls">
										<div class="row">
											<div class="col-sm-12">
												<label><?php echo lang('shortcodes') ?> :</label>
												<div class="form-shortcodes"></div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-sm-3">
												<label><?php echo lang('subject') ?> :</label>
											</div>
											<div class="col-sm-5">
											    <div class="form-group">
											        <input type="text" name="subject" class="form-control" value="<?php echo $this->input->post('subject') ? $this->input->post('subject') : $email['subject']; ?>">
											    </div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label><?php echo lang('body') ?> :</label>
											</div>
											<div class="col-sm-9">
											    <div class="form-group">
											        <textarea name="body" id="editor1" class="form-control"><?php echo $this->input->post('body') ? $this->input->post('body') : $email['email_body']; ?></textarea>
											        <script type="text/javascript">
												      CKEDITOR.replace( 'editor1' );
												      CKEDITOR.add            
												   	</script>
											    </div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab3">
								<div class="control-group">
									<div class="controls">
										<div class="row">
											<div class="col-md-12">
												<button type="submit" class="btn btn-sm btn-primary edit-email"><?php echo lang('save'); ?></button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<ul class="pager wizard">
							<li class="previous first" style="display:none;"><a href="#"><?php echo lang('first') ?></a></li>
							<li class="previous"><a href="#"><?php echo lang('previous') ?></a></li>
							<li class="next last" style="display:none;"><a href="#"><?php echo lang('last') ?></a></li>
							<li class="next"><a href="#"><?php echo lang('next') ?></a></li>
						</ul>
					</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){

	$("#form-edit-email").validate({
        rules:{
               'email_name': "required",
               'email_template_id': "required",
               'form_id': "required",
               'subject': "required"
            },
        messages:{
        		'email_name': {
				      required: "Please enter title"
				    },
				'email_template_id': {
					required : 'Please select email template'
				},
				'form_id': {
					required : 'Please select form'
				},
				'subject': {
					required : 'The subject field is required'
				}
            }
    });

	$('#rootwizard').bootstrapWizard({
		onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard .progress-bar').css({width:$percent+'%'});
        },
        'onNext': function(tab, navigation, index) {

        	var form = $("#form-edit-email");
        	form.validate();
            var valid = form.valid();

            switch( index )
			{
				case 1:
					var email_template = form.find('select[name="email_template_id"]');
					var email_template_id = email_template.val();
					
					if( email_template_id == '' )
					{
						var text = email_template.siblings('.text-danger').data('text');
						email_template.siblings('.text-danger').html(text);
						valid = false;
					}
					else
					{
						email_template.siblings('.text-danger').html('');
					}

					var form_template = form.find('select[name="form_id"]');
					var form_id = form_template.val();
					
					if( form_id == '' )
					{
						var text = form_template.siblings('.text-danger').data('text');
						form_template.siblings('.text-danger').html(text);
						valid = false;
					}
					else
					{
						form_template.siblings('.text-danger').html('');
					}

					break;

				case 2 :
					break;

				case 3 :
					break;
				
				default : break;
			}

            if( ! valid)
            {
                return false;
            }
            else
            {
            	if(index == 1)
                {
                	var form_id = form.find('select[name="form_id"]').val();
                	var form_type = form.find('select[name="form_id"] option:selected').attr('data-form-type');

					$.ajax({
						url : site_url('admin/form/ajax_get_form_fields'),
						data : {
							form_id : form_id,
							form_type : form_type
						},
						dataType : 'json',
						type : 'POST',
						success : function(response) {
							
							if(response.rc)
							{
								$('.form-shortcodes').html(response.html);
							}
							else
							{
								
							}
						}
					});
                }
            }
        },
        'onTabClick': function(tab, navigation, index) {
            return false;
        }
	});

	/*$('.add-email').click(function(){
	
		$.ajax({
			url : site_url('admin/email/ajax_add'),
			data : {
				 : 
			},
			dataType : 'json',
			type : 'POST',
			success : function(response) {
				
			}
		});

	});*/
});
</script>