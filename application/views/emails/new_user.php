<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
  <tr><td height="10"></td></tr>
  <tr>
    <td style="width:1%"></td>
    <td>
      <p><?php echo lang("hi") ?>, <?php echo $data['first_name']." ".$data['last_name'];?>
          <br><br>
          <span>
            <?php echo lang("new_user_added");?>.
            <br><br>
            <?php echo lang('login_to_check_your_account') ?>.
            <br>
            <?php echo base_url('user/login') ?>
          </span>
          <br>
        </p>
    <td style="width:1%"></td>
  </tr>
    <tr>
      <td style="width:1%"></td>
      <td><br><?php echo lang("with_kind_regards") ?><br><?php echo WEBSITE_NAME;?></td>
      <td style="width:1%"></td>
    </tr>
</table>