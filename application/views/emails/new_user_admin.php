<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
  <tr><td height="10"></td></tr>
  <tr>
    <td style="width:1%"></td>
    <td>
      <p><?php echo lang("hi") ?>, admin
          <br><br>
          <span>
            <?php echo lang("new_user_added_admin_body");?>:
            <br><br>
            <?php echo lang('name')." : ".$data['first_name']." ".$data['last_name'];?>
            <br>
            <?php echo lang('email')." : ".$data['email'];?>
          </span>
          <br><br>
        </p>
    <td style="width:1%"></td>
  </tr>
    <tr>
      <td style="width:1%"></td>
      <td><br><?php echo lang("with_kind_regards") ?><br><?php echo WEBSITE_NAME; ?></td>
      <td style="width:1%"></td>
    </tr>
</table>