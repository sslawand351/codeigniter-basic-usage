<?php

$this->load->view("emails/layout/header");

if(isset($_view) && $_view != "")
{
	$this->load->view($_view);
}

$this->load->view("emails/layout/footer");