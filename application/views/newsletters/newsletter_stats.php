<?php echo Assets::js("admin/js/jquery.bootstrap.wizard.js"); ?>
<?php echo Assets::css("admin/css/bootstrap-chosen.css"); ?>
<?php echo Assets::js("admin/ckeditor/ckeditor.js"); ?>
<?php echo Assets::js("admin/ckeditor/samples/js/sample.js"); ?>
<?php echo Assets::js("admin/js/custom/send_newsletter.js"); ?>
<?php echo Assets::css("admin/css/custom/newsletter_template_listing.css"); ?>

<style type="text/css">
.chosen-container {
    width: 100%!important;
}
.modal-header h3
{
	color: #58666e !important;
}
td.splitedcontenttableinset {
    padding: 10px;
}
#send-newsletter-popup .modal-dialog, #viewNewsletterTemplate .modal-dialog
{
	width:55%;
}
@media (max-width:768px)
{
	#send-newsletter-popup .modal-dialog, #viewNewsletterTemplate .modal-dialog
	{
		width:96%;
	}
}
</style>

<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('newsletter_stats') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				
				<div class="alert alert-success" style="<?php if(!$this->session->flashdata('success_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <span id="org_success_msg"><?php echo $this->session->flashdata('success_msg'); ?></span>
				</div>
				
				           
				<div class="alert alert-danger" style="<?php if(!$this->session->flashdata('error_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="org_error_msg"><?php echo $this->session->flashdata('error_msg'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="#send-newsletter-popup" data-toggle="modal" class="btn btn-primary btn-xs pull-right open-send-newsletter"><?php echo lang('send_newsletter') ?></a>
					
					<?php

						if( isset($opens_list_view) && $opens_list_view && isset($opens_list[0]['subject']))
						{
							echo lang('subject') . ' : '. $opens_list[0]['subject'];
						}
						else if ( isset($clicks_list_view) && $clicks_list_view && isset($clicks_list[0]['subject']))
						{
							echo lang('subject') . ' : '. $clicks_list[0]['subject'];
						}
						else if ( isset($sent_list_view) && $sent_list_view && isset($sent_list[0]['subject']))
						{
							echo lang('subject') . ' : '. $sent_list[0]['subject'];
						}
						else if ( isset($unsubscribe_list_view) && $unsubscribe_list_view && isset($unsubscribe_list_view[0]['subject']))
						{
							echo lang('subject') . ' : '. $unsubscribe_list_view[0]['subject'];
						}
						else
						{
							echo lang('list_of_newsletter_sent');
						}
					?>
				</div>
				<?php 
					if( isset($opens_list_view) && $opens_list_view )
					{
						$this->load->view('newsletters/_table_newsletter_opens');
					}
					else if ( isset($clicks_list_view) && $clicks_list_view )
					{
						$this->load->view('newsletters/_table_newsletter_clicks');
					}
					else if ( isset($sent_list_view) && $sent_list_view )
					{
						$this->load->view('newsletters/_table_newsletter_sent');
					}
					else if ( isset($bounce_list_view) && $bounce_list_view )
					{
						$this->load->view('newsletters/_table_newsletter_bounce');
					}
					else if ( isset($reject_list_view) && $reject_list_view )
					{
						$this->load->view('newsletters/_table_newsletter_reject');
					}
					else if ( isset($unsubscribe_list_view) && $unsubscribe_list_view )
					{
						$this->load->view('newsletters/_table_newsletter_unsubscribe');
					}
					else
					{
						// load table view
						$this->load->view('newsletters/_table_newsletter_stats');
					}
				?>
				<footer class="panel-footer">
			  		<div class="row">
						<div class="col-sm-12 text-right text-center-xs">
						<?php if( ! isset($opens_list_view) && ! isset($clicks_list_view) && ! isset($sent_list_view) && ! isset($bounce_list_view) && ! isset($reject_list_view) && ! isset($unsubscribe_list_view)) { ?>
							<?php echo $links; ?>
							<?php } ?>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
</div>

<!-- View newsletter template -->
<div class="modal fade" id="viewNewsletterTemplate" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3><?php echo lang('newsletter_template') ?></h3>
			</div>
			<div class="modal-body">
				<div class="message"></div>
				<!-- <div class="newsletter-template-view">
				</div> -->
				<iframe src="javascript:void(0)" frameborder="0" id="idIframe" onload="iframeLoaded(this)" class="newsletter-template-view" width="100%" height="100%" style="overflow:hidden;width:100%"></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
			</div>
		</div>
	</div>
</div><!-- End of View newsletter template -->

<!-- Send Newsletter Form wizard -->
<div class="modal fade" id="send-newsletter-popup" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3><?php echo lang('send_newsletter') ?></h3>
			</div>
			<div class="modal-body">
				<div class="message"></div>
				<form id="form-send-newsletter" method="post" action="<?php echo site_url("setting/send-newsletter"); ?>">
					<div id="rootwizard">
						<div class="navbar">
							<div class="navbar-inner">
								<ul>
									<li><a href="#tab1" data-toggle="tab"><?php echo lang('select_newsletter') ?></a></li>
									<li><a href="#tab2" data-toggle="tab"><?php echo lang('select_groups_and_users') ?></a></li>
									<li><a href="#tab3" data-toggle="tab"><?php echo lang('email_body') ?></a></li>
									<li><a href="#tab4" data-toggle="tab"><?php echo lang('email_preview') ?></a></li>
									<li><a href="#tab5" data-toggle="tab"><?php echo lang('send') ?></a></li>
								</ul>
							</div>
						</div>
						
						<div id="bar" class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
						</div>
			            <div class="credits-error"></div>
						<div class="tab-content">
							<div class="tab-pane" id="tab1">
								<div class="control-group">
									<div class="controls">
										<div class="row">
											<div class="form-group col-md-12">
												
												<br><label class="control-label" for="newsletter_templates"><?php echo lang('select_newsletter') ?><span class="text-danger">*</span></label>
											</div>
											<div class="form-group col-md-12">
												<select class="w-md form-control required" id="newsletter-template-list" name="newsletter_template_id" ui-jq="chosen">
													<?php foreach($newsletter_templates as $nt) { ?>
														<option value="<?php echo $nt['id']; ?>" data-channel-id="<?php echo $nt['channel_id']; ?>"><?php echo $nt['template_name']; ?></option>
													<?php } ?>													
												</select>
												<span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab2">
								<div class="control-group">
									<div class="controls">
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab3">
								<div class="control-group">
	                                <div class="controls">
	                                    <div class="row">
	                                        <div class="col-md-6">
	                                           <div class="form-group"> 
	                                           
	                                                <label class="control-label" for="from_email"><?php echo ucfirst(lang('from')); ?><span class="text-danger"> *</span> :</label>
	                                                <input type="text" class="form-control required" name="from_email" value="">
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="row">
	                                        <div class="col-md-12">
	                                           <div class="form-group"> 
	                                           
	                                                <label class="control-label" for="subject"><?php echo lang('subject') ?><span class="text-danger"> *</span> :</label>
	                                                <input type="text" class="form-control required" name="subject" value="">
	                                            </div>
	                                        </div>
	                                    </div>

	                                </div>
                            	</div>
							</div>
							<div class="tab-pane" id="tab4">
								<div class="control-group">
	                                <div class="controls">
	                                    <div class="row">
	                                        <div class="col-md-12">
	                                           <div class="form-group"> 
	                                           
	                                                <label class="control-label" for="email_preview"><?php echo lang('email_preview') ?><span class="text-danger"> *</span> :</label>
	                                                <!-- <div class="email_preview_html">
	                                                </div> -->
	                                                <iframe src="javascript:void(0)" frameborder="0" id="idIframe" onload="iframeLoaded(this)" class="newsletter-template-view" width="100%" height="100%" style="overflow:hidden;width:100%"></iframe>
	                                            </div>
	                                        </div>
	                                    </div>

	                                </div>
                            	</div>
							</div>
							
								<div class="tab-pane" id="tab5">
									<a class="btn btn-success submit-send-newsletter-form" id="send-email-pdf"><i class="fa fa-file"></i>&nbsp;&nbsp;<?php echo lang('send_email') ?></a>
								</div>
								<ul class="pager wizard">
									<li class="previous first" style="display:none;"><a href="#"><?php echo lang('first') ?></a></li>
									<li class="previous"><a href="#"><?php echo lang('previous') ?></a></li>
									<li class="next last" style="display:none;"><a href="#"><?php echo lang('last') ?></a></li>
									<li class="next"><a href="#"><?php echo lang('next') ?></a></li>
								</ul>
							</div>
						</div>
					</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close_modal generate_proposal_close" data-dismiss="modal"><?php echo lang('close') ?></button>
			</div>
		</div>
	</div>
</div><!-- End of Send Newsletter Form wizard -->