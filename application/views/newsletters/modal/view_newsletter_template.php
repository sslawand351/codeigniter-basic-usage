<!-- View newsletter template -->
<div class="modal fade" id="viewNewsletterTemplate" data-backdrop="static">
	<div class="modal-dialog" style="width:55%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3><?php echo lang('newsletter_template') ?></h3>
			</div>
			<div class="modal-body">
				<div class="message"></div>
				<!-- <div class="newsletter-template-view">
				</div> -->
				<iframe src="javascript:void(0)" frameborder="0" id="idIframe" onload="iframeLoaded(this)" class="newsletter-template-view" width="100%" height="100%" style="overflow:hidden;width:100%"></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
			</div>
		</div>
	</div>
</div><!-- End of View newsletter template -->