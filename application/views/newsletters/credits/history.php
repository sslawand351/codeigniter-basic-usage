<?php echo Assets::js('admin/js/custom/newsletter_credits.js'); ?>
<!-- content -->
 <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
		<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('credits') ?></h1>
				</div>
			</div>
		</div><!-- / main header -->
		
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo lang('credits') ?>
				</div>
				<div class="panel-body">
					<form action="<?php echo site_url('form/add')?>" id="edit_form" method="post" enctype="multipart/form-data">
					<div class="wrapper" style="padding-left:0px">
					    <ul id="myTabs" class="nav nav-tabs" role="tablist">
					        <li role="presentation" class="active"><a href="#info" id="info-tab" role="tab" data-toggle="tab" aria-controls="info" aria-expanded="true"><?php echo lang('incoming_credits') ?></a></li>
					        <li role="presentation" class=""><a href="#configuration" id="configuration-tab" role="tab" data-toggle="tab" aria-controls="configuration" aria-expanded="true"><?php echo lang('outgoing_credits') ?></a></li>
					    </ul>
					    <br>
					    <div id="myTabContent" class="tab-content">
					        <div role="tabpanel" class="main_tabs tab-pane fade in active" id="info" aria-labelledBy="info-tab">
					           <div class="purchased-table-container"></div>
							</div>

							<div role="tabpanel" class="main_tabs tab-pane fade" id="configuration" aria-labelledBy="technical-tab">
					        	<div class="consumed-table-container"></div>
					        </div>
					    </div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>