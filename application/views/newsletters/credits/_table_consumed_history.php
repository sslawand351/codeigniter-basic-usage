<div class="consumed-table-container">
<br>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th></th>
				<th><?php echo lang('source'); ?></th>
				<th><?php echo lang('credits') ?></th>
				<th><?php echo lang('consumed_on') ?></th>
			</tr>
			<?php if(!empty($consumed_credits) ) { ?>
			<?php
				foreach($consumed_credits as $value)
				{
			?>
			<tr>
				<td><?php echo ++$offset; ?></td>
				<td><?php echo $value['username']; ?></td>
				<td><?php echo $value['credits_amount']; ?></td>
				<td><?php echo date('d-m-Y G:i:s', strtotime($value['consumed_date'])); ?></td>
			</tr>
			<?php } ?>
			<?php } else{ ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_credits_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
</div>