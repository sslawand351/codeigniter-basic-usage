<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('credits') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				
				<div class="alert alert-success" style="<?php if( ! $this->session->flashdata('success_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <span id="org_success_msg"><?php echo $this->session->flashdata('success_msg'); ?></span>
				</div>
				
				           
				<div class="alert alert-danger" style="<?php if( ! $this->session->flashdata('error_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="org_error_msg"><?php echo $this->session->flashdata('error_msg'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="<?php echo site_url('newsletter/credits/history'); ?>" class="btn btn-primary btn-xs pull-right"><?php echo lang('view_history') ?></a>
					<?php echo lang('credits'); ?>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-6">
						<div class="panel-heading">
							<h4><?php echo lang('credit_status'); ?></h4>
						</div>
						<div class="table-responsive">		
							<table class="tftable table table-striped m-b-none">
								<tr>
									<th></th>
									<th><?php echo lang('credits'); ?></th>
								</tr>
								<!-- <tr>
									<td><?php echo lang('free_credits_available'); ?></td>
									<?php $credit = isset($credits['free_credits_available']) ? $credits['free_credits_available'] : 0; ?>
									<td><?php echo $credit; ?></td>
								</tr> -->
								<?php

									$free_credits_available = isset($credits['free_credits_available']) ? $credits['free_credits_available'] : 0;
									$paid_credits_available = isset($credits['credits_available']) ? $credits['credits_available'] : 0;
									$total_credits_available = $free_credits_available + $paid_credits_available;

								?>
								<tr>
									<td><?php echo lang('total_credits_available'); ?></td>
									<?php $credit = isset($credits['credits_available']) ? $credits['credits_available'] : 0; ?>
									<td><?php echo $total_credits_available; ?></td>
								</tr>
								<tr>
									<td><?php echo lang('total_consumed'); ?></td>
									<?php $credit = isset($credits['credits_used']) ? $credits['credits_used'] : 0; ?>
									<td><?php echo $credit; ?></td>
								</tr>
							</table>
						</div>
					</div>
					<?php if($this->user['type'] != 'master admin') { ?>
					<div class="col-sm-6">
						<div class="panel-heading">
							<h4><?php echo lang('purchase_credits'); ?></h4>
						</div>
						<br>
						<form action="<?php echo site_url('newsletter/credits/purchase'); ?>" method="post">
						<div class="col-md-12">
				           <div class="form-group">
				                <label class="control-label" for="number_of_credits"><?php echo lang('number_of_credits') ?><span class="text-danger"> *</span> :</label>
				                <select name="number_of_credits" class="required form_control" ui-jq="chosen" style="width:50%;">
				                    <option value="" selected><?php echo lang('please_select'); ?></option>
				                    <option value="1000">1000</option>
				                    <option value="5000">5000</option>
				                    <option value="10000">10000</option>
				                    <option value="15000">15000</option>
				                    <option value="20000">20000</option>
				                    <option value="25000">25000</option>
				                    <option value="30000">30000</option>
				                    <option value="50000">50000</option>
				                </select>
								<span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
				            </div>
				        </div>
				        <div class="col-md-12">
				        	<input type="submit" class="btn btn-glow primary" value="<?php echo lang('purchase'); ?>">
				        </div>
				        </form>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>