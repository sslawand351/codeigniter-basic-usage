<div class="row">
	<div class="form-group col-md-12">
		<span class="text-danger group-user-error" data-text="<?php echo lang('please_select_atleast_one_user_or_one_group'); ?>"></span>
		<br><label class="control-label" for="groups"><?php echo lang('select_groups') ?><span class="text-danger">*</span></label>
	</div>
	<div class="form-group col-md-12">
		<select multiple class="w-md form-control required" id="group-list" name="groups[]" ui-jq="chosen">
			<?php foreach($groups_has_users as $g) { ?>
				<option value="<?php echo $g['id']; ?>"><?php echo $g['name']; ?></option>
			<?php } ?>													
		</select>
		<span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
	</div>
</div>
<div class="row">
	<div class="form-group col-md-12"> 
		<label class="control-label" for="street"><?php echo lang('select_users') ?> <span class="text-danger">*</span></label>
	</div>
	<div class="form-group col-md-12">
		<select multiple class="w-md form-control required" name="users[]" id="lead-list" ui-jq="chosen">
			<?php if(is_array($users) && count($users) > 0) { ?>
			<?php foreach($users as $u) { ?>
				<option value="<?php echo $u['id']; ?>" data-userid="<?php echo $u['id']; ?>" data-email="<?php echo $u['email']; ?>"><?php echo $u['first_name'] . ' ' . $u['last_name'] . " ( " .$u['email']. " )" ; ?></option>
			<?php } ?>
			<?php } ?>
			
		</select>
		<span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
	</div>
</div>