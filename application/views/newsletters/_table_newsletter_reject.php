<div class="table-container">
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th><?php echo lang('name'); ?></th>
				<th><?php echo lang('email'); ?></th>
				<th>SENT AT</th>
				<th>STATUS</th>
			</tr>
			<?php if(!empty($reject_list) && $reject_list!='') { ?>
			<?php
				foreach($reject_list as $value)
				{
			?>
			<tr>
				<td><?php echo $value['to_name']; ?></td>
				<td><?php echo $value['to_email']; ?></td>
				<td>
					<?php if($value['sent_on'] != '0000-00-00 00:00:00' ) { ?>
					<?php echo date('d-m-Y G:i:s', strtotime($value['sent_on'])); ?>
					<?php } ?>
				</td>
				<td><?php echo ucfirst($value['event']); ?></td>
			</tr>
			<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_data_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	
</div>