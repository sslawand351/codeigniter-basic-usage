<?php echo Assets::js("admin/js/jquery.bootstrap.wizard.js"); ?>
<?php echo Assets::css("admin/css/bootstrap-chosen.css"); ?>
<?php echo Assets::js("admin/ckeditor/ckeditor.js"); ?>
<?php echo Assets::js("admin/ckeditor/samples/js/sample.js"); ?>
<?php echo Assets::js("admin/js/custom/newsletter_template.js"); ?>
<?php echo Assets::css("admin/css/custom/newsletter_template_listing.css"); ?>
<style type="text/css">
.chosen-container {
    width: 100%!important;
}
.modal-header h3
{
	color: #58666e !important;
}
td.splitedcontenttableinset {
    padding: 10px;
}
#send-newsletter-popup .modal-dialog, #viewNewsletterTemplate .modal-dialog
{
	width:55%;
}
@media (max-width:768px)
{
	#send-newsletter-popup .modal-dialog, #viewNewsletterTemplate .modal-dialog
	{
		width:96%;
	}
}
</style>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('newsletters') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				<div class="alert alert-success" style="<?php if(!$this->session->flashdata('success_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <span id="org_success_msg"><?php echo $this->session->flashdata('success_msg'); ?></span>
				</div>
				
				           
				<div class="alert alert-danger" style="<?php if(!$this->session->flashdata('error_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="org_error_msg"><?php echo $this->session->flashdata('error_msg'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="#build-newsletter-zone-popup" data-toggle="modal" class="btn btn-primary btn-xs pull-right build-zone"><?php echo lang('add_zone') ?></a>
					
					<a href="#viewNewsletterTemplate" data-toggle="modal" class="btn btn-success btn-xs pull-right view-newsletter-template" data-newsletter-template-id="<?php echo $newsletter['id'];?>" style="margin-right: 5px;"><?php echo lang('view') ?></a>
					

					<?php echo lang('list_of_zones_of_newsletter_template') ?><?php echo $newsletter['template_name']; ?>
				</div>
				<div class="panel-body">
					<?php $this->load->view('newsletters/_table_newsletter_zones'); ?>

				</div>
				
			</div>
		</div>
	</div>
</div>

<!-- Send Newsletter Form wizard -->
<div class="modal fade" id="build-newsletter-zone-popup" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3><?php echo lang('add_zone') ?></h3>
			</div>
			<div class="modal-body">
				<div class="message"></div>
				<form id="form-build-newsletter-zone" method="post" action="<?php echo site_url("setting/send-newsletter"); ?>">
				<input type="hidden" name="newsletter_template_id" value="<?php echo $newsletter['id']; ?>">
				<input type="hidden" name="update_flag" value="0">
                	<div class="row">
                        <div class="col-md-6">
                           <div class="form-group"> 
                           
                                <label class="control-label" for="zone_name"><?php echo ucfirst(lang('name')); ?><span class="text-danger"> *</span> :</label>
                                <input type="text" class="form-control required" name="zone_name" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group"> 
                           
                                <label class="control-label" for="layout"><?php echo lang('zone_type') ?><span class="text-danger"> *</span> :</label>
                                <select name="zone_type" class="required form_control" id="newsletter-zone-layout" ui-jq="chosen">
                                	<option value="" selected><?php echo lang('please_select'); ?></option>
                                	<option value="4"><?php echo lang('zone_layout_4') ?></option>
                                    <!-- <option value="1"><?php echo lang('zone_layout_1') ?></option>
                                    <option value="2"><?php echo lang('zone_layout_2') ?></option>
                                    <option value="3"><?php echo lang('zone_layout_3') ?></option> -->          
                                </select>
								<span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row newsletter-text-zone" style="display: none;">
                        <div class="col-md-12">
                           <div class="form-group"> 
                           
                                <label class="control-label" for="body"><?php echo lang('body') ?><span class="text-danger"> *</span> :</label>
                                <textarea id="editor" cols="20" rows="5" class="form-control ck-editor" name="zone_body"></textarea>
                                <span class="text-danger" data-text="<?php echo lang('this_field_is_required'); ?>"></span>
                            </div>
                        </div>
                    </div>

                    <div id="review-newsletter-new-zone">
                    </div>
	                                
				</form>

			</div>
			<div class="modal-footer">
				<?php /* ?>
				<button style="margin-bottom:10px;margin-top:10px;" data-email-preview="1" class="btn btn-info" id="newsletter-email-preview"><?php echo lang('email_preview'); ?></button>
				<?php */ ?>
				<button style="margin-bottom:10px;margin-top:10px;"  data-email-preview="0" class="btn btn-success search-button submit-newsletter-build-zone" id="newsletter-build-zone"><?php echo lang('save'); ?></button>
				<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
			</div>
		</div>
	</div>
</div><!-- End of Send Newsletter Form wizard -->

<!-- Delete newsletter zone modal -->
<div id="delete_zone_popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('delete_newsletter_zone') ?></h3>
			</div>
			<form id="delete_newsletter_zone_form" action="<?php echo site_url('newsletter/delete_newsletter_zone'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" id="newsletter-template-id" name="newsletter_template_id" value="<?php echo $newsletter['id']; ?>">
		    		<input type="hidden" class="newsletter_zone_id" name="newsletter_zone_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    				<?php echo lang('are_you_sure_you_want_to_delete_this_zone') ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="delete_photo_btn btn btn-default"><?php echo lang('ok') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('cancel') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of Delete newsletter zone modal -->

<?php $this->load->view('newsletters/modal/view_newsletter_template'); ?>

<script>
// Initialise ckeditor
initSample();

// update textarea on change of ckeditor text
for (var i in CKEDITOR.instances) {
        
        CKEDITOR.instances[i].on('change', function() { CKEDITOR.instances[i].updateElement() });
        
}
</script>