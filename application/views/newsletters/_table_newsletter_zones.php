<div class="table-container">
	<div class="row">
		<div class="col-md-8">
			<ul id="sort-newsletter-zones" class="list-group gutter list-group-lg list-group-sp" >
				<?php 
				if ( ! empty($newsletter_zones))
				{
					foreach ($newsletter_zones as $z) :
					?>

					<li class="list-group-item li-img" id="data_<?php echo $z['id']; ?>" >
						<label class="i-checks">

			                <span class = "img-name" style="margin-left:0px;"><?php echo $z['zone_name']?></span>
			                <?php /*if( check_permission('car_image_delete') ) {*/ ?>
			                <a href="#build-newsletter-zone-popup" data-toggle="modal" class="btn btn-primary btn-xs pull-right edit-newsletter-zone" data-newsletter-zone-id="<?php echo $z['id']; ?>"><?php echo lang('edit') ?></a>
							<a href="#delete_zone_popup" data-toggle="modal" data-newsletter-zone-id="<?php echo $z['id']; ?>"  class="delete_newsletter_zone btn m-b-xs btn-xs btn-danger"><i class="fa fa-times"></i></a>
							<?php /*}*/ ?>
			            </label>
					</li>
					<?php endforeach; ?>
				<?php } else { ?>
					<li class="list-group-item li-img"><?php echo lang('no_data_found') ?></li>
				<?php } ?>
			</ul>
		</div>
		<div class="col-md-4">
			
		</div>
	</div>
</div>