<div class="table-container">
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th><?php echo lang('subject'); ?></th>
				<th><?php echo lang('email_body'); ?></th>
				<th>SENT ON</th>
				<th>SENT</th>
				<th>OPENS</th>
				<th>CLICKS</th>
				<!-- <th>REJECTED</th>-->
				<th>BOUNCED</th>
				<th>UNSUBSCRIBED</th>
			</tr>
			<?php if(!empty($stats) && $stats!='') { ?>
			<?php
				foreach($stats as $value)
				{
			?>
			<tr>
				<td><?php echo $value['subject']; ?></td>
				<td>
					<a href="#viewNewsletterTemplate" data-toggle="modal" class="btn btn-info btn-xs open-view-newsletter-template-popup" data-newsletter-template-id="<?php echo $value['id']; ?>"><?php echo lang('view') ?></a>
				</td>
				<td>
					<?php if($value['added_on'] != '0000-00-00 00:00:00' ) { ?>
					<?php echo date('d-m-Y G:i:s', strtotime($value['added_on'])); ?>
					<?php } ?>
				</td>
				<td>
					<a href="<?php echo site_url('newsletter/sent/'.$value['id']); ?>"><?php echo $value['sent_count']; ?></a>
				</td>
				<td>
					<a href="<?php echo site_url('newsletter/open/'.$value['id']); ?>"><?php echo $value['open_count']; ?></a>
				</td>
				<td>
					<a href="<?php echo site_url('newsletter/click/'.$value['id']); ?>"><?php echo $value['click_count']; ?></a>
				</td>
				<?php /* ?>
				<td>
					<a href="<?php echo site_url('newsletter/reject/'.$value['id']); ?>"><?php echo $value['reject_count']; ?></a>
				</td>
				<?php */ ?>
				<td>
					<a href="<?php echo site_url('newsletter/bounce/'.$value['id']); ?>"><?php echo $value['bounce_count']; ?></a>
				</td>
				<td>
					<a href="<?php echo site_url('newsletter/unsubscribe_list/'.$value['id']); ?>"><?php echo $value['unsubscribed_count']; ?></a>
				</td>
				<td style="display:none" class="newsletter_template_html" data-newsletter-template-id="<?php echo $value['id']; ?>"><?php echo htmlentities($value['html']); ?></td>
			</tr>
			<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="10" style="text-align:center;"><?php echo lang('no_data_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	
</div>