<div class="table-container">
<br>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th></th>
				<th><?php echo lang('name'); ?></th>
				<th><?php echo lang('email'); ?></th>
				<th>SENT AT</th>
				<th>STATUS</th>
			</tr>
			<?php if(!empty($bounce_list) && $bounce_list!='') { ?>
			<?php
				foreach($bounce_list as $value)
				{
			?>
			<tr>
				<td><?php echo ++$offset; ?></td>
				<td><?php echo $value['to_name']; ?></td>
				<td><?php echo $value['to_email']; ?></td>
				<td>
					<?php if($value['sent_on'] != '0000-00-00 00:00:00' ) { ?>
					<?php echo date('d-m-Y G:i:s', strtotime($value['sent_on'])); ?>
					<?php } ?>
				</td>
				<td><?php echo $value['event'] == 'hard_bounce' ? 'Hard Bounce' : 'Soft Bounce' ; ?></td>
			</tr>
			<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_data_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
</div>