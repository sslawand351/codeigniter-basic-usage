<section id="locationListing">
            
    <div class="container">
        <div class="row location-list-container">
            <div class="col-xs-12 col-sm-9">
                <div class="row page_heading">
                    <div class="col-sm-8 col-xs-12 ">
                        <div class="main_heading">
                            <?php echo strtoupper(lang('unsubscription_successful'));?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-us-details">
                <div class="col-sm-12 col-xs-12 divider">
                    <!-- <div class='div-heading'><b><?php //echo strtoupper(lang('newsletter_unsubscription'));?></b></div> -->
                    <div class="content"><?php echo lang('newsletter_unsubscription_description') ?></div>
                </div>
            </div>
        </div>
    </div>
</section>