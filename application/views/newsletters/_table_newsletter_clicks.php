<div class="table-container">
<br>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th></th>
				<th>Name</th>
				<th>Email</th>
				<th>SENT AT</th>
				<th>CLICK COUNT</th>
				<th>CLICKED AT</th>
			</tr>
			<?php if(!empty($clicks_list) && $clicks_list!='') { ?>
			<?php
				foreach($clicks_list as $value)
				{
			?>
			<tr>
				<td><?php echo ++$offset; ?></td>
				<td><?php echo $value['to_name']; ?></td>
				<td><?php echo $value['to_email']; ?></td>
				<td>
					<?php if($value['sent_on'] != '0000-00-00 00:00:00' ) { ?>
					<?php echo date('d-m-Y G:i:s', strtotime($value['sent_on'])); ?>
					<?php } ?>
				</td>
				<td><?php echo $value['click_count']; ?></td>
				<td>
					<?php
					 	$clicked_time = unserialize($value['clicked_time']);
					 	//pr($clicked_time);
					 	if( ! empty($clicked_time) )
					 	{
					 		foreach ($clicked_time as $key => $c)
					 		{
					 			echo date('d-m-Y G:i:s', strtotime($c['time']) ) . ' -- '.$c['link'].' <br>';
					 		}
					 	} 
					?>
				</td>
			</tr>
			<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_data_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
</div>