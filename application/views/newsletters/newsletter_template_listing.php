<?php echo Assets::js("admin/js/jquery.bootstrap.wizard.js"); ?>
<?php echo Assets::css("admin/css/bootstrap-chosen.css"); ?>
<?php echo Assets::js("admin/ckeditor/ckeditor.js"); ?>
<?php echo Assets::js("admin/ckeditor/samples/js/sample.js"); ?>
<?php echo Assets::js("admin/js/custom/newsletter_template.js"); ?>
<?php echo Assets::css("admin/css/custom/newsletter_template_listing.css"); ?>
<style type="text/css">
.chosen-container {
    width: 100%!important;
}
.modal-header h3
{
	color: #58666e !important;
}
td.splitedcontenttableinset {
    padding: 10px;
}
</style>

<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('newsletters') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				
				<div class="alert alert-success" style="<?php if(!$this->session->flashdata('success_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="success-msg" data-msg="<?php echo lang('newsletter_template_created_successfully'); ?>"><?php echo $this->session->flashdata('success_msg'); ?></span>
				</div>

				<div class="alert alert-danger" style="<?php if(!$this->session->flashdata('error_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="org_error_msg"><?php echo $this->session->flashdata('error_msg'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="#send-newsletter-popup" data-toggle="modal" class="btn btn-primary btn-xs pull-right open-send-newsletter"><?php echo lang('create_newsletter') ?></a>
					
					<?php echo lang('newsletter_listing') ?>
				</div>
				<?php 
					// load table view
					$this->load->view('newsletters/_table_newsletter_templates');
				?>
				<footer class="panel-footer">
			  		<div class="row">
						<div class="col-sm-12 text-right text-center-xs">
							<?php echo $links; ?>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('newsletters/modal/view_newsletter_template'); ?>

<!-- delete-newsletter-template-popup Modal -->
<div id="delete-newsletter-template-popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('delete_newsletter_template') ?></h3>
			</div>
			<form id="form-delete-newsletter-template" action="<?php echo site_url('newsletter/delete_newsletter_template'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="newsletter_template_id" name="newsletter_template_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    			<?php echo lang('are_you_sure_you_want_to_delete_this_newsletter_template') ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="delete-newsletter-template-btn btn btn-default"><?php echo lang('ok') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of delete-newsletter-template-popup Modal -->

<!-- Send Newsletter Form wizard -->
<div class="modal fade" id="send-newsletter-popup" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3><?php echo lang('create_newsletter') ?></h3>
			</div>
			<div class="modal-body">
				<div class="message"></div>
				<form id="form-send-newsletter" method="post" action="<?php echo site_url("setting/send-newsletter"); ?>">
	                                	<div class="row">
	                                        <div class="col-md-6">
	                                           <div class="form-group"> 
	                                           
	                                                <label class="control-label" for="template_name"><?php echo ucfirst(lang('title')); ?><span class="text-danger"> *</span> :</label>
	                                                <input type="text" class="form-control required" name="template_name" value="">
	                                            </div>
	                                        </div>
	                                        <div class="form-group col-md-6"> 
												<label class="control-label" for="language"><?php echo lang('language') ?> <span class="text-danger">*</span></label>
												<select name="language_code" class="required form_control" ui-jq="chosen">
                                                    <option value="nl" selected><?php echo lang('dutch') ?></option>
                                                    <option value="en"><?php echo lang('english') ?></option>
                                                    <option value="fr"><?php echo lang('french') ?></option>
                                                </select>
												<?php echo form_error('language_code') ?>
											</div>
	                                    </div>
	                                
					</form>

			</div>
			<div class="modal-footer">
				<?php /* ?>
				<button style="margin-bottom:10px;margin-top:10px;" data-email-preview="1" class="btn btn-info" id="newsletter-email-preview"><?php echo lang('email_preview'); ?></button>
				<?php */ ?>
				<button style="margin-bottom:10px;margin-top:10px;"  data-email-preview="0" class="btn btn-success search-button" id="newsletter-email-submit"><?php echo lang('save'); ?></button>
				<button type="button" class="btn btn-default close_modal generate_proposal_close" data-dismiss="modal"><?php echo lang('close') ?></button>
			</div>
		</div>
	</div>
</div><!-- End of Send Newsletter Form wizard -->


<!-- Send Test Newsletter Form wizard -->
<div class="modal fade" id="send-test-newsletter-popup" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3><?php echo lang('send_preview') ?></h3>
			</div>
			<div class="modal-body">
				<div class="message"></div>
				<form id="form-send-test-newsletter" method="post" action="<?php echo site_url("setting/send-newsletter"); ?>">
                	<input type="hidden" name="newsletter_template_id">
                	<div class="row">
                        <div class="col-md-6">
                           <div class="form-group"> 
                           
                                <label class="control-label" for="from_email"><?php echo ucfirst(lang('from_email')); ?><span class="text-danger"> *</span> :</label>
                                <input type="text" class="form-control required" name="from_email" value="">
                            </div>
                        </div>
                        <div class="form-group col-md-6"> 
							<label class="control-label" for="to_email"><?php echo lang('to_email') ?> <span class="text-danger">*</span></label>
							<input type="text" class="form-control required" name="to_email" value="">
						</div>
                    </div>
	                                
					</form>

			</div>
			<div class="modal-footer">
				<button style="margin-bottom:10px;margin-top:10px;" class="btn btn-success" id="test-newsletter-email-submit"><?php echo lang('send'); ?></button>
				<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
			</div>
		</div>
	</div>
</div><!-- End of Send Newsletter Form wizard -->

<div id="template-newsletter-new-zone">
	<div class="option_item" data-id="999">
    	
	</div>
</div>
<script>
// Initialise ckeditor
/*initSample();

// update textarea on change of ckeditor text
for (var i in CKEDITOR.instances) {
        
        CKEDITOR.instances[i].on('change', function() { CKEDITOR.instances[i].updateElement() });
        
}*/
</script>