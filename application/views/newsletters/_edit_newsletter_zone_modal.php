<form id="form-build-newsletter-zone" method="post" action="<?php echo site_url("setting/send-newsletter"); ?>">
	<input type="hidden" name="newsletter_template_id" value="<?php echo $newsletter['id']; ?>">
    <input type="hidden" name="newsletter_zone_id" value="<?php echo $newsletter_zone['id']; ?>">
    <input type="hidden" name="update_flag" value="1">
	<div class="row">
        <div class="col-md-6">
           <div class="form-group"> 
           
                <label class="control-label" for="zone_name"><?php echo ucfirst(lang('name')); ?><span class="text-danger"> *</span> :</label>
                <input type="text" class="form-control required" name="zone_name" value="<?php echo $newsletter_zone['zone_name']; ?>">
            </div>
        </div>
        <div class="col-md-6">
           <div class="form-group"> 
           
                <label class="control-label" for="layout"><?php echo lang('zone_type') ?><span class="text-danger"> *</span> :</label>
                <select name="zone_type" class="required form_control" id="newsletter-zone-layout" ui-jq="chosen">
                    <option value="" selected><?php echo lang('please_select'); ?></option>
                    <option value="4" <?php echo $newsletter_zone['zone_type'] == 4 ? 'selected' : ''; ?>><?php echo lang('zone_layout_4') ?></option>
                    <?php /* ?>
                    <option value="1" <?php echo $newsletter_zone['zone_type'] == 1 ? 'selected' : ''; ?>><?php echo lang('zone_layout_1') ?></option>
                    <option value="2" <?php echo $newsletter_zone['zone_type'] == 2 ? 'selected' : ''; ?>><?php echo lang('zone_layout_2') ?></option>
                    <option value="3" <?php echo $newsletter_zone['zone_type'] == 3 ? 'selected' : ''; ?>><?php echo lang('zone_layout_3') ?></option>
                    <?php */ ?>
                </select>
				<span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
            </div>
        </div>
    </div>
    <div class="row newsletter-text-zone" style="<?php echo $newsletter_zone['zone_type'] != 4 ? 'display:none;' : ''; ?>">
        <div class="col-md-12">
           <div class="form-group"> 
           
                <label class="control-label" for="body"><?php echo lang('body') ?><span class="text-danger"> *</span> :</label>
                <textarea id="editor" cols="20" rows="5" class="form-control ck-editor" name="zone_body"><?php echo $newsletter_zone['zone_body']; ?></textarea>
                <span class="text-danger" data-text="<?php echo lang('this_field_is_required'); ?>"></span>
            </div>
        </div>
    </div>

    <div id="review-newsletter-new-zone">
    </div>
                    
</form>