<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('newsletters') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('sucess_msg')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('sucess_msg'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error_msg')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<?php echo $this->session->flashdata('error_msg'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo lang('unsubscribe_users_listing') ?>
				</div>
				
				<div class="table-responsive">		
					<table class="tftable table table-striped m-b-none">
						<tr>
							<th></th>
							<th><?php echo lang('name') ?></th>
							<th><?php echo lang('email') ?></th>
							<th><?php echo lang('unsubscribed_on') ?></th>
						</tr>
						<?php if(isset($unsubscribe_users) && !empty($unsubscribe_users)){?>
						<?php
							$count = 1;

							foreach($unsubscribe_users as $value): 
						?>
						<tr>
							<td><?php echo $count++; ?></td>
							<td><?php echo $value['user_name']; ?></td>
							<td><?php echo $value['email'];?></td>
							<td><?php echo date('d-m-Y G:i:s', strtotime($value['unsubscribed_on']));?></td>
						</tr>
						<?php endforeach; ?>
						<?php } else{?>
							<tr>
								<td colspan="3"><?php echo lang('no_newsletters_found') ?></td>
							</tr>
						<?php }?>
					</table>
				</div>
				
				<footer class="panel-footer">
		      		<div class="row">
						<div class="col-sm-12 text-right text-center-xs">
							<?php //echo $links; ?>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>

</div>