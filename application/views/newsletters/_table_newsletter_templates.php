<div class="table-container">
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th><?php echo lang('name') ?></th>
				<th><?php echo lang('created_on') ?></th>
				<th colspan="2"><?php echo lang('action') ?></th>
			</tr>
			<?php if(!empty($newsletter_templates) && $newsletter_templates!='') { ?>
			<?php
				foreach($newsletter_templates as $value)
				{
			?>
			<tr>
				<td><?php echo $value['template_name']; ?></td>
				<td><?php echo date('d-m-Y G:i:s', strtotime($value['added_on']) ); ?></td>
				<td>
					<a href="#viewNewsletterTemplate" data-toggle="modal" class="btn btn-success btn-xs view-newsletter-template" data-newsletter-template-id="<?php echo $value['id'];?>" style="margin-right: 5px;"><?php echo lang('view') ?></a>
					<?php /*if( check_permission('edit_group') ) {*/ ?>
					<a href="<?php echo site_url('newsletter/build/'.$value['id']); ?>" data-toggle="modal" class="btn btn-info btn-xs" data-newsletter-template-id="<?php echo $value['id']; ?>"><?php echo lang('build') ?></a>
					<?php /*}*/ ?>
					<?php /*if( check_permission('delete_organisation') ) {*/ ?>
						<a href="#delete-newsletter-template-popup" data-toggle="modal" data-newsletter-template-id="<?php echo $value['id']; ?>" class="delete-newsletter-template btn btn-xs btn-danger"><?php echo lang('delete') ?></a>
					<?php /*}*/ ?>
					<a href="#send-test-newsletter-popup" data-toggle="modal" data-newsletter-template-id="<?php echo $value['id']; ?>" class="send-test-newsletter btn btn-primary btn-xs"><?php echo lang('send_preview') ?></a>
				</td>
				<td style="display:none" class="newsletter_template_html" data-newsletter-template-id="<?php echo $value['id']; ?>"><?php echo htmlentities($value['email_html']); ?></td>
			</tr>
			<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_data_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	
</div>