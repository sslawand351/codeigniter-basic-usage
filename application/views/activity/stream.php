<style>
a
{
    padding:2px;
}
td
{
    padding:5px;
}
</style>

<?php 
    $activity_types = array(
        ACTIVITY_ADMIN_HAS_LOGGED_IN => 'Admin Login',
        ACTIVITY_ADMIN_HAS_LOGGED_OUT => 'Admin Logout',
        ACTIVITY_ADMIN_ADDED_ROLE => 'Add Role',
        ACTIVITY_ADMIN_UPDATED_ROLE => 'Update Role',
        ACTIVITY_ADMIN_DELETED_ROLE => 'Delete Role',
        ACTIVITY_ADMIN_ADDED_USER => 'Add user',
        ACTIVITY_ADMIN_UPDATED_USER => 'Update user',
        ACTIVITY_ADMIN_DELETED_USER => 'Delete user',
    );
?>

  <!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">
        
  <!-- main header -->
  <div class="bg-light lter b-b wrapper-md">
    <div class="row">
      <div class="col-sm-6 col-xs-12">
        <h1 class="m-n font-thin h3 text-black"><?php echo lang('activities'); ?></h1>
        <!-- <small class="text-muted">Location Listing</small> -->
      </div>
    </div>
  </div>
  <!-- / main header -->
  <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
    <div class="">
        <?php  if($this->session->flashdata('success_msg')) { ?>
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('success_msg'); ?>
              </div>
        <?php } ?>
        <?php  if($this->session->flashdata('error_msg')) { ?>            
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('error_msg'); ?>
              </div>
        <?php } ?>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <?php echo lang('activity_log'); ?>
      </div>
       <div class="panel-body">
        <form action="" method="get">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php

                            $user_options = array();

                            $user_options[''] = 'Select user';

                            if( ! empty($users))
                            {
                                foreach($users as $user)
                                {
                                    $user_options[$user['id']] = $user['first_name'].' '.$user['last_name'];
                                }
                            }

                            $selected = $this->input->get('user_id') ? $this->input->get('user_id') : '';
                            $extra = 'class="form-control" ui-jq="chosen"';
                            echo form_dropdown('user_id', $user_options, $selected, $extra); 
                         ?>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <select name="activity_type" class="form-control" ui-jq="chosen">
                            <option value=""><?php echo lang('select_activity_type'); ?></option>
                            <?php 
                                foreach($activity_types as $key => $type)
                                { 
                                    echo '<option value="'.$key.'" ';
                                    if(isset($_GET['activity_type']) && $_GET['activity_type']==$key)
                                    {
                                        echo 'selected';
                                    }
                                    echo '>'.$type.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-1">
                    <div class="form-group">
                        <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('search'); ?></button>
                    </div>
                </div>
            </div>    
            
        </form>
      </div>

    <div class="table-responsive">
        <table class="tftable table table-striped m-b-none">
            <tr>
              <th><?php echo lang('activity'); ?></th>
              <th><?php echo lang('time'); ?></th>
            </tr>
            <?php if( ! empty($activity_streams)) { ?>
            <?php foreach($activity_streams as $key => $activity) : ?>
              <tr>
                <td><?php echo $activity['activity_text']; ?></td>
                <td><?php echo ago(strtotime($activity['activity_on'])); ?> <?php echo lang('ago'); ?></td>
              </tr>
            <?php endforeach; ?>
            <?php } else { ?>
            <tr>
              <td colspan="2"><?php echo lang('no_activity_found'); ?></td>
            </tr>
            <?php } ?>
          </table>
      </div>

        <footer class="panel-footer">
            <div class="row">
                <div class="col-sm-12 text-right text-center-xs">
                    <?php echo $links; ?>
                </div>
            </div>
        </footer>
    </div>
  </div>

</div>
</div>

