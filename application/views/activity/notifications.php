<?php 
  if(count($activity_notifications) > 0): ?>
      <div class="noticebar-menu" id="activities-list">
          <?php foreach($activity_notifications as $activity): ?>
				<div class="list-group-item">
					<span class="clear block m-b-none">
						<?php echo $activity['activity_text']; ?><br>
            <div class="notification-data">
						  <small class="text-muted"><?php echo ago(strtotime($activity['activity_on'])); ?> <?php echo lang('ago') ?></small>
             <div class="text-muted" style="float:right;"><i><?php echo $activity['user_full_name']; ?></i></div>
            </div>
					</span>
				</div>
			<?php endforeach; ?>
      </div>
  <?php else: ?>
      <div class="noticebar-menu" style="padding: 10px;">
         <?php echo lang('there_are_no_activities_added_yet');?> 
      </div>
  <?php endif; ?>
