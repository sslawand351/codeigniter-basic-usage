<form action="<?php echo site_url('admin/language/update_json/en')?>" method="post">
<?php if( isset($en) ) { ?>
<input type="hidden" name="file_name" value="<?php echo $file_name; ?>">
<?php /* ?>
<div class="row">
	<div class="col-sm-3">
		<label><?php echo lang('page_title') ?><span class="text-danger"> *</span> :</label>
	</div>
	<div class="col-sm-5">
	    <div class="form-group">
	        <?php echo $en['page_title']; ?>
	        <input type="hidden" name="page_title" value="<?php echo $en['page_title']; ?>" class="form-control">
			
	    </div>
	</div>
</div>
<? */?>
<div class="row">
	<div class="col-md-3">
		<h3>Translations</h3>
	</div>
</div>
<div class="form-fields">
	<?php 
		$translations = $en['translations'];

		if( is_array($translations) && ! empty($translations) )
		{
			foreach( $translations as $index => $t )
			{
				$label = ucfirst(str_replace('_', ' ', $index));
				?>
				<div class="row">
					<div class="col-sm-3">
						<label><?php echo $label; ?><span class="text-danger"> </span> :</label>
					</div>
					<div class="col-sm-5">
					    <div class="form-group">
					        
					        <textarea name="translations[<?php echo $index; ?>]" class="form-control"><?php echo $t ?></textarea>
							<?php /*if(form_error('page_title')) { ?>
								<div class="text-danger"><?php echo form_error('page_title'); ?></div>
							<?php }*/ ?>
					    </div>
					</div>
				</div>
				<?php
			}
		}
	?>
	
</div>
<div class="row">
	<div class="col-md-12">
		<div class="field-box submitButtonDiv">
	        <label></label>
	        <input type="button" name="" class="btn btn-glow primary form-submit" value="<?php echo lang('update_english_translations');?>">
	    </div>
    </div>
</div>
<?php } ?>
</form>