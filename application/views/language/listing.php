<?php echo Assets::js("admin/js/custom/language.js"); ?>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">
		<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('language') ?></h1>
				</div>
			</div>
		</div><!-- / main header -->
		
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					 <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo lang('language_translations') ?>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-3">
							<label>Json<?php echo lang('add_field');?></label>
						</div>
						<div class="col-md-5">
							<div class="form-group">
					            <div class="field-box <?php if(isset($fieldType_error)) echo $fieldType_error;?>">
						            <select class="form-control" name="json_file_name" id="json-file-name" ui-jq='chosen'> 
						                <option value="">-- <?php echo lang('select_one');?> --</option>
						                <?php
						                $translations_dir = FCPATH . 'application/language/translations/en/';

										if (is_dir($translations_dir)){
											if ($dh = opendir($translations_dir)){
												while (($file = readdir($dh)) !== false){

													$ext = pathinfo($file, PATHINFO_EXTENSION);
													if($ext === 'json')
													{
														$option = ucfirst(str_replace('_', ' ', str_replace('.json', '', $file)));
														?>
														<option value="<?php echo $file; ?>"><?php echo $option; ?></option>
														<?php
													}
												}
												closedir($dh);
											}
										}
						                ?>
						            </select>
					            </div>
							</div>
						</div>
					</div>

					<div class="wrapper" style="padding-left:0px">
					    <ul id="myTabs" class="nav nav-tabs" role="tablist">
					        <li role="presentation" class="active"><a href="#english-translations" id="english-tab" role="tab" data-toggle="tab" aria-controls="info" aria-expanded="true"><?php echo lang('english') ?></a></li>
					        <li role="presentation" class=""><a href="#dutch-translations" id="dutch-tab" role="tab" data-toggle="tab" aria-controls="configuration" aria-expanded="true"><?php echo lang('dutch') ?></a></li>
					        <li role="presentation" class=""><a href="#french-translations" id="french-tab" role="tab" data-toggle="tab" aria-controls="configuration" aria-expanded="true"><?php echo lang('french') ?></a></li>
					    </ul>
					    <br>
					    <div id="myTabContent" class="tab-content">
					        <div role="tabpanel" class="main_tabs tab-pane fade in active" id="english-translations" aria-labelledBy="info-tab">
					        	
					           		<?php $this->load->view('language/_en_language_translations_form'); ?>

							</div>

							<div role="tabpanel" class="main_tabs tab-pane fade" id="dutch-translations" aria-labelledBy="technical-tab">
					        	<?php $this->load->view('language/_nl_language_translations_form'); ?>
					        </div>

					        <div role="tabpanel" class="main_tabs tab-pane fade" id="french-translations" aria-labelledBy="technical-tab">
					        	<?php $this->load->view('language/_fr_language_translations_form'); ?>
					        </div>
					    </div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>