<?php echo form_open('admin/form/configuration/'.$form['id'] ,array('class' => 'form-horizontal')); ?>

                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>                    
                                                        <th class="span2"><?php echo lang('definition');?></th>
                                                        <th class="span3"><span class="line"></span><?php echo lang('value');?></th>                    
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label><?php echo lang('confirmation_email_message_content');?></label>
                                                        </td>
                                                        <td>                                            
                                                            <?php if(form_error('confirmation_email_message_content')){$confirm_email_msg_error ="error";}?>
                                                            <div class="field-box <?php if(isset($confirm_email_msg_error)) echo $confirm_email_msg_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="confirmation_email_message_content" value="<?php if(isset($configuration['confirmation_email_message_content'])) echo $configuration['confirmation_email_message_content'];?>">
                                                                <?php if(isset($confirm_email_msg_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('confirmation_email_message_content',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label><?php echo lang('message_confirmation_mail_before_content');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('message_confirmation_mail_before_content')){$confirm_email_msg_before_error ="error";}?>
                                                            <div class="field-box <?php if(isset($confirm_email_msg_before_error)) echo $confirm_email_msg_before_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="message_confirmation_mail_before_content" value="<?php if(isset($configuration['message_confirmation_mail_before_content'])) echo $configuration['message_confirmation_mail_before_content'];?>">
                                                                <?php if(isset($confirm_email_msg_before_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('message_confirmation_mail_before_content',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>    
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('message_confirmation_mail_after_content');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('message_confirmation_mail_after_content')){$confirm_email_msg_after_error ="error";}?>
                                                            <div class="field-box <?php if(isset($confirm_email_msg_after_error)) echo $confirm_email_msg_after_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="message_confirmation_mail_after_content" value="<?php if(isset($configuration['message_confirmation_mail_after_content'])) echo $configuration['message_confirmation_mail_after_content'];?>">
                                                                <?php if(isset($confirm_email_msg_after_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('message_confirmation_mail_after_content',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                           
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('message_sent_successfully');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('message_sent_successfully')){$message_sent_successfully_error ="error";}?>
                                                            <div class="field-box <?php if(isset($message_sent_successfully_error)) echo $message_sent_successfully_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="message_sent_successfully" value="<?php if(isset($configuration['message_sent_successfully'])) echo $configuration['message_sent_successfully'];?>">
                                                                <?php if(isset($message_sent_successfully_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('message_sent_successfully',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                           
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('error_sending_message');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('error_sending_message')){$error_sending_message_error ="error";}?>
                                                            <div class="field-box <?php if(isset($error_sending_message_error)) echo $error_sending_message_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="error_sending_message" value="<?php if(isset($configuration['error_sending_message'])) echo $configuration['error_sending_message'];?>">
                                                                <?php if(isset($error_sending_message_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('error_sending_message',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('value_submit_button');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('value_submit_button')){$value_submit_button_error ="error";}?>
                                                            <div class="field-box <?php if(isset($value_submit_button_error)) echo $value_submit_button_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="value_submit_button" value="<?php if(isset($configuration['value_submit_button'])) echo $configuration['value_submit_button'];?>">
                                                                <?php if(isset($value_submit_button_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('value_submit_button',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('value_delete_button');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('value_delete_button')){$value_delete_button_error ="error";}?>
                                                            <div class="field-box <?php if(isset($value_delete_button_error)) echo $value_delete_button_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="value_delete_button" value="<?php if(isset($configuration['value_delete_button'])) echo $configuration['value_delete_button'];?>">
                                                                <?php if(isset($value_delete_button_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('value_delete_button',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('send_to_email_address');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('email_id_to_send')){$email_id_to_send_error ="error";}?>
                                                            <div class="field-box <?php if(isset($email_id_to_send_error)) echo $email_id_to_send_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="email_id_to_send" value="<?php if(isset($configuration['email_id_to_send'])) echo $configuration['email_id_to_send'];?>">
                                                                <?php if(isset($email_id_to_send_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('email_id_to_send',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                                                                        
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('email_subject');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('email_subject')){$email_subject_error ="error";}?>
                                                            <div class="field-box <?php if(isset($email_subject_error)) echo $email_subject_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="email_subject" value="<?php if(isset($configuration['email_subject'])) echo $configuration['email_subject'];?>">
                                                                <?php if(isset($email_subject_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('email_subject',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('bcc');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('bcc')){$bcc_error ="error";}?>
                                                            <div class="field-box <?php if(isset($bcc_error)) echo $bcc_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="bcc" value="<?php if(isset($configuration['bcc'])) echo $configuration['bcc'];?>">
                                                                <?php if(isset($bcc_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('bcc',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                             
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('cc');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('cc')){$cc_error ="error";}?>
                                                            <div class="field-box <?php if(isset($cc_error)) echo $cc_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="cc" value="<?php if(isset($configuration['cc'])) echo $configuration['cc'];?>">
                                                                <?php if(isset($cc_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('cc',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('email_address_sender');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('sender_email_id')){$sender_email_id_error ="error";}?>
                                                            <div class="field-box <?php if(isset($sender_email_id_error)) echo $sender_email_id_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="sender_email_id" value="<?php if(isset($configuration['sender_email_id'])) echo $configuration['sender_email_id'];?>">
                                                                <?php if(isset($sender_email_id_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('sender_email_id',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>
                                                        <tr>
                                                            <td>
                                                                <label class=""><?php echo lang('conversion_code');?></label>
                                                            </td>
                                                            <td>
                                                                <div class="field-box">                           
                                                                    <textarea id="conversion_code" class="span5 form-control" name="conversion_code"><?php if(isset($configuration['conversion_code'])) echo $configuration['conversion_code'];?></textarea>
                                                                </div>    
                                                                </div>                                            
                                                            </td>
                                                        </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('sender_name');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('sender_name')){$sender_name_error ="error";}?>
                                                            <div class="field-box <?php if(isset($sender_name_error)) echo $sender_name_error;?>">                                                                                
                                                                <input class="span5 form-control" type="text" name="sender_name" value="<?php if(isset($configuration['sender_name'])) echo $configuration['sender_name'];?>">
                                                                <?php if(isset($sender_name_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('sender_name',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class=""><?php echo lang('copy_to_customer');?></label>
                                                        </td>
                                                        <td>
                                                            <?php if(form_error('copy_to_customer')){$copy_to_customer_error ="error";}?>
                                                            <div class="field-box <?php if(isset($copy_to_customer_error)) echo $copy_to_customer_error;?>">                                                                                                                                
                                                                <select name="copy_to_customer" id="fieldType" class="span5" ui-jq="chosen">                        
                                                                    <option value="yes" <?php if (isset($configuration['copy_to_customer']) && $configuration['copy_to_customer'] == "yes") echo "selected"; ?>><?php echo lang('yes');?></option>
                                                                    <option value="no" <?php if (isset($configuration['copy_to_customer']) && $configuration['copy_to_customer'] == "no") echo "selected"; ?>><?php echo lang('no');?></option>                                
                                                                </select>
                                                                <?php if(isset($copy_to_customer_error)){?>
                                                                    <span class="alert-msg">
                                                                        <i class="icon-remove-sign"></i><?php echo form_error('copy_to_customer',' ',' '); ?>
                                                                    </span>
                                                                <?php }?>
                                                            </div>                                                                                                                    
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label></label>
                                                        </td>
                                                        <td>
                                                            <button type="submit" class="btn btn-glow primary"><?php echo lang('save');?></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>                                   
                                        <?php echo form_close();?>