<div class="form-shortcode-table table-responsive">	
	<table class="tftable table table-striped m-b-none">
		<tr>
			<th><?php echo lang('label')?></th>
			<th><?php echo lang('shortcode')?></th>
		</tr>
		<?php 

		if( ! empty($form_fields))
		{
			foreach ($form_fields as $f_key => $f)
			{
		?>
		<tr>
			<td><?php echo $f['label']; ?></td>
			<td><?php echo '{' . $f['shortcode'] . '}'; ?></td>
		</tr>
		<?php
			}
		}
		else
		{
		?>
		<tr>
			<td colspan="10"><?php echo lang('no_data_found'); ?></td>
		</tr>
		<?php
		}
		?>
	</table>
</div>