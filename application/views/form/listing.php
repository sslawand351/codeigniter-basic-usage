<?php echo Assets::js("admin/js/custom/form_listing.js"); ?>
<style>
code{
	white-space: pre;
}
</style>
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

		<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('form_listing') ?></h1>
				</div>
			</div>
		</div><!-- / main header -->
		
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					 <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php //if($this->permissions->is_permission_allowed('FORM_ADD')): ?>
					<div class="btn-group pull-right">
						<a href="<?php echo site_url('admin/form/add') ?>" class="btn btn-xs btn-success"><?php echo lang('add_form'); ?></a>
					</div>
					<?php //endif; ?>
					<?php echo lang('form_listing') ?>
				</div>
				
				<div class="table-responsive">		
					<table class="tftable table table-striped m-b-none">
						<tr>
							<th class="text-center"></th>
							<th class="text-center"><?php echo lang('name'); ?></th>
							<th class="text-center"><?php echo lang('created_on'); ?></th>
							<th class="text-center"><?php echo lang('action'); ?></th>
						</tr>
						<?php if( ! empty($forms) ):?>
						<?php
							$count = 1;
							foreach($forms as $f): 
						?>
						<tr>
							<td class="text-center"><?php echo $count++; ?></td>
							<td class="text-center"><?php echo $f['title']; ?></td>
							<td class="text-center"><?php echo date( 'Y-m-d G:i:s', strtotime($f['added_on']) ); ?></td>
							<td class="text-center">
								<a class="btn btn-info btn-xs" href="<?php echo site_url('admin/form/edit/' . $f['id'] ); ?>"><?php echo lang('edit'); ?></a>
								<a href="#delete-form-popup" data-toggle="modal" data-form-id="<?php echo $f['id']; ?>" class="delete-form btn btn-xs btn-danger"><?php echo lang('delete'); ?></a>
								<a href="#html-code-modal" data-toggle="modal" data-form-id="<?php echo $f['id']; ?>" class="html_code btn btn-xs btn-success"><?php echo lang('html_code'); ?></a>
							</td><!-- <div class="forms" form-id="<?php echo $f['id']; ?>" form-name="<?php echo $f['title']; ?>"></div> -->
							<td class="html-code" style="display:none;">[data-form-id=<?php echo $f['id']; ?> data-form-name=<?php echo $f['title']; ?>]</td>
						</tr>
						<?php endforeach; ?>
						<?php else:?>
							<tr>
								<td colspan="5"><?php echo lang('no_data_found') ?></td>
							</tr>
						<?php endif;?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- delete-form-popup -->
<div id="delete-form-popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('delete_form'); ?></h3>
			</div>
			<form id="delete-form" action="<?php echo site_url('admin/form/delete'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="form_id" name="form_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    			<?php echo lang('are_you_sure_you_want_to_delete_this_form'); ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="delete-form-btn btn btn-default"><?php echo lang('ok'); ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close'); ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of delete-form-popup -->

<!-- html code modal -->
<div id="html-code-modal" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('html_code'); ?></h3>
			</div>

		    <div class="modal-body" style="overflow-x: auto;">
	    		<code></code>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close'); ?></button>
			</div>
		</div>
	</div>
</div><!-- End of html code modal -->