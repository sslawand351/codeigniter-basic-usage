<?php echo Assets::css("admin/css/custom/form_add.css"); ?>
<div class="row">
	<div class="col-sm-3">
		<label>Form name<?php echo lang('form_name') ?><span class="text-danger"> *</span> :</label>
	</div>
	<div class="col-sm-5">
	    <div class="form-group">
	        
	        <input type="text" name="form_name" id="formTitle" value="<?php echo $form['title']; ?>" class="form-control">
			<?php if(form_error('form_name')) { ?>
				<div class="text-danger"><?php echo form_error('form_name'); ?></div>
			<?php } ?>
			<input type="hidden" id="formIdHidden" value="<?php if(isset($form['id'])) echo $form['id'];?>">
	    </div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label>Add New Field<?php echo lang('add_field');?></label>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			<?php if(form_error('fieldType')){$fieldType_error ="error";}?>
            <div class="field-box <?php if(isset($fieldType_error)) echo $fieldType_error;?>">
	            <select class="form-control" name="fieldType" id="fieldType" ui-jq='chosen'> 
	                <option value="">-- <?php echo lang('select_one');?> --</option>
	                <option value="textField"><?php echo lang('text_field');?></option>
	                <option value="passwordField"><?php echo lang('password_field');?></option>
	                <option value="textFieldLarge"><?php echo lang('textarea');?></option>
	                <option value="dateField"><?php echo lang('date_field');?></option>
	                <option value="emailField"><?php echo lang('email_field');?></option>
	                <option value="websiteField"><?php echo lang('website_field');?></option>
	                <option value="radioButtons"><?php echo lang('radio_button');?></option>
	                <!-- <option value="uploadField"><?php echo lang('upload_field');?></option> -->
	                <option value="checkBoxes"><?php echo lang('checkbox');?></option>
	                <option value="dropdown"><?php echo lang('dropdown');?></option>
	            </select>
            </div>
		</div>
	</div>
</div>
<div class="form-fields">
	<?php
		// Build form using fields array

		if( ! empty($form_fields) )
		{
			foreach ($form_fields as $f_key => $field )
			{
				switch( $field['field_type'] )
				{
					case FORM_FIELD_TYPE_TEXT:
					?>
						<div class="row" id="field_<?php echo $field['field_id'];?>">
							<div class="col-md-3">
								<label class='control-label'><?php echo $field['label'];?></label>
							</div>
							<div class="col-md-5">
								
									<div class='controls mainContainer col-md-10 m-l-n'>
										<div class="form-group">
											<input type='text' data-type='textField' class='form-control' data-field-id="<?php echo $field['field_id'];?>">
										</div>
									</div>
									<div class='col-md-2' style='float:right;'>
										<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
									</div>
									<div class='editTextBox editFieldForm col-md-12' style='margin-left: 0px;'>
										<div class='control-group'>
											<label class='control-label'>Label:</label>
											<div class='form-group'>
												<input type='text' value="<?php echo $field['label']; ?>" class='form-control'>
											</div>
										</div>

										<div class='control-group'>
											<label class='control-label'></label>
											<div class='controls' style='padding-bottom: 5px;'>
												<?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
												<label class="i-checks">
								                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
								                	<i></i>
								                 	Required
								                </label>
						                    </div>
						                    <label class='control-label'></label>
						                    <div class='controls' style='margin-left:125px;'>
						                    	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
						                        <button type='button' class='btn closeButton'>Close</button>
						                    </div>
						                </div>
						            </div>
							</div>
						</div>
					<?php
						break;

					case FORM_FIELD_TYPE_PASSWORD:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									
										<div class='controls mainContainer col-md-10 m-l-n'>
											<div class="form-group">
												<input type='password' data-type='passwordField' class='form-control' data-field-id="<?php echo $field['field_id'];?>">
											</div>
										</div>
										<div class='col-md-2' style='float:right;'>
											<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
										</div>
										<div class='editPasswordField editFieldForm col-md-12' style='margin-left: 0px;'>
											<div class='control-group'>
												<label class='control-label'>Label:</label>
												<div class='form-group'>
													<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
												</div>
											</div>

											<div class='control-group'>
												<label class='control-label'></label>
												<div class='controls' style='padding-bottom: 5px;'>
							                        <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
													<label class="i-checks">
									                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
									                	<i></i>
									                 	Required
									                </label>
							                    </div>
							                    <label class='control-label'></label>
							                    <div class='controls' style='margin-left:125px;'>
							                    	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                        <button type='button' class='btn closeButton'>Close</button>
							                    </div>
							                </div>
							            </div>
								</div>
							</div>
						<?php
						break;

					case FORM_FIELD_TYPE_TEXTAREA:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									<div class='controls mainContainer col-md-10 m-l-n'>
										<div class="form-group">
											<textarea data-type='textArea' class='form-control' data-field-id="<?php echo $field['field_id'];?>"></textarea>
										</div>
									</div>
									<div class='col-md-2' style='float:right;'>
										<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
									</div>
									<div class='editTextBoxLarge editFieldForm col-md-12' style='margin-left: 0px;'>
										<div class='control-group'>
											<label class='control-label'>Label:</label>
											<div class='form-group'>
												<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
											</div>
										</div>

										<div class='control-group'>
											<label class='control-label'></label>
											<div class='controls' style='padding-bottom: 5px;'>
							                    <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
												<label class="i-checks">
								                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
								                	<i></i>
								                 	Required
								                </label>
							                </div>
							                <label class='control-label'></label>
							                <div class='controls' style='margin-left:125px;'>
							                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                    <button type='button' class='btn closeButton'>Close</button>
							                </div>
							            </div>
							        </div>
								</div>
							</div>
						<?php
						break;

					case FORM_FIELD_TYPE_DATE:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									<div class='controls mainContainer col-md-10 m-l-n'>
										<div class="form-group">
											<input type='text' class='span2 form-control' data-date-format='mm/dd/yy' id='dp2' data-type='textDate' data-field-id="<?php echo $field['field_id'];?>">
										</div>
									</div>
									<div class='col-md-2' style='float:right;'>
										<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
									</div>
									<div class='editDateField editFieldForm col-md-12' style='margin-left: 0px;'>
										<div class='control-group'>
											<label class='control-label'>Label:</label>
											<div class='form-group'>
												<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
											</div>
										</div>

										<div class='control-group'>
											<label class='control-label'></label>
											<div class='controls' style='padding-bottom: 5px;'>
							                    <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
												<label class="i-checks">
								                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
								                	<i></i>
								                 	Required
								                </label>
							                </div>
							                <label class='control-label'></label>
							                <div class='controls' style='margin-left:125px;'>
							                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                    <button type='button' class='btn closeButton'>Close</button>
							                </div>
							            </div>
							        </div>
								</div>
							</div>
						<?php
						break;

					case FORM_FIELD_TYPE_EMAIL:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									<div class='controls mainContainer col-md-10 m-l-n'>
										<div class="form-group">
											<input type='email' data-type='textEmail' class='form-control' data-field-id="<?php echo $field['field_id'];?>">
										</div>
									</div>
									<div class='col-md-2' style='float:right;'>
										<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
									</div>
									<div class='editEmailField editFieldForm col-md-12' style='margin-left: 0px;'>
										<div class='control-group'>
											<label class='control-label'>Label:</label>
											<div class='form-group'>
												<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
											</div>
										</div>

										<div class='control-group'>
											<label class='control-label'></label>
											<div class='controls' style='padding-bottom: 5px;'>
							                    <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
												<label class="i-checks">
								                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
								                	<i></i>
								                 	Required
								                </label>
							                </div>
							                <label class='control-label'></label>
							                <div class='controls' style='margin-left:125px;'>
							                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                    <button type='button' class='btn closeButton'>Close</button>
							                </div>
							            </div>
							        </div>
								</div>
							</div>
						<?php
						break;

					case FORM_FIELD_TYPE_WEBSITE:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									<div class='controls mainContainer col-md-10 m-l-n'>
										<div class="form-group">
											<input type='text' data-type='textWebsite' class='form-control' data-field-id="<?php echo $field['field_id'];?>">
										</div>
									</div>
									<div class='col-md-2' style='float:right;'>
										<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
									</div>
									<div class='editWebsiteField editFieldForm col-md-12' style='margin-left: 0px;'>
										<div class='control-group'>
											<label class='control-label'>Label:</label>
											<div class='form-group'>
												<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
											</div>
										</div>

										<div class='control-group'>
											<label class='control-label'></label>
											<div class='controls' style='padding-bottom: 5px;'>
							                    <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
												<label class="i-checks">
								                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
								                	<i></i>
								                 	Required
								                </label>
							                </div>
							                <label class='control-label'></label>
							                <div class='controls' style='margin-left:125px;'>
							                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                    <button type='button' class='btn closeButton'>Close</button>
							                </div>
							            </div>
							        </div>
								</div>
							</div>	
						<?php
						break;

					case FORM_FIELD_TYPE_RADIO_BUTTON:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									<div class='controls mainContainer col-md-10 m-l-n radioButtonsContainer'>
										<?php

										if( ! empty($field['options']) )
										{
                                            foreach($field['options'] as $option)
                                            {
                                        ?>
										<div class="form-group">
											<input style='' type='radio' data-type='radioButton' name='choice' value="<?php echo $option['option_value']; ?>" data-option-id="<?php echo $option['id'];?>" class='formElement' data-field-id="<?php echo $field['field_id'];?>"> &nbsp; <?php echo $option['option_value'] ?> &nbsp;
										</div>
										<?php
											}
										}
                                        ?>
										<!-- <div class="form-group">
							            	<input style='' type='radio' data-type='radioButton' name='choice' value='Choice 2' class='formElement' data-field-id="<?php //echo $field['field_id'];?>"> &nbsp; Choice 2 &nbsp;
							            </div> -->
									</div>
									<div class='col-md-2' style='float:right;'>
										<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
									</div>
									<div class='editWebsiteField editFieldForm col-md-12' style='margin-left: 0px;'>
										<div class='control-group'>
											<label class='control-label'>Label:</label>
											<div class='form-group'>
												<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
											</div>
										</div>

										<div class='control-group optionsContainer'>
											<div class='addOption'>
												<i class="fa fa-plus-circle"></i> Add New option
											</div><br/>
											<?php

											if( ! empty($field['options']) )
											{
	                                            foreach($field['options'] as $option)
	                                            {
	                                        ?>
							                <div class='form-group'>
							                	<label class='control-label'>Option: </label>
							                    <input type='text' value="<?php echo $option['option_value']; ?>" data-option-id="<?php echo $option['id'];?>" class="form-control"> <i class='fa fa-trash-o deleteOption'></i>
							                </div>
							                <?php
												}
											}
	                                        ?>
							                <!-- <div class='form-group'>
							                	<label class='control-label'>Option: </label>
							                    <input type='text' value='Choice 2' class="form-control"> <i class='fa fa-trash-o deleteOption'></i>
							                </div> -->
							            </div>

										<div class='control-group'>
											<label class='control-label'></label>
											<div class='controls' style='padding-bottom: 5px;'>
							                    <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
												<label class="i-checks">
								                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
								                	<i></i>
								                 	Required
								                </label>
							                </div>
							                <label class='control-label'></label>
							                <div class='controls' style='margin-left:125px;'>
							                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                    <button type='button' class='btn closeButton'>Close</button>
							                </div>
							            </div>
							        </div>
								</div>
							</div>	
						<?php
						break;

					case FORM_FIELD_TYPE_FILE_UPLOAD:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									<div class='controls mainContainer col-md-10 m-l-n'>
										<div class="form-group">
											<input type='file' data-type='fileUpload' class='form-control' data-field-id="<?php echo $field['field_id'];?>">
										</div>
									</div>
									<div class='col-md-2' style='float:right;'>
										<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
									</div>
									<div class='editUploadField editFieldForm col-md-12' style='margin-left: 0px;'>
										<div class='control-group'>
											<label class='control-label'>Label:</label>
											<div class='form-group'>
												<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
											</div>
										</div>

										<div class='control-group'>
											<label class='control-label'></label>
											<div class='controls' style='padding-bottom: 5px;'>
							                    <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
												<label class="i-checks">
								                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
								                	<i></i>
								                 	Required
								                </label>
							                </div>
							                <label class='control-label'></label>
							                <div class='controls' style='margin-left:125px;'>
							                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                    <button type='button' class='btn closeButton'>Close</button>
							                </div>
							            </div>
							        </div>
								</div>
							</div>	
						<?php
						break;

					case FORM_FIELD_TYPE_CHECKBOX:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									<div class='controls mainContainer col-md-10 m-l-n  checkBoxesContainer'>
										<div class="form-group">
											<?php

											if( ! empty($field['options']) )
											{
	                                            foreach($field['options'] as $option)
	                                            {
	                                        ?>
											<label class="i-checks">
							                	<input style='margin-top:0px;' type='checkbox' data-type='checkbox' name='choice' value="<?php echo $option['option_value']; ?>" data-field-id="<?php echo $field['field_id'];?>" data-option-id="<?php echo $option['id']?>" class='form-control'>
							                	<i></i>
							                 	&nbsp; <?php echo $option['option_value']; ?> &nbsp;
							                </label><br>
							                <?php
												}
											}
	                                        ?>
							            </div>
									</div>
									<div class='col-md-2' style='float:right;'>
										<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
									</div>
									<div class='editCheckBoxes editFieldForm col-md-12' style='margin-left: 0px;'>
										<div class='control-group'>
											<label class='control-label'>Label:</label>
											<div class='form-group'>
												<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
											</div>
										</div>

										<div class='control-group optionsContainer'>
											<div class='addOption'>
												<i class="fa fa-plus-circle"></i> Add New option
											</div><br/>
							                <?php

											if( ! empty($field['options']) )
											{
	                                            foreach($field['options'] as $option)
	                                            {
	                                        ?>
							                <div class='form-group'>
							                	<label class='control-label'>Option: </label>
							                    <input type='text' value="<?php echo $option['option_value']; ?>" data-option-id="<?php echo $option['id'];?>" class="form-control"> <i class='fa fa-trash-o deleteOption'></i>
							                </div>
							                <?php
												}
											}
	                                        ?>
							            </div>

										<div class='control-group'>
											<label class='control-label'></label>
											<div class='controls' style='padding-bottom: 5px;'>
							                    <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
												<label class="i-checks">
								                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
								                	<i></i>
								                 	Required
								                </label>
							                </div>
							                <label class='control-label'></label>
							                <div class='controls' style='margin-left:125px;'>
							                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                    <button type='button' class='btn closeButton'>Close</button>
							                </div>
							            </div>
							        </div>
								</div>
							</div>	
						<?php
						break;

					case FORM_FIELD_TYPE_SELECT_DROPDOWN:
						?>
							<div class="row" id="field_<?php echo $field['field_id'];?>">
								<div class="col-md-3">
									<label class='control-label'><?php echo $field['label'];?></label>
								</div>
								<div class="col-md-5">
									
										<div class='controls mainContainer col-md-10 m-l-n dropdownContainer'>
											<div class="form-group">
												<select name='dropdown' data-type='dropdown' class='form-control' ui-jq='chosen' data-field-id="<?php echo $field['field_id'];?>">
													<?php

													if( ! empty($field['options']) )
													{
			                                            foreach($field['options'] as $option)
			                                            {
			                                        ?>
													<option value="<?php echo $option['option_value']; ?>" data-option-id="<?php echo $option['id'];?>"><?php echo $option['option_value']; ?></option>
								                    <?php
														}
													}
			                                        ?>
								                </select>
								            </div>
										</div>
										<div class='col-md-2' style='float:right;'>
											<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
										</div>
										<div class='editDropDown editFieldForm col-md-12' style='margin-left: 0px;'>
											<div class='control-group'>
												<label class='control-label'>Label:</label>
												<div class='form-group'>
													<input type='text' value="<?php echo $field['label'];?>" class='form-control'>
												</div>
											</div>

											<div class='control-group optionsContainer'>
												<div class='addOption'>
													<i class="fa fa-plus-circle"></i> Add New option
												</div><br/>
							                    <?php

												if( ! empty($field['options']) )
												{
		                                            foreach($field['options'] as $option)
		                                            {
		                                        ?>
							                    <div class='form-group'>
							                    	<label class='control-label'>Option: </label>
							                        <input type='text' value="<?php echo $option['option_value']; ?>" data-option-id="<?php echo $option['id'];?>" class='form-control'> <i class='fa fa-trash-o deleteOption'></i>
							                    </div>
							                    <?php
													}
												}
		                                        ?>
							                    <!-- <div class='form-group'>
							                    	<label class='control-label'>Option: </label>
							                        <input type='text' value='Choice 2' class='form-control'> <i class='fa fa-trash-o deleteOption'></i>
							                    </div>
							                    <div class='form-group'>
							                    	<label class='control-label'>Option: </label>
							                        <input type='text' value='Choice 3' class='form-control'> <i class='fa fa-trash-o deleteOption'></i>
							                    </div> -->
							                </div>

											<div class='control-group'>
												<label class='control-label'></label>
												<div class='controls' style='padding-bottom: 5px;'>
							                        <?php $checked = $field['is_required'] == "Yes" ? 'checked' : ''; ?>
													<label class="i-checks">
									                	<input type='checkbox' class='new required form-control' <?php echo $checked; ?>>
									                	<i></i>
									                 	Required
									                </label>
							                    </div>
							                    <label class='control-label'></label>
							                    <div class='controls' style='margin-left:125px;'>
							                    	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
							                        <button type='button' class='btn closeButton'>Close</button>
							                    </div>
							                </div>
							            </div>
								</div>
							</div>
						<?php
						break;
				}
			}
		}
	?>

</div>
<div class="row">
	<div class="col-md-12">
		<div class="field-box submitButtonDiv">
	        <label></label>
	        <input type="button" name="saveForm" class="btn btn-glow primary" value="<?php echo lang('save');?>">
	    </div>
    </div>
</div>