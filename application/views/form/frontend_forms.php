<!-- content -->
<?php echo Assets::js('admin/js/custom/frontend_form.js');?>
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('frontend_forms') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				
				<div class="alert alert-success" style="<?php if(!$this->session->flashdata('success')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="success-msg"><?php echo $this->session->flashdata('success'); ?></span>
				</div>

				<div class="alert alert-danger" style="<?php if(!$this->session->flashdata('error')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span><?php echo $this->session->flashdata('error'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">					
					<?php echo lang('frontend_forms') ?>
				</div>

				<div class="table-container">
					<div class="table-responsive">		
						<table class="tftable table table-striped m-b-none">
							<tr>
								<th class="text-center"><?php echo lang('name') ?></th>
								<th class="text-center"><?php echo lang('view_forms') ?></th>
								<th class="text-center"><?php echo lang('added_on') ?></th>
							</tr>
							<?php if(count($all_forms) > 0) { ?>
							<?php
								foreach($all_forms as $f)
								{
							?>
							<tr>
								<td class="text-center"><?php echo $f['title']; ?></td>
								<td class="text-center">
									<a type="button" class="btn btn-primary btn-xs view_forms" href="#form_modal" data-toggle="modal" data-id="<?php echo $f['form_id'];?>" data-form-id="<?php echo $f['id']?>"><?php echo lang('view') ?></a>
								</td>
								<td class="text-center"><?php echo date('d-m-Y G:i:s', strtotime($f['added_on']) ); ?></td>
							</tr>
							<?php } ?>
							<?php } else { ?>
								<tr>
									<td colspan="6" style="text-align:center;"><?php echo lang('no_data_found') ?> </td>
								</tr>
							<?php } ?>
						</table>
					</div>

				<footer class="panel-footer">
			  		<div class="row">
						<div class="col-sm-12 text-right text-center-xs">
							<?php echo $links; ?>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
</div>
<div id="form_modal" class="modal fade" data-backdrop="true" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('frontend_forms') ?></h3>
			</div>
		    <div class="modal-body">
		    	<div class="content"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
			</div>
		</div>
	</div>
</div>
<style>
	#form_modal .modal-body
	{
		max-height: 500px;
		overflow-y: scroll;
	}
</style>