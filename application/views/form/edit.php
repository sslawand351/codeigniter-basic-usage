<?php echo Assets::js("admin/js/custom/form_add.js"); ?>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">
		<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('forms') ?></h1>
				</div>
			</div>
		</div><!-- / main header -->
		
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo lang('add_form') ?>
				</div>
				<div class="panel-body">
					
					<div class="wrapper" style="padding-left:0px">
					    <ul id="myTabs" class="nav nav-tabs" role="tablist">
					        <li role="presentation" class="active"><a href="#info" id="info-tab" role="tab" data-toggle="tab" aria-controls="info" aria-expanded="true"><?php echo lang('form') ?></a></li>
					        <li role="presentation" class=""><a href="#configuration" id="configuration-tab" role="tab" data-toggle="tab" aria-controls="configuration" aria-expanded="true"><?php echo lang('configuration') ?></a></li>
					    </ul>
					    <br>
					    <div id="myTabContent" class="tab-content">
					        <div role="tabpanel" class="main_tabs tab-pane fade in active" id="info" aria-labelledBy="info-tab">
					        	<form action="<?php echo site_url('form/add')?>" id="edit_form" method="post" enctype="multipart/form-data">
					           		<?php $this->load->view('form/_form_edit'); ?>
					           	</form>
							</div>

							<div role="tabpanel" class="main_tabs tab-pane fade" id="configuration" aria-labelledBy="technical-tab">
					        	<?php $this->load->view('form/_form_edit_configuration'); ?>
					        </div>
					    </div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Form fields template -->

<!-- text field template -->
<div id="template-text-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Text field</label>
		</div>
		<div class="col-md-5">
			
				<div class='controls mainContainer col-md-10 m-l-n'>
					<div class="form-group">
						<input type='text' data-type='textField' class='form-control'>
					</div>
				</div>
				<div class='col-md-2' style='float:right;'>
					<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
				</div>
				<div class='editTextBox editFieldForm col-md-12' style='margin-left: 0px;'>
					<div class='control-group'>
						<label class='control-label'>Label:</label>
						<div class='form-group'>
							<input type='text' value='Text field' class='form-control'>
						</div>
					</div>

					<div class='control-group'>
						<label class='control-label'></label>
						<div class='controls' style='padding-bottom: 5px;'>
							<label class="i-checks">
			                	<input type='checkbox' class='new required form-control'>
			                	<i></i>
			                 	Required
			                </label>
	                    </div>
	                    <label class='control-label'></label>
	                    <div class='controls' style='margin-left:125px;'>
	                    	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                        <button type='button' class='btn closeButton'>Close</button>
	                    </div>
	                </div>
	            </div>
		</div>
	</div>
</div>

<!-- password template -->
<div id="template-password-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Password field</label>
		</div>
		<div class="col-md-5">
			
				<div class='controls mainContainer col-md-10 m-l-n'>
					<div class="form-group">
						<input type='password' data-type='passwordField' class='form-control'>
					</div>
				</div>
				<div class='col-md-2' style='float:right;'>
					<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
				</div>
				<div class='editPasswordField editFieldForm col-md-12' style='margin-left: 0px;'>
					<div class='control-group'>
						<label class='control-label'>Label:</label>
						<div class='form-group'>
							<input type='text' value='Password field' class='form-control'>
						</div>
					</div>

					<div class='control-group'>
						<label class='control-label'></label>
						<div class='controls' style='padding-bottom: 5px;'>
	                        <label class="i-checks">
			                	<input type='checkbox' class='new required form-control'>
			                	<i></i>
			                 	Required
			                </label>
	                    </div>
	                    <label class='control-label'></label>
	                    <div class='controls' style='margin-left:125px;'>
	                    	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                        <button type='button' class='btn closeButton'>Close</button>
	                    </div>
	                </div>
	            </div>
		</div>
	</div>
</div>

<!-- textarea template -->
<div id="template-textarea-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Text field (Large)</label>
		</div>
		<div class="col-md-5">
			<div class='controls mainContainer col-md-10 m-l-n'>
				<div class="form-group">
					<textarea data-type='textArea' class='form-control'></textarea>
				</div>
			</div>
			<div class='col-md-2' style='float:right;'>
				<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
			</div>
			<div class='editTextBoxLarge editFieldForm col-md-12' style='margin-left: 0px;'>
				<div class='control-group'>
					<label class='control-label'>Label:</label>
					<div class='form-group'>
						<input type='text' value='Text field (Large)' class='form-control'>
					</div>
				</div>

				<div class='control-group'>
					<label class='control-label'></label>
					<div class='controls' style='padding-bottom: 5px;'>
	                    <label class="i-checks">
		                	<input type='checkbox' class='new required form-control'>
		                	<i></i>
		                 	Required
		                </label>
	                </div>
	                <label class='control-label'></label>
	                <div class='controls' style='margin-left:125px;'>
	                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                    <button type='button' class='btn closeButton'>Close</button>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
</div>

<!-- date template -->
<div id="template-date-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Date field</label>
		</div>
		<div class="col-md-5">
			<div class='controls mainContainer col-md-10 m-l-n'>
				<div class="form-group">
					<input type='text' class='span2 form-control' data-date-format='mm/dd/yy' id='dp2' data-type='textDate'>
				</div>
			</div>
			<div class='col-md-2' style='float:right;'>
				<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
			</div>
			<div class='editDateField editFieldForm col-md-12' style='margin-left: 0px;'>
				<div class='control-group'>
					<label class='control-label'>Label:</label>
					<div class='form-group'>
						<input type='text' value='Date field' class='form-control'>
					</div>
				</div>

				<div class='control-group'>
					<label class='control-label'></label>
					<div class='controls' style='padding-bottom: 5px;'>
	                    <label class="i-checks">
		                	<input type='checkbox' class='new required form-control'>
		                	<i></i>
		                 	Required
		                </label>
	                </div>
	                <label class='control-label'></label>
	                <div class='controls' style='margin-left:125px;'>
	                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                    <button type='button' class='btn closeButton'>Close</button>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
</div>

<!-- email template -->
<div id="template-email-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Email field</label>
		</div>
		<div class="col-md-5">
			<div class='controls mainContainer col-md-10 m-l-n'>
				<div class="form-group">
					<input type='email' data-type='textEmail' class='form-control'>
				</div>
			</div>
			<div class='col-md-2' style='float:right;'>
				<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
			</div>
			<div class='editEmailField editFieldForm col-md-12' style='margin-left: 0px;'>
				<div class='control-group'>
					<label class='control-label'>Label:</label>
					<div class='form-group'>
						<input type='text' value='Email field' class='form-control'>
					</div>
				</div>

				<div class='control-group'>
					<label class='control-label'></label>
					<div class='controls' style='padding-bottom: 5px;'>
	                    <label class="i-checks">
		                	<input type='checkbox' class='new required form-control'>
		                	<i></i>
		                 	Required
		                </label>
	                </div>
	                <label class='control-label'></label>
	                <div class='controls' style='margin-left:125px;'>
	                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                    <button type='button' class='btn closeButton'>Close</button>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
</div>

<!-- website template -->
<div id="template-website-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Website field</label>
		</div>
		<div class="col-md-5">
			<div class='controls mainContainer col-md-10 m-l-n'>
				<div class="form-group">
					<input type='text' data-type='textWebsite' class='form-control'>
				</div>
			</div>
			<div class='col-md-2' style='float:right;'>
				<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
			</div>
			<div class='editWebsiteField editFieldForm col-md-12' style='margin-left: 0px;'>
				<div class='control-group'>
					<label class='control-label'>Label:</label>
					<div class='form-group'>
						<input type='text' value='Website field' class='form-control'>
					</div>
				</div>

				<div class='control-group'>
					<label class='control-label'></label>
					<div class='controls' style='padding-bottom: 5px;'>
	                    <label class="i-checks">
		                	<input type='checkbox' class='new required form-control'>
		                	<i></i>
		                 	Required
		                </label>
	                </div>
	                <label class='control-label'></label>
	                <div class='controls' style='margin-left:125px;'>
	                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                    <button type='button' class='btn closeButton'>Close</button>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
</div>

<!-- radio button template -->
<div id="template-radio-button-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Radio buttons</label>
		</div>
		<div class="col-md-5">
			<div class='controls mainContainer col-md-10 m-l-n radioButtonsContainer'>
				<div class="form-group">
					<input style='' type='radio' data-type='radioButton' name='choice' value='Choice 1' class='formElement'> &nbsp; Choice 1 &nbsp;
				</div>
				<div class="form-group">
	            	<input style='' type='radio' data-type='radioButton' name='choice' value='Choice 2' class='formElement'> &nbsp; Choice 2 &nbsp;
	            </div>
			</div>
			<div class='col-md-2' style='float:right;'>
				<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
			</div>
			<div class='editWebsiteField editFieldForm col-md-12' style='margin-left: 0px;'>
				<div class='control-group'>
					<label class='control-label'>Label:</label>
					<div class='form-group'>
						<input type='text' value='Radio buttons' class='form-control'>
					</div>
				</div>

				<div class='control-group optionsContainer'>
					<div class='addOption'>
						<i class="fa fa-plus-circle"></i> Add New option
					</div><br/>
	                <div class='form-group'>
	                	<label class='control-label'>Option: </label>
	                    <input type='text' value='Choice 1' class="form-control"> <i class='fa fa-trash-o deleteOption'></i>
	                </div>
	                <div class='form-group'>
	                	<label class='control-label'>Option: </label>
	                    <input type='text' value='Choice 2' class="form-control"> <i class='fa fa-trash-o deleteOption'></i>
	                </div>
	            </div>

				<div class='control-group'>
					<label class='control-label'></label>
					<div class='controls' style='padding-bottom: 5px;'>
	                    <label class="i-checks">
		                	<input type='checkbox' class='new required form-control'>
		                	<i></i>
		                 	Required
		                </label>
	                </div>
	                <label class='control-label'></label>
	                <div class='controls' style='margin-left:125px;'>
	                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                    <button type='button' class='btn closeButton'>Close</button>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
</div>

<!-- upload file template -->
<div id="template-upload-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Upload field</label>
		</div>
		<div class="col-md-5">
			<div class='controls mainContainer col-md-10 m-l-n'>
				<div class="form-group">
					<input type='file' data-type='fileUpload' class='form-control'>
				</div>
			</div>
			<div class='col-md-2' style='float:right;'>
				<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
			</div>
			<div class='editUploadField editFieldForm col-md-12' style='margin-left: 0px;'>
				<div class='control-group'>
					<label class='control-label'>Label:</label>
					<div class='form-group'>
						<input type='text' value='Upload field' class='form-control'>
					</div>
				</div>

				<div class='control-group'>
					<label class='control-label'></label>
					<div class='controls' style='padding-bottom: 5px;'>
	                    <label class="i-checks">
		                	<input type='checkbox' class='new required form-control'>
		                	<i></i>
		                 	Required
		                </label>
	                </div>
	                <label class='control-label'></label>
	                <div class='controls' style='margin-left:125px;'>
	                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                    <button type='button' class='btn closeButton'>Close</button>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
</div>

<!-- checkbox template -->
<div id="template-checkbox-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Check boxes</label>
		</div>
		<div class="col-md-5">
			<div class='controls mainContainer col-md-10 m-l-n  checkBoxesContainer'>
				<div class="form-group">
					<label class="i-checks">
	                	<input style='margin-top:0px;' type='checkbox' data-type='checkbox' name='choice' value='Choice 1' class='form-control'>
	                	<i></i>
	                 	&nbsp; Choice 1 &nbsp;
	                </label>
	            </div>
			</div>
			<div class='col-md-2' style='float:right;'>
				<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
			</div>
			<div class='editCheckBoxes editFieldForm col-md-12' style='margin-left: 0px;'>
				<div class='control-group'>
					<label class='control-label'>Label:</label>
					<div class='form-group'>
						<input type='text' value='Check box' class='form-control'>
					</div>
				</div>

				<div class='control-group optionsContainer'>
					<div class='addOption'>
						<i class="fa fa-plus-circle"></i> Add New option
					</div><br/>
	                
	                <div class='form-group'>
	                	<label class='control-label'>Option: </label>
	                    <input type='text' value='Choice 1' class="form-control"> <i class='fa fa-trash-o deleteOption'></i>
	                </div>
	            </div>

				<div class='control-group'>
					<label class='control-label'></label>
					<div class='controls' style='padding-bottom: 5px;'>
	                    <label class="i-checks">
		                	<input type='checkbox' class='new required form-control'>
		                	<i></i>
		                 	Required
		                </label>
	                </div>
	                <label class='control-label'></label>
	                <div class='controls' style='margin-left:125px;'>
	                	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                    <button type='button' class='btn closeButton'>Close</button>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
</div>

<!-- dropdown template -->
<div id="template-dropdown-field" class="template-form-field">
	<div class="row">
		<div class="col-md-3">
			<label class='control-label'>Dropdown</label>
		</div>
		<div class="col-md-5">
			
				<div class='controls mainContainer col-md-10 m-l-n dropdownContainer'>
					<div class="form-group">
						<select name='dropdown' data-type='dropdown' class='form-control' ui-jq='chosen'>
							<option value='Choice 1'>Choice 1</option>
		                    <option value='Choice 2'>Choice 2</option>
		                    <option value='Choice 3'>Choice 3</option>
		                </select>
		            </div>
				</div>
				<div class='col-md-2' style='float:right;'>
					<i class="fa fa-edit editThisField"></i> <i class='icon-trash icon loneIcon deleteThisField'></i>
				</div>
				<div class='editDropDown editFieldForm col-md-12' style='margin-left: 0px;'>
					<div class='control-group'>
						<label class='control-label'>Label:</label>
						<div class='form-group'>
							<input type='text' value='Dropdown' class='form-control'>
						</div>
					</div>

					<div class='control-group optionsContainer'>
						<div class='addOption'>
							<i class="fa fa-plus-circle"></i> Add New option
						</div><br/>
	                    
	                    <div class='form-group'>
	                    	<label class='control-label'>Option: </label>
	                        <input type='text' value='Choice 1' class='form-control'> <i class='fa fa-trash-o deleteOption'></i>
	                    </div>
	                    <div class='form-group'>
	                    	<label class='control-label'>Option: </label>
	                        <input type='text' value='Choice 2' class='form-control'> <i class='fa fa-trash-o deleteOption'></i>
	                    </div>
	                    <div class='form-group'>
	                    	<label class='control-label'>Option: </label>
	                        <input type='text' value='Choice 3' class='form-control'> <i class='fa fa-trash-o deleteOption'></i>
	                    </div>
	                </div>

					<div class='control-group'>
						<label class='control-label'></label>
						<div class='controls' style='padding-bottom: 5px;'>
	                        <label class="i-checks">
			                	<input type='checkbox' class='new required form-control'>
			                	<i></i>
			                 	Required
			                </label>
	                    </div>
	                    <label class='control-label'></label>
	                    <div class='controls' style='margin-left:125px;'>
	                    	<button type='button' class='btn saveButton' style='margin-right:10px;'>Save</button>
	                        <button type='button' class='btn closeButton'>Close</button>
	                    </div>
	                </div>
	            </div>
		</div>
	</div>
</div>

<!-- End of form fields template -->

<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
.chosen-container{
	width:100%!important;
}
</style>