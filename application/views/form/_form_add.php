<?php echo Assets::css("admin/css/custom/form_add.css"); ?>
<div class="row">
	<div class="col-sm-3">
		<label>Form name<?php echo lang('form_name') ?><span class="text-danger"> *</span> :</label>
	</div>
	<div class="col-sm-5">
	    <div class="form-group">
	        
	        <input type="text" name="form_name" id="formTitle" value="<?php echo $this->input->post('form_name'); ?>" class="form-control">
			<?php if(form_error('form_name')) { ?>
				<div class="text-danger"><?php echo form_error('form_name'); ?></div>
			<?php } ?>
	    </div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<label>Add New Field<?php echo lang('add_field');?></label>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			<?php if(form_error('fieldType')){$fieldType_error ="error";}?>
            <div class="field-box <?php if(isset($fieldType_error)) echo $fieldType_error;?>">
	            <select class="form-control" name="fieldType" id="fieldType" ui-jq='chosen'> 
	                <option value="">-- <?php echo lang('select_one');?> --</option>
	                <option value="textField"><?php echo lang('text_field');?></option>
	                <option value="passwordField"><?php echo lang('password_field');?></option>
	                <option value="textFieldLarge"><?php echo lang('textarea');?></option>
	                <option value="dateField"><?php echo lang('date_field');?></option>
	                <option value="emailField"><?php echo lang('email_field');?></option>
	                <option value="websiteField"><?php echo lang('website_field');?></option>
	                <option value="radioButtons"><?php echo lang('radio_button');?></option>
	                <!-- <option value="uploadField"><?php echo lang('upload_field');?></option> -->
	                <option value="checkBoxes"><?php echo lang('checkbox');?></option>
	                <option value="dropdown"><?php echo lang('dropdown');?></option>
	            </select>
            </div>
		</div>
	</div>
</div>
<div class="form-fields">



</div>
<div class="row">
	<div class="col-md-12">
		<div class="field-box submitButtonDiv">
	        <label></label>
	        <input type="button" name="createForm" class="btn btn-glow primary" value="<?php echo lang('save');?>">
	    </div>
    </div>
</div>