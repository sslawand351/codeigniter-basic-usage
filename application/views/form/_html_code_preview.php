<div class="col-md-12 form_view">
<h4><?php echo $form['title'];?></h4>
<form action="" id="frontend_form" method="POST" enctype="multipart/form-data" class="form-horizontal form-groups-bordered">
<input type="hidden" class="form_id" name="form_id" value="<?php echo $form['id']; ?>">
<input type="hidden" class="form_name" name="form_name" value="<?php echo $form['title']; ?>">
<input type="hidden" name="url" class="url" value="<?php echo $url; ?>">

<?php 
  // Build form using fields array

  if( ! empty($form_fields) )
  {
    foreach ($form_fields as $f_key => $field )
    {
      $name = str_replace('-', '_', $field['shortcode']);
      $error_div = '';
      $value = isset($post_data[$name]) ? $post_data[$name] : '';

      if($field['is_required'] == 'Yes')
      { 
        $error_div = '<span class="required text-danger">*</span>';
      }

      switch( $field['field_type'] )
      {
        case FORM_FIELD_TYPE_TEXT:
        ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n'>
      <div class="form-group">
        <input type="text" class="form-control" name="<?php echo $name; ?>" value="<?php echo $value; ?>" readonly>
      </div>
    </div>
  </div>
</div>
<?php
          break;

        case FORM_FIELD_TYPE_PASSWORD:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n'>
      <div class="form-group">
        <input type='password' class='form-control' name="<?php echo str_replace('-', '_', $field['shortcode']); ?>" readonly>
      </div>
    </div>
  </div>
</div>
<?php
          break;

        case FORM_FIELD_TYPE_TEXTAREA:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n'>
      <div class="form-group">
        <textarea class='form-control' name="<?php echo $name; ?>" readonly><?php echo $value;?></textarea>
      </div>
    </div>
  </div>
</div>
<?php
          break;

        case FORM_FIELD_TYPE_DATE:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n'>
      <div class="form-group">
        <input type='text' class='span2 form-control' data-date-format='mm/dd/yy' id='dp2' name="<?php echo $name; ?>" value="<?php echo $value;?>" placeholder="dd-mm-yyyy" readonly>
      </div>
    </div>
  </div>
</div>
<?php
          break;

        case FORM_FIELD_TYPE_EMAIL:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n'>
      <div class="form-group">
        <input type='email' class='form-control' name="<?php echo $name; ?>" value="<?php echo $value;?>" readonly>
      </div>
    </div>
  </div>
</div>
<?php
          break;

        case FORM_FIELD_TYPE_WEBSITE:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n'>
      <div class="form-group">
        <input type='text' class='form-control' name="<?php echo $name; ?>" value="<?php echo $value;?>" readonly>
      </div>
    </div>
  </div>
</div>  
<?php
          break;

        case FORM_FIELD_TYPE_RADIO_BUTTON:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n radioButtonsContainer'>
<?php

      if( ! empty($field['options']) )
      {
                foreach($field['options'] as $option)
                {
                  $checked = $value == $option['option_value'] ? 'checked' : '';
            ?>
      <div class="form-group">
        <input style='' type='radio' class='formElement' name="<?php echo $name; ?>" value="<?php echo $option['option_value']; ?>" <?php echo $checked; ?> readonly> &nbsp; <?php echo $option['option_value'] ?> &nbsp;
      </div>
<?php
        }
            ?>
<?php
      }
            ?>
    </div>
  </div>
</div>
<?php
          break;

        case FORM_FIELD_TYPE_FILE_UPLOAD:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n'>
      <div class="form-group">
        <input type='file' class='form-control' name="<?php echo $name; ?>" >
      </div>
    </div>
  </div>
</div>
<?php
          break;

        case FORM_FIELD_TYPE_CHECKBOX:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n  checkBoxesContainer'>
      <div class="form-group">
<?php

        if( ! empty($field['options']) )
        {
                    foreach($field['options'] as $option)
                    {
                      $checked = is_array($value) && in_array($option['option_value'], $value) ? 'checked' : '';
                ?>
        <label class="i-checks">
          <input type='checkbox' name="<?php echo $name . '[]'; ?>" value="<?php echo $option['option_value']; ?>" <?php echo $checked; ?> disabled>
            <i></i>
            &nbsp; <?php echo $option['option_value']; ?> &nbsp;
        </label><br>
<?php
          }
            ?>
<?php
      }
                ?>
      </div>
    </div>
  </div>
</div>
<?php
          break;

        case FORM_FIELD_TYPE_SELECT_DROPDOWN:
          ?>
<div class="row">
  <div class="col-md-3">
    <label class='control-label'><?php echo $field['label'];?> <?php echo $error_div; ?></label>
  </div>
  <div class="col-md-5">
    <div class='controls mainContainer col-md-10 m-l-n dropdownContainer'>
      <div class="form-group">
        <select name="<?php echo $name; ?>" class='form-control' ui-jq='chosen' disabled>
<?php

          if( ! empty($field['options']) )
          {
                        foreach($field['options'] as $option)
                        {
                          $selected = $value == $option['option_value'] ? 'selected' : '';
                    ?>
          <option value="<?php echo $option['option_value']; ?>" <?php echo $selected; ?>><?php echo $option['option_value']; ?></option>
<?php
            }
          }
                    ?>
        </select>
      </div>
    </div>
  </div>
</div>
<?php
          break;
      }
    }
  }
?>
</form>
</div>
<style>
  .form_view
  {
    border-bottom:1px solid #dee5e7;
  }
</style>