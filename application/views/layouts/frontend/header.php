<!DOCTYPE html>

<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo WEBSITE_NAME; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link href="<?php echo base_url()?>resources/css/bootstrap.min.css" type="text/css" rel="stylesheet">
  <script src="<?php echo base_url()?>resources/js/jquery.min.js"></script>
</head>


<script type="text/javascript">
  var SITE_URL="<?php echo site_url(); ?>";
  var BASE_URL="<?php echo base_url(); ?>";

</script>
<body class=" ">
 <nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
        </div>
        <ul class="nav navbar-nav pull-right">
        <form action="<?php echo site_url($this->session->userdata('flang').'/update-lang');?>" method ="post" id="language_frm">
          <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id');?>"/>
          <input type="hidden" name="url" value="<?php echo current_url();?>"/>
          <select class="change_language" name="language">
              <?php 
                  $languages = get_frontend_languages();

                  if(isset($languages) && !empty($languages)){
                      foreach($languages as $language){ 
                          
              ?>
              <?php //pr($_SESSION);pr($_COOKIE);?>
                  <option value="<?php echo strtolower($language['code']);?>" <?php if(strtolower($language['code']) == $this->session->userdata('flang')){echo 'selected';}?>><?php echo $language['code'];?></option>
              <?php }}?>
          </select>
      </form>
            <?php if(!$this->session->userdata('user_id'))
            {?>
                <li><a href="<?php echo site_url($this->session->userdata('flang').'/login')?>">Login</a></li>
            <?php }else{?>
                <li><a href="<?php echo site_url($this->session->userdata('flang').'/logout')?>">Logout</a></li>  
            <?php }?>
        </ul>
    </div>
</nav>

<?php  if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong></strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>
<?php  if($this->session->flashdata('error')) { ?>            
<div class="alert alert-danger">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>