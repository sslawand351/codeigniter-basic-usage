<div class="footer footer2">
    <div class="container footer-container">
        <div class="col-xs-12 col-sm-4 name div">
            <i class="ion-ios-email-outline active col-xs-4 newsletter" data-pack="ios" data-tags="snail, mail" style="display: inline-block;"></i>
            <span class="col-xs-8 ">Subscribe to our newsletter</span>
        </div>
        <div class="col-xs-12 col-sm-8 lt value div">

            <div class="email-container">
                <!-- <form class="submit_newsletter" > -->
                    <input type="text" name="email" class="subscriber-email" placeholder="<?php echo lang('email');?>"/>
                    <div class="error text-danger subscriber-email-error"></div>
                    <button type="button" class="subscribe-to-newsletter" >subscribe</button>
                <!-- </form> -->
            </div>
        </div>
        <div class="newsletter-subscription-msg" style="display:none;">
            <div class="alert alert-success"></div>
            <div class="alert alert-error"></div>
        </div>
    </div>
</div>

    <?php echo Assets::js('frontend/js/jquery.min.js'); ?>
    <?php echo Assets::js('frontend/js/bootstrap.min.js'); ?>
    <?php echo Assets::js('frontend/js/global.js'); ?>
    <?php echo Assets::js('frontend/js/dropzone.js'); ?>
       <?php
        $controller = $this->router->fetch_class();
        $action = $this->router->fetch_method();

        echo Assets::js("frontend/js/custom/".$controller."_".$action.".js");
        echo Assets::css("frontend/css/custom/".$controller."_".$action.".css");
    ?>
  </body>
</html>