<?php


if(isset($header) && $header)
    $this->load->view('header');

if(isset($inner_header) && $inner_header)
    $this->load->view('inner_pages_header');

if(isset($_view) && $_view)
    $this->load->view($_view);

?>