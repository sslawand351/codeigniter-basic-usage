<div class="msgs"></div>
<input type="hidden" class="active_group_id" name="group_id" value="<?php echo $this->input->post('group_id'); ?>">
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label"><?php echo lang('name') ?> :</label>
            <input type="text" class="form-control required" name="name" value="<?php echo $this->input->post('name'); ?>">
            
            <span class="text-danger error"><?php echo form_error('name'); ?></span>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label"><?php echo lang('email') ?> :</label>
            <input type="text" class="form-control required" name="email" value="<?php echo $this->input->post('email'); ?>">
            
            <span class="text-danger error"><?php echo form_error('email'); ?></span>
        </div>
    </div>
</div>