<?php echo Assets::js("admin/js/jquery.bootstrap.wizard.js"); ?>
<?php echo Assets::css("admin/css/bootstrap-chosen.css"); ?>
<?php echo Assets::js("admin/js/custom/group_users.js"); ?>
<style type="text/css">
.chosen-container {
    width: 100%!important;
}
.modal-header h3
{
	color: #58666e !important;
}
td.splitedcontenttableinset {
    padding: 10px;
}
</style>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('groups') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				
				<div class="alert alert-success" style="<?php if(!$this->session->flashdata('success_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <span id="org_success_msg"><?php echo $this->session->flashdata('success_msg'); ?></span>
				</div>
				
				           
				<div class="alert alert-danger" style="<?php if(!$this->session->flashdata('error_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="org_error_msg"><?php echo $this->session->flashdata('error_msg'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo lang('list_of_users_of_group') ?><?php echo $group['name']; ?>
				</div>

				<div class="panel-body">

					<div class="row row_even">
						<div class="col-md-12 col-sm-12">
							<label><?php echo lang('search_user') ?></label>
						</div>
					</div>

					<div class="row row_odd">
						<div class="col-md-4">
							<label class="control-label"><?php echo lang('email') ?> :</label>
							<input class="form-control" type="text" name="email" id="email-search" value="" placeholder="<?php echo lang('type_or_paste_here') ?>">
						</div>
						<div class="col-md-4">
							<label class="control-label"></label>
							<button style="margin-bottom:0px;margin-top:25px;"  class="btn btn-success search-button" id="user-search"><?php echo lang('search') ?></button>
						</div>
					</div>
					
					<div class="wrapper" style="padding-left:0px">
					    <ul id="myTabs" class="nav nav-tabs" role="tablist">
					    	<li role="presentation" class="active"><a href="#info" id="info-tab" role="tab" data-toggle="tab" aria-controls="info" aria-expanded="true"><?php echo lang('contacts') ?></a></li>
					        <li role="presentation"><a href="#technical" id="technical-tab" role="tab" data-toggle="tab" aria-controls="technical" aria-expanded="true"><?php echo lang('unsubscribed') ?></a></li>
					    </ul>
					    <br>
					    <div id="myTabContent" class="tab-content">
					    	<div role="tabpanel" class="main_tabs tab-pane fade in active" id="info" aria-labelledBy="info-tab">
								<div class="subscribed-table-container">
									
								</div>
							</div>

					        <div role="tabpanel" class="main_tabs tab-pane fade" id="technical" aria-labelledBy="technical-tab">
					        	<div class="unsubscribed-table-container">
						        	
								</div>
					        </div>
					    </div>
					</div>
				</div>	
				<footer class="panel-footer">
			  		<div class="row">
						<div class="col-sm-12 text-right text-center-xs">
							<?php //echo $links; ?>
						</div>
					</div>
				</footer>
				
			</div>
		</div>
	</div>
</div>

<!-- delete-user-from-group-popup Modal -->
<div id="delete-user-from-group-popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('delete_user') ?></h3>
			</div>
			<form id="form-delete-user-from-group" action="<?php echo site_url('group/delete_user_from_group'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="group_id" name="group_id" value="">
		    		<input type="hidden" class="group_user_id" name="group_user_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    			<?php echo lang('are_you_sure_you_want_to_delete_this_user_from_group') ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="delete-user-from-group-btn btn btn-default"><?php echo lang('ok') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of delete-user-from-group-popup Modal -->

<!-- unsubscribe-user-from-group-popup Modal -->
<div id="unsubscribe-user-from-group-popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('unsubscribe') ?></h3>
			</div>
			<form id="form-unsubscribe-user-from-group" action="<?php echo site_url('group/unsubscribe_user_from_group'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="group_id" name="group_id" value="">
		    		<input type="hidden" class="group_user_id" name="group_user_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    			<?php echo lang('are_you_sure_you_want_to_unsubscribe_this_user_from_group') ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="unsubscribe-user-from-group-btn btn btn-default"><?php echo lang('ok') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of unsubscribe-user-from-group-popup Modal -->

<!-- undo-unsubscribe-user-from-group-popup Modal -->
<div id="undo-unsubscribe-user-from-group-popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('delete_user') ?></h3>
			</div>
			<form id="form-undo-unsubscribe-user-from-group" action="<?php echo site_url('group/subscribe_user_to_group'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="group_id" name="group_id" value="">
		    		<input type="hidden" class="group_user_id" name="group_user_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    			<?php echo lang('are_you_sure_you_want_to_subscribe_this_user_again_to_group') ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="undo-unsubscribe-user-from-group-btn btn btn-default"><?php echo lang('ok') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of undo-unsubscribe-user-from-group-popup Modal -->

<!-- editGroup Modal -->
<div class="modal fade" id="edit-user-from-group-popup"> 
	<div class="modal-dialog"> 
		<div class="modal-content"> 
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('edit_user_from_group') ?></h3>
			</div>
			<form id="form-edit-group-user" method="post" action="">
				<div class="modal-body">
					<!-- The modal body is appended in ajax request -->

				</div>
				<div class="modal-footer">
					<input class="btn btn-info" type="button" id="btn-edit-group-user" value="<?php echo lang('update'); ?>" name="" />
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
			</form>
		</div>
	</div>
</div><!-- End Of editGroup Modal -->

<input type="hidden" id="hidden-group-id" value="<?php echo $group['id'];?>">