<div class="unsubscribed-table-container">
<br>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th></th>
				<th><?php echo lang('name') ?></th>
				<th><?php echo lang('email') ?></th>
				<th><?php echo lang('unsubscribed_on'); ?></th>
				<th colspan="2"><?php echo lang('action') ?></th>
			</tr>
			<?php if(!empty($unsubscribed_users) ) { ?>
			<?php
				foreach($unsubscribed_users as $value)
				{
			?>
			<tr>
				<td><?php echo ++$offset; ?></td>
				<td><?php echo $value['user_name']; ?></td>
				<td><?php echo $value['email']; ?></td>

				<?php //echo lang('unsubscribed_on'); ?>
				<td><?php //$unsubscription = explode('::', $value['unsubscribe_user']);
					
				?><?php echo $value['unsubscribe_user']; ?></td>
				<td>
					<a href="#delete-user-from-group-popup" data-toggle="modal" data-group-id="<?php echo $value['group_id']; ?>" data-id="<?php echo $value['id']; ?>" class="delete-user-from-group btn btn-xs btn-danger"><?php echo lang('delete') ?></a>
					<a href="#undo-unsubscribe-user-from-group-popup" data-toggle="modal" data-group-id="<?php echo $value['group_id']; ?>" data-id="<?php echo $value['id']; ?>" class="undo-unsubscribe-user-from-group btn btn-xs btn-primary"><?php echo lang('undo_unsubscribe') ?></a>
				</td>
			</tr>
			<?php } ?>
			<?php } else{ ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_users_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
</div>