<div class="row">
	<div class="col-sm-6">
        <div class="form-group">
            <label><?php echo lang('name') ?><span class="text-danger"> *</span> :</label>
            <input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control">
			<?php if(form_error('name')) { ?>
				<div class="text-danger error"><?php echo form_error('name'); ?></div>
			<?php } ?>
        </div>
    </div>
</div>