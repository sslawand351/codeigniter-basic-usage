<div class="subscribed-table-container">
<br>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th></th>
				<th><?php echo lang('name') ?></th>
				<th><?php echo lang('email') ?></th>
				<th colspan="2"><?php echo lang('action') ?></th>
			</tr>
			<?php if(!empty($users) ) { ?>
			<?php
				foreach($users as $value)
				{
			?>
			<tr>
				<td><?php echo ++$offset; ?></td>
				<td><?php echo $value['user_name']; ?></td>
				<td><?php echo $value['email']; ?></td>
				<td>
					<a href="javascript:void(0);" data-group-id="<?php echo $value['group_id']; ?>" data-id="<?php echo $value['id']; ?>" data-email="<?php echo $value['email']; ?>" class="open_edit_group_user btn btn-xs btn-info"><?php echo lang('edit') ?></a>
					<a href="#delete-user-from-group-popup" data-toggle="modal" data-group-id="<?php echo $value['group_id']; ?>" data-id="<?php echo $value['id']; ?>" class="delete-user-from-group btn btn-xs btn-danger"><?php echo lang('delete') ?></a>
					<a href="#unsubscribe-user-from-group-popup" data-toggle="modal" data-group-id="<?php echo $value['group_id']; ?>" data-id="<?php echo $value['id']; ?>" class="unsubscribe-user-from-group btn btn-xs btn-primary"><?php echo lang('unsubscribe') ?></a>
				</td>
			</tr>
			<?php } ?>
			<?php } else{ ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_users_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right text-center-xs">
			<?php echo $links; ?>
		</div>
	</div>
</div>