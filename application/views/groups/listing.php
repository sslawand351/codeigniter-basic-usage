<?php echo Assets::js("admin/js/custom/group_listing.js"); ?>
<?php echo Assets::js("admin/js/jquery.bootstrap.wizard.js"); ?>
<?php echo Assets::css("admin/css/bootstrap-chosen.css"); ?>
<?php echo Assets::js("admin/ckeditor/ckeditor.js"); ?>
<?php echo Assets::js("admin/ckeditor/samples/js/sample.js"); ?>
<?php echo Assets::js("admin/js/custom/send_newsletter.js"); ?>
<?php echo Assets::css("admin/css/custom/newsletter_template_listing.css"); ?>

<style type="text/css">
.chosen-container {
    width: 100%!important;
}
.modal-header h3
{
	color: #58666e !important;
}
td.splitedcontenttableinset {
    padding: 10px;
}
</style>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('groups') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="msgs">
				
				<div class="alert alert-success" style="<?php if(!$this->session->flashdata('success_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <span id="org_success_msg"><?php echo $this->session->flashdata('success_msg'); ?></span>
				</div>
				
				           
				<div class="alert alert-danger" style="<?php if(!$this->session->flashdata('error_msg')) echo 'display:none'; ?>">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<span id="org_error_msg"><?php echo $this->session->flashdata('error_msg'); ?></span>
				</div>
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="#send-newsletter-popup" data-toggle="modal" class="btn btn-primary btn-xs pull-right open-send-newsletter"><?php echo lang('send_newsletter') ?></a>
					<?php /*if( check_permission('add_group') ) {*/ ?>
					<a href="#addGroup" data-toggle="modal" class="btn btn-success btn-xs pull-right" id="open-add-group-popup" style="margin-right: 5px;"><?php echo lang('add_group') ?></a>
					
					<div class="btn-group pull-right csv">
					  <button type="button" class="btn btn-primary dropdown-toggle btn-xs" style="margin-right: 5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <?php echo lang('bulk_upload') ?> <span class="caret"></span>
					  </button>
					  	<ul class="dropdown-menu">
							<li><a href="#add-users-to-group-from-csv" data-toggle="modal" class="add-users-to-group-from-csv"><?php echo lang('add_users_from_csv') ?></a></li>
							<li><a href="<?php echo site_url('uploads/bulk upload csv/users.csv')?>" download><?php echo lang('sample_csv')?></a></li>
						</ul>
					</div>

					<?php /*}*/ ?>

					<?php echo lang('group_listing') ?>
				</div>

				<?php 
					// load table view
					$this->load->view('groups/_table');
				?>
			</div>
		</div>
	</div>
</div>

<!-- delete-group-popup Modal -->
<div id="delete-group-popup" class="modal fade" data-backdrop="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('delete_group') ?></h3>
			</div>
			<form id="form-delete-group" action="<?php echo site_url('group/delete'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="group_id" name="group_id" value="">
		    		<div class="row">
		    			<div class="col-md-12">
		    			<?php echo lang('are_you_sure_you_want_to_delete_this_group') ?>
		    			</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="delete-group-btn btn btn-default"><?php echo lang('ok') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of delete-group-popup Modal -->

<select id="clients_and_leads" style="display:none;">
	<?php /*foreach($leads as $lead) { $val['lead_id'] = $lead['id']; $val['user_id'] = $lead['client_user_id']; ?>
			<option value="<?php echo $lead['id']; ?>" data-value="<?php json_encode($val)?>" class="leads_org_type" data-leadid="<?php echo $lead['id']; ?>" data-userid="<?php echo $lead['client_user_id']; ?>" data-client="0" data-email="<?php echo $lead['email']; ?>" data-first-name="<?php echo $lead['first_name']; ?>" data-last-name="<?php echo $lead['last_name']; ?>" data-street="<?php echo $lead['street']; ?>" data-house-number="<?php echo $lead['house_number']; ?>" data-postal-code="<?php echo $lead['postal_code']; ?>" data-mobile="<?php echo $lead['mobile']; ?>" data-town="<?php echo $lead['town']; ?>" data-location-id="<?php echo $lead['location_id']; ?>" ><?php echo $lead['first_name'] . ' ' . $lead['last_name'] . ' ( ' . $lead['email'] . ' ) '; ?></option> 
		<?php } ?>
		<?php if( ! empty($lead_clients) ) { foreach($lead_clients as $c) { ?>
            <option  value="<?php echo $c['user_id']; ?>" class="users_org_type" data-leadid="0" data-userid="<?php echo $c['user_id']; ?>" data-client="1" data-email="<?php echo $c['email']; ?>" data-first-name="<?php echo $c['first_name']; ?>" data-last-name="<?php echo $c['last_name']; ?>" data-street="<?php echo $c['street']; ?>" data-house-number="<?php echo $c['house_number']; ?>" data-postal-code="<?php echo $c['postal_code']; ?>" data-mobile="<?php echo $c['mobile']; ?>" data-town="<?php echo $c['town']; ?>"><?php echo $c['first_name'] . ' ' . $c['last_name'] . ' ( ' . $c['email'] . ' ) ';  ?></option>
        <?php } }*/ ?>
</select>

<!-- add-users-popup Modal -->
<div id="add-users-popup" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="margin:0px;" class="add-users-to-group-title" data-value="<?php echo lang('add_users_to_group'); ?>"><?php echo lang('add_users_to_group'); ?></h3>
			</div>
			<form id="form-add-users-to-group" action="<?php echo site_url('group/associate_users'); ?>" method="post">
			    <div class="modal-body">
		    		<input type="hidden" class="active_group_id" name="group_id" value="">
		    		<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('name') ?> :</label>
                                <input type="text" class="form-control required" name="name" value="">
                                
                                <span class="text-danger error"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('email') ?> :</label>
                                <input type="text" class="form-control required" name="email" value="">
                                
                                <span class="text-danger error"></span>
                            </div>
                        </div>
					</div>
					<?php /* ?>
		    		<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('select_existing_lead_or_customers') ?> :</label>
                                <!-- select users Dropdown -->
                                <select multiple class="w-md form-control" style="display:none;" name="users[]" id="associated_users">
                            	</select>
                                
                                <span class="text-danger error"></span>
                            </div>
                        </div>
					</div>
					<?php */ ?>
				</div>
				<div class="modal-footer">
					<button type="submit" class="save_group_has_users btn btn-success"><?php echo lang('save') ?></button>
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
	    	</form>
		</div>
	</div>
</div><!-- End of add-users-popup Modal -->

<!-- addGroup Modal -->
<div class="modal fade" id="addGroup"> 
	<div class="modal-dialog"> 
		<div class="modal-content"> 
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('add_group') ?></h3>
			</div>
			<form id="form-add-group" method="post" action="<?php echo site_url('admin/group/add_group')?>">
				<div class="modal-body">
					<div class="row">
		          		<div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('name') ?><span class="text-danger"> *</span> :</label>
					            <input type="text" name="name" value="" class="form-control">
								<?php if(form_error('name')) { ?>
									<div class="text-danger"><?php echo form_error('name'); ?></div>
								<?php } ?>
				            </div>

				        </div>
				    </div>
				</div>
				<div class="modal-footer">
					<input class="btn btn-info" type="button" id="btn-add-group" value="<?php echo lang('add_group'); ?>" name="" />
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
			</form>
		</div>
	</div>
</div><!-- End Of addGroup Modal -->

<!-- editGroup Modal -->
<div class="modal fade" id="editGroup"> 
	<div class="modal-dialog"> 
		<div class="modal-content"> 
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('edit_group') ?></h3>
			</div>
			<form id="form-edit-group" method="post" action="<?php echo site_url('group/edit_group')?>">
				<div class="modal-body">
					<!-- The modal body is appended in ajax request -->

				</div>
				<div class="modal-footer">
					<input class="btn btn-info" type="button" id="btn-edit-group" value="<?php echo lang('update'); ?>" name="" />
					<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
				</div>
			</form>
		</div>
	</div>
</div><!-- End Of editGroup Modal -->

<!-- Add Users to group via csv import Modal -->
<div class="modal fade" id="add-users-to-group-from-csv"> 
	<div class="modal-dialog"> 
		<div class="modal-content"> 
			<div class="modal-header">
				<h3 style="margin:0px;"><?php echo lang('add_users_from_csv') ?></h3>
			</div>
			<form id="form-add-users-to-group-from-csv" name="form_add_users_csv" method="post" action="<?php echo site_url('group/import_csv_emails')?>" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="row">
		          		<div class="col-sm-6">
				            <div class="form-group">
					            <label>Select CSV file<span class="text-danger"> *</span> :</label>
					            <input type="file" name="csv_file" value="" class="form-control" id="csv">
								<div class="alert-error text-danger invalid_file" data-text="<?php echo lang('please_select_valid_file')?>"><?php echo $this->session->flashdata('error') ? $this->session->flashdata('error') : ''; ?></div>
				            </div>
				        </div>
				        <div class="col-sm-6">
                           <div class="form-group">
                                <label class="control-label" for="group"><?php echo lang('select_group') ?><span class="text-danger"> *</span> :</label>

                                <select multiple class="w-md form-control required" name="group_id[]" id="group-list-csv" ui-jq="chosen">
									<?php 
									    if( ! empty($groups['data']) )
									    {
									        foreach ($groups['data'] as $g)
									        {
									?>
									<option value="<?php echo $g['id']; ?>"><?php echo $g['name']; ?></option>
									<?php 
									        }
									    }
									?>												
								</select>
								<span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
                            </div>
                        </div>
				    </div>
					<div class="modal-footer">
						<input class="btn btn-info" type="button" id="btn-add-users-via-csv" value="<?php echo lang('save'); ?>" name="" />
						<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div><!-- End Of Add Users to group via csv import Modal -->

<style>
.chosen-container{
width:100%!important;
}

.search-field .default{
    width: 100%!important;
}
.add-organisation
{
	z-index: 9999999;
}
#send-newsletter-popup .modal-dialog
{
	width:55%;
}
@media (max-width:768px)
{
	#send-newsletter-popup .modal-dialog
	{
		width:96%;
	}
}
</style>



<!-- Send Newsletter Form wizard -->
<div class="modal fade" id="send-newsletter-popup" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3><?php echo lang('send_newsletter') ?></h3>
			</div>
			<div class="modal-body">
				<div class="message"></div>
				<form id="form-send-newsletter" method="post" action="<?php echo site_url("setting/send-newsletter"); ?>">
					<div id="rootwizard">
						<div class="navbar">
							<div class="navbar-inner">
								<ul>
									<li><a href="#tab1" data-toggle="tab"><?php echo lang('select_newsletter') ?></a></li>
									<li><a href="#tab2" data-toggle="tab"><?php echo lang('select_groups_and_users') ?></a></li>
									<li><a href="#tab3" data-toggle="tab"><?php echo lang('email_body') ?></a></li>
									<li><a href="#tab4" data-toggle="tab"><?php echo lang('email_preview') ?></a></li>
									<li><a href="#tab5" data-toggle="tab"><?php echo lang('send') ?></a></li>
								</ul>
							</div>
						</div>
						
						<div id="bar" class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
						</div>
			            <div class="credits-error"></div>
						<div class="tab-content">
							<div class="tab-pane" id="tab1">
								<div class="control-group">
									<div class="controls">
										<div class="row">
											<div class="form-group col-md-12">
												
												<br><label class="control-label" for="newsletter_templates"><?php echo lang('select_newsletter') ?><span class="text-danger">*</span></label>
											</div>
											<div class="form-group col-md-12">
												<select class="w-md form-control required" id="newsletter-template-list" name="newsletter_template_id" ui-jq="chosen">
													<?php foreach($newsletter_templates as $nt) { ?>
														<option value="<?php echo $nt['id']; ?>" data-channel-id="<?php echo $nt['channel_id']; ?>"><?php echo $nt['template_name']; ?></option>
													<?php } ?>													
												</select>
												<span class="text-danger" data-text="<?php echo lang('please_select'); ?>"></span>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab2">
								<div class="control-group">
									<div class="controls">
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab3">
								<div class="control-group">
	                                <div class="controls">
	                                    <div class="row">
	                                        <div class="col-md-6">
	                                           <div class="form-group"> 
	                                           
	                                                <label class="control-label" for="from_email"><?php echo ucfirst(lang('from')); ?><span class="text-danger"> *</span> :</label>
	                                                <input type="text" class="form-control required" name="from_email" value="">
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="row">
	                                        <div class="col-md-12">
	                                           <div class="form-group"> 
	                                           
	                                                <label class="control-label" for="subject"><?php echo lang('subject') ?><span class="text-danger"> *</span> :</label>
	                                                <input type="text" class="form-control required" name="subject" value="">
	                                            </div>
	                                        </div>
	                                    </div>

	                                </div>
                            	</div>
							</div>
							<div class="tab-pane" id="tab4">
								<div class="control-group">
	                                <div class="controls">
	                                    <div class="row">
	                                        <div class="col-md-12">
	                                           <div class="form-group"> 
	                                           
	                                                <label class="control-label" for="email_preview"><?php echo lang('email_preview') ?><span class="text-danger"> *</span> :</label>
	                                                <div class="email_preview_html">
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>

	                                </div>
                            	</div>
							</div>
							
								<div class="tab-pane" id="tab5">
									<a class="btn btn-success submit-send-newsletter-form" id="send-email-pdf"><i class="fa fa-file"></i>&nbsp;&nbsp;<?php echo lang('send_email') ?></a>
								</div>
								<ul class="pager wizard">
									<li class="previous first" style="display:none;"><a href="#"><?php echo lang('first') ?></a></li>
									<li class="previous"><a href="#"><?php echo lang('previous') ?></a></li>
									<li class="next last" style="display:none;"><a href="#"><?php echo lang('last') ?></a></li>
									<li class="next"><a href="#"><?php echo lang('next') ?></a></li>
								</ul>
							</div>
						</div>
					</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close_modal generate_proposal_close" data-dismiss="modal"><?php echo lang('close') ?></button>
			</div>
		</div>
	</div>
</div><!-- End of Send Newsletter Form wizard -->