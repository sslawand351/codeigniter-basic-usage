<div class="table-container">
	<div class="table-responsive">		
		<table class="tftable table table-striped m-b-none">
			<tr>
				<th><?php echo lang('name') ?></th>
				<th><?php echo lang('created_on') ?></th>
				<th colspan="2"><?php echo lang('action') ?></th>
			</tr>
			<?php if(!empty($groups['data']) && $groups['data']!='') { ?>
			<?php
				foreach($groups['data'] as $value)
				{
			?>
			<tr>
				<td><?php echo $value['name']; ?></td>
				<td><?php echo date('d-m-Y G:i:s', strtotime($value['added_on']) ); ?></td>
				<td>
					<?php if( $value['can_delete'] == 'yes' ) { ?>
					<a href="#editGroup" data-toggle="modal" class="btn btn-info btn-xs open-edit-group-popup" data-group-id="<?php echo $value['id']; ?>"><?php echo lang('edit') ?></a>
					<?php /*}*/ ?>
					
						<a href="#delete-group-popup" data-toggle="modal" data-group-id="<?php echo $value['id']; ?>" class="delete-group btn btn-xs btn-danger"><?php echo lang('delete') ?></a>
					<?php } ?>
					<a href="<?php echo site_url('group/users/'.$value['id']); ?>" data-toggle="modal" class="btn btn-info btn-xs" data-group-id="<?php echo $value['id']; ?>"><?php echo lang('users') ?></a>
					<?php /*if( check_permission('associate_oraganisation_to_user') ) {*/ ?>
						<a href="#add-users-popup" data-toggle="modal" data-group-id="<?php echo $value['id']; ?>" data-group-name="<?php echo $value['name']; ?>" class="add-users-to-group btn btn-xs btn-success"><?php echo lang('add_users') ?></a>
					<?php /*}*/ ?>
				</td>
			</tr>
			<?php } ?>
			<?php } else{ ?>
				<tr>
					<td colspan="6" style="text-align:center;"><?php echo lang('no_group_found') ?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	
	<footer class="panel-footer">
  		<div class="row">
			<div class="col-sm-12 text-right text-center-xs">
				<?php echo $links; ?>
			</div>
		</div>
	</footer>
</div>