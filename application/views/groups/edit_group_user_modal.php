<input type="hidden" class="user_previous_email" value="<?php echo $group_user_data['email'];?>" name="user_previous_email">
<div class="row">
	<!-- <div class="col-sm-6">
        <div class="form-group">
            <label><?php echo lang('name') ?> :</label>
            <input type="text" name="user_name" value="<?php echo $group_user_data['user_name']; ?>" class="group_user_name form-control">
            <span class="text-danger"></span>
        </div>
    </div> -->
    <div class="col-sm-6">
        <div class="form-group">
            <label><?php echo lang('email') ?><span class="text-danger"> *</span> :</label>
            <input type="text" name="email" value="<?php echo $group_user_data['email']; ?>" class="group_user_email form-control">
            <span class="text-danger"></span>
        </div>
    </div>
</div>
<div class="row" style="margin-top:20px;">
	<div class="col-sm-12">
		<div class="form-group">
	        <label><?php echo lang('groups') ?> :</label>
	    </div>
    </div>
	<?php $user_groups = explode(',',$group_user_data['user_groups']);
		if(!empty($groups)){ foreach($groups as $group){ if($group['all_contact'] != 'yes'){ ?>
		<div class="col-md-12 col-sm-12">
			<label class="i-checks" style="">
				 <input type="checkbox" class="user_groups " name="user_groups[]" value="<?php echo $group['id'];?>" <?php if(in_array($group['id'], $user_groups)){ echo "checked";}?>   ><i></i>&nbsp;&nbsp;&nbsp;
				 <?php echo $group['name'] ?>
			</label>
		</div>

	<?php }else { ?>
		<input type="hidden" name="user_groups[]" value="<?php echo $group['id'];?>" checked>
	<?php }  } }?>
</div>
<style>
.disabled_chk{
	cursor: none;
}
</style>