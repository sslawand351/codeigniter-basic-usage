<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('blogs') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					 <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="m-n font-thin h3 text-black">
						<p><?php echo ucfirst($article['title'])?></p>
					</h1>
				</div>
				<div class="table-responsive">	
					
					<div class="col-md-10  article-img">
						<?php if (file_exists(FCPATH . 'uploads/img/articles/'.$article['article_id'].'/'.$article['image_hash_name'])) { ?>
                        <img src="<?php echo site_url('uploads/img/articles/'.$article['article_id'].'/'.$article['image_hash_name']); ?>"> 
                        <?php } else { ?>
                        <img src="<?php echo site_url('resources/global/img/blog/blog_default.png'); ?>">
                        <?php } ?>
					</div>	
					<div class="col-md-10 desc">
						<?php echo $article['body']?>
					</div>
				</div>
				<div class="clear"></div>

			</div>
		</div>
	</div>

<script>
		$(document).on("click", ".delete_article", function(e){
        var article_id = $(this).attr('data-article-id');
        var link = site_url('index.php/article/delete/' + article_id);

        $('#delete_article_form').attr('action',link);
        $(".article_id").val(article_id);
        
    });

    $(".delete_article_btn").on("click",function(){
        var form = $("#delete_article_form");
        form.submit();
    });
</script>
<style>
	#article
	{
		height: 100px!important;
	}
	/*.desc
	{
		text-indent: 40px!important;

	}*/
	.clear
	{
		clear: both;
	}
	.article-img img
	{
		width: 100%;
	}
</style>