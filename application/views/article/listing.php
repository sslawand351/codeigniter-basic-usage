<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php echo Assets::js("admin/js/custom/blog_listing.js"); ?>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('blogs') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					 <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php if($this->permissions->is_permission_allowed('BLOG_ADD')): ?>
						<a class="btn btn-primary btn-xs pull-right" href="<?php echo site_url('article/add');?>"  ><?php echo lang('add_blog') ?></a>
					<?php endif; ?>
					
					<?php echo lang('blog_listing') ?>
				</div>
				<div class="blog-table table-responsive">	
					<table class="tftable table table-striped m-b-none">
					<tr>
						<th><?php echo lang('image')?></th>
						<th style="width:40%"><?php echo lang('description')?></th>
						<th><?php echo lang('added_on')?></th>
						<th><?php echo lang('action')?></th>
					</tr>
					 
					
					<?php 
	                    if(isset($articles['rc']) && !empty($articles['data'])){ 
	                   
	                        $article = $articles['data'];
	                        $count = 1;

					foreach ($articles['data'] as $value){ ?>
					
				        <tr class="row1" data-href='<?php echo site_url('article/article_by_url').'/'.$value['url']; ?>'>
				            <td class= " blog-img clickable-row">
				                <?php if (file_exists(FCPATH . 'uploads/img/articles/'.$value['a_id']."/".'thumb_small_'.$value['thumb_hash_name'])) { ?>
	                            <img src="<?php echo site_url('uploads/img/articles/'.$value['a_id']."/".'thumb_small_'.$value['thumb_hash_name']); ?>">    
	                            <?php } else { ?>
	                            <img width="200" src="<?php echo site_url('resources/global/img/blog/blog_default.png'); ?>">
	                            <?php } ?>
				            </td>
				            <td class="blog-content clickable-row">
				                    <h2><?php echo $value['title']?></h2>
				                    <div class="blog-para">
				                        <?php echo $value['short_description'];?>
				                    </div>
				            </td>
				            <td class= "added_on col-md-2 col-sm-6 col-xs-12">
				                <?php echo date('d-m-Y g:i:s',strtotime($value['added_on']));?>
				            </td>
				            <td class="blog-action col-md-2 col-sm-6 col-xs-12">
				            
							 <?php if($this->permissions->is_permission_allowed('BLOG_EDIT')): ?>
								<a class="btn btn-xs btn-info" href="<?php echo site_url('article/edit');?>/<?php echo $value['a_id'];?>"><?php echo lang('edit')?></a>
							<?php endif; ?>
							 <?php if($this->permissions->is_permission_allowed('BLOG_DELETE')): ?>
								<a class="btn btn-xs btn-danger delete_article" href="#delete_blog_popup" data-toggle="modal" data-article-id="<?php echo $value['a_id']; ?>"><?php echo lang('delete')?></a>
							<?php endif; ?>
				            </td>
				          </tr>
				    
			 	</div>   

			    <?php } $count++; } else{?>

					<div>
						<tr>
							<td><?php echo lang('no_blogs_added_yet') ?></td>
						</tr>
					</div>
				<?php }?>					
        				
        		 </table>	
			</div>
		</div>
	</div>
	
	<div id="delete_blog_popup" class="modal fade" data-backdrop="true" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 style="margin:0px;"><?php echo lang('delete_blog') ?></h3>
				</div>
				<form id="delete_article_form" action="" method="post">
				    <div class="modal-body">
			    		<input type="hidden" class="article_id" name="article_id" value="">
			    		<div class="row">
			    			<div class="col-md-12">
			    			<?php echo lang('are_you_sure_you_want_to_delete_this_user') ?> ?
			    			</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="delete_article_btn btn btn-default"><?php echo lang('ok') ?></button>
						<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
					</div>
		    	</form>
			</div>
		</div>
	</div>
</div>

<script>
		$(document).on("click", ".delete_article", function(e){
        var article_id = $(this).attr('data-article-id');
        var link = site_url('index.php/article/delete/' + article_id);

        $('#delete_article_form').attr('action',link);
        $(".article_id").val(article_id);
        
    });

    $(".delete_article_btn").on("click",function(){
        var form = $("#delete_article_form");
        form.submit();
    });
    


     
</script>
<style>
	#article
	{
		height: 100px!important;
	}
	/*#blog img
	{
		height: 100px!important;
	}*/
	.blog-action
	{
		top: 25px!important;
	}
	/*.blog-img img
	{
		width: 100%;
	}*/
	.blog-table
	{
		border-bottom: 1px solid #EAEAEA;
	}
	.added_on
	{
		top: 25px!important;
	}
	.tftable th
	{
		/*text-align: center;*/
		border-bottom: 1px solid #ddd;
	}
	.panel-heading
	{
		padding: 10px 15px!important;
	}
</style>