<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php echo Assets::js("admin/ckeditor/ckeditor.js"); ?>
<?php echo Assets::js("admin/js/plugins/bootstrap-filestyle/src/bootstrap-filestyle.js"); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/admin/ckeditor/config.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/admin/ckeditor/adapters/jquery.js"></script>

  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
	<!-- main header -->
	<div class="bg-light lter b-b wrapper-md">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<h1 class="m-n font-thin h3 text-black"><?php echo lang('edit_blog') ?></h1>
			</div>
		</div>
	</div>
	<!-- / main header -->
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
        <div class="row">
            <?php  if($this->session->flashdata('success')) { ?>
                  <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong></strong> <?php echo $this->session->flashdata('success'); ?>
                  </div>
            <?php } ?>
            <?php  if($this->session->flashdata('error')) { ?>            
                  <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $this->session->flashdata('error'); ?>
                  </div>
            <?php } ?>
        </div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo lang('edit_blog') ?>
			</div>
			<div class="panel-body">
				<form role="form" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                     <ul id="myTabs" class="nav nav-tabs" role="tablist">
                        <?php if(isset( $article ) && !empty($article)) { ?>
                        <?php foreach($article as $value) {?>
                        <li role="presentation" class="<?php if($value['language_name'] == 'english') { echo 'active';} ?> <?php if(form_error('data['.$value['language_id'].'][title]') || form_error('data['.$value['language_id'].'][url]') || form_error('data['.$value['language_id'].'][body]') ) { echo 'validation_error';}?>"><a href="#<?php echo $value['language_name']; ?>" id="<?php echo $value['language_name']; ?>-tab" role="tab" data-toggle="tab" aria-controls="<?php echo $value['language_name'];?>" aria-expanded="true"><?php echo ucfirst($value['language_name']); ?></a></li>
                            <?php 
                            }
                        ?>
                     </ul>
                     <div id="myTabContent" class="tab-content">

                    <?php foreach($article as $value) {?>
                      <div role="tabpanel" class="tab-pane fade in <?php if($value['language_name'] == 'english') { echo 'active';} ?> " id="<?php echo $value['language_name'];?>" aria-labelledBy="<?php echo $value['language_name'];?>-tab">
                    <div class="row">
                        <h4  class="language"><?php echo ucfirst($value['language_name']); ?></h4>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('title') ?><span class="text-danger"> *</span>:</label>
                                <input type="text" class="form-control" name="data[<?php echo $value['language_id']; ?>][title]" value="<?php echo ($this->input->post('data['.$value['language_id'].'][title]')) ?  $this->input->post('data['.$value['language_id'].'][title]') : $value['title'] ;?>"/>
                                <?php if(form_error('data['.$value["language_id"].'][title]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][title]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                        </div>
                    <div class="row">
                       <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('url') ?><span class="text-danger"> *</span> :</label>
                                <input type="text" class="form-control" name="data[<?php echo $value['language_id']; ?>][url]" value="<?php echo ($this->input->post('data['.$value['language_id'].'][url]')) ?  $this->input->post('data['.$value['language_id'].'][url]') : $value['url'] ;?>"/>
                                <?php if(form_error('data['.$value["language_id"].'][url]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][url]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('body') ?><span class="text-danger"> *</span> :</label>
                                <textarea id="editor<?php echo $value['language_id']; ?>" cols="50" rows="5" class ="form-control" name="data[<?php echo $value['language_id']; ?>][body]" > <?php echo ($this->input->post('data['.$value['language_id'].'][body]')) ? $this->input->post('data['.$value['language_id'].'][body]') : $value['body']; ?></textarea>
                                <script type="text/javascript">
                                  CKEDITOR.replace( 'editor<?php echo $value["language_id"]; ?>' );
                                  CKEDITOR.add            
                                </script>
                                <?php if(form_error('data['.$value['language_id'].'][body]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value['language_id'].'][body]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('page_title')?><span class="text-danger"> *</span> :</label>
                                <input type="text" class="form-control" name="data[<?php echo $value['language_id']; ?>][page_title]" value="<?php echo ($this->input->post('data['.$value['language_id'].'][page_title]')) ?  $this->input->post('data['.$value['language_id'].'][page_title]') : $value['page_title'] ;?>"/>
                                <?php if(form_error('data['.$value["language_id"].'][page_title]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][page_title]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('short_description') ?><span class="text-danger"> *</span> :</label>
                                <textarea class="form-control" name="data[<?php echo $value['language_id']; ?>][short_description]"><?php echo ($this->input->post('data['.$value['language_id'].'][short_description]')) ?  $this->input->post('data['.$value['language_id'].'][short_description]') : $value['short_description'] ;?></textarea>
                                <?php if(form_error('data['.$value["language_id"].'][short_description]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][short_description]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('meta_description') ?><span class="text-danger"> *</span>:</label>
                                <textarea class="form-control" name="data[<?php echo $value['language_id']; ?>][meta_description]"><?php echo ($this->input->post('data['.$value['language_id'].'][meta_description]')) ?  $this->input->post('data['.$value['language_id'].'][meta_description]') : $value['meta_description'] ;?></textarea>
                                <?php if(form_error('data['.$value["language_id"].'][meta_description]')) { ?>
                                <div class="text-danger"><?php echo form_error('data['.$value["language_id"].'][meta_description]');?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                    <?php }?>

                    <?php 
                        $dir = FCPATH . "uploads/img/articles/" . $value['blog_id'] . "/";

                        if( $value['image_hash_name'] != "" && file_exists( $dir . 'small_' . $value['image_hash_name'] ) )
                        {
                    ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><?php echo lang('image') ?> : </label>
                                    <img src="<?php echo base_url('uploads/img/articles/' . $value['blog_id'] . '/' . 'small_' . $value['image_hash_name'] ); ?>">
                                    <a href="#delete_photo_popup" data-toggle="modal" data-blog-id="<?php echo $value['blog_id']; ?>" title="Delete image" class="delete_photo btn m-b-xs btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                    <?php 
                        }
                        else
                        {
                    ?>
                        <!-- Add Image -->
                         <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><?php echo lang('add_images') ?> : </label>
                                    <input type="file" name="image" class="filestyle">
                                    <?php if(form_error('image')) { ?>
                                    <div class="text-danger"><?php echo form_error('image');?></div>
                                    <?php } ?>
                                </div>
                            </div>
                    <?php
                        }
                    ?>
                    <?php 
                        $dir = FCPATH . "uploads/img/articles/" . $value['blog_id'] . "/";

                        if( $value['thumb_hash_name'] != "" && file_exists( $dir . 'thumb_small_' . $value['thumb_hash_name'] ) )
                        {
                    ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><?php echo lang('thumbnail') ?> : </label>
                                    <img src="<?php echo site_url('uploads/img/articles/' . $value['blog_id'] . '/' . 'thumb_small_' . $value['thumb_hash_name'] ); ?>">
                                    <a href="#delete_thumb_popup" data-toggle="modal" data-blog-id="<?php echo $value['blog_id']; ?>" title="Delete image" class="delete_thumb btn m-b-xs btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php 
                        }
                        else
                        {
                    ?>
                        <!-- Add Image -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><?php echo lang('add_thumbnail') ?> : </label>
                                    <input type="file" name="thumb" class="filestyle">
                                    <?php if(form_error('thumb')) { ?>
                                    <div class="text-danger"><?php echo form_error('thumb');?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div><!-- End of Add Image -->
                    <?php
                        }
                    ?>
                    <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('update') ?></button>
                    <?php
                    }
                    else
                    {?>
                        <div class="row">
                           <div class="alert alert-danger">
                                <strong><?php echo lang('no_data_found') ?>!</strong>
                            </div>
                        </div>  
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>

<!-- delete_photo_popup Modal -->
<div id="delete_photo_popup" class="modal fade" data-backdrop="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 style="margin:0px;"><?php echo lang('delete_image') ?></h3>
            </div>
            <form id="delete_photo_form" action="" method="post">
                <div class="modal-body">
                    <input type="hidden" class="blog_id" name="blog_id" value="">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo lang('are_you_sure_you_want_to_delete_this_image') ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="delete_photo_btn btn btn-default"><?php echo lang('ok') ?></button>
                    <button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('cancel') ?></button>
                </div>
            </form>
			</div>
		</div>
	</div>
</div>

<div id="delete_thumb_popup" class="modal fade" data-backdrop="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 style="margin:0px;"><?php echo lang('delete_thumb') ?></h3>
            </div>
            <form id="delete_thumb_form" action="" method="post">
                <div class="modal-body">
                    <input type="hidden" class="blog_id" name="blog_id" value="">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo lang('are_you_sure_you_want_to_delete_this_thumbnail') ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="delete_thumb_btn btn btn-default"><?php echo lang('ok') ?></button>
                    <button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('cancel') ?></button>
                </div>
            </form>
        </div>
    </div>
</div><!-- End of delete_thumb_popup Modal -->

<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
.chosen-container{
	width:100%!important;
}
.text-danger
{
	display: inline-block;
}
</style>
<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
 }
 .language{
    margin-left:16px!important;
}
</style>
<script type="text/javascript">
$(document).ready(function(){

    $(document).on("click", ".delete_photo", function(e){
        var blog_id = $(this).attr('data-blog-id');
        var href= site_url('article/delete_image/' + blog_id);
        $('#delete_photo_form').attr('action',href);
        $(".blog_id").val(blog_id);
    });

    $(".delete_photo_btn").on("click",function(){
        var form = $("#delete_photo_form");
        form.submit();
    });
});
</script>

<script type="text/javascript">
$(document).ready(function(){

    $(document).on("click", ".delete_thumb", function(e){
        var blog_id = $(this).attr('data-blog-id');
        var href= site_url('article/delete_thumbnail/' + blog_id);
        $('#delete_thumb_form').attr('action',href);
        $(".blog_id").val(blog_id);
    });

    $(".delete_thumb_btn").on("click",function(){
        var form = $("#delete_thumb_form");
        form.submit();
    });
});
</script>