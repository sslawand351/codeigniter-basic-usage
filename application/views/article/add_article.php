<?php echo Assets::js("admin/ckeditor/ckeditor.js"); ?>
<?php echo Assets::js("admin/js/plugins/bootstrap-filestyle/src/bootstrap-filestyle.js"); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/admin/ckeditor/config.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/admin/ckeditor/adapters/jquery.js"></script>

 <div id="content" class="app-content" role="main">
    <div class="app-content-body">
        <!-- main header -->
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black"><?php echo lang('blogs') ?></h1>
                </div>
            </div>
        </div>
        <!-- / main header -->
        <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
            <div class="row">
                <?php  if(!empty($validation_error)) { ?>
                      <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $validation_error; ?>
                      </div>
                <?php } ?>
                <?php  if($this->session->flashdata('success')) { ?>
                      <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong></strong> <?php echo $this->session->flashdata('success'); ?>
                      </div>
                <?php } ?>
                <?php  if($this->session->flashdata('error')) { ?>            
                      <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('error'); ?>
                      </div>
                <?php } ?>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo lang('add_blog') ?>
                </div>
                <div class="panel-body">
                    <form role="form" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                         <ul id="myTabs" class="nav nav-tabs" role="tablist">
                            <?php if(isset($languages['data'])){ ?>
                            <?php 
                            foreach($languages['data'] as $language) {
                                ?>
                            <li role="presentation" class="<?php if($language['language'] == 'english') { echo 'active';} ?> <?php if(form_error('data['.$language['id'].'][name]') || form_error('data['.$language['id'].'][description]') ) { echo 'validation_error';}?> "><a href="#<?php echo $language['language']; ?>" id="<?php echo $language['language']; ?>-tab" role="tab" data-toggle="tab" aria-controls="<?php echo $language['language'];?>" aria-expanded="true"><?php echo ucfirst($language['language']); ?></a></li>
                                <?php 
                                } }
                            ?>
                         </ul>
                        <div id="myTabContent" class="tab-content">
                            

                            <?php foreach ($languages['data'] as  $language) { 
                            ?>
                            
                             <div role="tabpanel" class="tab-pane fade in <?php if($language['language'] == 'english') { echo 'active';} ?>" id="<?php echo $language['language'];?>" aria-labelledBy="<?php echo $language['language'];?>-tab">
                                <div class="row">
                                    <h4  class="language"><?php echo ucfirst($language['language']); ?></h4>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo lang('title') ?><span class="text-danger"> *</span> :</label>
                                                <input type="text" class="form-control" name="data[<?php echo $language['id']; ?>][title]" value="<?php echo ($this->input->post('data['.$language['id'].'][title]')) ?  $this->input->post('data['.$language['id'].'][title]') : "" ;?>"/>
                                                <?php if(form_error('data['.$language["id"].'][title]')) { ?>
                                                <div class="text-danger"><?php echo form_error('data['.$language["id"].'][title]');?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo lang('url')?><span class="text-danger"> *</span> :</label>
                                                <input type="text" class="form-control" name="data[<?php echo $language['id']; ?>][url]" value="<?php echo ($this->input->post('data['.$language['id'].'][url]')) ?  $this->input->post('data['.$language['id'].'][url]') : "" ;?>"/>
                                                <?php if(form_error('data['.$language["id"].'][url]')) { ?>
                                                <div class="text-danger"><?php echo form_error('data['.$language["id"].'][url]');?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo lang('body') ?><span class="text-danger"> *</span> :</label>
                                            <textarea id="editor<?php echo $language['id']; ?>" cols="50" rows="5" class ="form-control" name="data[<?php echo $language['id']; ?>][body]" > <?php echo ($this->input->post('data['.$language['id'].'][body]')) ? $this->input->post('data['.$language['id'].'][body]') : ""; ?></textarea>
                                            <script type="text/javascript">
                                              CKEDITOR.replace( 'editor<?php echo $language["id"]; ?>' );
                                              CKEDITOR.add            
                                           </script>
                                            <?php if(form_error('data['.$language['id'].'][body]')) { ?>
                                            <div class="text-danger"><?php echo form_error('data['.$language['id'].'][body]');?></div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                   <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo ucfirst(lang('page_title'))?> <span class="text-danger"> *</span> :</label>
                                            <input type="text" class="form-control" name="data[<?php echo $language['id']; ?>][page_title]" value="<?php echo ($this->input->post('data['.$language['id'].'][page_title]')) ?  $this->input->post('data['.$language['id'].'][page_title]') : "" ;?>"/>
                                            <?php if(form_error('data['.$language["id"].'][page_title]')) { ?>
                                            <div class="text-danger"><?php echo form_error('data['.$language["id"].'][page_title]');?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo ucfirst(lang('short_description'))?> <span class="text-danger"> *</span> :</label>
                                             <textarea name="data[<?php echo $language['id']; ?>][short_description]" data-required="true" class="form-control parsley-validated"><?php echo ($this->input->post('data['.$language['id'].'][short_description]')) ?  $this->input->post('data['.$language['id'].'][short_description]') : "" ;?></textarea>
                                            <?php if(form_error('data['.$language["id"].'][short_description]')) { ?>
                                            <div class="text-danger"><?php echo form_error('data['.$language["id"].'][short_description]');?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo ucfirst(lang('meta_description'))?> <span class="text-danger"> *</span> :</label>
                                             <textarea name="data[<?php echo $language['id']; ?>][meta_description]" data-required="true" class="form-control parsley-validated"><?php echo ($this->input->post('data['.$language['id'].'][meta_description]')) ?  $this->input->post('data['.$language['id'].'][meta_description]') : "" ;?></textarea>
                                            <?php if(form_error('data['.$language["id"].'][meta_description]')) { ?>
                                            <div class="text-danger"><?php echo form_error('data['.$language["id"].'][meta_description]');?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php }?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><?php echo lang('add_images') ?> : </label>
                                        <input type="file" name="image" class="filestyle">
                                        <?php if(form_error('image')) { ?>
                                        <div class="text-danger"><?php echo form_error('image');?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><?php echo lang('add_thumbnail') ?> : </label>
                                        <input type="file" name="thumb" class="filestyle">
                                        <?php if(form_error('thumb')) { ?>
                                        <div class="text-danger"><?php echo form_error('thumb');?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('save') ?></button>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
.chosen-container{
	width:100%!important;
}
.language{
    margin-left:16px!important;
}
.text-danger
{
	display: inline-block;
}
</style>
