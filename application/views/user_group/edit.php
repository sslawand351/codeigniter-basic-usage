<!-- content -->
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
	<!-- main header -->
	<div class="bg-light lter b-b wrapper-md">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<h1 class="m-n font-thin h3 text-black"><?php echo lang('update_role') ?></h1>
			</div>
		</div>
	</div>

	<!-- / main header -->
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">

		<?php if($this->session->flashdata('success')) { ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button> 
			<?php echo $this->session->flashdata('success'); ?>
		</div>
		<?php } ?>
		<?php if($this->session->flashdata('error')) { ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button> 
			<?php echo $this->session->flashdata('error'); ?>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo lang('role_add') ?>
			</div>
			<div class="panel-body">
				<form role="form" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">

	          		<div class="row">
		          		<div class="col-sm-6">
				            <div class="form-group">
								<label for="field-1" class="control-label"><span class="text-danger asterisk">* </span>Name:</label>
								<input class="form-control"  type="text" name="role_name" value="<?php echo $usergroup_name ? $usergroup_name : $this->input->post('role_name'); ?>" />
								<?php if(form_error('role_name')) { ?>
									<span class="text-danger"><?php echo form_error('role_name'); ?></span>
								<?php } ?>
							</div>
				        </div>
				    </div>
				    <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('save') ?></button>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>



<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
.chosen-container{
	width:100%!important;
}

</style>
