

<!-- content -->
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
	<!-- main header -->
	<div class="bg-light lter b-b wrapper-md">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<h1 class="m-n font-thin h3 text-black"><?php echo lang('permissions') ?></h1>
			</div>
		</div>
	</div>
	<!-- / main header -->
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl" id="wrapper-md">

	<?php if($this->session->flashdata('success')) { ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button> 
		<?php echo $this->session->flashdata('success'); ?>
	</div>
	<?php } ?>
	<?php if($this->session->flashdata('error')) { ?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">×</button> 
		<?php echo $this->session->flashdata('error'); ?>
	</div>
	<?php } ?>

		<div class="error_msg_remove_location" style="display:none;">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo lang('error_msg_remove_location');?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo "Modify Permissions";?>
			</div>
			<div class="panel-body">
				<form method="POST" action="<?php echo site_url('usergroup/edit_permissions/'.$usergroup_id); ?>">

					<div class="col-md-6">
						<div class="form-group row">
						<input type="hidden" name="test" value="1">
							<?php 
							$previous_group_id = 0;
							foreach($permission_matrix as $index => $p):

								if($p['permission_group_id'] != $previous_group_id)
								{
									echo '<div class="permission-group">'.$p['group_name'].'</div>';
								}
							?>
							<div class="permission-row">
								<div class="permission-name col-md-10"><?php echo $p['permission_name']; ?></div>
								<div class="permission-control col-md-2"><input 
									type="checkbox" 
									name="selected_permissions[]" 
									<?php echo ($p['conflict_group_id'] > 0) ? 'data-conflict-group-id="'.$p['conflict_group_id'].'"' : ''; ?>											value="<?php echo $p['id']; ?>" 
									<?php echo ($p['is_set'] == 1) ? ('checked="checked"') : ''; ?>
								/>
								</div>
								<div class="clear"></div>
							</div>

							<?php
							$previous_group_id = $p['permission_group_id'];

							endforeach; ?>
						</div>
						  <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('save') ?></button>
					</div>

				  
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
.chosen-container{
	width:100%!important;
}
.permission-group
{
	font-weight: bold;
	color:#3a3f51;
}
.permission-group {
	background-color:#edf1f2;
	margin:20px 0 0px 0;
	color:#a6a8b1;
	font-weight:bold;
	padding:5px;
	font-size:14px;
}
.permission-row {
	 padding:15px;
	 font-size:14px;
}
.permission-row .permission-name {
	float:left;	
}
</style>
