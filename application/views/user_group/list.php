<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('roles') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					 <strong></strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php if($this->permissions->is_permission_allowed('GROUP_ADD')): ?>
					<div class="btn-group pull-right">
						<a href="<?php echo site_url('usergroup/add') ?>" class="btn btn-xs btn-success">Add New User role</a>
					</div>
					<?php endif; ?>
					<?php echo lang('role_listing') ?>
				</div>
				
				<div class="table-responsive">		
					<table class="tftable table table-striped m-b-none">
						<tr>
							<th class="text-center">Usergroup id</th>
							<th class="text-center">Group Name</th>
							<th class="text-center"># of users</th>
							<th class="text-center"># of permissions</th>
							<th class="text-center">Added On</th>
							<th class="text-center">Action</th>
						</tr>
						</tr>
						<?php if(count($usergroups) > 0):?>
						<?php
							foreach($usergroups as $u): 
						?>
						<tr>
							<td class="text-center">
								<?php 
								// if($u['id'] == $current_user_usergroup_id) 
									echo $u['id'];
								?>
							</td>
							<td class="text-center"><?php echo $u['role_name']; ?></td>
							<td class="text-center"><?php echo $u['user_count']; ?></td>
							<td class="text-center"><?php echo $u['permission_count']; ?></td>
							<td class="text-center"><?php echo $u['added_on']; ?></td>
							<td class="text-center">
								<?php if($this->permissions->is_permission_allowed('GROUPS_SET_PERMISSIONS')){ 

									if($u['id']!= MASTER_ADMIN && ($this->user['usergroup_id']== SUPER_ADMIN || $this->user['usergroup_id']== MASTER_ADMIN)){ ?>
								<a href="<?php echo site_url('usergroup/edit_permissions/'.$u['id'])?>" class="btn btn-info btn-xs icon-left">Modify Permissions</a>
								<?php }}else{ echo "-";} ?>

								<?php if($this->permissions->is_permission_allowed('GROUPS_DELETE')){ ?>
									<a href="<?php echo site_url('usergroup/remove/'.$u['id'])?>" class="btn btn-danger btn-xs icon-left">Delete user role</a>
								<?php } ?>

								<?php if($this->permissions->is_permission_allowed('GROUPS_EDIT')){ ?>
									<a href="<?php echo site_url('usergroup/edit/'.$u['id'])?>" class="btn btn-primary btn-xs icon-left">Update user role</a>
								<?php } ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php else:?>
							<tr>
								<td colspan="5"><?php echo lang('no_users_found') ?></td>
							</tr>
						<?php endif;?>
					</table>
				</div>
			</div>
		</div>
	</div>
