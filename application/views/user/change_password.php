<!-- content -->
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
	<!-- main header -->
	<div class="bg-light lter b-b wrapper-md">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<h1 class="m-n font-thin h3 text-black"><?php echo lang('settings') ?></h1>
			</div>
		</div>
	</div>
	<!-- / main header -->
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo lang('change_password') ?>
			</div>
			<div class="panel-body">
				<form role="form" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">

	          		<div class="row">
		          		<div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('enter_existing_password') ?><span class="text-danger"> *</span> :</label>
					            <input type="password" name="existing_password" value="<?php echo $this->input->post('existing_password'); ?>" class="form-control">
								<?php if(form_error('existing_password')) { ?>
									<div class="text-danger"><?php echo form_error('existing_password'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				    </div>
				    <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('enter_new_password') ?><span class="text-danger"> *</span> :</label>
					            <input type="password" name="new_password" value="<?php echo $this->input->post('new_password'); ?>" class="form-control">
								<?php if(form_error('new_password')) { ?>
									<div class="text-danger"><?php echo form_error('new_password'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				    </div>

				    <div class="row">
		          		<div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('confirm_new_password') ?><span class="text-danger"> *</span> :</label>
					            <input type="password" name="confirm_password" value="<?php echo $this->input->post('confirm_password'); ?>" class="form-control">
								<?php if(form_error('confirm_password')) { ?>
									<div class="text-danger"><?php echo form_error('confirm_password'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				    </div>

				    <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('save') ?></button>

				</form>
			</div>
		</div>
	</div>
</div>



<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
</style>
