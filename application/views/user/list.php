<?php echo Assets::js("admin/js/custom/user_listing.js"); ?>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('users') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('success_msg')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('success_msg'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error_msg')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					 <?php echo $this->session->flashdata('error_msg'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php if($this->permissions->is_permission_allowed('USER_ADD') && $type!='frontend_user'): ?>
						<a class="btn btn-primary btn-xs pull-right" href="<?php echo site_url('user/add');?>"  ><?php echo lang('add_user') ?></a>
					<?php endif; ?>
					
					<?php echo lang('user_listing') ?>
				</div>
				
				<div class="table-responsive">		
					<table class="tftable table table-striped m-b-none">
						<tr>
							<th><?php echo lang('name') ?></th>
							<th><?php echo lang('email') ?></th>
							<th><?php echo lang('assigned_role')?></th>
							<th><?php echo lang('action') ?></th>
						</tr>
						<?php if(count($users) > 0):?>
						<?php
							foreach($users as $u): 
						?>
						<tr>
							<td><?php echo $u['first_name']." ".$u['last_name']; ?></td>
							<td><?php echo $u['email']; ?></td>
							<td><?php echo $u['user_type']?></td>
							<?php if($this->permissions->is_permission_allowed('USER_EDIT') || $this->permissions->is_permission_allowed('DELETE_USER')){ ?>
							<td>
								<?php if($this->permissions->is_permission_allowed('USER_EDIT')){
									if($u['id']!= MASTER_ADMIN && ($this->user['usergroup_id']== SUPER_ADMIN)){
								?>
									<a class="btn btn-info btn-xs" href="<?php echo site_url('user/edit/'.$u['id']);?>"><?php echo lang('edit') ?></a>
								<?php } else if( $this->user['usergroup_id'] == MASTER_ADMIN){?>
										<a class="btn btn-info btn-xs" href="<?php echo site_url('user/edit/'.$u['id']);?>"><?php echo lang('edit') ?></a>
									<?php }} ?>

								<?php if($this->permissions->is_permission_allowed('DELETE_USER')){
									if($u['id']!= MASTER_ADMIN && ($this->user['usergroup_id']== SUPER_ADMIN)){
								?>
									<a href="#delete_user_popup" data-toggle="modal" data-user-id="<?php echo $u['id']; ?>" class="delete_user btn btn-xs btn-danger"><?php echo lang('delete') ?></a>
								<?php } else if( $this->user['usergroup_id'] == MASTER_ADMIN && $u['id']!= MASTER_ADMIN ){?>
										<a href="#delete_user_popup" data-toggle="modal" data-user-id="<?php echo $u['id']; ?>" class="delete_user btn btn-xs btn-danger"><?php echo lang('delete') ?></a>
									<?php }} ?>
								
								
							</td>
							<?php }else{ echo "<td>-</td>";}?>
						</tr>
						<?php endforeach; ?>
						<?php else:?>
							<tr>
								<td colspan="5"><?php echo lang('no_users_found') ?></td>
							</tr>
						<?php endif;?>
					</table>
				</div>
				
				<footer class="panel-footer">
		      		<div class="row">
						<div class="col-sm-12 text-right text-center-xs">
							<?php echo $links; ?>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
	
	<div id="delete_user_popup" class="modal fade" data-backdrop="true" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 style="margin:0px;"><?php echo lang('delete_user') ?></h3>
				</div>
				<form id="delete_user_form" action="" method="post">
				    <div class="modal-body">
			    		<input type="hidden" class="user_id" name="user_id" value="">
			    		<div class="row">
			    			<div class="col-md-12">
			    			<?php echo lang('are_you_sure_you_want_to_delete_this_user') ?> ?
			    			</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="delete_user_btn btn btn-default"><?php echo lang('ok') ?></button>
						<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
					</div>
		    	</form>
			</div>
		</div>
	</div>
</div>
<script>
	$('.delete_user').on('click',function(){

		var user_id = $(this).attr('data-user-id');
		action = site_url('user/remove/' + user_id);
		$('#delete_user_form').attr('action',action);
	})
</script>