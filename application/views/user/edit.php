<?php echo Assets::js("admin/js/custom/user_edit.js"); ?>

<!-- content -->
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
	<!-- main header -->
	<div class="bg-light lter b-b wrapper-md">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<h1 class="m-n font-thin h3 text-black"><?php echo lang('users') ?></h1>
			</div>
		</div>
	</div>
	<!-- / main header -->
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl" id="wrapper-md">
		<div class="">
			<?php  if($this->session->flashdata('success_msg')) { ?>
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong></strong> <?php echo $this->session->flashdata('success_msg'); ?>
			</div>
			<?php } ?>
			<?php  if($this->session->flashdata('error_msg')) { ?>            
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <?php echo $this->session->flashdata('error_msg'); ?>
			</div>
			<?php } ?>
		</div>
		<div class="error_msg_remove_location" style="display:none;">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo lang('error_msg_remove_location');?>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo lang('user_edit') ?>
			</div>
			<div class="panel-body">
				<form role="form" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
				<input type="hidden" class="user_id" value="<?php echo $users['id']?>">
	          		<div class="row">
		          		<div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('first_name') ?><span class="text-danger"> *</span> :</label>
					            <input type="text" name="first_name" value="<?php echo ($this->input->post('first_name') ? $this->input->post('first_name') : $users['first_name']); ?>" class="form-control">
								<?php if(form_error('first_name')) { ?>
									<div class="text-danger"><?php echo form_error('first_name'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('last_name') ?><span class="text-danger"> *</span> :</label>
					            <input type="text" name="last_name" value="<?php echo ($this->input->post('last_name') ? $this->input->post('last_name') : $users['last_name']); ?>" class="form-control">
								<?php if(form_error('last_name')) { ?>
									<div class="text-danger"><?php echo form_error('last_name'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				    </div>

				    <div class="row">
		          		<div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('password') ?> :</label>
					            <input type="password" name="password" value="<?php echo $this->input->post('password'); ?>" class="form-control">
								<?php if(form_error('password')) { ?>
									<div class="text-danger"><?php echo form_error('password'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('confirm_password') ?> :</label>
					            <input type="password" name="passconf" value="<?php echo $this->input->post('passconf'); ?>" class="form-control">
								<?php if(form_error('passconf')) { ?>
									<div class="text-danger"><?php echo form_error('passconf'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				    </div>

				    <div class="row">
		          		<div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('email') ?><span class="text-danger"> *</span> :</label>
					            <input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $users['email']); ?>" class="form-control">
								<?php if(form_error('email')) { ?>
									<div class="text-danger"><?php echo form_error('email'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
					            <label><?php echo lang('language') ?><span class="text-danger"> *</span> :</label>
					            <?php $selected = $this->input->post('language_id') ? $this->input->post('language_id') : strtolower($users['language_id']); ?>
					            <select name="language_id" class="required form_control" ui-jq="chosen">
	                                <option value="4" <?php if($selected == "4"){echo "selected";}?> ><?php echo lang('dutch') ?></option>
	                                <option value="1" <?php if($selected == "1"){echo "selected";}?> ><?php echo lang('english') ?></option>
	                                <option value="2" <?php if($selected == "2"){echo "selected";}?> ><?php echo lang('french') ?></option>
	                            </select>
								<?php if(form_error('language')) { ?>
									<div class="text-danger"><?php echo form_error('language'); ?></div>
								<?php } ?>
				            </div>
				        </div>
				        
				    </div>
				    <?php if($users['type'] != 'frontend_user' && $users['id']!= MASTER_ADMIN){ ?>
					    <div class="row">
			          		<div class="col-sm-6">
					            <div class="form-group">
						            <label><?php echo ucfirst(lang('user_roles')) ?><span class="text-danger"> *</span> :</label>
						            <select class="form-control edit_usergroup" name="usergroup_id">
										<option value="">Select Roles</option>
										<?php foreach ($usergroup as $u)
										{?>
											<option value="<?php echo $u['id']?>" <?php echo isset($users['usergroup_id']) && $users['usergroup_id'] == $u['id'] ? "Selected='selected'" : ""?> ><?php echo $u['role_name']; ?></option>
										<?php
										}
										?>
									</select>
					            </div>
					        </div>
					    </div>
				    <?php } ?>
				    <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('save') ?></button>

				</form>
			</div>
		</div>
	</div>
</div>

<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
.chosen-container{
	width:100%!important;
}
</style>
