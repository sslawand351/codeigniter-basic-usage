		
  <!-- /content -->
  
  <!-- footer -->
  <footer id="footer" class="app-footer" role="footer">
    <div class="wrapper b-t bg-light">
      <span class="pull-right">2.0.3 <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
      &copy; 2016 Copyright.
    </div>
  </footer>
  <!-- / footer -->
  </div>
	<!-- template JS files -->
        <?php echo Assets::js("admin/js/plugins/bootstrap/dist/js/bootstrap.js"); ?>
        <?php echo Assets::js("admin/js/ui-load.js"); ?>
        <?php echo Assets::js("admin/js/ui-jp.config.js"); ?>
        <?php echo Assets::js("admin/js/ui-jp.js"); ?>
        <?php echo Assets::js("admin/js/ui-nav.js"); ?>
        <?php echo Assets::js("admin/js/ui-toggle.js"); ?>
        <?php echo Assets::js("admin/js/ui-client.js"); ?>
        <?php echo Assets::js("admin/js/plugins/chosen/chosen.jquery.min.js"); ?>    
        <?php echo Assets::js("admin/js/plugins/html5sortable/jquery.sortable.js"); ?>     
        <?php if(isset($search) && $search == TRUE){echo Assets::js("admin/js/bootstrap.min.js"); }?>   
           
    </body>
</html>