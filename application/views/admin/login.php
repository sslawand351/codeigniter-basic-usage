<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title><?php echo WEBSITE_NAME; ?> | <?php echo lang('admin_log_in') ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
            
        <?php echo Assets::css("admin/css/animate.css"); ?>
        <?php echo Assets::css("admin/css/font-awesome.min.css"); ?>
        <?php echo Assets::css("admin/css/simple-line-icons.css"); ?>
        <?php echo Assets::css("admin/css/font.css"); ?>
        <?php echo Assets::css("admin/js/plugins/bootstrap/dist/css/bootstrap.css"); ?>
        <?php echo Assets::css("admin/js/plugins/datepicker/css/bootstrap-datepicker.css"); ?>
        <?php echo Assets::css("admin/css/app.css"); ?>

        <?php echo Assets::js("admin/js/jquery-1.11.3.min.js"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    
    </head>
    <body>
<div class="app app-header-fixed ">
  

<div class="container w-xxl w-auto-xs" ng-controller="SigninFormController" ng-init="app.settings.container = false;">
  <a href class="navbar-brand block m-t"><?php echo WEBSITE_NAME; ?></a>
  <div class="m-b-lg">
    <div class="wrapper text-center">
      <strong><?php echo lang('sign_in') ?></strong>
    </div>
    <?php  if($this->session->flashdata('error_msg')) { ?>            
                        <div class="alert alert-danger">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo $this->session->flashdata('error_msg'); ?>
                        </div>
                  <?php } ?>
    <form name="form" method="post" action="" class="form-validation">
      <div class="text-danger wrapper text-center" ng-show="authError">
          
      </div>
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          <input type="text" name="email" class="form-control no-border" placeholder="Email" value="<?php echo $this->input->post('email'); ?>" />
                        <?php echo form_error('email'); ?>
        </div>
        <div class="list-group-item">
           <input type="password" name="password" class="form-control no-border" placeholder="Password"/>
                        <?php echo form_error('password'); ?>
        </div>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'><?php echo lang('log_in') ?></button>
     
    </form>
  </div>
  <div class="text-center" ng-include="'tpl/blocks/page_footer.html'">
    <p>
  <small class="text-muted"><?php echo WEBSITE_NAME; ?><br>&copy; 2014</small>
</p>
  </div>
</div>


</div>
 
       <?php echo Assets::js("admin/js/plugins/bootstrap/dist/js/bootstrap.js"); ?>
        <?php echo Assets::js("admin/js/ui-load.js"); ?>
        <?php echo Assets::js("admin/js/ui-jp.config.js"); ?>
        <?php echo Assets::js("admin/js/ui-jp.js"); ?>
        <?php echo Assets::js("admin/js/ui-nav.js"); ?>
        <?php echo Assets::js("admin/js/ui-toggle.js"); ?>
        <?php echo Assets::js("admin/js/ui-client.js"); ?>
    </body>
</html>