<?php
if(isset($header) && $header)
    $this->load->view('admin/header');

if(isset($sidebar) && $sidebar)
    $this->load->view('admin/sidebar');

if(isset($_view) && $_view)
    $this->load->view($_view);

if(isset($footer) && $footer)
    $this->load->view('admin/footer');

?>