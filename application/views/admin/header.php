<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
    <link rel="shortcut icon"  href="<?php echo base_url('resources/admin/img/favicon.ico');?>" type="image/x-icon">
    <meta charset="UTF-8">
    <title><?php echo isset($title) && $title != '' ? $title : WEBSITE_NAME; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <?php echo Assets::css("admin/css/jquery.jgrowl.css"); ?>

    <!-- template css files -->
    <?php echo Assets::css("admin/css/animate.css"); ?>
    <?php echo Assets::css("admin/css/font-awesome.min.css"); ?>
    <?php echo Assets::css("admin/css/simple-line-icons.css"); ?>
    <?php echo Assets::css("admin/css/font.css"); ?>
    <?php echo Assets::css("admin/js/plugins/bootstrap/dist/css/bootstrap.css"); ?>
    <?php echo Assets::css("admin/js/plugins/datepicker/css/bootstrap-datepicker.css"); ?>
    <?php echo Assets::css("admin/css/app.css"); ?>

    <?php echo Assets::js("admin/js/jquery-1.11.3.min.js"); ?>
    <?php echo Assets::js("admin/js/jquery.jgrowl.js"); ?>
    <?php echo Assets::js("admin/js/plugins/jquery-ui.min.js"); ?>
    <?php echo Assets::js("admin/js/plugins/jquery.colorbox-min.js"); ?>
    <?php echo Assets::js("admin/js/plugins/slimscroll/jquery.slimscroll.min.js"); ?>

    <?php echo Assets::js("admin/js/plugins/jquery-timepicker/jquery.timepicker.js"); ?>
    <?php echo Assets::css("admin/js/plugins/jquery-timepicker/jquery.timepicker.css"); ?>

    <?php echo Assets::js("admin/js/plugins/jquery-timepicker/lib/bootstrap-datepicker.js"); ?>
    <?php echo Assets::css("admin/js/plugins/jquery-timepicker/lib/bootstrap-datepicker.css"); ?>

    <?php echo Assets::js("admin/js/plugins/jquery-timepicker/datepair.min.js"); ?>
    <?php echo Assets::js("admin/js/plugins/datepicker/js/bootstrap-datepicker.js"); ?>
    <?php echo Assets::js("admin/js/plugins/jquery.blockUI.js"); ?>

    <?php echo Assets::js("admin/js/global.js"); ?>
    <?php echo Assets::js("admin/js/plugins/jquery_tab/jquery.tabbedcontent.min.js"); ?>
    <?php echo Assets::js("admin/js/jquery.validate.min.js"); ?>
    <?php echo Assets::js("admin/js/jquery.cookie.js"); ?>
    <?php echo Assets::js("admin/js/simpleform.min.js"); ?>
    <?php echo Assets::js("admin/js/bootbox.min.js"); ?>
    <?php echo Assets::js("admin/js/custom/activity_notification.js"); ?>
    <?php echo Assets::css("admin/css/bootstrap-chosen.css"); ?>
    <script type="text/javascript">
        var GLOBALS = {
            'site_url' : '<?php echo site_url(); ?>',
            'ci_index_page' : '<?php echo index_page(); ?>',
            'FCPATH' : '<?php echo addslashes(FCPATH); ?>',
            'lang' : '<?php echo $this->session->userdata('lang'); ?>'
        }
    </script>
    <script>
        var FCPATH = "<?php echo addslashes(FCPATH); ?>";
        var lang = "<?php echo $this->session->userdata('lang');?>";

    var tabs;
    jQuery(function($) {
        tabs = $('.tabscontent').tabbedContent({loop: true}).data('api');

        // switch to tab...
        $('a[href=#click-to-switch]').on('click', function(e) {
            var tab = prompt('Tab to switch to (number or id)?');
            if (!tabs.switchTab(tab)) {
                alert('That tab does not exist :\\');
            }
            e.preventDefault();
        });

        // Next and prev actions
        $('.controls a').on('click', function(e) {
            var action = $(this).attr('href').replace('#', '');
            tabs[action]();
            e.preventDefault();
        });
    });

   </script>
        
        <!-- Tag-it files end -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
     

<div class="app app-header-fixed ">
  

    <!-- header -->
  <header id="header" class="app-header navbar" role="menu">
      <!-- navbar header -->
      <div class="navbar-header bg-dark" style="margin: -1px 0 0 -1px;">
         

        <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs " ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>

        <a href="<?php echo base_url()."admin/dashboard"; ?>" class="navbar-brand text-lt">
         <!--  <i class="fa fa-btc"></i> -->
        <!--   <img src="<?php echo base_url()."resources/admin/img/logo.png"; ?>" alt="." class="hide"> -->
          <span class="hidden-folded m-l-xs"><?php echo WEBSITE_NAME; ?></span>
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
        <!-- buttons -->
        <div class="nav navbar-nav hidden-xs">
          <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app">
            <i class="fa fa-dedent fa-fw text"></i>
            <i class="fa fa-indent fa-fw text-active"></i>
          </a>
         <!--  <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="icon-user fa-fw"></i>
          </a> -->
        </div>

            

        <!-- / buttons -->

        <!-- link and dropdown -->
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span><?php echo $this->session->userdata('lang'); ?></span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <?php if($this->session->userdata('lang') != 'en'){?>
              <li>
                <a href="javascript:void(0)">
                  <div class="change-language" data-id="en">en</div>
                </a>
              </li>
              <?php } ?>
              <?php if($this->session->userdata('lang') != 'nl'){?>
              <li>
                <a href="javascript:void(0)">
                  <div class="change-language" data-id="nl">nl</div>
                </a>
              </li>
              <?php } ?>
              <?php if($this->session->userdata('lang') != 'fr'){?>
              <li>
                <a href="javascript:void(0)">
                  <div class="change-language" data-id="fr">fr</div>
                </a>
              </li>
              <?php } ?>
            </ul>
          </li>
        </ul>
        <!-- / link and dropdown -->

        <!-- search form -->
        <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" data-target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
          <!-- <div class="form-group">
            <div class="input-group">
              <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div> -->
        </form>
        <!-- / search form -->

        <!-- nabar right -->
     
        <ul class="nav navbar-nav navbar-right">

          <li class="dropdown" id="activities">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
              <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline"><?php echo lang('notifications') ?></span>
             <!--  <span class="badge badge-sm up bg-danger pull-right-xs" id="unread-activity-count"><?php //echo $unread_activities; ?></span> -->
            </a>
            <!-- dropdown -->
            
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <strong><?php echo lang('recent_activities') ?></strong>
                </div>
                <?php /*$activities = get_activities(25);*/ ?>
                <div class="list-group">
                  <?php /*
                  if(count($activities['activity_notifications']) > 0): ?>
                      <div class="noticebar-menu" id="activities-list">
                          <?php echo $this->load->view('activity/notifications', $activities, true); ?>
                      </div>
                  <?php else: ?>
                      <div class="noticebar-menu" style="padding: 10px;">
                         <?php echo lang('there_are_no_activities_added_yet');?> 
                      </div>
                  <?php endif; */ ?>
                </div>
                <div class="panel-footer text-sm">
                   
                  <a href="<?php echo site_url('activity/listing'); ?>" data-toggle="class:show animated fadeInRight"><?php echo lang('see_all_the_activities') ?></a>
                </div>
              </div>
            </div>
             <!-- / dropdown --> 
          </li>
          
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img src="<?php echo base_url('resources/admin/img/user.png') ?>"  alt="...">
                <i class="on md b-white bottom"></i>
              </span>
              <span class="hidden-sm hidden-md"><?php echo $this->session->userdata('name'); ?></span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              <li>
                <a href="<?php echo site_url('user/change_password') ?>" ui-sref="access.signin"><?php echo lang('settings') ?></a>
              </li>
              <li>
                <a href="<?php echo site_url('admin/logout') ?>" ui-sref="access.signin"><?php echo lang('log_out') ?></a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->
      </div>
      <!-- / navbar collapse -->
  </header>
  <!-- / header -->

        
<style>
.nav-btn
{
  margin-top: 10px;
   margin-bottom: 8px;
   margin-right:8px;

}
@media (max-width: 340px){
    .navbar-header > button {
      padding: 10px 8px!important;
  }
}
</style>