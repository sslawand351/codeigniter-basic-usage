<?php echo Assets::js('admin/js/plugins/flot/jquery.flot.js'); ?>
<?php echo Assets::js('admin/js/plugins/flot/jquery.flot.resize.js'); ?>
<?php echo Assets::js('admin/js/plugins/flot/jquery.flot.pie.js'); ?>
<?php echo Assets::js('admin/js/plugins/flot/jquery.flot.categories.js'); ?>
<?php echo Assets::js('admin/js/plugins/flot-spline/js/jquery.flot.spline.min.js'); ?>
<?php echo Assets::js('admin/js/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js'); ?>
<?php echo Assets::js('admin/js/plugins/flot.orderbars/js/jquery.flot.orderBars.js'); ?>

<?php echo Assets::js('admin/js/custom/user_dashboard.js'); ?>
<style>
.chosen-container {
	width: 100% !important;
}
.flot-x-axis {
	top: 40px !important;
	left: -10px !important;

}
.xaxisLabel {
	max-width: none !important;
    transform: rotate(-45deg) !important;
    -o-transform: rotate(-45deg) !important;
    -ms-transform: rotate(-45deg) !important;
    -moz-transform: rotate(-45deg) !important;
    -webkit-transform:  rotate(-45deg) !important;
    transform-origin: 0 0 !important;
    -o-transform-origin: 0 0 !important;
    -ms-transform-origin: 0 0 !important;
    -moz-transform-origin: 0 0 !important;
    -webkit-transform-origin: 0 0 !important;
}

</style>
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<!-- main header -->
		<div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
		    app.settings.asideFolded = false; 
		    app.settings.asideDock = false;
		  ">
	  <!-- main -->
	  		<div class="col" style="width: 100%;">
		<!-- main header -->
				<div class="bg-light lter b-b wrapper-md">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="m-n font-thin h3 text-black"><?php echo lang('dashboard') ?></h1>
						</div>
					</div>
				</div>
		<!-- / main header -->
				<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
					<div class="">
						<?php  if($this->session->flashdata('success_msg')) { ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong></strong> <?php echo $this->session->flashdata('success_msg'); ?>
						</div>
						<?php } ?>
						<?php  if($this->session->flashdata('error_msg')) { ?>            
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<?php echo $this->session->flashdata('error_msg'); ?>
						</div>
						<?php } ?>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo lang('welcome_to_admin_dashboard') ?>
						</div>
					</div>
				</div>
			</div>
			<div class="right-sidebar-activities"></div>
		</div>
	</div>
</div>