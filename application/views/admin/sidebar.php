<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(! isset($_activeTab)) $_activeTab = ''; ?>
  <!-- aside -->
  <aside id="aside" class="app-aside hidden-xs bg-dark">
      <div class="aside-wrap">
        <div class="navi-wrap">

          <!-- nav -->
          <nav ui-nav class="navi clearfix" id = "scrollable">
            <ul class="nav">
              <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                <span><?php echo lang('navigation') ?></span>
              </li>
              <li class="line dk"></li>
              <li class="<?php if($_activeTab !="" && $_activeTab == ADMIN_TAB_7) echo 'active'; ?>" >
                <a href="<?php echo site_url('admin/dashboard'); ?>" class="auto">
                  <i class="glyphicon glyphicon-home"></i>
                  <span><?php echo lang('dashboard') ?></span>
                </a>
              </li>
              <li class="<?php if($_activeTab !="" && $_activeTab == ADMIN_TAB_1) echo 'active'; ?>" >
                <a href class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <i class="glyphicon glyphicon-user"></i>
                  <span><?php echo lang('users') ?></span>
                </a>
                <ul class="nav nav-sub dk">
                <?php if($this->permissions->is_permission_allowed('VIEW_USER')): ?>
                <li>
                    <a href="<?php echo site_url('user/listing?type=admin_user') ?>"> 
                      <span><?php echo ucfirst(lang('admin_user')) ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('user/listing?type=frontend_user') ?>"> 
                      <span><?php echo ucfirst(lang('frontend_user')) ?></span>
                    </a>
                </li>
                <?php endif; ?>
           
                </ul>
                <ul class="nav nav-sub dk">
                  <li>
                    <a href="<?php echo site_url('usergroup/listing'); ?>">
                      <span><?php echo ucfirst(lang('user_roles')) ?></span>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="line dk"></li>
              <li class="<?php if($_activeTab !="" && in_array($_activeTab, array(ADMIN_TAB_4, ADMIN_TAB_2))) echo 'active'; ?>" >
                <a href class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <i class="fa fa-file-text"></i>
                  <span class="content"><?php echo ucfirst(lang('content_and_data')); ?></span>
                </a>
                <ul class="nav nav-sub dk">
                  <?php if($this->permissions->is_permission_allowed('VIEW_BLOG')): ?>
                  <li>
                    <a href="<?php echo site_url('article/listing'); ?>"> 
                       <i class="fa fa-fw fa-angle-right text"></i>
                      <span><?php echo ucfirst(lang('articles')); ?></span>
                    </a>
                  </li>
                  <?php endif;?>
                  <?php // if($this->permissions->is_permission_allowed('LIST_STATIC_PAGES')): ?>
                  <li>
                    <a href="<?php echo site_url('admin/static_page/listing'); ?>">
                       <i class="fa fa-fw fa-angle-right text"></i> 
                      <span><?php echo ucfirst(lang('static_pages')); ?></span>
                    </a>
                   </li>
                 <?php // endif; ?>
                </ul>
              </li>

              <?php if($this->permissions->is_permission_allowed('NEWSLETTER_MODULE')): ?>
              <li class="<?php if($_activeTab !="" && $_activeTab == ADMIN_TAB_8) echo 'active'; ?>" >
                <a href class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <i class="glyphicon glyphicon-envelope"></i>
                  <span><?php echo lang('marketing') ?></span>
                </a>
                <ul class="nav nav-sub dk">
                  <?php if($this->permissions->is_permission_allowed('NEWSLETTER_MODULE')): ?>
                  <li class="<?php if(isset($_activeTab) && $_activeTab !="" && $_activeTab == ADMIN_TAB_8) echo 'active'; ?>">
                    <a href class="auto">
                      <span class="pull-right text-muted">
                        <i class="fa fa-fw fa-angle-right text"></i>
                        <i class="fa fa-fw fa-angle-down text-active"></i>
                      </span>
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <span><?php echo lang('newsletters') ?></span>
                    </a>
                    <ul class="nav nav-sub dk">
                      <li>
                        <a href="<?php echo site_url('newsletter/template_listing'); ?>">
                         <i class="fa fa-circle text"></i>
                          <span><?php echo lang('newsletter') ?></span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo site_url('groups'); ?>">
                          <i class="fa fa-circle text"></i>
                          <span><?php echo lang('groups') ?></span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo site_url('newsletter/stats'); ?>">
                          <i class="fa fa-circle text"></i>
                          <span><?php echo lang('newsletter_stats') ?></span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo site_url('newsletter/credits/manage'); ?>">
                          <i class="fa fa-circle text"></i>
                          <span><?php echo lang('credits') ?></span>
                        </a>
                      </li>
                    </ul>
                  </li>
                   <?php endif; ?>
                </ul>
              </li>
              <?php endif; ?>
              <li class="line dk"></li>
              <li class="<?php if($_activeTab !="" && in_array($_activeTab, array(ADMIN_TAB_6, ADMIN_TAB_3))) echo 'active'; ?>" >
                <a href class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <i class="glyphicon glyphicon-search"></i>
                  <span><?php echo ucfirst(lang('custom_module')) ?></span>
                </a>
                <ul class="nav nav-sub dk">
                  <li class="<?php if(isset($_activeTab) && $_activeTab !="" && $_activeTab == ADMIN_TAB_3) echo 'active'; ?>">
                    <a href class="auto">
                      <span class="pull-right text-muted">
                        <i class="fa fa-fw fa-angle-right text"></i>
                        <i class="fa fa-fw fa-angle-down text-active"></i>
                      </span>
                       <i class="fa fa-fw fa-angle-right text"></i>
                      <span><?php echo ucfirst(lang('customize_form')) ?></span>
                    </a>
                    <ul class="nav nav-sub dk">
                    <li>
                        <a href="<?php echo site_url('admin/form/listing'); ?>"> 
                          <i class="fa fa-circle text"></i>
                          <span><?php echo ucfirst(lang('listing')); ?></span>
                        </a>
                     </li>
                     <li>
                        <a href="<?php echo site_url('admin/form/add'); ?>">
                           <i class="fa fa-circle text"></i>
                          <span><?php echo ucfirst(lang('add')); ?></span>
                        </a>
                     </li>
                     <li>
                        <a href="<?php echo site_url('frontend-forms'); ?>">
                           <i class="fa fa-circle text"></i>
                          <span><?php echo ucfirst(lang('frontend_forms')); ?></span>
                        </a>
                     </li>          
                    </ul>
                  </li>
                  <li class="<?php if(isset($_activeTab) && $_activeTab !="" && $_activeTab == ADMIN_TAB_6) echo 'active'; ?>">
                    <a href class="auto">
                      <span class="pull-right text-muted">
                        <i class="fa fa-fw fa-angle-right text"></i>
                        <i class="fa fa-fw fa-angle-down text-active"></i>
                      </span>
                      <i class="fa fa-fw fa-angle-right text"></i> 
                      <span><?php echo lang('email') ?></span>
                    </a>
                    <ul class="nav nav-sub dk">
                      <li>
                        <a href="<?php echo site_url('admin/email/template_listing'); ?>">
                         <i class="fa fa-circle text"></i>
                          <span><?php echo lang('templates') ?></span>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo site_url('admin/email/listing'); ?>">
                          <i class="fa fa-circle text"></i>
                          <span><?php echo lang('email') ?></span>
                        </a>
                      </li>
                    </ul>
                  </li>          
                </ul>
              </li>
              <li class="<?php if($_activeTab !="" && $_activeTab == ADMIN_TAB_5) echo 'active'; ?>" >
                <a href class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <i class="fa fa-cog"></i>
                  <span><?php echo lang('settings') ?></span>
                </a>
                <ul class="nav nav-sub dk">
                  <?php /* ?>
                  <li>
                      <a href="<?php echo site_url('user/listing?type=admin_user') ?>">
                        <i class="fa fa-fw fa-angle-right text"></i> 
                        <span><?php echo ucfirst(lang('admin_user')) ?></span>
                      </a>
                  </li>
                  <?php */ ?>
                  <li>
                    <a href="<?php echo site_url('admin/language/listing'); ?>">
                      <i class="fa fa-fw fa-angle-right text"></i> 
                      <span><?php echo ucfirst(lang('language')); ?></span>
                    </a>
                  </li>
                  <?php /* ?>
                  <li>
                    <a href="<?php echo site_url('usergroup/listing'); ?>">
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <span><?php echo ucfirst(lang('user_roles')) ?></span>
                    </a>
                  </li>
                  <?php */ ?>
                </ul>
              </li> 
            </ul>
          </nav>
          <!-- nav -->
 <!-- <li class="line dk hidden-folded"></li> -->
          <!-- aside footer -->
          <!-- <div class="wrapper m-t">
            <div class="text-center-folded">
              <span class="pull-right pull-none-folded">60%</span>
              <span class="hidden-folded"><?php // echo lang('milestone') ?></span>
            </div>
            <div class="progress progress-xxs m-t-sm dk">
              <div class="progress-bar progress-bar-info" style="width: 60%;">
              </div>
            </div>
            <div class="text-center-folded">
              <span class="pull-right pull-none-folded">35%</span>
              <span class="hidden-folded"><?php // echo lang('release') ?></span>
            </div>
            <div class="progress progress-xxs m-t-sm dk">
              <div class="progress-bar progress-bar-primary" style="width: 35%;">
              </div>
            </div>
          </div> -->
          <!-- / aside footer -->
        </div>
      </div>
  </aside>
  <!-- / aside -->


<style>
#aside
{
    position: fixed;
  /*  height: 370px;*/
    overflow-y: auto;
    top: 49px;
    overflow-x: hidden;
     bottom: 0px;
}
.streamline { margin-bottom: 15px; }
#see_all_activities a { padding: 20px; }
#see_all_activities
{
  position: relative;
  text-align: center;
  height: 60px;
  bottom: 20px;
  margin: 20px auto;
}
#right-aside
{
  /*height: 500px;*/
  height: 100%;
  position: fixed;
  top: 49px;
  overflow-y: scroll;
  overflow-x: hidden;
  z-index: 700;
  /*margin-bottom: 20px;*/
}
.navi ul.nav li li a
{
  padding-left: 20!important;
}
.nav-sub .dk
{
  z-index: 1000000;
}
.dk
{
    background-color: #2e3344!important;
}
@media (min-width: 768px)
{
  .app-aside-folded .navi > ul > li > ul 
  {
      position: absolute;
      top: 0 !important;
      left: 100%;
      z-index: 1050;
      width: 217px;
      height: 0 !important;
      -webkit-box-shadow: 0 2px 6px rgba(0, 0, 0, 0.1);
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.1);
  }
}
.app-aside-folded #aside 
{
    overflow-y: visible!important;
    overflow-x: visible!important;
}
.bg-dark .dk 
{
    background-color: #2e3344;
}
.navi ul.nav li a > i {
    position: relative;
    float: left;
    width: 40px;
    margin: -10px -10px;
    margin-right: 5px;
    overflow: hidden;
    line-height: 40px;
    text-align: center;
}
@media (min-width: 768px)
{
  .app-aside-folded .navi > ul > li > a i 
  {
      display: block;
      float: none;
      width: auto;
      margin: 0;
      font-size: 16px;
      line-height: 50px;
      border: none !important;
  }
}
.glyphicon-user
{
  color: #5DF3FA;
}
.glyphicon-search
{
  color:#F8FA5D;
}
.fa-trash
{
  color: #6B5DFA;
}
.glyphicon-level-up
{
  color:#F17284;
}
.fa-file-text-o
{
  color: #54C852;
}
.fa-file-text
{
  color: #4774E3;
}
.fa-language
{
  color: #A931C7;
}
.glyphicon-envelope
{
  color: #31C771;
}
.fa-cog
{
  color: #EA685F;
}
.glyphicon-home
{
  color: #F8FA5D;
}
.content
{
    display: -webkit-box;
}
.navi ul.nav li li a
{
  padding-left: 20px!important;
}
.fa-circle
{
  font-size: 8px!important;
}
</style>