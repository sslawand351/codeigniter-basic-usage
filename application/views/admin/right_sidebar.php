<!-- right col -->
<div class="col w-md bg-white-only b-l bg-auto no-border-xs right-sidebar-activities">
  <div class="wrapper-md recent-activities-sidebar" id="right-aside">      
    <!-- streamline -->
    <div class="m-b text-md">Recent Activities</div>
    <div class="streamline b-l m-b">
      <?php if( ! empty($activity_notifications)) { ?>
        <?php foreach($activity_notifications as $key => $activity) : ?>
        <div class="sl-item b-primary">
          <div class="m-l">
            <div class="text-muted"><?php echo ago(strtotime($activity['activity_on'])); ?> <?php echo lang('ago'); ?></div>
            <p><?php echo $activity['activity_text']; ?></p>
          </div>
        </div>
        <?php endforeach; ?>
      <?php } else { ?>
        <div class="sl-item b-primary">
          <div class="m-l">
          <p><?php echo lang('no_activity_found'); ?></p>
          </div>
        </div>
      <?php } ?>
    </div>
    <!-- / streamline -->
    <div class="text-center" id="see_all_activities">
      <a href="<?php echo site_url('activity/listing'); ?>" data-toggle="class:show animated fadeInRight"><?php echo ucfirst(lang('see_all_the_activities')) ?></a>
    </div>
  </div>

</div>
<!-- / right col -->