<script type="text/javascript" src="http://cdn.ckeditor.com/4.5.8/standard-all/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/admin/ckeditor/config.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/admin/ckeditor/adapters/jquery.js"></script>

  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
	<!-- main header -->
	<div class="bg-light lter b-b wrapper-md">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<h1 class="m-n font-thin h3 text-black"><?php echo lang('add_blog') ?></h1>
			</div>
		</div>
	</div>
	<!-- / main header -->
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo lang('add_blog') ?>
			</div>
			<div class="panel-body">
				<form role="form" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">

      		 		<div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucfirst(lang('article_name'))?><div class="text-danger">*</div></label>
                        <div class="col-sm-10">
                            <input type="text" name="title" data-required="true" class="name form-control parsley-validated">
                             <span class="text-danger"><?php echo form_error('title');?></span>
                            <ul>
                                <li class="formError" style="font-size:12px;display:block;margin-left:-40px;margin-top:6px;margin-bottom:-10px;"></li>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo ucfirst(lang('url'))?><div class="text-danger">*</div></label>
                        <div class="col-sm-10">
                            <input type="text" name="url" data-required="true" class="name form-control parsley-validated">
                            <span class="text-danger"><?php echo form_error('url');?></span>
                            <ul>
                                <li class="formError" style="font-size:12px;display:block;margin-left:-40px;margin-top:6px;margin-bottom:-10px;"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo lang('body')?><div class="text-danger">*</div></label>
                        <div class="col-sm-10">
                        <textarea  name="description" id="editor1" class="description name form-control parsley-validated" data-required="true" ></textarea>
                        <script type="text/javascript">
					      CKEDITOR.replace( 'editor1' );
					      CKEDITOR.add            
					   </script>
                            <!-- <textarea name="description" rows = "10" data-required="true" class="name form-control parsley-validated" ></textarea> -->
                             <span class="text-danger"><?php echo form_error('description');?></span>
                            <ul>
                                <li class="formError" style="font-size:12px;display:block;margin-left:-40px;margin-top:6px;margin-bottom:-10px;"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        
                        <label class="col-sm-2 control-label"><?php echo ucfirst(lang('page_title'))?><div class="text-danger">*</div></label>
                        <div class="col-sm-10">
                            <input type="text" name="page_title" data-required="true" class="name form-control parsley-validated">
                             <span class="text-danger"><?php echo form_error('page_title');?></span>
                            <ul>
                                <li class="formError" style="font-size:12px;display:block;margin-left:-40px;margin-top:6px;margin-bottom:-10px;"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta-description  <div class="text-danger">*</div></label>
                        <div class="col-sm-10">
                            <textarea name="meta_description" data-required="true" class="name form-control parsley-validated "></textarea>
                             <span class="text-danger"><?php echo form_error('meta_description');?></span>
                            <ul>
                                <li class="formError" style="font-size:12px;display:block;margin-left:-40px;margin-top:6px;margin-bottom:-10px;"></li>
                            </ul>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta-keywords  <div class="text-danger">*</div></label>
                        <div class="col-sm-10">
                            <input type="text" name="meta_keywords" data-required="true" class="name form-control parsley-validated ">
                             <span class="text-danger"><?php echo form_error('meta_keywords');?></span>
                            <ul>
                                <li class="formError" style="font-size:12px;display:block;margin-left:-40px;margin-top:6px;margin-bottom:-10px;"></li>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Image <div class="text-danger">*</div></label>
                        <div class="col-sm-10">
                            <input type="file" name="image" data-required="true" class="file form-control parsley-validated">
                             <span class="text-danger"><?php echo form_error('image');?></span>
                            <ul>
                                <li class="formError" style="font-size:12px;display:block;margin-left:-40px;margin-top:6px;margin-bottom:-10px;"></li>
                            </ul>
                        </div>
                    </div>
				    <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('save') ?></button>

				</form>
			</div>
		</div>
	</div>
</div>



<style>
.form-horizontal .form-group {
     margin-right: 0px!important; 
     margin-left: 0px!important; 
}
.chosen-container{
	width:100%!important;
}
.text-danger
{
	display: inline-block;
}
</style>
