<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">

	<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"><?php echo lang('blogs') ?></h1>
				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
			<div class="">
				<?php  if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong></strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>
				<?php  if($this->session->flashdata('error')) { ?>            
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					 <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php if($this->permissions->is_permission_allowed('BLOG_ADD')): ?>
						<a class="btn btn-primary btn-xs pull-right" href="<?php echo site_url('article/add');?>"  ><?php echo lang('add_blog') ?></a>
					<?php endif; ?>
					
					<?php echo lang('blog_listing') ?>
				</div>
				
				<div class="table-responsive">		
					<table class="tftable table table-striped m-b-none">
						<tr>
							<th><?php echo lang('title') ?></th>
							<th><?php echo lang('description') ?></th>
							<th><?php echo lang('added_on') ?></th>
							<th><?php echo lang('action') ?></th>
						</tr>
						<?php 
                            if(isset($articles['rc'])){
                           
                                $article = $articles['data'];
                                $count = 1;
							foreach( $articles['data'] as $value ) {?>
	
						
							<td><?php echo $value['title'];?></td>
							<td style="width:50%;"><?php echo $value['meta_description'];?> </td>
							<td><?php echo date('jS, F, Y',$value['added_on']);?> </td>
							<td>
							
								 <a class="publish_article btn btn-xs btn-primary" role="button" data-id="<?php echo $value['id'];?>" data-toggle="modal" href="#publish_article" ng-click="focusInput=true"><?php echo lang('publish')?></a>
								 <?php if($this->permissions->is_permission_allowed('BLOG_EDIT')): ?>
									<a class="btn btn-xs btn-info" href="<?php echo site_url('article/edit');?>/<?php echo $value['id'];?>"><?php echo lang('edit')?></a>
								<?php endif; ?>
								 <?php if($this->permissions->is_permission_allowed('BLOG_EDIT')): ?>
									<a class="btn btn-xs btn-danger delete_article" href="#delete_blog_popup" data-toggle="modal" data-article-id="<?php echo $value['id']; ?>"><?php echo lang('delete')?></a>
								<?php endif; ?>
								 
							</td>
							
						</tr>
						
						<?php } $count++; }else{?>
							<tr>
								<td colspan="5"><?php echo lang('no_blogs_added_yet') ?></td>
							</tr>
						<?php }?>
					</table>
				</div>
				
				
			</div>
		</div>
	</div>
	
	<div id="delete_blog_popup" class="modal fade" data-backdrop="true" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 style="margin:0px;"><?php echo lang('delete_blog') ?></h3>
				</div>
				<form id="delete_article_form" action="" method="post">
				    <div class="modal-body">
			    		<input type="hidden" class="article_id" name="article_id" value="">
			    		<div class="row">
			    			<div class="col-md-12">
			    			<?php echo lang('are_you_sure_you_want_to_delete_this_user') ?> ?
			    			</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="delete_article_btn btn btn-default"><?php echo lang('ok') ?></button>
						<button type="button" class="btn btn-default close_modal" data-dismiss="modal"><?php echo lang('close') ?></button>
					</div>
		    	</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="publish_article" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="myModalLabel"><?php echo lang('publish_blogs')?></h3>
                </div>
                <form class="bs-example form-horizontal clearfix" method="post" id="add_article_form" action="<?php echo base_url();?>article/add_location">
                <div class="modal-body">
                    
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="option col-sm-12">
                                           <!--  <input type="checkbox" name="article_location[]" class="article_location" value="<?php echo ANTWERPEN;?>">&nbsp;Antwerpen<br>
                                            <input type="checkbox" name="article_location[]" class="article_location" value="<?php echo OVERIJSE;?>">&nbsp;Overijse<br>
                                            <input type="checkbox" name="article_location[]" class="article_location" value="<?php echo VERELLEN_GEEL;?>">&nbsp;Verellen Geel<br> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                          
                    
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo lang('close')?></button>
                    <button type="submit" class="btn btn-primary"><?php echo lang('save')?></button>
                </div>
                </form>
            </div>
        </div>
    </div>
<script>
		$(document).on("click", ".delete_article", function(e){
        var article_id = $(this).attr('data-article-id');
        var link = site_url('index.php/article/delete/' + article_id);

        $('#delete_article_form').attr('action',link);
        $(".article_id").val(article_id);
        
    });

    $(".delete_article_btn").on("click",function(){
        var form = $("#delete_article_form");
        form.submit();
    });
</script>