<!DOCTYPE html>
<html>
<head>
  <title>LOGIN</title>
  <link href="<?php echo base_url()?>resources/css/bootstrap.min.css" type="text/css" rel="stylesheet">
  <script src="<?php echo base_url()?>resources/js/jquery.min.js"></script>
</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">LOGIN TEST</a>
      </div>
      <ul class="nav navbar-nav">
      <?php if(!$this->session->userdata('user_id'))
      {?>
        <li><a href="<?php echo site_url('User/login')?>">Login</a></li>
      <?php }else{?>
        <li><a href="<?php echo site_url('User/dashboard')?>">My_dashboard</a></li>
        <li><a href="<?php echo site_url('User/logout')?>">Logout</a></li>  
      <?php }?>
      </ul>
    </div>
</nav>
</body>
</html>