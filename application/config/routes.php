<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'user';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin/login'] 				= 'admin/user/login';
$route['admin/dashboard'] 				= 'admin/user/dashboard';
$route['admin/logout'] 				= 'admin/user/logout';
$route['user/listing'] 				= 'admin/user/listing';
$route['user/listing/(:any)'] 				= 'admin/user/listing/$1';
$route['user/deleted_listing'] 				= 'admin/user/deleted_listing';
$route['user/deleted_listing/(:any)'] 			= 'admin/user/deleted_listing/$1';
$route['user/add'] 				= 'admin/user/add';
$route['user/edit'] 				= 'admin/user/edit';
$route['user/edit/(:any)'] 				= 'admin/user/edit/$1';
$route['user/change_password'] 				= 'admin/user/change_password';
$route['user/remove'] 				= 'admin/user/remove';
$route['user/remove/(:any)'] 				= 'admin/user/remove/$1';
$route['user/undo_delete'] 				= 'admin/user/undo_delete';
$route['user/undo_delete/(:num)'] 				= 'admin/user/undo_delete/$1';
$route['article/listing'] 				= 'admin/article/listing';
$route['article/add'] 				= 'admin/article/add';
$route['article/edit/(:any)'] 				= 'admin/article/edit/$1';
$route['article/delete/(:any)'] 				= 'admin/article/delete/$1';
$route['article/delete_image/(:any)'] 				= 'admin/article/delete_image/$1';
$route['article/delete_thumbnail/(:any)'] 				= 'admin/article/delete_thumbnail/$1';
$route['usergroup/listing'] 	= 'admin/usergroup/listing';
$route['usergroup/edit_permissions/(:any)'] 	= 'admin/usergroup/edit_permissions/$1';
$route['usergroup/add'] 	= 'admin/usergroup/add';
$route['usergroup/edit/(:num)'] 	= 'admin/usergroup/edit/$1';
$route['usergroup/remove/(:num)'] 	= 'admin/usergroup/remove/$1';
$route['activity/listing'] 	= 'admin/activity/listing';
$route['activity/update_activity_seen_for_user'] 	= 'admin/activity/update_activity_seen_for_user';
$route['activity/ajax_get_activities'] 	= 'admin/activity/ajax_get_activities';
$route['article/article_by_url/(:any)'] 				= 'admin/article/article_by_url/$1';

$route['user/register'] 				= 'frontend/user/register';
$route['user/login'] 					= 'frontend/user/login';
$route['user/forgot_password'] 			= 'frontend/user/forgot_password';
$route['admin/static_page/listing'] = 'admin/static_page/listing';
$route['admin/static_page/listing/(:any)'] = 'admin/static_page/listing/$1mi';
$route['admin/static_page/listing/edit/(:any)'] = 'admin/static_page/listing/edit';
$route['admin/static_page/listing/delete/(:any)'] = 'admin/static_page/listing/delete';

$route['frontend-forms'] = 'admin/form/frontend_forms';
$route['frontend-forms/(:num)'] = 'admin/form/frontend_forms/$1';

//Newsletter routes
$route['admin/newsletter/template_listing'] 	= 'admin/newsletter/template_listing';
$route['newsletter/template_listing'] 	= 'admin/newsletter/template_listing';
$route['newsletter/template_listing/(:num)'] 	= 'admin/newsletter/template_listing/$1';
$route['groups'] 					= 'admin/group/listing';
$route['groups/(:num)'] 					= 'admin/group/listing/$1';
$route['admin/groups'] 					= 'admin/group/listing';
$route['newsletter/stats'] = 'admin/newsletter/stats';
$route['admin/newsletter/stats'] = 'admin/newsletter/stats';
$route['newsletter/sent'] = 'admin/newsletter/sent';
$route['admin/newsletter/sent'] = 'admin/newsletter/sent';
$route['newsletter/open'] = 'admin/newsletter/open';
$route['admin/newsletter/open'] = 'admin/newsletter/open';
$route['newsletter/click'] = 'admin/newsletter/click';
$route['admin/newsletter/click'] = 'admin/newsletter/click';
$route['newsletter/build/(:num)'] = 'admin/newsletter/build/$1';
$route['admin/newsletter/build/$1'] = 'admin/newsletter/build/$1';
$route['newsletter/delete_newsletter_template'] = 'admin/newsletter/delete_newsletter_template';
$route['group/import_csv_emails'] = 'admin/group/import_csv_emails';
$route['group/users/(:num)'] = 'admin/group/users/$1';
$route['group/delete'] = 'admin/group/delete';
$route['group/delete_user_from_group'] = 'admin/group/delete_user_from_group';
$route['group/unsubscribe_user_from_group'] = 'admin/group/unsubscribe_user_from_group';
$route['group/subscribe_user_to_group'] = 'admin/group/subscribe_user_to_group';
$route['newsletter/sent/(:num)'] = 'admin/newsletter/sent/$1';
$route['newsletter/sent/(:num)/(:num)'] = 'admin/newsletter/sent/$1/$2';
$route['newsletter/open/(:num)'] = 'admin/newsletter/open/$1';
$route['newsletter/open/(:num)/(:num)'] = 'admin/newsletter/open/$1/$2';
$route['newsletter/click/(:num)'] = 'admin/newsletter/click/$1';
$route['newsletter/click/(:num)/(:num)'] = 'admin/newsletter/click/$1/$2';
$route['newsletter/bounce/(:num)'] = 'admin/newsletter/bounce/$1';
$route['newsletter/bounce/(:num)/(:num)'] = 'admin/newsletter/bounce/$1/$2';
$route['newsletter/unsubscribe_list/(:num)'] = 'admin/newsletter/unsubscribe_list/$1';
$route['newsletter/unsubscribe_list/(:num)/(:num)'] = 'admin/newsletter/unsubscribe_list/$1/$2';
$route['newsletter/delete_newsletter_zone'] = 'admin/newsletter/delete_newsletter_zone';
$route['newsletter/credits/manage'] = 'admin/newsletter_credit/manage';
$route['newsletter/credits/purchase'] = 'admin/newsletter_credit/purchase';
$route['newsletter/credits/history'] = 'admin/newsletter_credit/history';


$route['user/ajax_newsletter_subscription'] = 'frontend/user/ajax_newsletter_subscription';
$route['(nl|fr|en)/user/cancel_newsletter_subscription/(:num)/(:any)'] = 'frontend/user/cancel_newsletter_subscription/$2/$3';
$route['(nl|fr|en)/newsletter_unsubscription/(:num)/(:num)/(:any)'] = 'frontend/user/newsletter_unsubscription/$2/$3/$4';

$route['en/dashboard'] = 'frontend/user/dashboard';
$route['en/login'] = 'frontend/user/login';
$route['en/register'] = 'frontend/user/register';
$route['en/logout'] = 'frontend/user/logout';
$route['en/forgot-password'] = 'frontend/user/forgot_password';
$route['en/update-lang'] = 'frontend/user/update_language';

$route['fr/dashboard'] = 'frontend/user/dashboard';
$route['fr/login'] = 'frontend/user/login';
$route['fr/register'] = 'frontend/user/register';
$route['fr/logout'] = 'frontend/user/logout';
$route['fr/forgot-password'] = 'frontend/user/forgot_password';
$route['fr/update-lang'] = 'frontend/user/update_language';

$route['nl/dashboard'] = 'frontend/user/dashboard';
$route['nl/login'] = 'frontend/user/login';
$route['nl/register'] = 'frontend/user/register';
$route['nl/logout'] = 'frontend/user/logout';
$route['nl/forgot-password'] = 'frontend/user/forgot_password';
$route['nl/update-lang'] = 'frontend/user/update_language';

$route['newsletter/credits/manage'] = 'admin/newsletter_credit/manage';
$route['newsletter/credits/purchase'] = 'admin/newsletter_credit/purchase';
$route['newsletter/credits/history'] = 'admin/newsletter_credit/history';


$route['(nl|en|fr)/(:any)']	    			= 'admin/static_page/view';