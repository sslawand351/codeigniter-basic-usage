<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('USERS_PER_PAGE_LIMIT', 50);
define('ADMIN_TAB_1', 'tab1');
define('ADMIN_TAB_2', 'tab2');
define('ADMIN_TAB_3', 'tab3');
define('ADMIN_TAB_4', 'tab4');
define('ADMIN_TAB_8', 'tab8');
define('ADMIN_TAB_5', 'tab5');
define('ADMIN_TAB_6', 'tab6');
define('ADMIN_TAB_7', 'tab7');

define('ACTIVITIES_PER_PAGE_LIMIT', '5');


define('ACTIVITY_ADMIN_HAS_LOGGED_IN','1');
define('ACTIVITY_ADMIN_HAS_LOGGED_OUT','2');
define('ACTIVITY_ADMIN_ADDED_ROLE','3');
define('ACTIVITY_ADMIN_UPDATED_ROLE','4');
define('ACTIVITY_ADMIN_DELETED_ROLE','5');
define('ACTIVITY_ADMIN_ADDED_USER','6');
define('ACTIVITY_ADMIN_UPDATED_USER','7');
define('ACTIVITY_ADMIN_DELETED_USER','8');
define('ACTIVITY_ADMIN_ADD_BLOG','9');
define('ACTIVITY_ADMIN_UPDATE_BLOG','10');
define('ACTIVITY_ADMIN_DELETE_BLOG','11');

// Newsletters Activity constants
defined('ACTIVITY_ADMIN_ADDED_NEWSLETTER_GROUP')        			OR define('ACTIVITY_ADMIN_ADDED_NEWSLETTER_GROUP', '12');
defined('ACTIVITY_ADMIN_UPDATED_NEWSLETTER_GROUP')        			OR define('ACTIVITY_ADMIN_UPDATED_NEWSLETTER_GROUP', '13');
defined('ACTIVITY_ADMIN_DELETED_NEWSLETTER_GROUP')        			OR define('ACTIVITY_ADMIN_DELETED_NEWSLETTER_GROUP', '14');
defined('ACTIVITY_ADMIN_USERS_ADDED_TO_NEWSLETTER_GROUP')   		OR define('ACTIVITY_ADMIN_USERS_ADDED_TO_NEWSLETTER_GROUP', '15');
defined('ACTIVITY_ADMIN_USERS_REMOVED_FROM_NEWSLETTER_GROUP') 		OR define('ACTIVITY_ADMIN_USERS_REMOVED_FROM_NEWSLETTER_GROUP', '16');
defined('ACTIVITY_ADMIN_ADDED_NEWLETTER_TEMPLATE')        			OR define('ACTIVITY_ADMIN_ADDED_NEWLETTER_TEMPLATE', '17');
defined('ACTIVITY_ADMIN_DELETED_NEWLETTER_TEMPLATE')        		OR define('ACTIVITY_ADMIN_DELETED_NEWLETTER_TEMPLATE', '18');
defined('ACTIVITY_ADMIN_ADDED_USERS_TO_NEWSLETTER_GROUP_VIA_CSV') 	OR define('ACTIVITY_ADMIN_ADDED_USERS_TO_NEWSLETTER_GROUP_VIA_CSV', '19');
defined('ACTIVITY_ADMIN_ADDED_NEWLETTER_ZONE')						OR define('ACTIVITY_ADMIN_ADDED_NEWLETTER_ZONE', '20');
defined('ACTIVITY_ADMIN_UPDATED_NEWLETTER_ZONE')					OR define('ACTIVITY_ADMIN_UPDATED_NEWLETTER_ZONE', '21');
defined('ACTIVITY_ADMIN_DELETED_NEWLETTER_ZONE')					OR define('ACTIVITY_ADMIN_DELETED_NEWLETTER_ZONE', '22');
defined('ACTIVITY_USER_SUBSCRIBED_FOR_NEWSLETTER')					OR define('ACTIVITY_USER_SUBSCRIBED_FOR_NEWSLETTER','23');
defined('ACTIVITY_USER_UNSUBSCRIBED_NEWSLETTER')					OR define('ACTIVITY_USER_UNSUBSCRIBED_NEWSLETTER','24');
defined('ACTIVITY_ADMIN_UNDO_DELETE_USER')							OR define('ACTIVITY_ADMIN_UNDO_DELETE_USER','25');

define('ACTIVITY_ADMIN_ADDED_STATIC_PAGE','26');
define('ACTIVITY_ADMIN_UPDATED_STATIC_PAGE','27');
define('ACTIVITY_ADMIN_DELETED_STATIC_PAGE','28');
define('ACTIVITY_ADMIN_PUBLISHED_STATIC_PAGE','29');
define('ACTIVITY_ADMIN_UNPUBLISHED_STATIC_PAGE','30');
define('ACTIVITY_ADMIN_PUBLISHED_BLOG','31');
define('ACTIVITY_ADMIN_UNPUBLISHED_BLOG','32');



define('MASTER_ADMIN', 1);
define('SUPER_ADMIN', 2);

/*
 * Thumbnail constants
 */

define('IMAGE_WIDTH_LARGE', 500);
define('IMAGE_WIDTH_MEDIUM', 300);
define('IMAGE_WIDTH_SMALL', 150);

define('IMAGE_HEIGHT_LARGE', 333);
define('IMAGE_HEIGHT_MEDIUM', 200);
define('IMAGE_HEIGHT_SMALL', 100);

// FORM FIELDS
define('FORM_FIELD_TYPE_TEXT', 'textField');
define('FORM_FIELD_TYPE_EMAIL', 'textEmail');
define('FORM_FIELD_TYPE_TEXTAREA', 'textArea');
define('FORM_FIELD_TYPE_PASSWORD', 'passwordField');
define('FORM_FIELD_TYPE_RADIO_BUTTON', 'radioButton');
define('FORM_FIELD_TYPE_CHECKBOX', 'checkbox');
define('FORM_FIELD_TYPE_SELECT_DROPDOWN', 'dropdown');
define('FORM_FIELD_TYPE_WEBSITE', 'textWebsite');
define('FORM_FIELD_TYPE_FILE_UPLOAD', 'fileUpload');
define('FORM_FIELD_TYPE_DATE', 'textDate');

define('NEWSLETTERS_PER_PAGE_LIMIT', 50);
define('PER_PAGE_LIMIT', 50);
defined('NEWSLETTER_GROUP_ID_MAIN_CHANNEL')			OR define('NEWSLETTER_GROUP_ID_MAIN_CHANNEL', '1');
defined('NEWSLETTER_FREE_CREDITS')			OR define('NEWSLETTER_FREE_CREDITS', '1000');

define('EMAILS_PER_PAGE_LIMIT', 50);
define('FORMS_PER_PAGE_LIMIT', 50);
define('ARTICLES_PER_PAGE_LIMIT', 10);
define('STATIC_PAGES_PER_PAGE_LIMIT', 10);

define('FORM_TYPE_PREDEFINED_EMAIL_TEMPLATE', 'predefined-email-template');
define('FORM_TYPE_CUSTOM_FORM', 'custom-form');

define('MASTER_ADMIN_EMAIL', 'admin@way2web.be');
define('SUPER_ADMIN_EMAIL', 'info@rcodebase.be');

define('WEBSITE_NAME', 'Rcodebase');
define('TITLE_PATTERN', ' - ');
define('PRIMARY_LANG', 'nl');
define('PRIMARY_LANGUAGE_ID', '1');

define('EMAIL_VENDOR_MANDRILL', 'Mandrill');

if(ENVIRONMENT == 'production') {
	define('MANDRILL_API_KEY', '_hUwntrtBEBa3zwxh3gI7w');
	define('MANDRILL_SUBACCOUNT', 'payal');
	define('SEND_FROM_EMAIL_ID', 'sagarl@codebox.co.in');
	define('SEND_FROM_EMAIL_NAME', 'Team Rcodebase');
	define('ADMIN_EMAIL', 'sagarl@codebox.co.in');
} elseif(ENVIRONMENT == 'testing') {
	define('MANDRILL_API_KEY', '_hUwntrtBEBa3zwxh3gI7w');
	define('MANDRILL_SUBACCOUNT', 'payal');
	define('SEND_FROM_EMAIL_ID', 'sagarl@codebox.co.in');
	define('SEND_FROM_EMAIL_NAME', 'Team Rcodebase');
	define('ADMIN_EMAIL', 'sagarl@codebox.co.in');
} else {
	define('MANDRILL_API_KEY', '_hUwntrtBEBa3zwxh3gI7w');
	define('MANDRILL_SUBACCOUNT', 'payal');
	define('SEND_FROM_EMAIL_ID', 'sagarl@codebox.co.in');
	define('SEND_FROM_EMAIL_NAME', 'Team Rcodebase');
	define('ADMIN_EMAIL', 'sagarl@codebox.co.in');
}