<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Social extends BaseController
{
	public function __construct(){
			parent::__construct();
			require('vendor/autoload.php');
			$this->load->model('User_model');
	}

	public function facebook()
	{
			$fb = new Facebook\Facebook([
				'app_id' => '1789773881260306',
				'app_secret' => '89b860bb2490c5863fc5ef015b4015d1',
				'default_graph_version' => 'v2.5',
			]);

			$helper = $fb->getRedirectLoginHelper();

			$permissions = ['email', 'public_profile']; // Optional permissions
			$loginUrl = $helper->getLoginUrl(site_url("social/fb_oauth"), $permissions);
			redirect($loginUrl);
	}

	function fb_oauth()
	{

			$fb = new Facebook\Facebook([
				'app_id' => '1789773881260306',
				'app_secret' => '89b860bb2490c5863fc5ef015b4015d1',
				'default_graph_version' => 'v2.5',
			]);

			$helper = $fb->getRedirectLoginHelper();

			try {
				$accessToken = $helper->getAccessToken();
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				// When Graph returns an error
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				// When validation fails or other local issues
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			try {
				// Returns a `Facebook\FacebookResponse` object
				$response = $fb->get('/me?fields=id,name,email,first_name,last_name', $accessToken);
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			$user = $response->getGraphUser();
			$user_exists = $this->User_model->check_email($user['email']);
			
			if($user_exists['rc'])
			{
				$user_data = array(
						'user_id' => $user_exists['data']['id'],
						'user_type' => $user_exists['data']["user_type"],
						'name'=>$user_exists['data']['first_name']." ".$user_exists['data']['last_name'],
						'is_admin' => FALSE,
				);

				$this->User_model->set_client_login_session($user_data);
			}
			else
			{
				$hash = md5(microtime().rand());                        
				$user_data = array(
					'first_name'  => $user['first_name'],
					'last_name'   => $user['first_name'],
					'email'       => $user['email'],
					'added_on'    => date('Y-m-d G:i:s'),
					"password"    => $hash
				);

				$response = $this->User_model->add_users($user_data);

				$user_exists = $this->User_model->check_email($user['email']);
				$user_data = array(
						'user_id' => $user_exists['data']['id'],
						'user_type' => $user_exists['data']["user_type"],
						'name'=>$user_exists['data']['first_name']." ".$user_exists['data']['last_name'],
						'is_admin' => FALSE,
				);

				$this->User_model->set_client_login_session($user_data);
			}
			redirect('frontend/user/dashboard');
	}
}