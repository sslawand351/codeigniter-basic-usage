<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
        // load the required models
        $this->load->model(
            array(
                    'User_model', 
                )
            );
        $this->load->library(array('Mailer'));
        
        ini_set('max_execution_time', 3000);
    }

    function send_queued_email()
    {
        $this->load->library('mailer');
        $this->mailer->send_queued_email_mandrill();
    }

    function send_queued_newsletter_emails()
    {
        $sql = "SELECT id, cron_execution_status FROM current_email_cron_execution WHERE cron_execution_status = '0' AND id = '1'";

        $cron_execution = $this->db->query($sql)->row_array();

        if( isset($cron_execution['id']))
        {
            echo 'success';
            $update_data = array(
                'cron_execution_status' => '1',
                'added_on' => date('Y-m-d G:i:s')
            );

            $this->db->where('id', '1');
            $this->db->update('current_email_cron_execution', $update_data);

            $this->load->library('mailer');
            $this->mailer->send_queued_newsletter_email_mandrill();

            $update_data = array(
                'cron_execution_status' => '0',
                'added_on' => date('Y-m-d G:i:s')
            );

            $this->db->where('id', '1');
            $this->db->update('current_email_cron_execution', $update_data);
        }
        else
        {
            echo 'error';
        }
    }

    function free_credits_per_month()
    {
        $this->load->model('Newsletter_credit_model');
        $this->Newsletter_credit_model->free_credits_per_month();
    }
}