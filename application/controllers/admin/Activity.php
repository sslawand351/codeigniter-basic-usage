<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Activity extends AdminController {
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('Activity_model','User_model'));
    }
    
    function listing()
    {
        // check if the user is logged in or not
        $this->authenticate();

        $this->load->library('pagination');

        $query = '';

        $get_data = $this->input->get();
        $count = 0;
        foreach ($get_data as $param => $value) {
            $count++;
            if($param!='offset')
            {
                $query .= $param.'='.$value;

                if(count($get_data) != $count)
                {
                    $query .= '&';
                }
            }
        }

        if($this->user['is_admin'])
        {
            $activities = $this->Activity_model->get_all_activities_count($get_data);
        }
        else
        {
            $user_id = $this->user['id'];
            $activities = $this->Activity_model->get_all_activities_count($get_data, 0, $user_id);
        }

        $activity_count = $activities['rc'] ? $activities['data'][0]['activity_count'] : 0 ;

        $config = array(
            'base_url' => site_url('admin/activity/listing?'.$query),
            'total_rows' => $activities['rc'] ? $activities['data'][0]['activity_count'] : 0,
            'per_page' => ACTIVITIES_PER_PAGE_LIMIT,
            'uri_segment' => 3,
            'enable_query_strings' => TRUE,
            'page_query_string' => TRUE,
            'query_string_segment' => 'offset'
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);
        // $limit = array('limit' => ACTIVITIES_PER_PAGE_LIMIT, 'limit_start' => $limit_start);

        $limit_start = (is_numeric ($this->input->get('offset'))) ? $this->input->get('offset') : 0;
        $data['page'] = (is_numeric($limit_start) && $limit_start<0) ? $limit_start : 0;
        
        if($this->user['is_admin'])
        {
            $activities = $this->Activity_model->fetch_all_activities(ACTIVITIES_PER_PAGE_LIMIT, $limit_start, $get_data);
        }
        else
        {
            $user_id = $this->user['id'];
            $activities = $this->Activity_model->fetch_all_activities(ACTIVITIES_PER_PAGE_LIMIT, $limit_start, $get_data, $user_id);
        }
        
        $data['activity_streams'] = $activities['data'];
        
        foreach ($data['activity_streams'] as $key => $activity)
        {
            $data['activity_streams'][$key]['activity_text'] = $this->Activity_model->get_activity_text($activity);
        }
    
        $users = $this->User_model->get_all_users();
        $data['users'] = $users['data'];

        $data["header"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["footer"] = TRUE;
        $data["_view"] = "activity/stream";
        $data['title'] = set_title(lang('activity_listing'));
        $data["active"] = "activity";
        $this->load->view('admin/basetemplate',$data); 
    }

    function update_activity_seen_for_user()
    {
        $this->authenticate();
        
        $this->load->model('User_model', 'um');
        
        $params = array(
            'last_seen_activity_on' => date("Y-m-d G:i:s")
        );
        
        $this->um->update_users($this->user['id'], $params);
    }

    
    function ajax_get_activities()
    {
        if( $this->input->is_ajax_request() )
        {
            $limit = 30;

            $response = $this->Activity_model->get_activities(array(), $limit);

            $result['activity_notifications'] = $response['data'];
                
            foreach ($result['activity_notifications'] as $key => $activity)
            {
                $result['activity_notifications'][$key]['activity_text'] = $this->Activity_model->get_activity_text($activity);
            }

            $html = $this->load->view('admin/right_sidebar', $result, true);
            echo json_encode($html);
        }
    }

    function ajax_get_activity_notifications()
    {
        if( $this->input->is_ajax_request() )
        {
            $limit = 30;

            $response = $this->Activity_model->get_activities(array(), $limit);

            $result['activity_notifications'] = $response['data'];
                
            foreach ($result['activity_notifications'] as $key => $activity)
            {
                $result['activity_notifications'][$key]['activity_text'] = $this->Activity_model->get_activity_text($activity);
            }

            $html = $this->load->view('activity/notifications', $result, true);
            echo json_encode($html);
        }
    }
}