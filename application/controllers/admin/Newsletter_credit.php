<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_credit extends AdminController{

    function __construct()
    {
        parent::__construct();
        $this->load->library('permissions');
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $this->load->model(array('Newsletter_credit_model', 'User_model', 'Email_model'));
    }

    function manage()
    {
        $this->authenticate();
        $user_id = $this->user['id'];

        $credits = $this->Newsletter_credit_model->get_all_credits();
        $data['credits'] = $credits['data'];
        $data['header'] = TRUE;
        $data['_activeTab'] = ADMIN_TAB_8;
        $data["sidebar"] = TRUE;
        $data['_view']  = 'newsletters/credits/details';
        $data['footer'] = TRUE;
        $this->load->view('admin/basetemplate', $data);
    }
    
    function purchase()
    {
        $this->authenticate();
        $user_id = $this->user['id'];

        $lang_code = $this->session->userdata('lang');
        
        if( isset($_POST) )
        {
            if( isset($_POST['number_of_credits']) && $_POST['number_of_credits'] != '' && $_POST['number_of_credits'] != 0)
            {
                $purchase_result = $this->Newsletter_credit_model->purchase($_POST, $user_id);

                if( $purchase_result )
                {
                    $this->load->model(array('User_model','Email_model'));

                    $where = array(
                        'form_name' => 'credit-purchase-email-to-admin-'.strtoupper($lang_code),
                        'form_type' => FORM_TYPE_PREDEFINED_EMAIL_TEMPLATE
                    );
                    $emails = $this->Email_model->get_all_emails($where);

                    if($emails['rc'])
                    {
                        foreach ($emails['data'] as $e_key => $email)
                        {
                            $user = $this->User_model->get_users($user_id);

                            $email_data['email'] = $email;
                            //build array for shortcode values
                            $email_data['form_data'] = array(
                                'first_name'=>$user['first_name'],
                                'last_name'=>$user['last_name'],
                                'number_of_credits'=>$_POST['number_of_credits'],
                                'submited_on'=>date('Y-m-d G:i:s')
                            );

                            $email_data['form_fields'] = array();
                            $from_email = SEND_FROM_EMAIL_ID;
                            $to_email = MASTER_ADMIN_EMAIL;
                            $to_name = '';
                            $subject = $email['subject'];

                            $from_name = SEND_FROM_EMAIL_NAME;
                            $tag = $email['title'];
                            $body = $this->load->view('email_module/email', $email_data, TRUE);

                            $email_data = array(
                                "tag"             =>  $tag,
                                "subject"         =>  $subject,
                                "body"            =>  $body,
                                "status"          =>  'email_added',
                                "from_email"      =>  $from_email,
                                "to_email"        =>  $to_email,
                                "to_name"         =>  $to_name,
                                "from_name"       =>  $from_name,
                                "added_on"        =>  time(),
                                "vendor_type"     =>  EMAIL_VENDOR_MANDRILL
                            );

                            $email_stats_add_result_id = $this->Email_model->add_email_stats($email_data);
                        }
                    }

                    /*$this->load->model(array('User_model','Email_model'));
                    $user = $this->User_model->get_users($user_id);
                    $from_email = SEND_FROM_EMAIL_ID;
                    $to_email = "namratag@codebox.net.in"; 
                    $subject = WEBSITE_NAME .': Newsletter credits';

                    $from_name = "";
                    $tag = "purchase_newsletter_credits";
                    $body = $user['first_name'] . " " . $user['last_name'] . " has purchased " . $_POST['number_of_credits'] . " credits on " . date('d-m-Y G:i:s');
                    $to_name = '';

                    $email_data = array(
                        "tag"             =>  $tag,
                        "subject"         =>  $subject,
                        "body"            =>  $body,
                        "status"          =>  'email_added',
                        "from_email"      =>  $from_email,
                        "to_email"        =>  $to_email,
                        "to_name"         =>  $to_name,
                        "from_name"       =>  $from_name,
                        "added_on"        =>  time(),
                        "vendor_type"     =>  EMAIL_VENDOR_MANDRILL
                    );

                    $email_stats_add_result_id = $this->Email_model->add_email_stats($email_data);*/

                    $this->session->set_flashdata('success_msg', lang('credits_purchase_successfully'));
                }
                else
                {
                    $this->session->set_flashdata('error_msg', lang('credits_purchase_error'));
                }
            }
            else
            {
                $this->session->set_flashdata('error_msg', lang('please_select_credits'));
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg', lang('credits_purchase_error'));
        }

        redirect("newsletter/credits/manage");
    }
    
    function history()
    {
        $this->authenticate();
        $user_id = $this->user['id'];
        $data['header'] = TRUE;
        $data['_activeTab'] = ADMIN_TAB_8;
        $data['_view']  = 'newsletters/credits/history';
        $data['footer'] = TRUE;
        $data["sidebar"] = TRUE;
        $this->load->view('admin/basetemplate', $data);
    }

    function free_credits_per_month()
    {
        $this->Newsletter_credit_model->free_credits_per_month();
    }

    function ajax_get_credits_history($offset = 0)
    {
        if( $this->input->is_ajax_request() && $this->is_logged_in() )
        {
            $user_id = $this->user['id'];
            $user = $this->User_model->get_users( $user_id );

            if( ! isset($user['id']) )
            {
                $this->session->set_flashdata('error_msg', lang('invalid_data') );
                $response['rc'] = FALSE;
                echo json_encode($response); exit;
            }

            $params = array('count_flag' => TRUE);

            if( $this->input->post('purchase_credits') )
            {
                $credits = $this->Newsletter_credit_model->get_credits_purchased_history_by_user_id($user_id, $params);
            }
            else
            {
                $credits = $this->Newsletter_credit_model->get_credits_consumed_history_by_user_id($user_id, $params);
            }
            
            $config = array(
                'base_url' => site_url('admin/newsletter_credit/ajax_get_credits_history'),
                'total_rows' => isset($credits['data'][0]['count']) ? $credits['data'][0]['count'] : 0,
                'per_page' => PER_PAGE_LIMIT,
                'uri_segment' => 4,
                'div' => 'car-block',
                'function_type' => $this->input->post('purchase_credits') ? 'purchased_credits' : 'consumed_credits'
            );
            $this->load->model('Pagination_model');
            $data['links'] = $this->Pagination_model->ajax_config($config);
            $data['offset'] = $offset;
            $limit = array('limit' => PER_PAGE_LIMIT, 'limit_start' => $offset);

            if( $this->input->post('purchase_credits') )
            {
                $credits = $this->Newsletter_credit_model->get_credits_purchased_history_by_user_id($user_id, $limit);
                $data['purchased_credits'] = $credits['data'];
                $_view = 'newsletters/credits/_table_purchase_history';
            }
            else
            {
                $credits = $this->Newsletter_credit_model->get_credits_consumed_history_by_user_id($user_id, $limit);
                $data['consumed_credits'] = $credits['data'];
                $_view = 'newsletters/credits/_table_consumed_history';
            }

            $jsondata['rc'] = TRUE;
            $jsondata['html'] = $this->load->view($_view, $data, TRUE);

            echo json_encode($jsondata);
        }
    }    
}