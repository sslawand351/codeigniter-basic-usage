<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends AdminController {

	function __construct()
    {
        parent::__construct();
    }

    function listing()
    {
    	$this->authenticate();
        $data['header'] = TRUE;
        $data['sidebar'] = TRUE;
        $data['_view']  = 'language/listing';
        $data['footer'] = TRUE;
        $data['_activeTab'] = ADMIN_TAB_5;
        $this->load->view('admin/basetemplate', $data);
    }

    function ajax_load_translation_forms()
    {
    	if( $this->input->is_ajax_request() && $this->is_logged_in() )
    	{
    		$file_name = $this->input->post('file_name');
    		$this->load->helper('file');
    		$file_path = FCPATH . 'application/language/translations/';
    		
    		$data['file_name'] = $file_name;
    		$en_json = read_file( $file_path . 'en/'.$file_name);
    		$en_array = json_decode($en_json, true);
    		$data['en'] = $en_array;
    		/*pr($en_array);
    		echo $en_json;exit;*/
    		$response['en_html'] = $this->load->view('language/_en_language_translations_form', $data, TRUE);

    		$nl_json = read_file( $file_path . 'nl/'.$file_name);
    		$nl_array = json_decode($nl_json, true);
    		$data['nl'] = $nl_array;
    		$response['nl_html'] = $this->load->view('language/_nl_language_translations_form', $data, TRUE);

    		$fr_json = read_file( $file_path . 'fr/'.$file_name);
    		$fr_array = json_decode($fr_json, true);
    		$data['fr'] = $fr_array;
    		$response['fr_html'] = $this->load->view('language/_fr_language_translations_form', $data, TRUE);

    		echo json_encode($response);
    	}
    }

    function update_json($lang)
    {
    	$this->authenticate();

        $hash = '';
        if($this->input->post())
        {
            $file_name = $this->input->post('file_name');
            $file_path = FCPATH . 'application/language/translations/' . $lang . '/' . $file_name;
            $hash = str_replace('.json', '', $file_name);
            $hash = '#'.$hash;

            $array = $_POST;
            unset($array['file_name']);
            $jsondata = json_encode($array, JSON_PRETTY_PRINT);

            $this->load->helper('file');
            write_file($file_path, $jsondata);
            $this->update_lang($lang);

            $this->session->set_flashdata('success', lang('language_updated_successfully'));
        }
        else
        {
            $this->session->set_flashdata('error', 'Invalid Data');
        }
    	
    	redirect('admin/language/listing' . $hash); exit;
    }

    function update_lang($lang)
    {
        $this->authenticate();
    	$this->load->helper('file');
		$json_file_path = FCPATH . 'application/language/translations/';

		$translations_dir = FCPATH . 'application/language/translations/' . $lang . '/';
		$lang_array = array();

		$lang_text = "<?php defined('BASEPATH') OR exit('No direct script access allowed');\r\n";

		if (is_dir($translations_dir))
        {
			if ($dh = opendir($translations_dir))
            {
				while (($file = readdir($dh)) !== false)
                {
					$ext = pathinfo($file, PATHINFO_EXTENSION);
					if($ext === 'json')
					{
						$json = read_file( $translations_dir . $file );
						$json_array = json_decode($json, true);

						$name = ucfirst(str_replace('_', ' ', str_replace('.json', '', $file)));
                        $lang_text .= "\r\n// " . $name ."\r\n";
						if( isset($json_array['translations']) )
						{
							foreach( $json_array['translations'] as $index => $t )
							{
                                $t = str_replace('\\', '\\\\', $t);
                                $t = str_replace('"', '\"', $t);
								$lang_text .= "\$lang['" . $index ."'] = \"" . $t . "\";\r\n"; 
								/*$lang_array['lang'][$index] = $t;*/
							}
						}
					}
				}

				closedir($dh);
			}
		}

		$lang_file_path = FCPATH . 'application/language/' . $lang . '/text_lang.php';
		write_file($lang_file_path, $lang_text);
    }
}