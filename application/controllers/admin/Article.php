<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends AdminController
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Article_model','Activity_model'));
        if( ! ini_get('date.timezone') )
        {
            date_default_timezone_set('GMT');
        }
    }

    function index()
    {
        $this->load->library('pagination');

        $page_no = isset($_GET['page_no']) ? $_GET['page_no'] : 0;

        $limit_start = 0;
        $limit_end = ARTICLES_PER_PAGE_LIMIT;
        if($page_no > 0){

            $page = (int)$page_no - 1;
            $next = ((int)$page*ARTICLES_PER_PAGE_LIMIT);
            $limit_start = $next;
            $limit_end = ARTICLES_PER_PAGE_LIMIT;
        }

        $response['all_articles'] = $this->Article_model->get_total_articles();
        $response['articles'] = $this->Article_model->get_all_articles('articles',$limit_start, $limit_end);

        $data['articles'] = !empty($response['articles']['data']) ? $response['articles']['data'] : "";

        $data['recent_articles'] = $this->Article_model->get_recent_articles();

        $config = array(
            'base_url' => site_url('article/index?'),
            'total_rows' => isset($response['all_articles']['data']) && !empty($response['all_articles']['data']) ? count($response['all_articles']['data']) : 0,
            'per_page' => ARTICLES_PER_PAGE_LIMIT,
            'uri_segment' => 2,
            'enable_query_strings' => TRUE,
            'page_query_string' => TRUE,
            'use_page_numbers' => TRUE,
            'query_string_segment' => 'page_no'
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $data['active_page'] = 'default';
        $data['active_menu'] = 'blogs';
        $data['active_title'] = 'Blogs | Auto Natie';
        $data['meta_description'] = '';

        $data['header'] = TRUE;
        $data['footer'] = TRUE;
        $data['sidebar'] = TRUE;
        $data['_view'] = 'article/listing';
        $this->load->view( 'admin/basetemplate',$data);
    }

    function add()
    {
        $this->authenticate();

        $languages = $this->Article_model->get_languages();

        if($languages['rc'])
        {
            $validation_flag = FALSE;
            $flag = FALSE;

            foreach( $languages['data'] as $value)
            {
                $id = $value['id'];

                $validation_flag = ($validation_flag || (empty($this->input->post('data['.$id.'][title]')) && empty($this->input->post('data['.$id.'][body]')) && empty($this->input->post('data['.$id.'][page_title]')) && empty($this->input->post('data['.$id.'][meta_description]')) && empty($this->input->post('data['.$id.'][short_description]')) && empty($this->input->post('data['.$id.'][author]'))));

                if(!empty($this->input->post('data['.$id.'][title]')) || !empty($this->input->post('data['.$id.'][body]')) || !empty($this->input->post('data['.$id.'][page_title]')) || !empty($this->input->post('data['.$id.'][meta_description]')) || !empty($this->input->post('data['.$id.'][short_description]')) || !empty($this->input->post('data['.$id.'][author]')))
                {
                     $flag = TRUE;
                    $this->form_validation->set_rules('data['.$id.'][title]','Title','trim|required');
                    $this->form_validation->set_rules('data['.$id.'][url]','Url','trim|is_unique[articles_by_language.url]');
                    $this->form_validation->set_rules('data['.$id.'][body]','Body','trim|required');
                    $this->form_validation->set_rules('data['.$id.'][page_title]','Page title','trim|required');
                    $this->form_validation->set_rules('data['.$id.'][short_description]','Short description','trim|required');
                    $this->form_validation->set_rules('data['.$id.'][meta_description]','Meta description','trim|required');;
                }
            }

             $data['validation_error'] = '';
            //var_dump(isset($_POST));exit;
            if( !empty($_POST) && $validation_flag && ! $flag )
            {
                $data['validation_error'] = lang('you_should_add_blog_in_atleast_one_language');

            }
            else
            {
                $validation_flag = FALSE;
            }

            $this->form_validation->set_rules('image', 'Image', 'callback_required_image');
            $this->form_validation->set_rules('thumb', 'Thumb', 'callback_required_image');

            if( $this->form_validation->run() == FALSE || (isset($_POST) && $validation_flag))
            {
                $data['languages'] = $languages;
                
                $data['header'] = TRUE;
                $data['footer'] = TRUE;
                $data['active']['tab'] = FALSE;
                $data['active']['arrow'] = FALSE;
                $data['active_menu'] = 'article';
                $data['sidebar'] = TRUE;
                $data["_activeTab"] = ADMIN_TAB_2;
                $data['_view'] = 'article/add_article';
                $this->load->view( 'admin/basetemplate',$data);
                
            }
            else
            {
                $url = $this->Article_model->check_url_occurrence($this->input->post('title'));

                 $articledata = array(
                    'title' =>  $this->input->post('data[1][title]'),
                    // 'url'=> $this->input->post('data[1][url]'),
                    'added_by' => $this->user['id'],
                    'added_on' => date('Y-m-d g:i:s')
                );

                $result = $this->Article_model->add( $articledata, $_FILES );

                $article_id = $result['id'];

                if($result['rc'])
                {
                 /*adding language id wise blogs to databse*/
                    $languages_data = array();
                    foreach( $languages['data'] as $value ) 
                    {
                        $language_id       = $value['id'];

                        $url = $this->Article_model->check_url_occurrence($this->input->post('data['.$language_id.'][title]'));

                            $languages_data[] = array(
                                                    'article_id' => $article_id,
                                                    'language_id' => $language_id,
                                                    'title' => $this->input->post('data['.$language_id.'][title]'),
                                                    'url' => !empty($this->input->post('data['.$language_id.'][url]'))?$this->input->post('data['.$language_id.'][url]') : $url['url'],
                                                    'url_occurrence'=>$url['url_occurrence'],
                                                    'body' => $this->input->post('data['.$language_id.'][body]'),
                                                    'page_title'=>$this->input->post('data['.$language_id.'][page_title]'),
                                                    'short_description'=>$this->input->post('data['.$language_id.'][short_description]'),
                                                    'meta_description'=>$this->input->post('data['.$language_id.'][meta_description]')
                                                );

                    }

                    $blogs_by_language = $this->Article_model->add_articles_by_language($languages_data);
                    $data['articles'] = $blogs_by_language;

                    if($blogs_by_language['rc'])
                    {
                        $this->session->set_flashdata('success', $blogs_by_language['msg']);    
                    }    
                    else
                    {
                        $this->session->set_flashdata('error', $blogs_by_language['msg']);
                    }

                    redirect('article/listing');
                }
                else
                {
                    $this->session->set_flashdata('error', $result['msg']);  
                    redirect('article/listing');
                }
                    
            }
        }
        else
        {
            redirect('article/index');

        }
    }

    /*
        function to list articles
    */

    function listing()
    {
        $this->authenticate();

        $data['articles'] =  $this->Article_model->get_all_blogs();
     
        $data['header'] = TRUE;
        $data['footer'] = TRUE;
        $data['active']['tab'] = FALSE;
        $data['active']['arrow'] = FALSE;
        $data['active_menu'] = 'article';
        $data['sidebar'] = TRUE;
        $data["_activeTab"] = ADMIN_TAB_2;
        $data['_view'] = 'article/listing';
        $this->load->view( 'admin/basetemplate',$data);

    }

    function article_by_url($url = "")
    {
        $this->authenticate();

        if(!checkUrlParam('articles_by_language', 'url', $url))
        {
            $this->session->set_flashdata("error","Invalid url");
            redirect('article/listing');
            exit;
        }
        else
        {
            $response['article'] = $this->Article_model->get_article_by_url($url);
            $id = isset($response['article']['data']['id']) ? $response['article']['data']['id'] : 0;
            $data['article'] = $response['article']['data'];
            
            $data['active_page'] = 'default';
            $data['active_menu'] = 'blogs';
            $data['active_title'] = 'Blogs';
            $data['meta_description'] = '';
            $data['sidebar'] = TRUE;
            $data['header'] = TRUE;
            $data['footer'] = TRUE;
            $data['_view'] = 'article/article_detail';
            
            $this->load->view('admin/basetemplate', $data);
        }
        
    }

    function delete_image()
    {
        $this->authenticate();

        $blog_id = $this->input->post('blog_id');
        $result = $this->Article_model->delete_image( $blog_id );

        $msg_type = $result['rc'] ? 'success' : 'error';

        $this->session->set_flashdata( $msg_type, $result['msg'] );
        redirect('article/edit/' . $blog_id); exit;
    }

    function delete_thumbnail()
    {
        $this->authenticate();

        $blog_id = $this->input->post('blog_id');

        $result = $this->Article_model->delete_thumbnail( $blog_id );

        $msg_type = $result['rc'] ? 'success' : 'success';

        $this->session->set_flashdata( $msg_type, $result['msg'] );
        redirect('article/edit/' . $blog_id); exit;
    }

    /*
        function to edit articles
        @param  $id - article id

    */

    function unique_url($id,$url)
    {
       $data = $this->Article_model->check_unique_url($url,$id);
      
        if(!empty($data['url']))
        {
            $articledata = $this->Article_model->get_article_by_url($url);
            
            if($articledata['rc'])
            {
                $this->form_validation->set_message('unique_url','The Url '.$this->input->post('url').' is already present');           
            }
            
            // $this->form_validation->set_message('unique_email', lang('the_email').$email.lang('is_already_registered_with_us'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function edit($id)
    {
        $this->authenticate();
        
        $article = $this->Article_model->get_blog($id);

        if($article['rc'])
        {
            $languages = $this->Article_model->get_languages();
            if($languages['rc'])
            {
                foreach( $languages['data'] as $value)
                {
                    $language_id = $value['id'];

                    if(!empty($this->input->post('data['.$language_id.'][title]')))
                    {   
                        $this->form_validation->set_rules('data['.$language_id.'][title]','Title','trim|required');
                        $this->form_validation->set_rules('data['.$language_id.'][url]','Url','trim|callback_unique_url['.$language_id.']');
                        $this->form_validation->set_rules('data['.$language_id.'][body]','Body','trim|required');
                        $this->form_validation->set_rules('data['.$language_id.'][page_title]','Page title','trim|required');
                        $this->form_validation->set_rules('data['.$language_id.'][short_description]','Short description','trim|required');
                        $this->form_validation->set_rules('data['.$language_id.'][meta_description]','Meta description','trim|required');
                    }
                    
                }       
                // pr($_POST); exit;
                if( isset($_FILES['image']) && !empty($_FILES['image']) )
                    $this->form_validation->set_rules('image', 'Image', 'callback_required_image');
                if( isset($_FILES['thumb']) && !empty($_FILES['thumb']) )
                    $this->form_validation->set_rules('thumb', 'Thumb', 'callback_required_image');

                if( $this->form_validation->run() )
                {
                        $params = array(
                            'title' =>  $this->input->post('data['.$language_id.'][title]'),
                            'updated_on' => date('Y-m-d G:i:s')
                        );

                        /*editing language_id wise blogs to databse*/

                        $languages_data =array();

                        foreach( $languages['data'] as $value ) 
                        {
                            $language_id =  $value['id'];

                            if(!empty($this->input->post('data['.$language_id.'][title]')))
                            {
                                $languages_data[] = array(
                                                        'article_id' => $id,
                                                        'language_id' => $language_id,
                                                        'title' => $this->input->post('data['.$language_id.'][title]'),
                                                        'url' => $this->input->post('data['.$language_id.'][url]'),
                                                        'body' => $this->input->post('data['.$language_id.'][body]'),
                                                        'page_title'=>$this->input->post('data['.$language_id.'][page_title]'),
                                                        'short_description'=>$this->input->post('data['.$language_id.'][short_description]'),
                                                        'meta_description'=>$this->input->post('data['.$language_id.'][meta_description]')
                                                    );
                            }
                        }

                        $response = $this->Article_model->update($id, $params, $languages_data, $_FILES);

                        if($response['rc'])
                        {
                            $this->session->set_flashdata('success', $response['msg']);
                            redirect('article/listing'); 
                        }      
                        else
                        {
                            $this->session->set_flashdata('error', $response['msg']);  
                        }

                        redirect('article/edit/'.$id);
                }
                else
                {
                    $article = $this->Article_model->get_blog($id);
                    if($article['rc'])
                        $data['article'] = $article['data'];
                    else
                    $data['article'] = array();
                    //pr($blogs); exit;
                    $data['blog_id'] = $id;
                    $data["header"] = TRUE;
                    $data["sidebar"] = TRUE;
                    $data["footer"] = TRUE;
                    $data["_view"] = "article/edit_article";
                    $data['title'] = "Edit blog";
                    $data["active"] = "blog";
                    $data["_activeTab"] = ADMIN_TAB_2;
                    $this->load->view("admin/basetemplate", $data);
                }
            }
            else
            {
                 $this->session->set_flashdata('error', $response['msg']);      
                 redirect('article/listing');
            }
            
        }
        else
        {
            $this->session->set_flashdata("error",lang('invalid_id'));
            redirect('article/listing');
        }
    }

        
    /*
        function to delete articles
        @param  $id - article id
    */

    function delete()
    {
       $this->authenticate();

        //check permission is assigned to user
        //$this->authorize('group_listing');

       $id = $this->uri->segment(3);
        // $id = $this->input->post('blog_id');
        // echo $id; exit;
        // Get blog
        $blog = $this->Article_model->get_blog($id, 1);

        if( $blog['rc'])
        {
            $delete_result = $this->Article_model->delete($id, $blog);

            if($delete_result['rc'])
            {
                $this->session->set_flashdata('success', $delete_result['msg']);
            }
            else
            {
                $this->session->set_flashdata('error', $delete_result['msg']);
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('invalid_data'));
        }
        
        redirect('article/listing'); exit;
    }

    function change_status( $slug="", $id = 0)
    {
        $this->authenticate();
        $article = $this->Article_model->get_blog($id, PRIMARY_LANGUAGE_ID);

        if( $article['rc'])
        {
            if( $slug == "publish")
                $status = '1';
            else if( $slug == "unpublish")
                $status = '0';

            $params = array( "is_publish" => $status);
            $result = $this->Article_model->change_status($id, $params);

            if($result['rc'])
            {
                $other_data = array('blog_name' => $article['data']['title']);
                $activity_data = array(
                    'activity_by_user_id' => $this->user['id'],
                    'admin_id' => $this->user['id'],
                    'user_id' => $this->user['id'],
                    'activity_type' => $params['is_publish'] == 1 ? ACTIVITY_ADMIN_PUBLISHED_BLOG : ACTIVITY_ADMIN_UNPUBLISHED_BLOG,
                    'activity_on' => date('Y-m-d G:i:s'),
                    'other_activity_data' => serialize($other_data)
                );
                $this->Activity_model->add_activity($activity_data);
                
                $this->session->set_flashdata('success', $result['msg']);
            }
            else
            {
                $this->session->set_flashdata('error', $result['msg']);
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('invalid_data'));
        }
        
        redirect('article/listing'); exit;
    }

    /*
        callback function
    */

    function required_image()
    {

        $type = $_FILES['image']['type'];
        if(isset($type) && $type != "")
        {
            if($type != "image/jpg" && $type != "image/png" && $type != "image/jpeg" && $type!= "image/gif" ) 
            {
                $this->form_validation->set_message('required_image','Invalid file type');
                return false;
            }

            else
            {
                return true;
            }
        }

    }

    function is_valid_image($image_name,$params)
    {
        $parts = explode(",", $params);
        $image_slug = $parts[0];
        $multiple = $parts[1];

        switch($multiple)
        {
            case 0:
                if (isset($_FILES[$image_slug]) && !empty($_FILES[$image_slug]['name']))
                {
                    $file_info = @getimagesize($_FILES[$image_slug]['tmp_name']);
                    
                    if(!empty($file_info))
                    {
                        return TRUE;
                    }
                    else
                    {
                        // possibly do some clean up ... then throw an error
                        $this->form_validation->set_message('is_valid_image', lang('the_uploaded_file_doesnt_seem_to_be_an_image'));
                        return FALSE;
                    }
                }
                else
                {
                    // throw an error because nothing was uploaded
                    $this->form_validation->set_message('is_valid_image', "You must upload an image!");
                    return FALSE;
                }

                break;

            case 1: 
                if (isset($_FILES[$image_slug]['name'][0]) && !empty($_FILES[$image_slug]['name'][0]))
                {
                    foreach($_FILES[$image_slug]['tmp_name'] as $tmp_name)
                    {
                        $file_info = @getimagesize($tmp_name);
                        
                        if(!empty($file_info))
                        {
                            return TRUE;
                        }
                        else
                        {
                            // possibly do some clean up ... then throw an error
                            $this->form_validation->set_message('is_valid_image', lang('the_uploaded_file_doesnt_seem_to_be_an_image'));
                            return FALSE;
                        }
                    }
                }
                else
                {
                    // throw an error because nothing was uploaded
                    //$this->form_validation->set_message('image_upload', "You must upload an image!");
                    return TRUE;
                }
                break;
        }
    }


}
