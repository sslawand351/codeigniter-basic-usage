<?php
/* 
 * Generated by CRUDigniter v1.0 Beta
 * www.crudigniter.com
 */
 
class Usergroup extends AdminController
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Usergroup_model','Activity_model'));
        $this->load->library('pagination');
    }  

    function edit_permissions($usergroup_id = 0)
    {
        $this->authenticate('GROUPS_SET_PERMISSIONS');
        
        if(isset($_POST['selected_permissions']))
        {
            $result = $this->Usergroup_model->save_permissions($usergroup_id,$_POST['selected_permissions']);

            if(!empty($result))
            {
                $usergroup = $this->Usergroup_model->get_usergroup($usergroup_id);

                 $other_data = array('user_group'=> $usergroup['role_name']);

                    $activity_data = array(

                    'activity_by_user_id' => $this->user['id'],
                    'admin_id'=>$this->user['id'],
                    'user_id'=>$this->user['id'],
                    'activity_type' => ACTIVITY_ADMIN_UPDATED_ROLE,
                    'other_activity_data' => serialize($other_data),
                    'activity_on' => date('Y-m-d G:i:s')
                );
                $this->Activity_model->add_activity($activity_data);
            }
            $this->session->set_flashdata('success',lang('role_assigned_successfully'));
            redirect('usergroup/listing');
        }
        else
        {
            if($this->input->post('test') == '1')
            {
                $this->Usergroup_model->delete_all_permissions_by_usergroup_id($usergroup_id);
                $this->session->set_flashdata('error',lang('please_add_atleast_one_permission'));
                redirect('usergroup/listing');
            }
            
            $data['usergroup_id'] = $usergroup_id;
            $data['permission_matrix'] = $this->Usergroup_model->usergroup_permission_matrix($usergroup_id);

            // get usergroup users
            $data['usergroup_users'] = $this->Usergroup_model->get_usergroup_users($usergroup_id);

            $data['title'] = "Edit Permissions";
            $data['header'] = TRUE;
            $data['footer'] = TRUE;
            $data['sidebar'] = TRUE;
            $data['active_tab'] = "users";
            $data["_activeTab"] = ADMIN_TAB_1;
            $data['_view'] = 'user_group/edit_permissions';
            $this->load->view('admin/basetemplate', $data);
        }
    }

    /*
     * Listing of user_groups
     */
    function listing()
    {
        $this->authenticate();

        $data['usergroups'] = $this->Usergroup_model->get_all_userroles();
        $data['current_user_usergroup_id'] = $this->user['usergroup_id'];
        $data['title'] = "User Groups";
        $data['active_tab'] = "users";
        $data['header'] = TRUE;
        $data['footer'] = TRUE;
        $data['sidebar'] = TRUE;
        $data["_activeTab"] = ADMIN_TAB_1;
        $data['_view'] = 'user_group/list';
        $this->load->view('admin/basetemplate', $data);
    }

    
    /*
     * Adding new user_groups
     */
    function add()
    {   
        $this->authenticate('GROUP_ADD');

        $this->load->library('form_validation');  

        $this->form_validation->set_rules('role_name','Role Name','required|trim');

        if($this->form_validation->run())
        {   

            if($this->input->post('role_name') == "Super Admin" || $this->input->post('role_name') == "super admin")
            {
                $this->session->set_flashdata('error', "You can not add this group.Plz try with some other name.");
                redirect('usergroup/add');
            }

            $params = array(
                'role_name'  => $this->input->post('role_name'),
                'role_code'  => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $this->input->post('role_name'))),
                'added_on'=> date('Y-m-d g:i:s'),
            );
            
            $response = $this->Usergroup_model->add($params);

            $this->session->set_flashdata('success', "Role added successfully");

            $other_data = array('role_name' => strip_tags(trim($this->input->post('role_name'))));
             $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_ADDED_ROLE,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

                $this->Activity_model->add_activity($activity_data);
           
            if($this->input->is_ajax_request())
            {
                

                $jsn_response["id"] = $response;
                echo json_encode($jsn_response);
            }
            else
            {
                redirect('usergroup/listing'); 
            }
        }
        else
        {  
            if($this->input->is_ajax_request())
            {
                $response["rc"] = FALSE;
                $response["errors"] = validation_errors();
                echo json_encode($response);
            } 
            else
            {
                $data['header'] = TRUE;
                $data['footer'] = TRUE;
                $data['sidebar'] = TRUE;
                $data["_activeTab"] = ADMIN_TAB_1;
                $data['title'] = "Add";
                $data['active_tab'] = 'users';
                $data['sub_tab'] = 'user_group_list';
                $data['_view'] = 'user_group/add';
                $this->load->view('admin/basetemplate', $data);
            }
        }
    }

        /*
     * Adding new user_groups
     */
    function remove($usergroup_id = 0)
    {   
        $this->authenticate('GROUP_DELETE'); 

        $associated_users = $this->Usergroup_model->get_usergroup_users($usergroup_id);

        if(empty($associated_users))
        {
            $user_roles = $this->Usergroup_model->get_usergroup($usergroup_id);

            $response = $this->Usergroup_model->remove_role($usergroup_id);
            $this->session->set_flashdata('success', "Role deleted successfully");

            $other_data = array('role_name' => $user_roles['role_name']);

            $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_DELETED_ROLE,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );
            $this->Activity_model->add_activity($activity_data);
            redirect('usergroup/listing'); 
        }
        else
        {
            $this->session->set_flashdata('error', "Can't delete this user role as users are associated with it.");
            redirect('usergroup/listing');
        }
    }

    /*
     * Adding new user_groups
     */
    function edit($usergroup_id = 0)
    {   
        $this->authenticate('GROUP_EDIT');

        $this->load->library('form_validation');  

        $this->form_validation->set_rules('role_name','Role Name','required|trim');

        if($this->form_validation->run())
        {   
            $params = array(
                'role_name'  => $this->input->post('role_name'),
                'role_code'  => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $this->input->post('role_name'))),
                'added_on'=> date('Y-m-d g:i:s'),
            );
            
            $response = $this->Usergroup_model->edit($usergroup_id,$params);
            
            $this->session->set_flashdata('success', "Role updated successfully");

            $other_data = array('role_name' => strip_tags(trim($this->input->post('role_name'))));

             $activity_data = array(
                'activity_by_user_id' => $this->user['id'],
                'admin_id' => $this->user['id'],
                'user_id' => $this->user['id'],
                'activity_type' => ACTIVITY_ADMIN_UPDATED_ROLE,
                'other_activity_data' => serialize($other_data),
                'activity_on' => date('Y-m-d G:i:s')
            );

            $this->Activity_model->add_activity($activity_data);
           
            if($this->input->is_ajax_request())
            {
                $jsn_response["id"] = $response;
                echo json_encode($jsn_response);
            }
            else
            {
                redirect('usergroup/listing'); 
            }
        }
        else
        {  
            if($this->input->is_ajax_request())
            {
                $response["rc"] = FALSE;
                $response["errors"] = validation_errors();
                echo json_encode($response);
            } 
            else
            {
                $usergroup = $this->Usergroup_model->get_usergroup($usergroup_id);

                $data['usergroup_name'] = $usergroup['role_name'];
                $data['header'] = TRUE;
                $data['footer'] = TRUE;
                $data['sidebar'] = TRUE;
                $data["_activeTab"] = ADMIN_TAB_1;
                $data['title'] = "Add";
                $data['active_tab'] = 'users';
                $data['sub_tab'] = 'user_group_list';
                $data['_view'] = 'user_group/edit';
                $this->load->view('admin/basetemplate', $data);
            }
        }
    }
}