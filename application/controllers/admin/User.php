<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends AdminController {

function __construct() 
{
    parent::__construct();

    $this->load->helper('cookie');
    $this->load->helper('url','security','language_helper','language','global_helper');
    $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->model(array('User_model','Email_model','Admin_model','Activity_model'));
}

function password_check($str)
{
    if (preg_match('/^\S*$/', $str)) {
        return TRUE;
    }
    else
    {
        $this->form_validation->set_message('password_check', lang('password_does_not_allow_white_space'));
        return FALSE;
    }
}

function unique_email($email,$user_id)
{
   $data['email']= $this->User_model->check_unique_email($email,$user_id);
    if(!empty($data['email']))
    {
        $userdata = $this->User_model->get_user_by_email($email);
        
        if($userdata['rc'])
        {
            $this->form_validation->set_message('unique_email','The Email '.$this->input->post('email').' is already registered as a '.$userdata['data']['user_type'].'.');           
        }
        else
        {
            $this->form_validation->set_message('unique_email','The Email '.$this->input->post('email').' is already registered.');           
        }
        
        // $this->form_validation->set_message('unique_email', lang('the_email').$email.lang('is_already_registered_with_us'));
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

public function index()
{
    //$this->load->view('welcome_message');
   
    if(!$this->is_logged_in())
    {
        redirect('admin/login');
    }
    else
    {
        redirect('admin/dashboard');
    }
}

function set_language($langAbbr)
{
    $data['name'] = 'lang';
    $data['value'] = $langAbbr;
    $cookie = array(
        'name'=>$data['name'],
        'value'  => $data['value'],
        'expire' => time()+86500,
        'domain' => '',
        'prefix' => ''
    );
    
    set_cookie($cookie);
    redirect($_SERVER['HTTP_REFERER']); exit;
}

function login()
{
    if(!$this->is_logged_in())
    {
        // $redirect_to = $this->input->get('redirect');

        $this->form_validation->set_rules('email','Email','required|valid_email|trim');
        $this->form_validation->set_rules('password','Password','required|trim|MD5');

        $this->form_validation->set_error_delimiters('<p class="text-red">','</p>');

        if ($this->form_validation->run() == FALSE)
        {
            //pass the partial views
            $data["header"] = FALSE;
            $data["footer"] = FALSE;
            $data["_view"] = "admin/login";
            $data['title'] = set_title(lang('log_in'));
            $this->load->view("admin/basetemplate", $data);
        }
        else
        {
            $email      = $this->input->post('email');
            $password   = $this->input->post('password');

            $response = $this->Admin_model->login($email,$password);

            if($response['rc'])
            {
                redirect('admin/dashboard');
            }
            else
            {
                $this->session->set_flashdata('error_msg', $response['msg']);
                redirect('admin/login');
            }
        }
    }
    else //if($this->user['is_admin'])
    {
        redirect('admin/dashboard');
    }
}


    function checkUrlParam($tableName, $columnName, $param, $is_numeric = FALSE)
    {
        $CI =& get_instance();
        /* check for numeric parameter*/
        if($is_numeric == TRUE && !is_numeric($param))
        {
            return FALSE;
        }
        
        $result = $CI->db->get_where($tableName, array($columnName => (string)$param))->row_array();
        if(isset($result['id']))
            return TRUE;
        else
            return FALSE;
    }

    function registeration_success($first_name)
    {
    
        if($this->checkUrlParam('users','first_name',$first_name))
        {
            $data['first_name'] = $first_name;
            $data["header"]   = TRUE;
            $data["_view"]   = "registration_success";
            $this->load->view("basetemplate",$data);
        }
        else
        {
            redirect("User/register");
        }
     
    }

    function logout()
    {
         $this->authenticate();

        setcookie("admin_rememberme", "", time()-10, "/");

        $other_data = array();

        $activity_data = array(

        'activity_by_user_id' => $this->user['id'],
        'admin_id'=>$this->user['id'],
        'activity_type' => ACTIVITY_ADMIN_HAS_LOGGED_OUT,
        'other_activity_data' => serialize($other_data),
        'activity_on' => date('Y-m-d G:i:s')
        );

        $this->Activity_model->add_activity($activity_data);
        $this->session->unset_userdata('admin_id');
        
        redirect('admin/login');
    }

    function forgot_password()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        
        if($this->form_validation->run() == FALSE)
       {    
            // echo "hii";exit;
            $data["header"]   = TRUE;
            $data["footer"]   = TRUE;
            $data["_view"]    = "forgot_password";
            $this->load->view("basetemplate",$data);

        }
        else
        {
            // echo "success";exit;
            $email = $this->input->post('email');
                    
            $this->load->helper('string');
            $resetKey = md5(random_string('sha1', 12) + time());
              
            $data = array(
                'password_reset_key' => $resetKey
            );
            $user_data = $this->User_model->get_user_by_email($email);
            $user = $user_data['data'];
            if($user_data['rc'])
            {   
                $reset = $this->User_model->set_password_reset_key($email, $data);
                if($reset)
                {   
                    $email_body['reset_link'] = site_url().'/User/get_new_password/'.$resetKey;
                    $email_body['user_email'] = $email;
                    $email_body['_view'] = 'reset_password';
                    $html = $this->load->view('basetemplate', $email_body, true);
                    
                    //new - mandrill
                    $to_email = $user['email'];
                    $to_name = $user['first_name']." ".$user['last_name'];
                    $from_email = SEND_FROM_EMAIL_ID;
                    $from_name = SEND_FROM_EMAIL_NAME;
                    $subject = 'reset_pwd_email_subject';
                    $tag = "reset_password_email";
                    $body = $html;
                    //echo $html;
                    
                    $email_data = array(
                    "tag"             =>  $tag,
                    "subject"         =>  $subject,
                    "body"            =>  $body,
                    "status"          =>  'email_added',
                    "from_email"      =>  $from_email,
                    "to_email"        =>  $to_email,
                    "to_name"         =>  $to_name,
                    "from_name"       =>  $from_name,
                    "vendor_type"     =>  EMAIL_VENDOR_MANDRILL,
                    "added_on"        =>  time()
                    );
                    $this->Email_model->add_email_stats($email_data);
                    
                    $this->session->set_flashdata('success','Your reset password request has been registered. Please check your email for further instructions on the same.');
                }
                else
                {
                    echo "FAiled to reset password";exit;
                }
                
                $data['email'] = $email;
                redirect('User/forgot_password');
            }
            else
            {
                $this->session->set_flashdata('error','email does not exists in our database');
                redirect('User/forgot_password');
            }  
        }
    }

    function get_new_password($hash='')
    {
        $this->form_validation->set_rules('password','Password','trim|required|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[password]');
        
        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);
        // pr($url[5]);exit;
        if($url[5] == '')
        {
            $this->session->set_flashdata('error','Invalid request');
            redirect('user');    
        }
        else
        {   
            $response = $this->User_model->check_password_reset_key($url[5]);
            // pr($response);
            // echo "hii";exit;
            if(!$response['rc'])
            {
                $this->session->set_flashdata('error',$response['msg']);
                redirect('user');
            }
        }
        $password = md5($this->input->post('password'));
     
        if($this->form_validation->run() == TRUE)
        {   
            $response = $this->User_model->update_password($password,$url[5]);
            // pr($response);exit;
            if($response['rc'])
            {
                $this->session->set_flashdata('success',$response['msg']);
                redirect('User/login');
            }
            else
            {
                $this->session->set_flashdata('error',$response['msg']);
                redirect(site_url());
            }
        }
        else
        {   
            $data["header"]   = TRUE;
            $data["footer"]   = TRUE;
            $data["_view"]          = "get_new_password";
            $this->load->view("basetemplate",$data);
        }
    }


    function dashboard()
    {
        $this->authenticate();

        $data["sidebar"] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["_view"] = "admin/dashboard";

        $data['title'] = set_title(lang('dashboard'));
        $data["active"] = "dashboard";
        $data["_activeTab"] = ADMIN_TAB_7;

        $this->load->view("admin/basetemplate", $data);        
    }

    function listing()
    {
        $this->authenticate();
        //$this->authorize('VIEW_USER');
        $type = $this->input->get('type');  

        $params = array(
            'type'=> $type
        );

        $users = $this->User_model->get_all_users($params);

        $config = array(
            'base_url' => site_url('user/listing?type='.$type),
            'total_rows' => $users['rc'] ? count($users['data']) : 0,
            'per_page' => USERS_PER_PAGE_LIMIT,
            'uri_segment' => 2,
            'page_query_string' => TRUE,
            'query_string_segment' => 'page'
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->input->get('page')) ? $this->input->get('page') : 0;
        $limit = array('limit' => USERS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);

        $users = $this->User_model->get_all_users($params, $limit);

        $data['users'] = $users['data'];
        $data['type'] = $type;

        $data["sidebar"] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["_view"] = "user/list";
        $data['title'] = set_title(lang('user_listing'));
        $data["active"] = "user";
        $data["_activeTab"] = ADMIN_TAB_1;

        $this->load->view("admin/basetemplate", $data);
    }
        
    function add()
    {   
       // check if the user is logged in or not
        $this->authenticate();

        // Check for permissions
        // $this->authorize('user_add');

        $this->load->library('form_validation');         
        $this->form_validation->set_rules('first_name','First Name','required');        
        $this->form_validation->set_rules('last_name','Last Name','required');        
        $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[users.email]');        
        $this->form_validation->set_rules('password','Password','required|matches[passconf]|callback_password_check');
        $this->form_validation->set_rules('passconf','Confirm Password','required|callback_password_check'); 
        $this->form_validation->set_rules('usergroup_id','User roles','required');

        if(!$this->form_validation->run()) 
        {
            
            $userdata = $this->User_model->get_user_by_email($this->input->post('email'));
            if($userdata['rc'])
            {
                $this->form_validation->set_message('is_unique','The Email '.$this->input->post('email').' is already registered as a '.$userdata['data']['user_type'].'.');           
            }
            else
            {
                $this->form_validation->set_message('is_unique','The Email '.$this->input->post('email').' is already registered.');
            }

        }

        if($this->form_validation->run())
        {  
            $usergroup = $this->Usergroup_model->get_usergroup($this->input->post('usergroup_id'));

            if(isset($usergroup['id']))
            {
                $params = array(
                    'first_name'    => $this->input->post('first_name'),
                    'last_name'     => $this->input->post('last_name'),
                    'email'         => $this->input->post('email'),
                    'password'      => md5($this->input->post('password')),
                    'user_type'     => $usergroup['role_name'],
                    'usergroup_id'  => $this->input->post('usergroup_id'),
                    'language_id'  => $this->input->post('language_id'),
                    'added_on'      => date("Y-m-d G:i:s"),
                    'modified_on'   => date("Y-m-d G:i:s"),
                    'added_by'      => $this->user['id'],
                );
                
                $result = $this->User_model->add_users($params);

                if($result['rc']) {
                    $other_data = array('user_name'=>strip_tags(trim($this->input->post('first_name'))).' '.strip_tags(trim($this->input->post('last_name'))));
                    
                    $activity_data = array(
                        'activity_by_user_id' => $this->user['id'],
                        'admin_id' => $this->user['id'],
                        'user_id' => $result['data']['user_id'],
                        'activity_type' => ACTIVITY_ADMIN_ADDED_USER,
                        'other_activity_data' => serialize($other_data),
                        'activity_on' => date('Y-m-d G:i:s')
                    );

                    $this->Activity_model->add_activity($activity_data);
                    $this->session->set_flashdata('success_msg', $result['msg']);
                } else {
                    $this->session->set_flashdata('error_msg', $result['msg']);
                }

                redirect('user/listing?type=admin_user');
            }
        }
        else
        {   
            $data['usergroup'] = $this->Usergroup_model->get_all_userroles();
            $data["header"] = TRUE;
            $data["footer"] = TRUE;
            $data["sidebar"] = TRUE;
            $data["_view"] = "user/add";
            $data['title'] = set_title(lang('add_user'));
            $data["active"] = "dashboard";
            $data["_activeTab"] = ADMIN_TAB_1;
            $this->load->view("admin/basetemplate", $data);
        }
    }

    function edit($id)
    {
        $this->authenticate();
        $user = $this->User_model->get_users($id);

        $type = $user['user_type'] == 'user' ? "frontend_user" : "admin_user";

        if(checkParam('users', 'id', $id, TRUE))
        {
            $this->load->library('form_validation');         
            $this->form_validation->set_rules('first_name','First Name','required');        
            $this->form_validation->set_rules('last_name','Last Name','required');        
            $this->form_validation->set_rules('email','Email','required|valid_email|callback_unique_email['.$id.']'); 

            if($this->input->post('password') != '')
            {
                $this->form_validation->set_rules('password','Password','required|matches[passconf]|callback_password_check');
                $this->form_validation->set_rules('passconf','Confirm Password','required|callback_password_check');    
            }  
                   

            if($this->form_validation->run() == TRUE)
            {   
                $params = array(
                    'first_name'    => $this->input->post('first_name'),
                    'last_name'     => $this->input->post('last_name'),
                    'language_id'     => $this->input->post('language_id'),
                    'email'         => $this->input->post('email'),
                    'modified_on'   => date("Y-m-d G:i:s")
                );
                if($user['usergroup_id'] != MASTER_ADMIN)
                    $params['usergroup_id'] = $this->input->post('usergroup_id');

                if($this->input->post('password') != '')
                {
                    $params['password'] = md5($this->input->post('password'));
                }

                $result = $this->User_model->update_users($id, $params); 

                if($result['rc'])
                {
                    // ADD ACTIVITY DATA
                    $other_data = array('user_name'=>strip_tags(trim($this->input->post('first_name'))).' '.strip_tags(trim($this->input->post('last_name'))), 'user_type' => 'admin');
                    $activity_data = array(
                        'activity_by_user_id' => $this->user['id'],
                        'admin_id' => $this->user['id'],
                        'user_id' => $id,
                        'activity_type' => ACTIVITY_ADMIN_UPDATED_USER,
                        'other_activity_data' => serialize($other_data),
                        'activity_on' => date('Y-m-d G:i:s')
                    );

                    $this->Activity_model->add_activity($activity_data);

                    $this->session->set_flashdata('success_msg', $result['msg']);
                    
                }
                else
                {
                    $this->session->set_flashdata('error_msg', $result['msg']);
                }

                redirect('user/listing?type='.$type); 
            }
            else
            {  
                $data['users'] = $this->User_model->get_users($id);
                $data['usergroup'] = $this->Usergroup_model->get_all_userroles();
                
                $data['users']['type'] = $type;
                $data["sidebar"] = TRUE;
                $data["header"] = TRUE;
                $data["footer"] = TRUE;
                $data["_view"] = "user/edit";
                $data['title'] = set_title(lang('edit_user'));
                $data["active"] = "dashboard";
                $data["_activeTab"] = ADMIN_TAB_1;
                $this->load->view("admin/basetemplate", $data);
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg', lang('invalid_data'));
            redirect('user/listing?type='.$type); 
        }
    }

    function remove()
    {
        // check if the user is logged in or not
        $this->authenticate();
        
        $id = $this->input->post('user_id');
        // Check for permissions
        //$this->authorize('user_delete');

        $user = $this->User_model->get_users($id);

        if($user['user_type'] == 'user')
        {
            $type = 'frontend_user';
        }
        else 
        {
             $type = 'admin_user';
        }
        // Check for valid id
        if(isset($user['id']))
        {
            $result = $this->User_model->delete_user($id);

            if($result['rc'])
            {
                // ADD ACTIVITY DATA
                $other_data = array('user_name'=>$user['first_name'].' '.$user['last_name'], 'user_type' => 'admin');
                $activity_data = array(
                    'activity_by_user_id' => $this->user['id'],
                    'admin_id' => $this->user['id'],
                    'user_id' => $id,
                    'activity_type' => ACTIVITY_ADMIN_DELETED_USER,
                    'other_activity_data' => serialize($other_data),
                    'activity_on' => date('Y-m-d G:i:s')
                );

                $this->Activity_model->add_activity($activity_data);

                $this->session->set_flashdata('success_msg', $result['msg']);
                redirect('user/listing?type='.$type); 
            }
            else
            {
                $this->session->set_flashdata('error_msg', $result['msg']);
                redirect('user/listing?type='.$type); 
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg', lang('invalid_data'));
            redirect('user/listing?type='.$type); 
        }
    }

    function change_password()
    {

        $this->authenticate();
        $this->form_validation->set_rules('existing_password', 'Existing Password', 'required|callback_password_exists_check|trim');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|MD5|trim');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|MD5|matches[new_password]|trim');
        if($this->form_validation->run())
        {
         
            $password = array(
                            'password' => $this->input->post('new_password')
                            );
            
            $id = $this->session->userdata('user_id');
            $result = $this->User_model->set_new_password($password,$id);

            if($result['rc'])
            {
                $this->session->set_flashdata('success_msg',$result['msg']);
                redirect('admin/dashboard');exit;
            }
            else
            {
                $this->session->set_flashdata('error_msg',$result['msg']);
                redirect('admin/dashboard');exit;
            }
        }

        else
        {
                $data["sidebar"] = TRUE;
                $data["header"] = TRUE;
                $data["footer"] = TRUE;
                $data["_view"] = "user/change_password";
                $data['title'] = set_title(lang('change_password'));
                $data["active"] = "dashboard";
                $data["_activeTab"] = ADMIN_TAB_2;
                $this->load->view("admin/basetemplate", $data);
        }
    }

    function password_exists_check($password)
    {
        $id = $this->session->userdata('user_id');
        $result = $this->User_model->get_existing_password_by_id($id);
        if($result['rc'])
        {
            $existing_password = $result['data']['password'];
            $password = md5($password);
            if(strcmp($password,$existing_password) == 0)
            {
                return true;
            }
            else
            {
                $this->form_validation->set_message('password_exists_check', lang('please_enter_valid_password'));
                return FALSE;
            }   
        }
    }

    function deleted_listing()
    {
        $this->authenticate();
        //$this->authorize('VIEW_USER');
        $this->load->library('pagination'); 

        $params = array(
            'is_deleted'=>'1'
            );

        $users = $this->User_model->get_all_users($params);

        $config = array(
            'base_url' => site_url('user/deleted_listing'),
            'total_rows' => $users['rc'] ? count($users['data']) : 0,
            'per_page' => USERS_PER_PAGE_LIMIT,
            'uri_segment' => 2,
            'page_query_string' => TRUE,
            'query_string_segment' => 'page'
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->input->get('page')) ? $this->input->get('page') : 0;
        $limit = USERS_PER_PAGE_LIMIT;

        $users = $this->User_model->get_all_users($params, $limit, $limit_start);

        $data['users'] = $users['data'];

        $data["sidebar"] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["_view"] = "user/deleted_users";
        $data['title'] = set_title('Deleted users');
        $data["active"] = "user";
        $data["_activeTab"] = ADMIN_TAB_1;

        $this->load->view("admin/basetemplate", $data);
    }

    function undo_delete($user_id = 0)
    {
        // check if the user is logged in or not
        $this->authenticate();
        // Check for permissions
       // $this->authorize('UNDO_DELETE');

        $user = $this->User_model->get_users($user_id);
        // Check for valid id
        if(isset($user['id']))
        {
            $result = $this->User_model->undo_delete_user($user['id']);

            if($result['rc'])
            {
                // ADD ACTIVITY DATA
                $other_data = array('user_name'=>$user['first_name'].' '.$user['last_name'], 'user_type' => $user['user_type']);
                $activity_data = array(
                    'activity_by_user_id' => $this->user['id'],
                    'admin_id' => $this->user['id'],
                    'user_id' => $user_id,
                    'activity_type' => ACTIVITY_ADMIN_UNDO_DELETE_USER,
                    'other_activity_data' => serialize($other_data),
                    'activity_on' => date('Y-m-d G:i:s')
                );

                $this->Activity_model->add_activity($activity_data);

                $this->session->set_flashdata('success_msg', $result['msg']);
                redirect('user/deleted_listing'); 
            }
            else
            {
                $this->session->set_flashdata('error_msg', $result['msg']);
                redirect('user/deleted_listing'); 
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg', lang('invalid_data'));
            redirect('user/deleted_listing'); 
        }
    }
}
