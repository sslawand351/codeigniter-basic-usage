<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Static_page extends AdminController
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Static_page_model','Activity_model'));
        if( ! ini_get('date.timezone') )
        {
            date_default_timezone_set('GMT');
        }
    }

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  add new page 
     * @param  
     * @return NULL
     */

    function add()
    {
        $this->authenticate();
        //$this->load->library('form_validation');
        $languages = $this->Static_page_model->get_languages();

        if($languages['rc'])
        {
            $validation_flag = FALSE;
            $flag = FALSE;

            foreach( $languages['data'] as $value)
            {
                $id = $value['id'];

                 $validation_flag = ($validation_flag || (empty($this->input->post('data['.$id.'][page_title]')) && empty($this->input->post('data['.$id.'][body]')) && empty($this->input->post('data['.$id.'][page_name]')) && empty($this->input->post('data['.$id.'][meta_description]'))));

                if(!empty($this->input->post('data['.$id.'][page_name]')))
                {
                   $flag = TRUE;
                    $this->form_validation->set_rules('data['.$id.'][url]','Url','trim|required|regex_match[/advertise|about-us|help$/]|is_unique[static_pages_by_language.url]');

                    $this->form_validation->set_rules('data['.$id.'][page_name]','Page Name','trim|required');
                    $this->form_validation->set_rules('data['.$id.'][body]','Body','trim|required');
                    $this->form_validation->set_rules('data['.$id.'][page_title]','Title','trim|required');
                    $this->form_validation->set_rules('data['.$id.'][meta_description]','Meta description','trim|required');
                   
                }
            }
            
            $data['validation_error'] = '';
            //var_dump(isset($_POST));exit;
            if( isset($_POST) && $validation_flag && ! $flag )
            {
                $data['validation_error'] = lang('you_should_add_static_page_in_atleast_one_language');

            }
            else
            {
                $validation_flag = FALSE;
            }

            if( $this->form_validation->run() == FALSE  || (isset($_POST) && $validation_flag))
            {
                $data['languages'] = $languages;
               
                $data['header'] = TRUE;
                $data['footer'] = TRUE;
                $data['active']['tab'] = FALSE;
                $data['active']['arrow'] = FALSE;
                $data['active_menu'] = 'article';
                $data['sidebar'] = TRUE;
                $data["_activeTab"] = ADMIN_TAB_4;
                $data['_view'] = 'static_pages/add_page';
                $this->load->view( 'admin/basetemplate',$data);
                
            }
            else
            {
                
                $url = $this->Static_page_model->check_url_occurrence($this->input->post('page_name'));

                 $pagedata = array(
                    'title' =>  $this->input->post('data[1][page_name]'),   
                    'added_by' => $this->user['id'],
                    'added_on' => date('Y-m-d g:i:s')
                );

                $result = $this->Static_page_model->add( $pagedata, $_FILES );

                $static_page_id = $result['data'];

                if($result['rc'])
                {
                 
                    $languages_data = array();
                    foreach( $languages['data'] as $value ) 
                    {
                        $language_id       = $value['id'];

                        $url = $this->Static_page_model->check_url_occurrence($this->input->post('data['.$language_id.'][page_name]'));

                            $languages_data[] = array(
                                                    'page_id' => $static_page_id,
                                                    'language_id' => $language_id,
                                                    'page_name'=>$this->input->post('data['.$language_id.'][page_name]'),
                                                    'url' => !empty($this->input->post('data['.$language_id.'][url]'))?$this->input->post('data['.$language_id.'][url]') : $url['url'],
                                                    'url_occurrence'=>$url['url_occurrence'],
                                                    'body' => $this->input->post('data['.$language_id.'][body]'),
                                                    'page_title'=>$this->input->post('data['.$language_id.'][page_title]'),
                                                    'meta_description'=>$this->input->post('data['.$language_id.'][meta_description]'),
                                                    
                                                    'added_on'=>date('Y-m-d G:i:s')
                                                );

                    }

                    $pages_by_language = $this->Static_page_model->add_pages_by_language($languages_data);
                    $data['pages'] = $pages_by_language;

                    if($pages_by_language['rc'])
                    {
                        $other_data['page_name'] = $pagedata['title'];
                        $activity_data = array(
                            'activity_by_user_id' => $this->user['id'],
                            'admin_id' => $this->user['id'],
                            'user_id' => $this->user['id'],
                            'activity_type' => ACTIVITY_ADMIN_ADDED_STATIC_PAGE,
                            'other_activity_data' => serialize($other_data),                    
                            'activity_on' => date('Y-m-d G:i:s')
                        );

                        $this->Activity_model->add_activity($activity_data);
                        $this->session->set_flashdata('success_msg', $pages_by_language['msg']);    
                    }    
                    else
                    {
                        $this->session->set_flashdata('error_msg', $pages_by_language['msg']);
                    }

                    redirect('admin/static_page/listing');
                }
                else
                {
                    $this->session->set_flashdata('error', $result['msg']);  
                    redirect('admin/static_page/listing');
                }
                    
            }
        }
        else
        {
            redirect('Static_page/index');

        }
    }

    function listing()
    {
        $this->authenticate();
        // Get all pages
        $total_number_of_results = $this->Static_page_model->get_all_pages();
        $data['total_number_of_results'] = !empty($total_number_of_results['data']) ? $total_number_of_results['data'] : 0;

        $config = array(
            'base_url' => site_url('admin/static_page/listing'),
            'total_rows' => count($data['total_number_of_results']),
            'per_page' => STATIC_PAGES_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        
        $pages = $this->Static_page_model->get_all_pages(STATIC_PAGES_PER_PAGE_LIMIT, $limit_start);
        $data['pages'] = $pages['data'];

        $data["header"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["footer"] = TRUE;
        $data["_view"] = "static_pages/pages_listing";
        $data['title'] = "Static_page Listing";
       
        $data["_activeTab"] = ADMIN_TAB_4;
        $this->load->view("admin/basetemplate", $data);
    }

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  edit existing page 
     * @param  
     * @return NULL
     */
    
    function edit($id)
    {
        $this->authenticate();
        $this->load->library('form_validation');
        $page = $this->Static_page_model->get_page($id);

        if($page['rc'])
        {
            $languages = $this->Static_page_model->get_languages();
            if($languages['rc'])
            {
                foreach( $languages['data'] as $value)
                {
                    $language_id = $value['id'];

                    if(!empty($this->input->post('data['.$language_id.'][page_name]')))
                    {   
                        $this->form_validation->set_error_delimiters('<li>', '</li>');
                        $this->form_validation->set_rules('data['.$language_id.'][page_title]','Title','trim|required');
                        $this->form_validation->set_rules('data['.$language_id.'][url]','Url','trim|required|callback_unique_url['.$id.','.$language_id.']');
                        $this->form_validation->set_rules('data['.$language_id.'][body]','Body','trim|required');
                        $this->form_validation->set_rules('data['.$language_id.'][page_name]','Page title','trim|required');
                        $this->form_validation->set_rules('data['.$language_id.'][meta_description]','Meta description','trim|required');
                        
                    }
                    
                }       

                if( $this->form_validation->run() )
                {
                        $params = array(
                            'title' =>  $this->input->post('data['.$language_id.'][title]'),
                            'updated_on' => date('Y-m-d G:i:s')
                        );

                        /*editing language_id wise pages to database*/

                        $languages_data =array();

                        foreach( $languages['data'] as $value ) 
                        {
                            $language_id =  $value['id'];

                            if(!empty($this->input->post('data['.$language_id.'][page_name]')))
                            {
                                $languages_data[] = array(
                                    'page_id' => $id,
                                    'language_id' => $language_id,
                                    'page_name' => $this->input->post('data['.$language_id.'][page_name]'),
                                    'url' => !empty($this->input->post('data['.$language_id.'][url]'))?$this->input->post('data['.$language_id.'][url]') : $this->input->post('data['.$language_id.'][title]'),
                                    'body' => $this->input->post('data['.$language_id.'][body]'),
                                    'page_title'=>$this->input->post('data['.$language_id.'][page_title]'),
                                    'meta_description'=>$this->input->post('data['.$language_id.'][meta_description]'),
                        
                                    'updated_on'=>date('Y-m-d G:i:s')
                                );
                            }
                        }

                        $response = $this->Static_page_model->update($id, $params, $languages_data, $_FILES);

                        if($response['rc'])
                        {
                            $other_data['page_name'] = $params['title'];
                            $activity_data = array(
                                'activity_by_user_id' => $this->user['id'],
                                'admin_id' => $this->user['id'],
                                'user_id' => $this->user['id'],
                                'activity_type' => ACTIVITY_ADMIN_UPDATED_STATIC_PAGE,
                                'other_activity_data' => serialize($other_data),                    
                                'activity_on' => date('Y-m-d G:i:s')
                            );
                            $this->Activity_model->add_activity($activity_data);
                            
                            $this->session->set_flashdata('success_msg', $response['msg']);
                            redirect('admin/static_page/listing'); 
                        }      
                        else
                        {
                            $this->session->set_flashdata('error_msg', $response['msg']);  
                        }

                        redirect('admin/static_page/edit/'.$id);
                }
                else
                {

                    $page = $this->Static_page_model->get_page($id);
                    if($page['rc'])
                        $data['page'] = $page['data'];
                    else
                    $data['page'] = array();

                    //$data['page_category'] = $this->Static_page_model->get_page_categories();

                    $data['page_id'] = $id;
                    $data["header"] = TRUE;
                    $data["sidebar"] = TRUE;
                    $data["footer"] = TRUE;
                    $data["_view"] = "static_pages/edit_page";
                    $data['title'] = "Edit page";
                    $data["active"] = "page";
                    $data["_activeTab"] = ADMIN_TAB_4;
                    $this->load->view("admin/basetemplate", $data);
                }
            }
            else
            {
                 $this->session->set_flashdata('error_msg', $response['msg']);      
                 redirect('admin/static_page/listing');
            }
            
        }
        else
        {
            $this->session->set_flashdata("error_msg",lang('invalid_id'));
            redirect('article/listing');
        }
    }

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  unique url
     * @param  
     * @return NULL
     */

    function unique_url($url,$array)
    {
        $url_data = explode(',',$array);

        $id=$url_data[0];
        $language_id=$url_data[1];

        $data = $this->Static_page_model->check_unique_url($id,$language_id,$url);

        if(!empty($data['url']))
        {
           $this->form_validation->set_message('unique_url','This Url '.$this->input->post('url').' is already present');           
           return FALSE;
        }

        return TRUE;
    }
      

    /**
     * @author Kiran Borse <kiranb@codebox.co.in>
     * @desc  delete page 
     * @param  
     * @return NULL
     */

    function delete()
    {
       $this->authenticate();

       $id = $this->uri->segment(4);

        $page = $this->Static_page_model->get_page($id);

        if( $page['rc'])
        {
            $delete_result = $this->Static_page_model->delete($id, $page);

            if($delete_result['rc'])
            {
                $other_data = array('page_name' => $page['data'][0]['title']);
                $activity_data = array(
                    'activity_by_user_id' => $this->user['id'],
                    'admin_id' => $this->user['id'],
                    'user_id' => $this->user['id'],
                    'activity_type' => ACTIVITY_ADMIN_DELETED_STATIC_PAGE,
                    'activity_on' => date('Y-m-d G:i:s'),
                    'other_activity_data' => serialize($other_data)
                );

                $this->Activity_model->add_activity($activity_data);

                $this->session->set_flashdata('success_msg', $delete_result['msg']);
            }
            else
            {
                $this->session->set_flashdata('error_msg', $delete_result['msg']);
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg', lang('invalid_data'));
        }
        
        redirect('admin/static_page/listing'); exit;
    }

    
    function change_status( $slug="", $page_id = 0)
    {
        $this->authenticate();
        $page = $this->Static_page_model->get_page($page_id);

        if( $page['rc'])
        {
            if( $slug == "offline")
                $status = '1';
            else if( $slug == "online")
                $status = '0';

            $params = array( "is_offline" => $status);
            $result = $this->Static_page_model->change_status($page_id, $params);

            if($result['rc'])
            {                
                $other_data = array('page_name' => $page['data'][0]['title']);
                $activity_data = array(
                    'activity_by_user_id' => $this->user['id'],
                    'admin_id' => $this->user['id'],
                    'user_id' => $this->user['id'],
                    'activity_type' => $params['is_offline'] == 0 ? ACTIVITY_ADMIN_PUBLISHED_STATIC_PAGE : ACTIVITY_ADMIN_UNPUBLISHED_STATIC_PAGE,
                    'activity_on' => date('Y-m-d G:i:s'),
                    'other_activity_data' => serialize($other_data)
                );
                $this->Activity_model->add_activity($activity_data);
                
                $this->session->set_flashdata('success_msg', $result['msg']);
            }
            else
            {
                $this->session->set_flashdata('error_msg', $result['msg']);
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg', lang('invalid_data'));
        }
        
        redirect('admin/static_page/listing'); exit;
    }

    function view()
    {
        $redirect_url = $_SERVER['REDIRECT_QUERY_STRING'];
        //$redirect_url = $_SERVER['HTTP_REFERER'];
        $this->load->model(array('Form_model', 'Email_model'));
        //build form validation rules based on form id
        $form_id = $this->input->post('form_id');

        if($this->input->post('form_id'))
        {
            $form = $this->Form_model->get_form($form_id);

            if($form['rc'])
            {
                $form_fields = $this->Form_model->get_form_fields($form_id);

                if($form_fields['rc'])
                {
                    foreach ($form_fields['data'] as $f_key => $f)
                    {
                        $rule = '';

                        if($f['is_required'] == 'Yes')
                        {
                            $rule .= '|required';
                        }

                        if($f['field_type'] == FORM_FIELD_TYPE_DATE)
                        {
                            $rule .= '|callback_is_valid_date';
                        }

                        if($f['field_type'] == FORM_FIELD_TYPE_EMAIL)
                        {
                            $rule .= '|valid_email';
                        }

                        $name = str_replace('-', '_', $f['shortcode']);
                        $name = $f['field_type'] == FORM_FIELD_TYPE_CHECKBOX ? $name . '[]' : $name;
                        $this->form_validation->set_rules($name, $f['label'], 'trim|xss_clean' . $rule);
                    }
                }
            }
        }

        $lang_params['code'] = strtoupper($this->session->userdata('lang'));
        //$language = $this->User_model->get_language_by_params($lang_params);

       /* if($language['rc'])
            $language_id = $language['data']['id'];
        else*/
            $language_id = 0;

        $page_url = $this->uri->segment(2);
        
        $static_page_content = $this->Static_page_model->get_static_page_by_url($page_url, $language_id);

        if($static_page_content['rc'])
        {
            $data['page'] = $static_page_content['data'];
            
            if($this->form_validation->run() === TRUE)
            {
                $form_data = array(
                    'form_id' => $form_id,
                    'form_data' => json_encode($_POST),
                    'added_on' => date('Y-m-d G:i:s')
                );
                
                $add_result = $this->Form_model->add_form_data($form_data);
                $msg_label = $add_result['rc'] ? 'success' : 'error';

                $form_configuration = $this->Form_model->get_form_configuration($form_id);
                
                if($form_configuration['rc'])
                {
                    $msg = $add_result['rc'] ? $form_configuration['data']['message_sent_successfully'] : $form_configuration['data']['error_sending_message'];
                }
                else
                {
                    $msg = $add_result['msg'];
                }
                
                $this->session->set_flashdata($msg_label, $msg);

                if($add_result['rc'])
                {
                    $where = array('form_id' => $form_id);
                    $emails = $this->Email_model->get_all_emails($where);

                    if($emails['rc'])
                    {
                        foreach ($emails['data'] as $e_key => $email)
                        {
                            $email_data['email'] = $email;
                            $email_data['form_data'] = $_POST;
                            $email_data['form_fields'] = $form_fields['data'];
                            $from_email = SEND_FROM_EMAIL_ID;
                            $to_email = ADMIN_EMAIL; 
                            $subject = $email['subject'];

                            $from_name = "";
                            $tag = $email['title'];
                            $body = $this->load->view('email_module/email', $email_data, TRUE);
                            $to_name = '';

                            $email_data = array(
                                "tag"             =>  $tag,
                                "subject"         =>  $subject,
                                "body"            =>  $body,
                                "status"          =>  'email_added',
                                "from_email"      =>  $from_email,
                                "to_email"        =>  $to_email,
                                "to_name"         =>  $to_name,
                                "from_name"       =>  $from_name,
                                "added_on"        =>  time(),
                                "vendor_type"     =>  EMAIL_VENDOR_MANDRILL
                            );

                            $email_stats_add_result_id = $this->Email_model->add_email_stats($email_data);
                        }
                    }
                }

                redirect($redirect_url); exit;
            }
            else
            {
                $data["header"] = TRUE;
                $data["sidebar"] = TRUE;
                $data["footer"] = TRUE;
                $data["_view"] = "static_pages/page_view";
                $data['title'] = "Edit page";
                $data["active"] = "page";
                $data["_activeTab"] = ADMIN_TAB_4;
                $this->load->view('layouts/frontend/basetemplate', $data);
            }
        }
        else
        {
            redirect($this->session->userdata('flang') . '/'); exit;
        }
    }

    function is_valid_date($date)
    {
        if($date !="")
        {
            $parts = explode("-", $date);

            if (count($parts) == 3)
            {  
                if (checkdate($parts[1], $parts[0], $parts[2]))
                {
                    return TRUE;
                }
                else
                {
                    $this->form_validation->set_message('is_valid_date', '%s must be valid date.');
                    return FALSE;
                }
            }
            else
            {
                $this->form_validation->set_message('is_valid_date', '%s must be a valid date.');
                return FALSE;
            }
        }
        else
        {
            return TRUE;
        }
    }
}
