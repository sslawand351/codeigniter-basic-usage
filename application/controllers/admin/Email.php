<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends AdminController {

    function __construct()
    {
        parent::__construct();
        $this->load->library('permissions');
        //$this->permissions->authorize('EMAIL_MODULE');
        $this->load->model( array('Email_model', 'User_model', 'Form_model') );
    }

    function template_listing()
    {
        //$this->authenticate();

        $email_templates = $this->Email_model->get_all_email_templates();

        $config = array(
            'base_url' => site_url('admin/email/template_listing'),
            'total_rows' => $email_templates['rc'] ? count($email_templates['data']) : 0,
            'per_page' => EMAILS_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit = array('limit' => EMAILS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);

        $email_templates = $this->Email_model->get_all_email_templates($limit);
        $data['email_templates'] = $email_templates['data'];

        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "email_module/templates/listing";
        $data['title']  = set_title(lang('email_templates'));
        $data["_activeTab"] = ADMIN_TAB_6;
        $this->load->view("admin/basetemplate", $data);
    }

    function create_template()
    {
        $this->authenticate();
        $this->form_validation->set_rules('template_name','Title', 'trim|required');
        $this->form_validation->set_rules('header_background','Header background', 'trim|callback_is_valid_image[header_background,0]');
        $this->form_validation->set_rules('footer_background','Footer background', 'trim|callback_is_valid_image[footer_background,0]');
        $this->form_validation->set_rules('body_background_color','Body background color', 'trim|callback_is_valid_hex_color');
        $this->form_validation->set_rules('content_background_color','Content background color', 'trim|callback_is_valid_hex_color');
            
        if( $this->form_validation->run() == TRUE )
        {
            // Get language id
            $language_code = $this->session->userdata('lang');
            $language = $this->User_model->get_id_by_language_code( $language_code );
            $language_id = $language['rc'] ? $language['data']['id'] : 4 ;

            // add email template
            $email_template_data = array(
                'title' => trim($this->input->post('template_name')),
                'body_color' => $this->input->post('body_background_color'),
                'content_color' => $this->input->post('content_background_color'),
                /*'language_id'   => $language_id,*/
                'added_on'      => date('Y-m-d G:i:s'),
                'modified_on'   => date('Y-m-d G:i:s'),
                'added_by'      => $this->user['id']
            );

            $add_result = $this->Email_model->add_email_template($email_template_data);

            if($add_result['rc'])
            {
                $email_template_id = $add_result['data']['id'];
                $this->Email_model->add_header_image($email_template_id, $_FILES['header_background']);
                $this->Email_model->add_footer_image($email_template_id, $_FILES['footer_background']);
                $msg_label = 'success';
            }
            else
            {
                $msg_label = 'error';
            }
            
            $this->session->set_flashdata($msg_label, $add_result['msg']);

            redirect('admin/email/template_listing'); exit;
        }
        else
        {
            $data["header"] = TRUE;
            $data["footer"] = TRUE;
            $data["sidebar"] = TRUE;
            $data["active"] = 'Newsletters';
            $data["_view"]  = "email_module/templates/add";
            $data['title']  = set_title(lang('create_email_template'));
            $data["_activeTab"] = ADMIN_TAB_6;
            $this->load->view("admin/basetemplate", $data);
        }
    }

    function edit_template($id = 0)
    {
        $this->authenticate();

        $email_template = $this->Email_model->get_email_template($id);

        if($email_template['rc'])
        {
            $data['email_template'] = $email_template['data'];
        }
        else
        {
            $this->session->set_flashdata('error', lang('invalid_data'));
            redirect('admin/email/listing'); exit;
        }

        $this->form_validation->set_rules('template_name','Title', 'trim|required');
        $this->form_validation->set_rules('header_background','Header background', 'trim|callback_is_valid_image[header_background,0]');
        $this->form_validation->set_rules('footer_background','Footer background', 'trim|callback_is_valid_image[footer_background,0]');
        $this->form_validation->set_rules('body_background_color','Body background color', 'trim|callback_is_valid_hex_color');
        $this->form_validation->set_rules('content_background_color','Content background color', 'trim|callback_is_valid_hex_color');
            
        if( $this->form_validation->run() == TRUE )
        {
            // Get language id
            $language_code = $this->session->userdata('lang');
            $language = $this->User_model->get_id_by_language_code( $language_code );
            $language_id = $language['rc'] ? $language['data']['id'] : 4 ;

            // add email template
            $email_template_data = array(
                'title' => trim($this->input->post('template_name')),
                'body_color' => $this->input->post('body_background_color'),
                'content_color' => $this->input->post('content_background_color'),
                /*'language_id'   => $language_id,*/
                'modified_on'   => date('Y-m-d G:i:s')
            );

            $update_result = $this->Email_model->update_email_template($id, $email_template_data);

            if($update_result['rc'])
            {
                $email_template_id = $email_template['data']['id'];
                $this->Email_model->add_header_image($email_template_id, $_FILES['header_background']);
                $this->Email_model->add_footer_image($email_template_id, $_FILES['footer_background']);
                $msg_label = 'success';
            }
            else
            {
                $msg_label = 'error';
            }
            
            $this->session->set_flashdata($msg_label, $update_result['msg']);

            redirect('admin/email/template_listing'); exit;
        }
        else
        {
            $data["header"] = TRUE;
            $data["footer"] = TRUE;
            $data["sidebar"] = TRUE;
            $data["active"] = 'email_module';
            $data["_view"]  = "email_module/templates/edit";
            $data['title']  = set_title(lang('edit_email_template'));
            $data["_activeTab"] = ADMIN_TAB_6;
            $this->load->view("admin/basetemplate", $data);
        }
    }

    function delete_email_template()
    {
        $this->authenticate();
        //check permission is assigned to user
        //$this->authorize('delete_group');

        $email_template_id = $this->input->post('email_template_id');
        
        $delete_result = $this->Email_model->delete_email_template($email_template_id);

        if($delete_result['rc'])
        {
            $this->session->set_flashdata( 'success_msg', $delete_result['msg'] );
        }
        else
        {
            $this->session->set_flashdata( 'error_msg', $delete_result['msg'] );
        }

        redirect('admin/email/template_listing');exit;
    }

    function is_valid_image($image_name,$params)
    {
        $parts = explode(",", $params);
        $image_slug = $parts[0];
        $multiple = $parts[1];

        switch($multiple)
        {
            case 0:
                if (isset($_FILES[$image_slug]) && !empty($_FILES[$image_slug]['name']))
                {
                    $file_info = @getimagesize($_FILES[$image_slug]['tmp_name']);
                    
                    if(!empty($file_info))
                    {
                        return TRUE;
                    }
                    else
                    {
                        // possibly do some clean up ... then throw an error
                        $this->form_validation->set_message('is_valid_image', lang('the_uploaded_file_doesnt_seem_to_be_an_image'));
                        return FALSE;
                    }
                }
                else
                {
                    // throw an error because nothing was uploaded
                    //$this->form_validation->set_message('image_upload', "You must upload an image!");
                    return TRUE;
                }
                break;

            case 1: 
                if (isset($_FILES[$image_slug]['name'][0]) && !empty($_FILES[$image_slug]['name'][0]))
                {
                    foreach($_FILES[$image_slug]['tmp_name'] as $tmp_name)
                    {
                        $file_info = @getimagesize($tmp_name);
                        
                        if(!empty($file_info))
                        {
                            return TRUE;
                        }
                        else
                        {
                            // possibly do some clean up ... then throw an error
                            $this->form_validation->set_message('is_valid_image', lang('the_uploaded_file_doesnt_seem_to_be_an_image'));
                            return FALSE;
                        }
                    }
                }
                else
                {
                    // throw an error because nothing was uploaded
                    //$this->form_validation->set_message('image_upload', "You must upload an image!");
                    return TRUE;
                }
                break;

            default: break;
        }
    }

    function is_valid_hex_color($color)
    {
        if(preg_match('/(#[a-f0-9]{3}([a-f0-9]{3})?)/i', $color))
        {
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('is_valid_hex_color', 'Please enter valid hex color');
            return FALSE;
        }
    }

    function delete_template_image($image_type, $email_template_id)
    {
        $this->authenticate();
        $email_template = $this->Email_model->get_email_template($email_template_id);

        if($email_template['rc'] && ($image_type == 'header' || $image_type == 'footer'))
        {
            $data['email_template'] = $email_template['data'];

            $image = FCPATH . 'uploads/email_module/templates/images/' .$image_type. '/' . $email_template_id . '/' . $email_template['data'][$image_type. '_image'];

            if(file_exists($image) && $email_template['data'][$image_type. '_image'] != '')
            {
                unlink($image);

                $email_template_data = array(
                    $image_type .'_image' => ''
                );
                $this->Email_model->update_email_template($email_template_id, $email_template_data);

                $this->session->set_flashdata('success', lang('image_deleted_successfully'));
                redirect('admin/email/edit_template/' . $email_template_id); exit;
            }
            else
            {
                $this->session->set_flashdata('error', lang('invalid_data'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('invalid_data'));
        }

        redirect('admin/email/listing'); exit;
    }

    function add()
    {
        $this->authenticate();
        $this->form_validation->set_rules('email_name','Title', 'trim|required');
        $this->form_validation->set_rules('email_template_id','Email Template', 'trim|required');
        $this->form_validation->set_rules('form_id','Form', 'trim|required');
        $this->form_validation->set_rules('subject','Subject', 'trim|required');
        $this->form_validation->set_rules('body','Body', 'trim|required');
            
        if($this->form_validation->run() === TRUE)
        {
            // Get language id
            /*$language_code = $this->session->userdata('lang');
            $language = $this->User_model->get_id_by_language_code($language_code);
            $language_id = $language['rc'] ? $language['data']['id'] : 4 ;*/
            $form_id = (int)$this->input->post('form_id');
            $form_name = $form_id != 0 ? '' : $this->input->post('form_id');
            $form_type = $form_id != 0 ? 'custom-form' : 'predefined-email-template';
            // add email template
            $email_data = array(
                'title' => trim($this->input->post('email_name')),
                'email_template_id' => $this->input->post('email_template_id'),
                'form_id' => $form_id,
                'form_type' => $form_type,
                'form_name' => $form_name,
                'subject' => $this->input->post('subject'),
                'email_body' => $this->input->post('body'),
                'added_on' => date('Y-m-d G:i:s'),
                'modified_on' => date('Y-m-d G:i:s'),
                'added_by' => $this->user['id']
            );

            $add_result = $this->Email_model->add_email($email_data);

            if($add_result['rc'])
            {
                $email_template_id = $add_result['data']['id'];
                $msg_label = 'success';
            }
            else
            {
                $msg_label = 'error';
            }
            
            $this->session->set_flashdata($msg_label, $add_result['msg']);

            redirect('admin/email/listing'); exit;
        }
        else
        {
            $email_templates = $this->Email_model->get_all_email_templates();
            $data['email_templates'] = $email_templates['data'];

            $forms = $this->Form_model->get_all_forms();
            $data['forms'] = $forms['data'];

            $data["header"] = TRUE;
            $data["footer"] = TRUE;
            $data["sidebar"] = TRUE;
            $data["_view"]  = "email_module/add";
            $data['title']  = set_title(lang('add_email'));
            $data["_activeTab"] = ADMIN_TAB_6;
            $this->load->view("admin/basetemplate", $data);
        }
    }

    function edit($id)
    {
        $this->authenticate();

        $email = $this->Email_model->get_email($id);

        if($email['rc'])
        {
            $data['email'] = $email['data'];
        }
        else
        {
            $this->session->set_flashdata('error', lang('invalid_data'));
            redirect('admin/email/listing'); exit;
        }

        $this->form_validation->set_rules('email_name','Title', 'trim|required');
        $this->form_validation->set_rules('email_template_id','Email Template', 'trim|required');
        $this->form_validation->set_rules('form_id','Form', 'trim|required');
        $this->form_validation->set_rules('subject','Subject', 'trim|required');
        $this->form_validation->set_rules('body','Body', 'trim|required');
            
        if($this->form_validation->run() === TRUE)
        {
            // Get language id
            /*$language_code = $this->session->userdata('lang');
            $language = $this->User_model->get_id_by_language_code($language_code);
            $language_id = $language['rc'] ? $language['data']['id'] : 4 ;*/
            $form_id = (int)$this->input->post('form_id');
            $form_name = $form_id != 0 ? '' : $this->input->post('form_id');
            $form_type = $form_id != 0 ? 'custom-form' : 'predefined-email-template';
            
            // add email template
            $email_data = array(
                'title' => trim($this->input->post('email_name')),
                'email_template_id' => $this->input->post('email_template_id'),
                'form_id' => $form_id,
                'form_type' => $form_type,
                'form_name' => $form_name,
                'subject' => $this->input->post('subject'),
                'email_body' => $this->input->post('body'),
                'modified_on' => date('Y-m-d G:i:s')
            );

            $update_result = $this->Email_model->update_email($id, $email_data);

            $msg_label = $update_result['rc'] ? 'success' : 'error';
            
            $this->session->set_flashdata($msg_label, $update_result['msg']);

            redirect('admin/email/listing'); exit;
        }
        else
        {
            $email_templates = $this->Email_model->get_all_email_templates();
            $data['email_templates'] = $email_templates['data'];

            $forms = $this->Form_model->get_all_forms();
            $data['forms'] = $forms['data'];

            $data["header"] = TRUE;
            $data["footer"] = TRUE;
            $data["sidebar"] = TRUE;
            $data["_view"]  = "email_module/edit";
            $data['title']  = set_title(lang('edit_email'));
            $data["_activeTab"] = ADMIN_TAB_6;
            $this->load->view("admin/basetemplate", $data);
        }
    }

    function delete()
    {
        $this->authenticate();

    }

    function listing()
    {
        //$this->authenticate();
        $params = array();
        $emails = $this->Email_model->get_all_emails($params, TRUE);
        $total_count = $emails['data']['count'];

        $config = array(
            'base_url' => site_url('admin/email/listing'),
            'total_rows' => $total_count,
            'per_page' => EMAILS_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit = array('limit' => EMAILS_PER_PAGE_LIMIT, 'start' => $limit_start);

        $emails = $this->Email_model->get_all_emails($params, FALSE, $limit);
        $data['emails'] = $emails['data'];

        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["_view"]  = "email_module/listing";
        $data['title']  = set_title(lang('email_listing'));
        $data["_activeTab"] = ADMIN_TAB_6;
        $this->load->view("admin/basetemplate", $data);
    }

    function delete_email()
    {
        $this->authenticate();
        //check permission is assigned to user
        //$this->authorize('delete_group');

        $email_id = $this->input->post('email_id');
        
        $delete_result = $this->Email_model->delete_email($email_id);

        if($delete_result['rc'])
        {
            $this->session->set_flashdata( 'success', $delete_result['msg'] );
        }
        else
        {
            $this->session->set_flashdata( 'error', $delete_result['msg'] );
        }

        redirect('admin/email/listing');exit;
    }
}