<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends AdminController
{
	function __construct()
	{
		parent::__construct();
        $this->load->library('permissions');
        $this->permissions->authorize('NEWSLETTER_MODULE');
		$this->load->model('Group_model');
		$this->load->library('form_validation');    
	}

    function listing()
    {
        $this->authenticate();
        $this->load->model('Newsletter_model');
        //check permission is assigned to user
        //$this->authorize('group_listing');

        $groups = $this->Group_model->get_all_groups();

        $config = array(
            'base_url' => site_url('groups'),
            'total_rows' => $groups['rc'] ? count($groups['data']) : 0,
            'per_page' => PER_PAGE_LIMIT,
            'uri_segment' => 2
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $limit = array('limit' => PER_PAGE_LIMIT, 'limit_start' => $limit_start);

        $groups = $this->Group_model->get_all_groups($limit);

        $data['groups'] = $groups;

        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();
        $data['newsletter_templates'] = $newsletter_templates['rc'] ? $newsletter_templates['data'] : array();

        $data["sidebar"] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["_view"] = "groups/listing";
        $data['title'] = set_title(lang('newsletter_groups'));
        $data["active"] = "groups";
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }

    function ajax_add_group()
    {
        if( $this->input->is_ajax_request() )
        {
            $this->form_validation->set_rules('name','Name','trim|required|is_unique[groups.name]');
            $data = array();

            if($this->form_validation->run())
            {
                $params = array(
                            'name' =>$this->input->post('name'),
                            'added_by' => $this->user['id'],
                            'added_on' => date('Y-m-d G:i:s'),
                            'modified_on' => date('Y-m-d G:i:s')
                    );

                $result = $this->Group_model->add($params);
                $group_id = $result['data'];

                if($result['rc'])
                {
                    $response['rc'] = TRUE;
                    $response['msg'] = $result['msg'];
                    $response['group_id'] = $group_id;
                    $_POST = array();
                    $response['add_modal'] = $this->load->view("groups/addModal", $data, true);

                    $response['_table_view'] = $this->fetch_group_table();
                }
                else
                {
                    $response['rc'] = FALSE;
                    $response['msg'] = $result['msg'];
                    $response['group_id'] = '';
                    $response['add_modal'] = $this->load->view("groups/addModal", $data, true);
                }

                 
            }
            else
            {
                $response['rc'] = FALSE;
                $response['msg'] = '';
                $response['group_id'] = '';
                $response['add_modal'] = $this->load->view("groups/addModal", $data, true);
            }

            echo json_encode($response);
        }
    }

    function fetch_group_table()
    {
        $this->load->library('pagination');

        $groups = $this->Group_model->get_all_groups();

        $config = array(
            'base_url' => site_url('groups'),
            'total_rows' => $groups['rc'] ? count($groups['data']) : 0,
            'per_page' => PER_PAGE_LIMIT,
            'uri_segment' => 2
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $limit = array('limit' => PER_PAGE_LIMIT, 'limit_start' => $limit_start);

        $groups = $this->Group_model->get_all_groups($limit);

        $data['groups'] = $groups;

        $response['_table_view'] = $this->load->view('groups/_table', $data, true);

        return $response['_table_view'];
    }

    function ajax_edit_group()
    {
        if( $this->input->is_ajax_request() )
        {
            // Get POST data
            $group_id = $this->input->post('group_id');

            // Get group by id. check group exists or not.
            $group = $this->Group_model->get_group( $group_id );

            $response['group']['rc'] = $group['rc'];

            if( $group['rc'] )
            {
                $data['group'] = $group['data'];

                $is_unique = ( $this->input->post('name') && strtolower(trim($this->input->post('name'))) != strtolower($data['group']['name'] )) ? '|is_unique[groups.name]' : '';
                
                // Set form validation rules 
                $this->form_validation->set_rules('name','Name','trim|required'.$is_unique);
                
                if($this->form_validation->run())
                {
                    $params = array(
                        'name' => $this->input->post('name'),
                        'added_by' => $this->user['id'],
                        'modified_on' => date('Y-m-d G:i:s')
                    );

                    $update_result = $this->Group_model->update( $group_id, $params);

                    if($update_result['rc'])
                    {
                        $response['rc'] = TRUE;
                        $response['msg'] = $update_result['msg'];
                        $response['_table_view'] = $this->fetch_group_table();
                    }
                    else
                    {
                        $response['rc'] = FALSE;
                        $response['msg'] = $update_result['msg'];
                        $response['edit_modal'] = $this->load->view("groups/editModal", $data, true);
                    }
                }
                else
                {
                    $response['rc'] = FALSE;
                    $response['msg'] = '';
                    $response['edit_modal'] = $this->load->view("groups/editModal", $data, true);
                }
            }
            else
            {
                $response['msg'] = lang('invalid_data');
            }

            echo json_encode($response);
        }
    }

    function ajax_add_users_to_group()
    {        
        if( $this->input->is_ajax_request() )
        {
            // Get POST data
            $group_id = $this->input->post('group_id');
            $name = $this->input->post('name');
            $email = $this->input->post('email');

            // Get group by id. check group exists or not.
            $group = $this->Group_model->get_group( $group_id );

            $response['group']['rc'] = $group['rc'];
            $GLOBALS['group_id'] = $group_id;

            if( $group['rc'] )
            {
                $data['group'] = $group['data'];

                $this->form_validation->set_rules('name','Name','trim|required');
                $this->form_validation->set_rules('email','Email',
                    array(
                            'trim',
                            'required',
                            'valid_email',
                            array('check_user_exists_in_group_callable', 
                                function( $email ) {
                                    return $this->Group_model->check_user_exists_in_group( $GLOBALS['group_id'], $email );
                                } 
                            )
                    )
                );
                
                $this->form_validation->set_message('check_user_exists_in_group_callable', $email . ' is already exists in this group. Please enter valid email.');
                if( $this->form_validation->run() )
                {
                    $groups_has_users_data = array(
                        'group_id' => $group_id,
                        'user_name' => $name,
                        'email' => $email,
                        'added_by' => $this->user['id'],
                        'added_on' => date('Y-m-d G:i:s')
                    );

                    $add_result = $this->Group_model->add_users_to_group( $group_id, $groups_has_users_data );
                    $response['rc'] = TRUE;
                    $response['msg'] = $add_result['msg'];
                }
                else
                {
                    $response['rc'] = FALSE;
                    $response['msg'] = '';
                    $response['template_modal'] = $this->load->view("groups/addUserstoGroupModal", $data, true);
                }
            }
            else
            {
                $response['msg'] = lang('invalid_data');
            }

            echo json_encode($response);
        }
    }

    function delete()
    {
        $this->authenticate();
        //check permission is assigned to user
        //$this->authorize('delete_group');

        $group_id = $this->input->post('group_id');
        
        $result = $this->Group_model->delete_group($group_id);

        if($result['rc'])
        {
            $this->session->set_flashdata( 'success_msg', $result['msg'] );
        }
        else
        {
            $this->session->set_flashdata( 'error_msg', $result['msg'] );
        }

        redirect('groups');exit;
    }

    /*function ajax_delete_user_from_group()
    {
        if( $this->input->is_ajax_request() )
        {
            // Get POST data
            $group_id = $this->input->post('group_id');
            $user_id = $this->input->post('user_id');
            $lead_id = $this->input->post('lead_id'); 

            // Get group by id. check group exists or not.
            $group = $this->Group_model->get_group( $group_id );

            $response['group']['rc'] = $group['rc'];

            if( $group['rc'] )
            {
                $data['group'] = $group['data'];

                $delete_result = $this->Group_model->delete_user_from_group( $group_id, $user_id, $lead_id );

                $response['rc'] = $delete_result['rc'];
                $response['msg'] = $delete_result['msg'];
                

                $group_users = $this->Group_model->get_users_by_group_id( $group_id );
                $data['group_users'] = $group_users['data'];
                                    
                $response['template_modal'] = $this->load->view("groups/addUserstoGroupModal", $data, true);
            
            }
            else
            {
                $response['msg'] = lang('invalid_data');
            }

            echo json_encode($response);
        }
    }*/

    function users( $group_id )
    {
        $this->authenticate();
        $group = $this->Group_model->get_group( $group_id );

        if( ! $group['rc'] )
        {
            $this->session->set_flashdata('error_msg', lang('invalid_data') );
            redirect('groups'); exit;
        }
        else
        {
            $data['group'] = $group['data'];
        }

        /*$search_params = array('is_unsubscribe_to_newsletter' => 'no');*/
        $users = $this->Group_model->get_users_by_group_id($group_id);
        $data['users'] = $users['data'];

        /*$search_params = array('is_unsubscribe_to_newsletter' => 'yes'); */
        $unsubscribed_users = $this->Group_model->get_unsubscribed_users_by_group_id($group_id);
        $data['unsubscribed_users'] = $unsubscribed_users['data'];

        $data["sidebar"] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["_view"] = "groups/users_listing";
        $data['title'] = set_title(lang('newsletter_groups'));
        $data["active"] = "groups";
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }

    function check_user_exists_in_group( $email )
    {

    }

    function import_csv_emails()
    {
        $this->authenticate();

        $this->load->library('form_validation');
        ob_start();
        $success_rows = 0;

        $group_ids = $this->input->post('group_id[]');

         // define('CSV_PATH','c:/xampp/htdocs/beerens/resources/admin/csv/');

        if($_FILES["csv_file"]["name"]=='')
        {
            $this->session->set_flashdata('error_msg', "Please select file");
        }
    
        if($this->form_validation->run() == FALSE)
        {
            if(isset($_FILES["csv_file"]) && !empty($_FILES["csv_file"]["name"]))
            {
                $extension = pathinfo($_FILES['csv_file']['name'], PATHINFO_EXTENSION);

                if($extension == 'csv')
                {
                    $dir = FCPATH . "uploads/tmp_csv/" . $this->user['id'] . "/";

                    if(!is_dir($dir))
                    {   
                        @mkdir($dir, 0777,true);
                    }

                    if(move_uploaded_file($_FILES['csv_file']['tmp_name'], $dir . 'user.csv'))
                    {

                        foreach( $group_ids as $group_id )
                        {
                            $this->Group_model->load_data_infile($group_id, $this->user['id']);
                        }

                        $this->session->set_flashdata('success_msg','Users added to groups successfully.');
                        unlink($dir . 'user.csv');
                    }

                    redirect('groups');
                }
                else
                {
                    $this->session->set_flashdata('error','Invalid file format.');
                    redirect('groups#add-users-to-group-from-csv');exit;
                }
            }
        }
        else
        {
            $this->session->set_flashdata('error','Please select a csv file');
            redirect('groups');exit;
        }
    }

    function delete_user_from_group()
    {
        $this->authenticate();

        $group_user_id = $this->input->post('group_user_id');
        $group_id = $this->input->post('group_id');
        
        $result = $this->Group_model->delete_user_from_group( $group_user_id );

        if( $result['rc'] )
        {
            $this->session->set_flashdata('success_msg', $result['msg']);
        }
        else
        {
            $this->session->set_flashdata('error_msg', $result['msg']);
        }

        redirect('group/users/'.$group_id);exit;
    }

    function unsubscribe_user_from_group()
    {
        $this->authenticate();

        $group_user_id = $this->input->post('group_user_id');
        $group_id = $this->input->post('group_id');
        
        $result = $this->Group_model->unsubscribe_user_from_group( $group_user_id );

        if( $result['rc'] )
        {
            $this->session->set_flashdata('success_msg', $result['msg']);
        }
        else
        {
            $this->session->set_flashdata('error_msg', $result['msg']);
        }

        redirect('group/users/'.$group_id);exit;
    }

    function subscribe_user_to_group()
    {
        $this->authenticate();

        $group_user_id = $this->input->post('group_user_id');
        $group_id = $this->input->post('group_id');
        
        $result = $this->Group_model->subscribe_user_to_group( $group_user_id );

        if( $result['rc'] )
        {
            $this->session->set_flashdata('success_msg', $result['msg']);
        }
        else
        {
            $this->session->set_flashdata('error_msg', $result['msg']);
        }

        redirect('group/users/'.$group_id);exit;
    }

    function ajax_get_group_users_listing($offset = 0)
    {
        if( $this->input->is_ajax_request() && $this->is_logged_in() )
        {
            $group_id = $this->input->post('group_id');
            $group = $this->Group_model->get_group( $group_id );
            $email = $this->input->post('email');

            if( ! $group['rc'] )
            {
                $this->session->set_flashdata('error_msg', lang('invalid_data') );
                $response['rc'] = FALSE;
                echo json_encode($response); exit;
                //redirect('groups'); exit;
            }
            else
            {
                $data['group'] = $group['data'];
            }

            $params = array('count_flag' => TRUE, 'email' => $email);

            if($this->input->post('is_unsubscribe'))
            {
                $users = $this->Group_model->get_unsubscribed_users_by_group_id($group_id, $params);
            }
            else
            {
                $users = $this->Group_model->get_users_by_group_id($group_id, $params);
            }
            
            $config = array(
                'base_url' => site_url('group/ajax_get_group_users_listing'),
                'total_rows' => isset($users['data'][0]['count']) ? $users['data'][0]['count'] : 0,
                'per_page' => PER_PAGE_LIMIT,
                'uri_segment' => 4,
                'div' => 'car-block',
                'function_type' => $this->input->post('is_unsubscribe') ? 'unsubscribed_user' : 'user'
            );
            $this->load->model('Pagination_model');
            $data['links'] = $this->Pagination_model->ajax_config($config);
            $data['offset'] = $offset;

            $limit = array('limit' => PER_PAGE_LIMIT, 'limit_start' => $offset, 'email' => $email);

            if( $this->input->post('is_unsubscribe') )
            {
                $users = $this->Group_model->get_unsubscribed_users_by_group_id($group_id, $limit);
                $data['unsubscribed_users'] = $users['data'];
                $_view = 'groups/_table_unsubscribed_users';
            }
            else
            {
                $users = $this->Group_model->get_users_by_group_id($group_id, $limit);
                $data['users'] = $users['data'];
                $_view = 'groups/_table_subscribed_users';
            }

            $jsondata['rc'] = TRUE;
            $jsondata['html'] = $this->load->view($_view, $data, TRUE);

            echo json_encode($jsondata);
            /*$unsubscribed_users = $this->Group_model->get_unsubscribed_users_by_group_id($group_id);
            $data['unsubscribed_users'] = $unsubscribed_users['data'];*/
        }
    }

    function ajax_get_user_by_group_has_user_id()
    {
        if( $this->input->is_ajax_request() )
        {
            $email = $this->input->post('email');
            $group_user_data = $this->Group_model->get_user_group_info_by_email($email);
            $groups = $this->Group_model->get_all_groups();

            if( !empty( $group_user_data ))
            {
                $data['group_user_data'] = $group_user_data;
                $data['groups'] = $groups['data'];
                $response['rc'] = true;
                $response['edit_user_group_modal'] = $this->load->view("groups/edit_group_user_modal", $data, true);
            }
            else
            {
                $response['rc'] = false;
                $response['msg'] = lang('invalid_data');
            }

            echo json_encode($response);
        }
        
    }

    function ajax_update_group_user()
    {
        if( $this->input->is_ajax_request() )
        {   
            $user_previous_email = $this->input->post('user_previous_email');
            $new_email = $this->input->post('email');

            $duplicate_email = $this->Group_model->get_user_group_info_by_email($new_email);
                
            if($new_email != $user_previous_email &&  !empty($duplicate_email))
            {
                $response['rc'] = false;
                $response['email_error_msg'] = 'This email is already exists in database.';
                echo json_encode($response);
            }
            else
            {
                $user_groups = array_unique($this->input->post('user_groups'));
                $this->Group_model->delete_user_from_group_by_email($user_previous_email, $user_groups);
                
                $user_info = $this->Group_model->get_user_group_info_by_email( $user_previous_email );

                if(!empty($user_groups))
                {
                    foreach($user_groups as $group){
                        $data = array(
                            'group_id'=> $group,
                            'email'=> $this->input->post('email'),
                            'added_on'=> date('Y-m-d G:i:s')
                            );
                        $this->Group_model->add_users_to_group( $group, $data);
                    }
                    
                }
                $response['rc'] = true;
                $response['msg'] = lang('user_updated_successfully');
                echo json_encode($response);
            }
        }
        
    }
}
