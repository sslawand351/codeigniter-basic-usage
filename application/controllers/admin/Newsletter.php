<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends AdminController {

    function __construct() 
    {
        parent::__construct();
        $this->load->library('permissions');
        $this->load->model(array('Group_model', 'Email_model', 'Newsletter_model'));
    }

    function template_listing()
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');

        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();

        $config = array(
            'base_url' => site_url('newsletter/template_listing'),
            'total_rows' => $newsletter_templates['rc'] ? count($newsletter_templates['data']) : 0,
            'per_page' => NEWSLETTERS_PER_PAGE_LIMIT,
            'uri_segment' => 3
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $limit = array('limit' => NEWSLETTERS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);

        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates($limit);
        $data['newsletter_templates'] = $newsletter_templates['data'];

        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/newsletter_template_listing";
        $data['title']  = set_title(lang('newsletter_template_listing'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }

    function ajax_create_newsletter_template()
    {
        if( $this->input->is_ajax_request() )
        {
            $this->form_validation->set_rules('template_name','Name', 'trim|required');
            
            if( $this->form_validation->run() == TRUE )
            {
                $this->load->model('User_model');
                // Get language id
                $language_code = $this->input->post('language_code');
                $language = $this->User_model->get_id_by_language_code( $language_code );
                $language_id = $language['rc'] ? $language['data']['id'] : 4 ;

                // add newsletter template
                $newsletter_template_data = array(
                    'template_name' => trim($this->input->post('template_name')),
                    'language_id'   => $language_id,
                    'added_on'      => date('Y-m-d G:i:s'),
                    'added_by'      => $this->user['id']
                );

                $add_result = $this->Newsletter_model->add_newsletter_template($newsletter_template_data);

                 $limit = array(
                    'limit' => NEWSLETTERS_PER_PAGE_LIMIT,
                    'limit_start' => 0
                );

                $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates($limit);
                $data['newsletter_templates'] = $newsletter_templates['data'];
                $response['html_table_newsletter_template'] = $this->load->view('newsletters/_table_newsletter_templates', $data, true );

                $response['rc'] = TRUE;
                $response['msg'] = lang('newsletter_created_successfully');
            }
            else
            {
                $response['rc'] = FALSE;
            }

            echo json_encode($response);
            exit;
        }
    }

    function delete_newsletter_template()
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        //check permission is assigned to user
        //$this->authorize('delete_group');

        $newsletter_template_id = $this->input->post('newsletter_template_id');
        
        $delete_result = $this->Newsletter_model->delete_newsletter_template($newsletter_template_id);

        if($delete_result['rc'])
        {
            $this->session->set_flashdata( 'success_msg', $delete_result['msg'] );
        }
        else
        {
            $this->session->set_flashdata( 'error_msg', $delete_result['msg'] );
        }

        redirect('newsletter/template_listing');exit;
    }

    function stats()
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');

        $stats = $this->Newsletter_model->get_newsletter_stats();

        $config = array(
            'base_url' => site_url('newsletter/stats'),
            'total_rows' => $stats['rc'] ? count($stats['data']) : 0,
            'per_page' => NEWSLETTERS_PER_PAGE_LIMIT,
            'uri_segment' => 2
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $limit = array('limit' => NEWSLETTERS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);

        $stats = $this->Newsletter_model->get_newsletter_stats($limit);
        $data['stats'] = $stats['data'];

        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();
        $data['newsletter_templates'] = $newsletter_templates['rc'] ? $newsletter_templates['data'] : array();

        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/newsletter_stats";
        $data['title']  = set_title(lang('newsletter_stats'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);

    }

    function mandrill_open_event()
    {
        // To Do - Testing Remaining
        $key = isset($_GET['key']) ? $_GET['key'] : '';

        if($key == "bfe72d24dc7830d306e7e0e9d42df3b2")
        {
            $data = $this->input->post();

            if (isset($data['mandrill_events'])) {
                $data = json_decode($data['mandrill_events'], TRUE);
                $this->Newsletter_model->mandrill_open_event($data);
            }           
        }
    }

    function mandrill_click_event()
    {
        // To Do - Testing Remaining
        $key = isset($_GET['key']) ? $_GET['key'] : '';

        if($key == "bfe72d24dc7830d306e7e0e9d42df3b2")
        {
            $data = $this->input->post();

            if (isset($data['mandrill_events'])) {
                $data = json_decode($data['mandrill_events'], TRUE);
                $this->Newsletter_model->mandrill_click_event($data);
            }
        }
    }

    function mandrill_bounce_event()
    {
        // To Do - Testing Remaining
        $mandrill_hash = isset($_GET['key']) ? $_GET['key'] : '';

        if ($mandrill_hash == "3832fca06c3fa097236aa2749d46f555a") {
            $data = $this->input->post();

            if (isset($data['mandrill_events'])) {
                $stats = json_decode($data['mandrill_events']);

                if (is_array($stats) && isset($stats)) {
                    foreach ($stats as $stat) {
                        if ($stat->event == 'hard_bounce' || $stat->event == 'soft_bounce') {
                            $this->Newsletter_model->insert_nl_stat($stat);
                        }
                    }
                }
            }
        } else {
            show_error("You are not authorised to view this page");
        }
    }

    function mandrill_reject_event()
    {
        // To Do - Testing Remaining
        $mandrill_hash = isset($_GET['key']) ? $_GET['key'] : '';

        if ($mandrill_hash == "3832fca06c3fa097236aa2749d46f555") {
            $data = $this->input->post();

            if (isset($data['mandrill_events'])) {
                $stats = json_decode($data['mandrill_events']);

                if (is_array($stats) && isset($stats)) {
                    foreach ($stats as $stat) {
                        if ($stat->event == 'reject') {
                            $this->Newsletter_model->insert_nl_stat($stat);
                        }
                    }
                }
            }
        } else {
            show_error("You are not authorised to view this page");
        }
    }

    function open( $newsletter_sent_id )
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $params = array( 'count_flag' => TRUE );
        $opens_list = $this->Newsletter_model->get_newsletter_opens_list( $newsletter_sent_id, $params );

        $config = array(
            'base_url' => site_url('newsletter/open/' . $newsletter_sent_id),
            'total_rows' => isset($opens_list['data'][0]['count']) ? $opens_list['data'][0]['count'] : 0,
            'per_page' => NEWSLETTERS_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);
        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit = array('limit' => NEWSLETTERS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);
 
        $opens_list = $this->Newsletter_model->get_newsletter_opens_list( $newsletter_sent_id, $limit );
        $data['opens_list'] = $opens_list['data'];

        $data['offset'] = $limit_start;
 
        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();
        $data['newsletter_templates'] = $newsletter_templates['rc'] ? $newsletter_templates['data'] : array();
 
        $data['opens_list_view'] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/newsletter_stats";
        $data['title']  = set_title(lang('newsletter_stats'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }
 
    function bounce( $newsletter_sent_id )
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $params = array( 'count_flag' => TRUE );
        $bounce_list = $this->Newsletter_model->get_newsletter_bounce_list( $newsletter_sent_id, $params );

        $config = array(
            'base_url' => site_url('newsletter/bounce/' . $newsletter_sent_id),
            'total_rows' => isset($bounce_list['data'][0]['count']) ? $bounce_list['data'][0]['count'] : 0,
            'per_page' => NEWSLETTERS_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);
        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit = array('limit' => NEWSLETTERS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);
 
        $bounce_list = $this->Newsletter_model->get_newsletter_bounce_list( $newsletter_sent_id, $limit );
        $data['bounce_list'] = $bounce_list['data'];

        $data['offset'] = $limit_start;
 
        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();
        $data['newsletter_templates'] = $newsletter_templates['rc'] ? $newsletter_templates['data'] : array();
 
        $data['bounce_list_view'] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/newsletter_stats";
        $data['title']  = set_title(lang('newsletter_stats'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }
 
    function reject( $newsletter_sent_id )
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $reject_list = $this->Newsletter_model->get_newsletter_reject_list( $newsletter_sent_id );
        $data['reject_list'] = $reject_list['data'];
 
        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();
        $data['newsletter_templates'] = $newsletter_templates['rc'] ? $newsletter_templates['data'] : array();
 
        $data['reject_list_view'] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/newsletter_stats";
        $data['title']  = set_title(lang('newsletter_stats'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }
 
    function click( $newsletter_sent_id )
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $params = array( 'count_flag' => TRUE );
        $clicks_list = $this->Newsletter_model->get_newsletter_clicks_list( $newsletter_sent_id, $params );

        $config = array(
            'base_url' => site_url('newsletter/click/' . $newsletter_sent_id),
            'total_rows' => isset($clicks_list['data'][0]['count']) ? $clicks_list['data'][0]['count'] : 0,
            'per_page' => NEWSLETTERS_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);
        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit = array('limit' => NEWSLETTERS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);
        $data['offset'] = $limit_start;
 
        $clicks_list = $this->Newsletter_model->get_newsletter_clicks_list( $newsletter_sent_id, $limit );
        $data['clicks_list'] = $clicks_list['data'];
 
        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();
        $data['newsletter_templates'] = $newsletter_templates['rc'] ? $newsletter_templates['data'] : array();
 
        $data['clicks_list_view'] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/newsletter_stats";
        $data['title']  = set_title(lang('newsletter_stats'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }
 
    function sent( $newsletter_sent_id )
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $params = array( 'count_flag' => TRUE );
        $sent_list = $this->Newsletter_model->get_newsletter_sent_list( $newsletter_sent_id, $params );

        $config = array(
            'base_url' => site_url('newsletter/sent/' . $newsletter_sent_id),
            'total_rows' => isset($sent_list['data'][0]['count']) ? $sent_list['data'][0]['count'] : 0,
            'per_page' => NEWSLETTERS_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);
        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit = array('limit' => NEWSLETTERS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);
 
        $sent_list = $this->Newsletter_model->get_newsletter_sent_list( $newsletter_sent_id, $limit );
        $data['sent_list'] = $sent_list['data'];
 
        $data['offset'] = $limit_start;
 
        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();
        $data['newsletter_templates'] = $newsletter_templates['rc'] ? $newsletter_templates['data'] : array();
 
        $data['sent_list_view'] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/newsletter_stats";
        $data['title']  = set_title(lang('newsletter_stats'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }

    function unsubscribe_list($newsletter_sent_id)
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $params = array( 'count_flag' => TRUE );
        $unsubscribe_list = $this->Newsletter_model->get_newsletter_unsubscribe_list( $newsletter_sent_id, $params );
        
        $config = array(
            'base_url' => site_url('newsletter/unsubscribe_list/' . $newsletter_sent_id),
            'total_rows' => isset($unsubscribe_list['data'][0]['count']) ? $unsubscribe_list['data'][0]['count'] : 0,
            'per_page' => NEWSLETTERS_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);
        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit = array('limit' => NEWSLETTERS_PER_PAGE_LIMIT, 'limit_start' => $limit_start);

        $unsubscribe_list = $this->Newsletter_model->get_newsletter_unsubscribe_list( $newsletter_sent_id, $limit );
        $data['unsubscribe_list'] = $unsubscribe_list['data'];

        $data['offset'] = $limit_start;

        $newsletter_templates = $this->Newsletter_model->get_all_newsletter_templates();
        $data['newsletter_templates'] = $newsletter_templates['rc'] ? $newsletter_templates['data'] : array();

        $data['unsubscribe_list_view'] = TRUE;
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/newsletter_stats";
        $data['title']  = set_title(lang('newsletter_stats'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $data["_subactiveTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }

    // unuse function
    function update_newsletter_stats_event_via_sendgrid()
    {
        $data = file_get_contents("php://input");
        $events = json_decode($data, true);

        if( ! empty($events) )
        {
            foreach ($events as $e)
            {
                $mail_hash = isset($e['mail_hash']) ? $e['mail_hash'] : '';
                if( $mail_hash != '' )
                {
                    $this->Newsletter_model->update_newsletter_stats_event_via_sendgrid($e);
                }
            }
        }
    }

    function unsubscribe_users_listing()
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $unsubscribe_users = $this->Newsletter_model->get_unsubscribed_users_listing();
        $data['unsubscribe_users'] = $unsubscribe_users['data'];
        
        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/unsubscribe_users_listing";
        $data['title']  = set_title(lang('unsubscribe_users_listing'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }

    function build( $newsletter_template_id )
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $newsletter = $this->Newsletter_model->get_newsletter_template( $newsletter_template_id );

        if( ! $newsletter['rc'] )
        {
            $this->session->set_flashdata('error_msg', lang('invalid_data') );
            redirect('newsletter/template_listing'); exit;
        }
        else
        {
            $data['newsletter'] = $newsletter['data'];
        }

        $newsletter_zones = $this->Newsletter_model->get_all_newsletter_zones( $newsletter_template_id );
        $data['newsletter_zones'] = $newsletter_zones['data'];

        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["active"] = 'Newsletters';
        $data["_view"]  = "newsletters/build_newsletter_template";
        $data['title']  = set_title(lang('build_newsletter_template'));
        $data["_activeTab"] = ADMIN_TAB_8;
        $this->load->view("admin/basetemplate", $data);
    }

    function ajax_create_newsletter_zone()
    {
        if( $this->input->is_ajax_request() )
        {
            $newsletter_template_id = $this->input->post('newsletter_template_id');
            $zone_type = $this->input->post('zone_type');
            
            $this->form_validation->set_rules('zone_name','Name', 'trim|required');
            $this->form_validation->set_rules('zone_type','Zone type', 'required');
            
            if( $this->form_validation->run('zone_type') )
            {
                if( $zone_type == 4 )
                {
                    $this->form_validation->set_rules('zone_body','Zone body', 'required');
                }
                else
                {
                    // TO DO products
                    /*$this->form_validation->set_rules('channel_id','Channel', 'required');
                    $this->form_validation->set_rules('zone_cars[]','Zone cars', 'required');*/
                }
            }
            
            if( $this->form_validation->run() == TRUE )
            {
                $newsletter_zone_data = array(
                    'newsletter_template_id' => $newsletter_template_id,
                    'zone_name' => $this->input->post('zone_name'),
                    'zone_type' => $zone_type,
                    'zone_body' => $zone_type == 4 ? $this->input->post('zone_body') : '',
                    'added_on' => date('Y-m-d G:i:s'),
                    'modified_on' => date('Y-m-d G:i:s'),
                    'added_by' => $this->user['id']
                );

                $add_zone_result = $this->Newsletter_model->add_newsletter_zone($newsletter_zone_data);

                $newsletter_zone_id = $add_zone_result['id'];

                $response['newsletter_zone'] = $add_zone_result;
                
                /*if( $zone_type != 4 )
                {
                    $zone_cars = $this->input->post('zone_cars');
                    $newsletter_zone_cars_data = array();

                    foreach ( $zone_cars as $zc_key => $car_id )
                    {
                        // TO DO
                        $newsletter_zone_cars_data[] = array(
                            'newsletter_zone_id' => $newsletter_zone_id,
                            'car_id' => $car_id
                        );
                    }

                    $add_zone_products_result = $this->Newsletter_model->add_newsletter_zone_has_cars($newsletter_zone_cars_data);
                }*/

                $newsletter = $this->Newsletter_model->get_newsletter_template($newsletter_template_id);
                $data['newsletter'] = $newsletter['data'];

                $newsletter_zones = $this->Newsletter_model->get_all_newsletter_zones($newsletter_template_id);
                $data['newsletter_zones'] = $newsletter_zones['data'];
                $response['html_table_newsletter_zones'] = $this->load->view('newsletters/_table_newsletter_zones', $data, true );

                $response['rc'] = TRUE;
            }
            else
            {
                $response['rc'] = FALSE;
            }
            
            echo json_encode($response);
            exit;
        }
    }

    function delete_newsletter_zone()
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        $id = $this->input->post('newsletter_zone_id');
        $newsletter_template_id = $this->input->post('newsletter_template_id');
        
        $redirect_url = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'newsletter/'.$id;

        $result = $this->Newsletter_model->delete_newsletter_zone($id, $newsletter_template_id);

        if ($result['rc'])
        {
            $this->session->set_flashdata('success_msg', $result['msg']);
        }
        else
        {
            $this->session->set_flashdata('error_msg', $result['msg']);
        }

        redirect($redirect_url); exit;
    }

    function ajax_newsletter_zones_sort()
    {
        if( $this->input->is_ajax_request() )
        {
            $tableName = 'newsletter_template_has_zones';
                        
            if(sortable(explode(",", $this->input->post('data')),$tableName))
            {
                $data['status'] = TRUE;
                echo json_encode($data);
            }
            else
            {
                $data['status'] = FALSE;
                echo json_encode($data);
            }
        }
    }

    function ajax_get_newsletter_zone()
    {
        if( $this->input->is_ajax_request() )
        {
            $newsletter_zone_id = $this->input->post('newsletter_zone_id');
            $newsletter_zone = $this->Newsletter_model->get_newsletter_zone( $newsletter_zone_id );

            $newsletter = $this->Newsletter_model->get_newsletter_template( $newsletter_zone['data']['newsletter_template_id'] );
            $data['newsletter'] = $newsletter['data'];
            
            $response['newsletter_zone'] = $newsletter_zone;

            if( $newsletter_zone['rc'] )
            {
                $data['newsletter_zone'] = $newsletter_zone['data'];
                $response['html'] = $this->load->view('newsletters/_edit_newsletter_zone_modal', $data, true );
            }

            echo json_encode($response);
        }
    }

    function ajax_edit_newsletter_zone()
    {
        if( $this->input->is_ajax_request() )
        {
            $newsletter_zone_id = $this->input->post('newsletter_zone_id');
            $newsletter_template_id = $this->input->post('newsletter_template_id');

            $zone_type = $this->input->post('zone_type');

            $this->form_validation->set_rules('zone_name','Name', 'trim|required');
            $this->form_validation->set_rules('zone_type','Zone type', 'required');
            
            if( $this->form_validation->run('zone_type') )
            {
                if( $zone_type == 4 )
                {
                    $this->form_validation->set_rules('zone_body','Zone body', 'required');
                }
                else
                {
                    // TO DO
                    /*$this->form_validation->set_rules('zone_cars[]','Zone cars', 'required');*/
                }
            }

            if( $this->form_validation->run() == TRUE )
            {
                $newsletter_zone_data = array(
                    'newsletter_template_id' => $newsletter_template_id,
                    'zone_name' => $this->input->post('zone_name'),
                    'zone_type' => $zone_type,
                    'zone_body' => $zone_type == 4 ? $this->input->post('zone_body') : '',
                    'modified_on' => date('Y-m-d G:i:s')
                );

                $update_zone_result = $this->Newsletter_model->update_newsletter_zone($newsletter_zone_id, $newsletter_zone_data);

                $response['newsletter_zone'] = $update_zone_result;

                //TO Do
                /*$this->Newsletter_model->delete_newsletter_zone_cars($newsletter_zone_id);*/

                /*if( $zone_type != 4 )
                {
                    // TO DO
                    $zone_cars = $this->input->post('zone_cars');
                    $newsletter_zone_cars_data = array();

                    foreach ( $zone_cars as $zc_key => $car_id )
                    {
                        $newsletter_zone_cars_data[] = array(
                            'newsletter_zone_id' => $newsletter_zone_id,
                            'car_id' => $car_id
                        );
                    }

                    $add_zone_cars_result = $this->Newsletter_model->add_newsletter_zone_has_cars($newsletter_zone_cars_data);
                }*/

                $newsletter = $this->Newsletter_model->get_newsletter_template($newsletter_template_id);
                $data['newsletter'] = $newsletter['data'];

                $newsletter_zones = $this->Newsletter_model->get_all_newsletter_zones($newsletter_template_id);
                $data['newsletter_zones'] = $newsletter_zones['data'];
                $response['html_table_newsletter_zones'] = $this->load->view('newsletters/_table_newsletter_zones', $data, TRUE);

                $response['rc'] = TRUE;
            }
            else
            {
                $response['rc'] = FALSE;
            }
            
            echo json_encode($response);
            exit;
        }
    }

    function ajax_email_preview()
    {
        if( $this->input->is_ajax_request() )
        {
            $newsletter_template_id = $this->input->post('newsletter_template_id');

            $newsletter = $this->Newsletter_model->get_newsletter_template($newsletter_template_id);
            $data['newsletter'] = $newsletter['data'];

            $newsletter_zones = $this->Newsletter_model->get_all_newsletter_zones($newsletter_template_id);
            $data['newsletter_zones'] = $newsletter_zones['data'];
            // TO DO
            $car_ids = array();

            /*foreach( $newsletter_zones['data'] as $nz )
            {
                $zone_cars = explode(',', $nz['zone_cars']);
                foreach ($zone_cars as $key => $car_id)
                {
                    if( ! in_array($car_id, $car_ids) )
                        $car_ids[] = $car_id;
                }
            }*/

            /*$zone_cars = ( ! empty($car_ids)) ? $this->Car_model->get_all_cars_for_newsletter($car_ids) : array();

            $data['zone_cars'] = ( ! empty($car_ids)) ? $zone_cars['data'] : array();*/

            $url = site_url(); 
            $_view = 'email_templates/send_newsletter'; 
            $basetemplate = 'email_templates/baseTemplate';

            //TO DO
            /*if( ! empty($zone_cars) )
            {
                foreach ($zone_cars['data'] as $c_key => $c_value)
                {      
                    $array['zone_cars'][$c_value['id']] = $c_value;
                        $finance = $this->Finance_model->get_car_finance_data_by_car_details($c_value);
                    
                        if($finance['rc'])
                            $array['zone_cars'][$c_value['id']]['monthly_payment'] = $finance['data']['monthly_payment'];
                }
            }*/

            $array['newsletter'] = $newsletter['data'];

            $array['newsletter_zones'] = $newsletter_zones['data']; 
            $array['language_code'] = $this->session->userdata('lang');
            $array['url'] = $url;
            $array['_view'] = $_view;
            $email_html = $this->load->view($basetemplate, $array, TRUE);

            $unsubscription_text = '<table style="width:100%;"><tr><td style="text-align:center;">
            <a class="newsletter-unsubscription-link" href="javascript:void(0)" data-href="' . $url.$array['language_code'].'/" style="font-size:11px; text-align:center;">' . lang('unsubscribe_me_please') . '</a><tr><td></table>';

            
            $email_html .= $unsubscription_text;

            $response['html'] = $email_html;

            echo json_encode($response);
            exit;
        }
    }

    function ajax_send_newsletter()
    {
        if( $this->input->is_ajax_request() )
        {
            ini_set('memory_limit', '2048M');
            set_time_limit(400);
            $groups = $this->input->post('groups');
            $group_ids = is_array($groups) && ! empty($groups) ? implode(',', $groups) : '';
            
            $user_ids = $this->input->post('user_ids');

            $newsletter_template_id = $this->input->post('newsletter_template_id');

            $newsletter = $this->Newsletter_model->get_newsletter_template($newsletter_template_id);
            $data['newsletter'] = $newsletter['data'];
            
            $from_email = $this->input->post('from_email');
            $subject = $this->input->post('subject');
            $users = $this->Group_model->get_users_for_newsletter($group_ids, $user_ids);

            $response['users']['rc'] = $users['rc'];
            if( $users['rc'] )
            {

            }
            else
            {
                $response['error_users'] = lang('please_select_atleast_one_user_or_one_group') . ' '. lang('selected_users_or_groups_has_unsubscribed_to_newsletters');
            }

            $this->form_validation->set_rules('from_email','From email', 'trim|required|valid_email');
            $this->form_validation->set_rules('subject','Subject', 'trim|required');
            $this->form_validation->set_rules('newsletter_template_id','Newsletter', 'required');
            
            if( $this->form_validation->run() == TRUE && $users['rc'] )
            {
                $newsletter_zones = $this->Newsletter_model->get_all_newsletter_zones($newsletter_template_id);
                $data['newsletter_zones'] = $newsletter_zones['data'];
                $car_ids =array();
                //TO DO 
                /*foreach( $newsletter_zones['data'] as $nz )
                {
                    $zone_cars = explode(',', $nz['zone_cars']);
                    foreach ($zone_cars as $key => $car_id)
                    {
                        if( ! in_array($car_id, $car_ids) )
                            $car_ids[] = $car_id;
                    }
                }*/

                /*$zone_cars = ( ! empty($car_ids)) ? $this->Car_model->get_all_cars_for_newsletter($car_ids) : array();

                $data['zone_cars'] = ( ! empty($car_ids)) ? $zone_cars['data'] : array();*/

                $url = site_url(); 
                $_view = 'email_templates/send_newsletter'; 
                $basetemplate = 'email_templates/baseTemplate';


                // TO DO
                /*if( ! empty($zone_cars) )
                {
                    foreach ($zone_cars['data'] as $c_key => $c_value)
                    {      
                        $array['zone_cars'][$c_value['id']] = $c_value;
                            $finance = $this->Finance_model->get_car_finance_data_by_car_details($c_value);
                        
                            if($finance['rc'])
                                $array['zone_cars'][$c_value['id']]['monthly_payment'] = $finance['data']['monthly_payment'];
                    }
                }*/

                $array['newsletter'] = $newsletter['data'];

                $array['newsletter_zones'] = $newsletter_zones['data']; 
                $array['language_code'] = $this->session->userdata('lang');
                $array['url'] = $url;
                $array['_view'] = $_view;
                $email_html = $this->load->view($basetemplate, $array, TRUE);

                $from_name = "";
                $tag = "send_newsletter";
                $body = $email_html;

                $unsubscription_text = '<table style="width:100%;"><tr><td style="text-align:center;">
                <a class="newsletter-unsubscription-link" href="javascript:void(0)" data-href="' . $url.$array['language_code'].'/" style="font-size:11px; text-align:center;">' . lang('unsubscribe_me_please') . '</a><tr><td></table>';

                $body .= $unsubscription_text;

                if( $this->input->post('is_email_preview') && $this->input->post('is_email_preview') == 1 )
                {
                    $response['rc'] = TRUE;
                    $response['is_email_preview']['rc'] = TRUE;
                    $response['email_html'] = $body;
                    echo json_encode($response);
                    exit;
                }

                $unsubscription_text = '*|UNSUBSCRIBE_HREF_VALUE|*';

                $newsletter_data = array(
                    'subject' => $subject,
                    'from_email' => $from_email,
                    'html' => $email_html . $unsubscription_text,
                    'added_on' => date('Y-m-d G:i:s'),
                    'added_by' => $this->user['id']
                );

                $add_result_id = $this->Newsletter_model->add_sent_newsletter($newsletter_data);
                $newsletter_sent_id = $add_result_id;

                foreach($groups as $group_id)
                {
                    $newletter_sent_groups_relation = array(
                        'newsletter_sent_id' => $add_result_id,
                        'group_id' => $group_id
                    );

                    $this->Newsletter_model->add_newletter_sent_groups_relation($newletter_sent_groups_relation);
                }

                $email_stats_array = array();
                    
                foreach ($users['data'] as $u ) {
                    
                    if( ( trim($u['subscription_key']) == '' || is_null($u['subscription_key']) || strtotime('+1 day', strtotime($u['subscription_key_created_on']) ) < time() ) && $u['type'] == 'group')
                    {
                        $u['subscription_key'] = md5(microtime().rand());
                        $this->Newsletter_model->update_newsletter_subscription_key( $u['u_id'], $u['type'], $u['subscription_key']);
                    }

                    $unsubscription_text = '';

                    if($u['type'] == 'group')
                    {
                        $unsubscription_text = '<table style="width:100%;"><tr><td style="text-align:center;">
                        <a class="newsletter-unsubscription-link" href="' . $url.$array['language_code'].'/newsletter_unsubscription/' . $u['u_id'] . '/' . $newsletter_sent_id . '/' . $u['subscription_key'] . '" data-href="' . $url.$array['language_code'].'/" style="font-size:11px; text-align:center;">' . lang('unsubscribe_me_please') . '</a><tr><td></table>';
                    }

                    $html = $unsubscription_text;

                    $to_email = $u['email'];
                    $to_name = $u['user_name'];

                    $email_stats_array[] = array(
                        "tag"             =>  $tag,
                        "subject"         =>  $subject,
                        "body"            =>  (string)$html,
                        "status"          =>  'email_added',
                        "from_email"      =>  $from_email,
                        "to_email"        =>  $to_email,
                        "to_name"         =>  $to_name,
                        "from_name"       =>  $from_name,
                        "added_on"        =>  time(),
                        "vendor_type"     =>  EMAIL_VENDOR_MANDRILL,
                        "newsletter_sent_id" => $newsletter_sent_id
                    );
                }

                $this->load->model('Newsletter_credit_model');
                $credits = $this->Newsletter_credit_model->get_all_credits();

                $total_credits = $credits['rc'] ? ($credits['data']['free_credits_available'] + $credits['data']['credits_available'] ) : 0;

                if( count($email_stats_array) < $total_credits )
                {
                    $credits_used = count($email_stats_array);
                    $credits_consumed = array(
                        'user_id' => $this->user['id'],
                        'credits_amount' => $credits_used,
                        'credits_available' => $total_credits - $credits_used,
                        'consumed_date' => date('Y-m-d G:i:s')
                    );

                    $free_credits_available = $credits['data']['free_credits_available'] - $credits_used;
                    $credits_available = $free_credits_available < 0 ? ($total_credits - $credits_used) : $credits['data']['free_credits_available'];

                    $credits_update = array(
                        'free_credits_available' => $free_credits_available < 0 ? 0 : $free_credits_available,
                        'credits_available' => $credits_available,
                        'credits_used' => $credits['data']['credits_used'] + $credits_used
                    );

                    $credits_result = $this->Newsletter_credit_model->update_newsletter_credits_consumed($credits_consumed, $credits_update);

                    if( $credits_result )
                    {
                        // $email_stats_result = $this->Newsletter_model->add_email_stats_batch($email_stats_array);

                        $email_stats_array_chunk = array_chunk($email_stats_array, 10000);

                        if(is_array($email_stats_array_chunk) && count($email_stats_array_chunk) > 0)
                        {
                            foreach ($email_stats_array_chunk as $chunk_key => $email_stats_arr)
                            {
                                $email_stats_result = $this->Newsletter_model->add_email_stats_batch($email_stats_arr);
                            }
                        }

                        $response['rc'] = TRUE;
                        $response['msg'] = lang('email_sent_successfully');
                    }
                    else
                    {
                        $response['rc'] = FALSE;
                        $response['credits']['rc'] = FALSE;
                        $response['msg'] = lang('error_occured_please_try_again');
                    }
                }
                else
                {
                    $response['rc'] = FALSE;
                    $response['credits']['rc'] = FALSE;
                    $response['msg'] = lang('you_dont_have_enough_credits_to_send_newsletters_please_update_credits');
                }
            }
            else
            {
                $response['rc'] = FALSE;
                $response['error']['from_email'] = form_error('from_email');
                $response['error']['subject'] = form_error('subject');
            }

            echo json_encode($response);
            exit;
        }
    }

    function ajax_get_groups_and_users()
    {
        if( $this->input->is_ajax_request() )
        {
            $channel_id = $this->input->post('channel_id');

            $groups = $this->Group_model->get_all_groups_has_users();
            $data['groups_has_users'] = $groups['rc'] ? $groups['data'] : array();

            $params = array('type'=>'frontend_user');

            $users = $this->User_model->get_all_users($params);
            $data['users'] = $users['data'];
            
            // TO DO users

            $response['html'] = $this->load->view('newsletters/_send_newsletter_select_groups_and_users', $data, TRUE);
            echo json_encode($response);
            exit;
        }
    }

    function ajax_send_test_newsletter()
    {
        if( $this->input->is_ajax_request() )
        {
            $from_email = $this->input->post('from_email');
            $to_email = $this->input->post('to_email'); 
            $subject = WEBSITE_NAME . ' : Newsletter';

            $this->form_validation->set_rules('from_email','From email', 'trim|required|valid_email');
            $this->form_validation->set_rules('to_email','To email', 'trim|required|valid_email');
            $this->form_validation->set_rules('newsletter_template_id','Newsletter', 'required');
            
            if( $this->form_validation->run() == TRUE )
            {
                $newsletter_template_id = $this->input->post('newsletter_template_id');
                
                $newsletter = $this->Newsletter_model->get_newsletter_template( $newsletter_template_id );
                $data['newsletter'] = $newsletter['data'];

                $newsletter_zones = $this->Newsletter_model->get_all_newsletter_zones( $newsletter_template_id );
                $data['newsletter_zones'] = $newsletter_zones['data'];
                //$car_ids =array();

                // TO DO product
                /*foreach( $newsletter_zones['data'] as $nz )
                {
                    $zone_cars = explode(',', $nz['zone_cars']);
                    foreach ($zone_cars as $key => $car_id)
                    {
                        if( ! in_array($car_id, $car_ids) )
                            $car_ids[] = $car_id;
                    }
                }*/

                /*$zone_cars = ( ! empty($car_ids)) ? $this->Car_model->get_all_cars_for_newsletter($car_ids) : array();
                $data['zone_cars'] = ( ! empty($car_ids)) ? $zone_cars['data'] : array();*/

                $url = site_url(); 
                $_view = 'email_templates/send_newsletter'; 
                $basetemplate = 'email_templates/baseTemplate';

                // TO DO
                /*if( ! empty($zone_cars) )
                {
                    foreach ($zone_cars['data'] as $c_key => $c_value)
                    {      
                        $array['zone_cars'][$c_value['id']] = $c_value;
                            $finance = $this->Finance_model->get_car_finance_data_by_car_details($c_value);
                        
                        if($finance['rc'])
                            $array['zone_cars'][$c_value['id']]['monthly_payment'] = $finance['data']['monthly_payment'];
                    }
                }*/

                $array['newsletter'] = $newsletter['data'];
                $array['newsletter_zones'] = $newsletter_zones['data']; 
                $array['language_code'] = $this->session->userdata('lang');
                $array['url'] = $url;
                $array['_view'] = $_view;
                $email_html = $this->load->view($basetemplate, $array, TRUE);

                $from_name = "";
                $tag = "send_newsletter_preview";
                $body = $email_html;
                $email_html = $body;
                $to_name = '';

                $email_data = array(
                    "tag"             =>  $tag,
                    "subject"         =>  $subject,
                    "body"            =>  $email_html,
                    "status"          =>  'email_added',
                    "from_email"      =>  $from_email,
                    "to_email"        =>  $to_email,
                    "to_name"         =>  $to_name,
                    "from_name"       =>  $from_name,
                    "added_on"        =>  time(),
                    "vendor_type"     =>  EMAIL_VENDOR_MANDRILL
                );

                $email_stats_add_result_id = $this->Email_model->add_email_stats($email_data);

                $response['rc'] = TRUE;
                $response['msg'] = lang('email_sent_successfully');
            }
            else
            {
                $response['rc'] = FALSE;
                $response['error']['from_email'] = form_error('from_email');
                $response['error']['subject'] = form_error('subject');
            }
            echo json_encode($response);
            exit;
        }
    }

    function view($newsletter_template_id)
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');
        // $newsletter_template_id = $this->input->post('newsletter_template_id');

        $newsletter = $this->Newsletter_model->get_newsletter_template($newsletter_template_id);
        $data['newsletter'] = $newsletter['data'];

        $newsletter_zones = $this->Newsletter_model->get_all_newsletter_zones($newsletter_template_id);
        $data['newsletter_zones'] = $newsletter_zones['data'];

        $url = site_url(); 
        $_view = 'email_templates/send_newsletter'; 
        $basetemplate = 'email_templates/baseTemplate';

        $array['newsletter'] = $newsletter['data'];

        $array['newsletter_zones'] = $newsletter_zones['data']; 
        $array['language_code'] = $this->session->userdata('lang');
        $array['url'] = $url;
        $array['_view'] = $_view;
        $email_html = $this->load->view($basetemplate, $array, TRUE);

        $unsubscription_text = '<table style="width:100%;"><tr><td style="text-align:center;">
        <a class="newsletter-unsubscription-link" href="javascript:void(0)" data-href="' . $url.$array['language_code'].'/" style="font-size:11px; text-align:center;">' . lang('unsubscribe_me_please') . '</a><tr><td></table>';

        
        $email_html .= $unsubscription_text;

        $response['html'] = $email_html;
        $this->load->view('newsletters/template/view', $response);
    }

    function v($newsletter_sent_id)
    {
        $this->authenticate();
        $this->permissions->authorize('NEWSLETTER_MODULE');

        $newsletter = $this->Newsletter_model->get_newsletter_sent($newsletter_sent_id);

        if(! $newsletter['rc'])
        {
            redirect('newsletter/stats'); exit;
        }

        $email_html = str_replace('*|UNSUBSCRIBE_HREF_VALUE|*', '<table style="width:100%;"><tr><td style="text-align:center;">
        <a class="newsletter-unsubscription-link" href="javascript:void(0)" style="font-size:11px; text-align:center;">' . lang('unsubscribe_me_please') . '</a><tr><td></table>', $newsletter['data']['html']);

        $response['html'] = $email_html;
        $this->load->view('newsletters/template/view', $response);
    }
}