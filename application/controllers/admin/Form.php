<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends AdminController {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Form_model','Email_model'));
    }

    function listing()
    {
        $this->authenticate();
        $forms = $this->Form_model->get_all_forms();
        $data['forms'] = $forms['data'];
        $data['header'] = TRUE;
        $data['sidebar'] = TRUE;
        $data['_view']  = 'form/listing';
        $data['footer'] = TRUE;
        $data['_activeTab'] = ADMIN_TAB_3;
        $this->load->view('admin/basetemplate', $data);
    }

    function add()
    {
        $this->authenticate();
        $this->form_validation->set_rules('title','Title','required|xss_clean|trim');
        $this->form_validation->set_rules('fieldType','Field','required');
        $this->form_validation->set_message('required', lang('required_msg'));
        
        if( $this->form_validation->run() )
        {
            redirect("form/listing/".$id);
        }
        else
        {
            $data['header'] = TRUE;
            $data['sidebar'] = TRUE;
            $data['_view']  = 'form/add';
            $data['footer'] = TRUE;
            $data['_activeTab'] = ADMIN_TAB_3;
            $this->load->view('admin/basetemplate', $data);
        }
    }

    function edit($id)
    {
        $this->authenticate();
        $form = $this->Form_model->get_form($id);

        if( ! $form['rc'] )
        {
            redirect('admin/form/listing'); exit;
        }

        $data['form'] = $form['data'];

        $form_configuration = $this->Form_model->get_form_configuration($id);
        $data['configuration'] = $form_configuration['data'];


        $form_fields = $this->Form_model->get_form_fields($id);
        $data['form_fields'] = $form_fields['data'];
        //$data["entireForm"] = $this->Form_model->getEntireForm($id);
                           
        $tempArray = array();
        foreach($form_fields['data'] as $f_key => $f )
        {
            if( isset($f['field_id']) )
            {
                $form_field_options = $this->Form_model->get_form_field_options( $f['field_id'] );
                $f["options"] = $form_field_options['rc'] ? $form_field_options['data'] : array();
            }
                
                $tempArray[] = $f;
        }
        $data['form_fields'] = $tempArray;//pr($data['form_fields']);exit;
        //pr($data); exit;
        $data['header'] = TRUE;
        $data['sidebar'] = TRUE;
        $data['_view']  = 'form/edit';
        $data['footer'] = TRUE;
        $data['_activeTab'] = ADMIN_TAB_3;
        $this->load->view('admin/basetemplate', $data);
    }

    function delete()
    {
        $this->authenticate();
        $form_id = $this->input->post('form_id');

        // TO DO check dependency

        $delete_result = $this->Form_model->delete_form( $form_id );
            
        if($delete_result['rc'])
        {
              $this->session->set_flashdata('success', $delete_result['msg']);
        }     
        else
        {
            $this->session->set_flashdata('error', $delete_result['msg']); 
        }

        redirect('admin/form/listing');
    }

    function ajax_add_form()
    {
        if($this->input->is_ajax_request())
        {
            $array = $_POST;
            /*$form_data = array(
                'title' => $array['form'][0], 
                'website_id' => $array['form'][1], 
                'added_on' => date('Y-m-d G:i:s'),
                'added_by' => $this->user['id'],
                'modified_on' => date('Y-m-d G:i:s')
            );

            if(isset($array['form'][2]))
            {
                foreach($array['form'][2] as $fields)
                {
                    $form_fields = array(
                        'field_type' => $fields[0], 
                        'label' => $fields[1], 
                        'is_required' => $fields[2], 
                        'form_id'=> $form_id
                    );

                    $field_has_option_data = array(
                        'option_value' => $options, 
                        'field_id'=> $field_id
                    );
                }
            }*/

            $response = $this->Form_model->add_form($_POST);

            $this->session->set_flashdata('success', $response['msg']);
            echo json_encode($response);
        }
    }

    function ajax_update_form()
    {
        if( $this->input->is_ajax_request() )
        {
            $website_id = $_POST['form'][1];
            $form_id = $_POST['form'][2];
            
            $response = $this->Form_model->update_form( $form_id, $_POST );
            
            $this->session->set_flashdata('success', $response['msg']);
            echo json_encode($response);
            exit;
        }
    }

    function configuration( $form_id )
    {
        $this->authenticate();
        $form = $this->Form_model->get_form($form_id);

        if( ! $form['rc'] )
        {
            redirect('admin/form/listing'); exit;
        }

        $data['form'] = $form['data'];

        $configArray = $this->input->post();
        $configArray['form_id'] = $form_id;
        
        $success = $this->Form_model->add_form_configuration($configArray);
        
        if($success == "updated")
        {                        
            $this->session->set_flashdata('success',lang('configuration_update_success'));
        }
        else if($success == "added")
        {
            $this->session->set_flashdata('success', lang('configuration_add_success'));
        }
        else
        {
            $this->session->set_flashdata('error', lang('configuration_add_error'));
        }
        redirect("admin/form/listing/"); exit;
    }

    function ajax_get_form_html()
    {
        if($this->input->is_ajax_request())
        {
            if($this->is_logged_in())
            {
                $form_id = $this->input->post('form_id');
                $form = $this->Form_model->get_form($form_id);

                if( ! $form['rc'] )
                {
                    $response['rc'] = FALSE;
                    $response['form']['rc'] = FALSE;
                    $response['msg'] = lang('invalid_data');
                }
                else
                {
                    $data['form'] = $form['data'];

                    $form_configuration = $this->Form_model->get_form_configuration($form_id);
                    $data['configuration'] = $form_configuration['data'];


                    $form_fields = $this->Form_model->get_form_fields($form_id);
                    $data['form_fields'] = $form_fields['data'];
                                       
                    $tempArray = array();
                    foreach($form_fields['data'] as $f_key => $f )
                    {
                        if( isset($f['field_id']) )
                        {
                            $form_field_options = $this->Form_model->get_form_field_options( $f['field_id'] );
                            $f["options"] = $form_field_options['rc'] ? $form_field_options['data'] : array();
                        }
                            
                            $tempArray[] = $f;
                    }

                    $data['form_fields'] = $tempArray;

                    $response['rc'] = TRUE;
                    $response['html_code'] = $this->load->view('form/_html_code', $data, TRUE);
                }
            }
            else
            {
                $response['rc'] = FALSE;
                $response['login']['rc'] = FALSE;
                $response['msg'] = lang('your_session_is_expired_please_log_in_again');
            }

            echo json_encode($response);
        }
    }

    function ajax_get_form()
    {
        if($this->input->is_ajax_request())
        {
            $form_id = $this->input->post('form_id');
            $form = $this->Form_model->get_form($form_id);
            $post_data = json_decode(html_entity_decode($this->input->post('post_data')), TRUE);
            
            $this->form_validation->set_data($post_data);

            if( ! $form['rc'] )
            {
                $response['rc'] = FALSE;
                $response['form']['rc'] = FALSE;
                $response['msg'] = lang('invalid_data');
            }
            else
            {
                $data['form'] = $form['data'];

                $form_configuration = $this->Form_model->get_form_configuration($form_id);
                $data['configuration'] = $form_configuration['data'];


                $form_fields = $this->Form_model->get_form_fields($form_id);
                $data['form_fields'] = $form_fields['data'];
                                   
                $tempArray = array();
                foreach($form_fields['data'] as $f_key => $f )
                {
                    if( isset($f['field_id']) )
                    {
                        $form_field_options = $this->Form_model->get_form_field_options( $f['field_id'] );
                        $f["options"] = $form_field_options['rc'] ? $form_field_options['data'] : array();
                    }
                        
                    $tempArray[] = $f;

                    $rule = '';

                    if($f['is_required'] == 'Yes')
                    {
                        $rule = '|required';
                    }

                    if($f['field_type'] == FORM_FIELD_TYPE_DATE)
                    {
                        $rule .= '|callback_is_valid_date';
                    }

                    if($f['field_type'] == FORM_FIELD_TYPE_EMAIL)
                    {
                        $rule .= '|valid_email';
                    }

                    $name = str_replace('-', '_', $f['shortcode']);
                    $name = $f['field_type'] == FORM_FIELD_TYPE_CHECKBOX ? $name . '[]' : $name;
                    $this->form_validation->set_rules($name, $f['label'], 'trim|xss_clean' . $rule);
                    
                }

                if(count($post_data) > 0)
                {
                    $this->form_validation->run();
                }

                $data['post_data'] = $post_data;
                $data['form_fields'] = $tempArray;

                $response['rc'] = TRUE;
                $response['html_code'] = $this->load->view('form/_html_code', $data, TRUE);
            }

            echo json_encode($response);
        }
    }

    function ajax_get_form_fields()
    {
        if($this->input->is_ajax_request())
        {
            if($this->is_logged_in())
            {
                $form_id = $this->input->post('form_id');
                $form_type = $this->input->post('form_type');

                if($form_type == 'predefined-email-template')
                {
                    $predefined_shortcodes = array(
                        'registration-email-to-admin-NL' => array(
                            array(
                                'label' => 'name',
                                'shortcode' => 'name'
                            ),
                            array(
                                'label' => 'email',
                                'shortcode' => 'email'
                            )
                        ),
                        'registration-email-to-admin-FR' => array(
                            array(
                                'label' => 'name',
                                'shortcode' => 'name'
                            ),
                            array(
                                'label' => 'email',
                                'shortcode' => 'email'
                            )
                        ),
                        'registration-email-to-user-NL' => array(
                            array(
                                'label' => 'login url',
                                'shortcode' => 'login-url'
                            ),
                             array(
                                'label' => 'name',
                                'shortcode' => 'name'
                            ),

                        ),
                        'registration-email-to-user-FR' => array(
                            array(
                                'label' => 'login url',
                                'shortcode' => 'login-url'
                            ),
                             array(
                                'label' => 'name',
                                'shortcode' => 'name'
                            ),

                        ),
                        'reset-password-email-to-user-NL' => array(
                            array(
                                'label' => 'reset password link',
                                'shortcode' => 'reset_password_link'
                            ),
                            array(
                                'label' => 'user name',
                                'shortcode' => 'user_name'
                            ),
                            array(
                                'label' => 'user email',
                                'shortcode' => 'user_email'
                            )
                        ),
                        'reset-password-email-to-user-FR' => array(
                            array(
                                'label' => 'reset password link',
                                'shortcode' => 'reset_password_link'
                            ),
                            array(
                                'label' => 'user name',
                                'shortcode' => 'user_name'
                            ),
                            array(
                                'label' => 'user email',
                                'shortcode' => 'user_email'
                            )
                        ),
                        'credit-purchase-email-to-admin-NL'=>array(
                           array(
                                'label' => 'first name',
                                'shortcode' => 'first_name'
                            ),
                            array(
                                'label' => 'last name',
                                'shortcode' => 'last_name'
                            ),
                            array(
                                'label' => 'number of credits',
                                'shortcode' => 'number_of_credits'
                            ),
                            array(
                                'label' => 'submited on',
                                'shortcode' => 'submited_on'
                            )
                        ),
                        'credit-purchase-email-to-admin-FR'=>array(
                            array(
                                'label' => 'first name',
                                'shortcode' => 'first_name'
                            ),
                            array(
                                'label' => 'last name',
                                'shortcode' => 'last_name'
                            ),
                            array(
                                'label' => 'number of credits',
                                'shortcode' => 'number_of_credits'
                            ),
                            array(
                                'label' => 'submited on',
                                'shortcode' => 'submited_on'
                            )
                        ),
                        'newsletter-subscription-email-to-user-NL'=>array(
                            array(
                                'label' => 'cancel subscription link',
                                'shortcode' => 'cancel_subscription_link'
                            )
                        ),
                        'newsletter-subscription-email-to-user-FR'=>array(
                            array(
                                'label' => 'cancel subscription link',
                                'shortcode' => 'cancel_subscription_link'
                            )
                        )
                    );

                    if(isset($predefined_shortcodes[$form_id]))
                    {
                        $data['form_fields'] = $predefined_shortcodes[$form_id];
                        $response['form_fields'] = $predefined_shortcodes[$form_id];
                        $response['html'] = $this->load->view('form/shortcodes', $data, TRUE);
                        $response['rc'] = TRUE;
                        $response['msg'] = '';
                    }
                    else
                    {
                        $response['rc'] = FALSE;
                        $response['msg'] = lang('invalid_data');
                    }
                }
                else
                {
                    $form = $this->Form_model->get_form($form_id);

                    if($form['rc'])
                    {
                        $form_fields = $this->Form_model->get_form_fields($form_id);
                        $data['form_fields'] = $form_fields['data'];
                        $response['form_fields'] = $form_fields['data'];
                        $response['html'] = $this->load->view('form/shortcodes', $data, TRUE);
                        $response['rc'] = TRUE;
                        $response['msg'] = '';
                    }
                    else
                    {
                        $response['rc'] = FALSE;
                        $response['msg'] = lang('invalid_data');
                    }
                }
            }
            else
            {
                $response['login']['rc'] = FALSE;
                $response['msg'] = lang('your_session_is_expired_please_log_in_again');
            }

            echo json_encode($response);
        }
    }

    function frontend_forms()
    {
        $this->authenticate();
        $params = array();
        $forms = $this->Form_model->get_frontend_forms( $params );

        $config = array(
            'base_url' => site_url('admin/form/frontend_forms'),
            'total_rows' => $forms['rc'] ? count($forms['data']) : 0,
            'per_page' => FORMS_PER_PAGE_LIMIT,
            'uri_segment' => 4
        );
        $this->load->model('Pagination_model');
        $data['links'] = $this->Pagination_model->config($config);

        $limit_start = is_numeric($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit = array('limit' => FORMS_PER_PAGE_LIMIT, 'start' => $limit_start);

        $all_forms = $this->Form_model->get_frontend_forms($params, FALSE, $limit);
        $data['all_forms'] = $all_forms['data'];

        $data["header"] = TRUE;
        $data["footer"] = TRUE;
        $data["sidebar"] = TRUE;
        $data["_view"]  = "form/frontend_forms";
        $data['title']  = set_title('Frontend forms');
        $data["_activeTab"] = ADMIN_TAB_3;
        $this->load->view("admin/basetemplate", $data);
    }

    function ajax_get_frontend_form_list()
    {
        $form_id = $this->input->post('form_id');
        $id = $this->input->post('id');
        $params = array(
            'form_id'=>$form_id,
            'id'=>$id
            );

        $forms = $this->Form_model->get_frontend_forms_by_form_id($params, FALSE,'');

        if($forms['rc'])
        {
            $response["success"] = TRUE;
            $response["msg"] = "";
        }
        else
        {
            $response["success"] = FALSE;
            $response["msg"] = "";
        }

        $data['forms'] = $forms['data'];
        $response['forms'] = $this->load->view('form/_frontend_form_list', $data, TRUE);
        echo json_encode($response);
    }

    function is_valid_date($date)
    {
        if($date !="")
        {
            $parts = explode("-", $date);

            if (count($parts) == 3)
            {  
                if (checkdate($parts[1], $parts[0], $parts[2]))
                {
                     return TRUE;
                }
                 else
                {
                    
                    $this->form_validation->set_message('is_valid_date', '%s must be valid date.');
                    return FALSE;
                }
            }
            else
            {
                  $this->form_validation->set_message('is_valid_date', '%s must be a valid date.');
                 return FALSE;
            }
        }
        else
        {
            return TRUE;
        }
    }

    function ajax_add_frontend_forms()
    {
        if($this->input->is_ajax_request())
        {
            $url_params = $_SERVER['HTTP_REFERER'];
            $url = explode('/',$url_params);

            $form_id = $this->input->post('form_id');
            $form = $this->Form_model->get_form($form_id);
            $form_name = $this->input->post('form_name');

            $post_data = $this->input->post();
            //pr($post_data);
            $this->form_validation->set_data($post_data);

            if( ! $form['rc'] )
            {
                $response['rc'] = FALSE;
                $response['form']['rc'] = FALSE;
                $response['msg'] = lang('invalid_data');
            }
            else
            {
                $data['form'] = $form['data'];
                //pr($data['form']);exit;
                $data['url'] = $url[5];

                $form_configuration = $this->Form_model->get_form_configuration($form_id);
                $data['configuration'] = $form_configuration['data'];


                $form_fields = $this->Form_model->get_form_fields($form_id);
                $data['form_fields'] = $form_fields['data'];       
                $tempArray = array();
                foreach($form_fields['data'] as $f_key => $f )
                {
                    if( isset($f['field_id']) )
                    {
                        $form_field_options = $this->Form_model->get_form_field_options( $f['field_id'] );
                        $f["options"] = $form_field_options['rc'] ? $form_field_options['data'] : array();
                    }
                        
                    $tempArray[] = $f;

                    $rule = '';

                    if($f['is_required'] == 'Yes')
                    {
                        $rule = '|required';
                    }

                    if($f['field_type'] == FORM_FIELD_TYPE_DATE)
                    {
                        $rule .= '|callback_is_valid_date';
                    }

                    if($f['field_type'] == FORM_FIELD_TYPE_EMAIL)
                    {
                        $rule .= '|valid_email';
                    }

                    $name = str_replace('-', '_', $f['shortcode']);
                    $name = $f['field_type'] == FORM_FIELD_TYPE_CHECKBOX ? $name . '[]' : $name;
                    $this->form_validation->set_rules($name, $f['label'], 'trim|xss_clean' . $rule);
                    
                }
//exit;
                if(count($post_data) > 0)
                {
                    //var_dump($this->form_validation->run());echo validation_errors();exit;
                    if($this->form_validation->run())
                    {
                        $data['post_data'] = $post_data;
                        $data['form_fields'] = $tempArray;
                        $html_code = $this->load->view('form/_html_code_preview', $data, TRUE);

                        $form_data = array(
                            'form_id' => $form_id,
                            'form_data' => json_encode($html_code),
                            'added_on' => date('Y-m-d G:i:s')
                        );

                        $add_result = $this->Form_model->add_form_data($form_data);
                       //pr($add_result);exit;
                        $msg_label = $add_result['rc'] ? 'success' : 'error';

                        $form_configuration = $this->Form_model->get_form_configuration($form_id);

                        if($form_configuration['rc'])
                        {
                            $msg = $add_result['rc'] ? $form_configuration['data']['message_sent_successfully'] : $form_configuration['data']['error_sending_message'];
                        }
                        else
                        {
                            $msg = $add_result['msg'];
                        }
                        
                        $this->session->set_flashdata($msg_label, $msg);

                        if($add_result['rc'])
                        {
                            $post_data = array();
                            $where = array('form_id' => $form_id);
                            $emails = $this->Email_model->get_all_emails($where);
                            if($emails['rc'])
                            {
                                foreach ($emails['data'] as $e_key => $email)
                                {
                                    $email_data['email'] = $email;
                                    $email_data['form_data'] = $_POST;
                                    $email_data['form_fields'] = $form_fields['data'];
                                    $from_email = SEND_FROM_EMAIL_ID;
                                    $to_email = ADMIN_EMAIL; 
                                    $subject = $email['subject'];

                                    $from_name = "";
                                    $tag = $email['title'];
                                    $body = $this->load->view('email_module/email', $email_data, TRUE);
                                    $to_name = '';

                                    $email_data = array(
                                        "tag"             =>  $tag,
                                        "subject"         =>  $subject,
                                        "body"            =>  $body,
                                        "status"          =>  'email_added',
                                        "from_email"      =>  $from_email,
                                        "to_email"        =>  $to_email,
                                        "to_name"         =>  $to_name,
                                        "from_name"       =>  $from_name,
                                        "added_on"        =>  time(),
                                        "vendor_type"     =>  EMAIL_VENDOR_MANDRILL
                                    );

                                    $email_stats_add_result_id = $this->Email_model->add_email_stats($email_data);
                                }
                            }
                        }
                        $response['rc'] = TRUE;
                        $response['msg'] = "Your form has been successfully submitted"; 
                    }
                    else
                    {
                            $response['rc'] = FALSE;
                            $response['msg'] = "Error occurred"; 
                    }
                    $data['post_data'] = $post_data;
                    $data['form_fields'] = $tempArray;
                    
                    $response['html_code'] = $this->load->view('form/_html_code', $data, TRUE);
                }
                echo json_encode($response);
            }
        }
    }
}