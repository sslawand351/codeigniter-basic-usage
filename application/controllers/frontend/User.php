<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends BaseController {
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('User_model', 'Email_model'));
    }

    function dashboard()
    {
        $data["header"]   = TRUE;
        $data["footer"]   = TRUE;
        $data['_view']='frontend/user/dashboard';
        $this->load->view('layouts/frontend/basetemplate',$data);
    }

    function login()
    {   
        if(!$this->is_logged_in())
        {
            $this->form_validation->set_rules('email','Email','required|valid_email|trim');
            $this->form_validation->set_rules('password','Password','required|trim');


            if ($this->form_validation->run() == FALSE)
            {
                $data['header'] = TRUE;
                $data["_view"]  = "frontend/user/login";
                $this->load->view("layouts/frontend/basetemplate",$data);
            }
            else
            {
                $email      = $this->input->post('email');
                $password   = $this->input->post('password');
                $remember_me   = $this->input->post('remember_me');
                
                $response = $this->User_model->login($email, $password, $remember_me);

                if($response['rc'])
                {
                    redirect($this->session->userdata('flang').'/dashboard');
                }
                else
                {
                    $this->session->set_flashdata('error', $response['msg']);
                    redirect($this->session->userdata('flang').'/login');
                }
            }
        }
        else
        {
             redirect($this->session->userdata('flang').'/dashboard');
        }

    }


    function register()
    {
        $this->form_validation->set_rules('first_name','First Name','required');        
        $this->form_validation->set_rules('last_name','Last Name','required');        
        $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[users.email]');        
        $this->form_validation->set_rules('password','Password','required|matches[passconf]');
        $this->form_validation->set_rules('passconf','Confirm Password','required|matches[password]');       
        $this->form_validation->set_error_delimiters('<p class="red">','</p>');
        $this->form_validation->set_message('is_unique','Email '.$this->input->post('email').'is_already_registered');

        if($this->form_validation->run())
        {
            $super_admin = $this->User_model->get_users(SUPER_ADMIN);
            $language_id = $this->User_model->get_id_by_language_code($this->session->userdata('flang'));
            $params = array(
                'first_name'    => trim($this->input->post('first_name')),
                'last_name'     => trim($this->input->post('last_name')),
                'email'         => trim($this->input->post('email')),
                'password'      => md5($this->input->post('password')),
                'user_type'     => 'user',
                'added_on'      => date("Y-m-d G:i:s"),
                'modified_on'   => date("Y-m-d G:i:s"),
                'language_id'   => $language_id['data']['id']

            );
            $response = $this->User_model->register($params);
            $lang_code = $this->session->userdata('flang');

            if($response['rc'])
            {
                $where = array(
                    'form_name' => 'registration-email-to-admin-'.strtoupper($lang_code),
                    'form_type' => FORM_TYPE_PREDEFINED_EMAIL_TEMPLATE
                );
                $emails = $this->Email_model->get_all_emails($where);

                if($emails['rc'])
                {
                    foreach ($emails['data'] as $e_key => $email)
                    {
                        $email_data['email'] = $email;
                        $email_data['form_data'] = array(
                            'name' => $params['first_name'] .' '. $params['last_name'],
                            'email' => $params['email']
                        );
                        $email_data['form_fields'] = array();
                        $from_email = SEND_FROM_EMAIL_ID;
                        $to_email = SUPER_ADMIN_EMAIL;
                        $to_name = $super_admin['first_name']." ".$super_admin['last_name'];
                        $subject = $email['subject'];

                        $from_name = SEND_FROM_EMAIL_NAME;
                        $tag = $email['title'];
                        $body = $this->load->view('email_module/email', $email_data, TRUE);

                        $email_data = array(
                            "tag"             =>  $tag,
                            "subject"         =>  $subject,
                            "body"            =>  $body,
                            "status"          =>  'email_added',
                            "from_email"      =>  $from_email,
                            "to_email"        =>  $to_email,
                            "to_name"         =>  $to_name,
                            "from_name"       =>  $from_name,
                            "added_on"        =>  time(),
                            "vendor_type"     =>  EMAIL_VENDOR_MANDRILL
                        );

                        $email_stats_add_result_id = $this->Email_model->add_email_stats($email_data);
                    }
                }

                //email to admin
                // $email_body['data'] = $params;
                // $basetemplate = "emails/new_user_admin.php";
                // $html = $this->load->view($basetemplate, $email_body, true);
                // //new - mandrill
                // $to_email = $super_admin['email'];
                // $to_name = $super_admin['first_name']." ".$super_admin['last_name'];
                // $from_email = SEND_FROM_EMAIL_ID;
                // $from_name = SEND_FROM_EMAIL_NAME;
                // $subject = 'New User Update';
                // $tag = "new_user_has_been_added";
                // $body = $html;
                // //echo $html;
                // $email_data = array(
                //     "tag"             =>  $tag,
                //     "subject"         =>  $subject,
                //     "body"            =>  $body,
                //     "status"          =>  'email_added',
                //     "from_email"      =>  $from_email,
                //     "to_email"        =>  $to_email,
                //     "to_name"         =>  $to_name,
                //     "from_name"       =>  $from_name,
                //     "added_on"        =>  time()
                //     );

                // $this->Email_model->add_email_stats($email_data);

                $where = array(
                    'form_name' => 'registration-email-to-user-'.strtoupper($lang_code),
                    'form_type' => FORM_TYPE_PREDEFINED_EMAIL_TEMPLATE
                );
                $emails = $this->Email_model->get_all_emails($where);

                if($emails['rc'])
                {
                    foreach ($emails['data'] as $e_key => $email)
                    {
                        $email_data['email'] = $email;
                        $email_data['form_data'] = array(
                            'login_url' => site_url('user/login'),
                            'name'=>$params['first_name']." ".$params['last_name']
                        );
                        $email_data['form_fields'] = array();
                        $from_email = SEND_FROM_EMAIL_ID;
                        $to_email = $params['email'];
                        $to_name = $params['first_name']." ".$params['last_name'];
                        $subject = $email['subject'];

                        $from_name = SEND_FROM_EMAIL_NAME;
                        $tag = $email['title'];
                        $body = $this->load->view('email_module/email', $email_data, TRUE);

                        $email_data = array(
                            "tag"             =>  $tag,
                            "subject"         =>  $subject,
                            "body"            =>  $body,
                            "status"          =>  'email_added',
                            "from_email"      =>  $from_email,
                            "to_email"        =>  $to_email,
                            "to_name"         =>  $to_name,
                            "from_name"       =>  $from_name,
                            "added_on"        =>  time(),
                            "vendor_type"     =>  EMAIL_VENDOR_MANDRILL
                        );

                        $email_stats_add_result_id = $this->Email_model->add_email_stats($email_data);
                    }
                }

                // $basetemplate_user = "emails/new_user.php";
                // $html_user = $this->load->view($basetemplate_user, $email_body, true);
                // //new - mandrill
                // $to_email_user = $params['email'];
                // $to_name_user = $params['first_name']." ".$params['last_name'];
                // $from_email_user = SEND_FROM_EMAIL_ID;
                // $from_name_user = SEND_FROM_EMAIL_NAME;
                // $subject_user = 'Registration Successfull';
                // $tag_user = "registered_to_rcodebase";
                // $body_user = $html_user;
                // //echo $html;
                // $email_data_user = array(
                //     "tag"             =>  $tag_user,
                //     "subject"         =>  $subject_user,
                //     "body"            =>  $body_user,
                //     "status"          =>  'email_added',
                //     "from_email"      =>  $from_email_user,
                //     "to_email"        =>  $to_email_user,
                //     "to_name"         =>  $to_name_user,
                //     "from_name"       =>  $from_name_user,
                //     "added_on"        =>  time()
                //     );

                // $this->Email_model->add_email_stats($email_data_user);

                $first_name = $params['first_name'];
                redirect($this->session->userdata('flang').'/dashboard'); 
            }
            else
            {
                $this->session->set_flashdata('error',$response['msg']);
                redirect($this->session->userdata('flang').'/register'); 
            }
        }
        else
        {
            $data['header'] = TRUE;
            $data["_view"]  = "frontend/user/register";
            $this->load->view("layouts/frontend/basetemplate",$data);
        }
    } 

    function logout()
    {
        if($this->is_logged_in())
        {  
            if(isset($_COOKIE['user_rememberme']))
            {
                $this->User_model->delete_auth_token($this->session->userdata('user_id'));
                delete_cookie("user_rememberme");
            }
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('flanguage');
            $this->session->unset_userdata('flang');
        }

         redirect(site_url('frontend/User/login'));
    }

    function forgot_password()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        
        if($this->form_validation->run() == FALSE)
       {    
            $data["header"]   = TRUE;
            $data["footer"]   = TRUE;
            $data["_view"]    = "frontend/user/forgot_password";
            $this->load->view("layouts/frontend/basetemplate",$data);

        }
        else
        {
            $email = $this->input->post('email');
                    
            $this->load->helper('string');
            $resetKey = md5(random_string('sha1', 12) + time());
              
            $data = array(
                'password_reset_key' => $resetKey
            );
            $user_data = $this->User_model->get_user_by_email($email);
            $user = $user_data['data'];
            if($user_data['rc'])
            {   
                $reset = $this->User_model->set_password_reset_key($email, $data);
                if($reset)
                {   
                    $email_body['reset_link'] = site_url().'frontend/user/get_new_password/'.$resetKey;
                    $email_body['user_email'] = $email;
                    $email_body['_view'] = 'frontend/user/reset_password';
                    $html = $this->load->view('layouts/frontend/basetemplate', $email_body, true);
                    
                    //new - mandrill
                    $to_email = $user['email'];
                    $to_name = $user['first_name']." ".$user['last_name'];
                    $from_email = SEND_FROM_EMAIL_ID;
                    $from_name = SEND_FROM_EMAIL_NAME;
                    $subject = 'reset_pwd_email_subject';
                    $tag = "reset_password_email";
                    $body = $html;
                    //echo $html;
                    
                    $email_data = array(
                    "tag"             =>  $tag,
                    "subject"         =>  $subject,
                    "body"            =>  $body,
                    "status"          =>  'email_added',
                    "from_email"      =>  $from_email,
                    "to_email"        =>  $to_email,
                    "to_name"         =>  $to_name,
                    "from_name"       =>  $from_name,
                    "vendor_type"     =>  EMAIL_VENDOR_MANDRILL,
                    "added_on"        =>  time()
                    );
                    $this->Email_model->add_email_stats($email_data);
                    
                    $this->session->set_flashdata('success','Your reset password request has been registered. Please check your email for further instructions on the same.');
                }
                else
                {
                    echo "Failed to reset password";exit;
                }
                
                $data['email'] = $email;
                redirect($this->session->userdata('flang').'/forgot_password');
            }
            else
            {
                $this->session->set_flashdata('error','email does not exists in our database');
                redirect($this->session->userdata('flang').'/forgot_password');
            }  
        }
    }

    function get_new_password($hash='')
    {
        $this->form_validation->set_rules('password','Password','trim|required|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[password]');
        
        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);
        // pr($url[5]);exit;
        if($url[5] == '')
        {
            $this->session->set_flashdata('error','Invalid request');
            redirect('frontend/user/forgot_password');    
        }
        else
        {   
            $response = $this->User_model->check_password_reset_key($url[5]);
            // pr($response);
            // echo "hii";exit;
            if(!$response['rc'])
            {
                $this->session->set_flashdata('error',$response['msg']);
                redirect('user/forgot_password');
            }
        }
        $password = md5($this->input->post('password'));
     
        if($this->form_validation->run() == TRUE)
        {   
            $response = $this->User_model->update_password($password,$url[5]);
            // pr($response);exit;
            if($response['rc'])
            {
                $this->session->set_flashdata('success',$response['msg']);
                redirect($this->session->userdata('flang').'/login');
            }
            else
            {
                $this->session->set_flashdata('error',$response['msg']);
                redirect(site_url());
            }
        }
        else
        {   
            $data["header"]   = TRUE;
            $data["footer"]   = TRUE;
            $data["_view"]    = "frontend/user/get_new_password";
            $this->load->view("layouts/frontend/basetemplate",$data);
        }
    }

    function ajax_newsletter_subscription()
    {
        if( $this->input->is_ajax_request() )
        {
            $lang = $this->session->userdata('lang');
            $email = $this->input->post('email');

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_check_user_is_subscribed');

            if( $this->form_validation->run() === TRUE )
            {
                $this->load->model('Newsletter_model');

                $newsletter_subscriber_data = array('email_id' => $email, 'is_unsubscribe_to_newsletter' => 'yes');
                $newsletter_subscriber = $this->Newsletter_model->get_newsletter_subscriber( $newsletter_subscriber_data );

                $subscriber_data = array(
                        'email_id' =>$email,
                        'subscription_key' => md5(microtime().rand()),
                        'subscription_key_created_on' => date('Y-m-d G:i:s'),
                        'is_unsubscribe_to_newsletter' => 'no',
                        'added_on' => date('Y-m-d G:i:s')
                    );

                if( $this->is_logged_in() )
                {
                    $user = $this->User_model->get_users($this->user['id']);
                    $subscriber_data['user_name'] = $user['email'] == $subscriber_data['email_id'] ? $user['first_name'] . " " . $user['last_name'] : '';
                }
                else
                {
                    $subscriber_data['user_name'] = '';
                }

                if( $newsletter_subscriber['rc'] )
                {
                    $subscriber_data['id'] = $newsletter_subscriber['data']['id'];
                }

                $is_unsubscribed = $newsletter_subscriber['rc'];

                $add_result = $this->Newsletter_model->subscribe_to_newsletter( $subscriber_data, $is_unsubscribed );

                $response['form_error']['rc'] = FALSE;

                if( $add_result['rc'] )
                {
                    // $email_body['cancel_subscription_link'] = site_url( $lang . '/user/cancel_newsletter_subscription/'.$add_result['data']['id'] . '/' . $subscriber_data['subscription_key']);
                    // $email_body['user_email'] = $email;
                    // $email_body['_view'] = 'email_templates/newsletter_subscription';
                    // //$html = $this->load->view('emails/newsletter_subscription', $email_body, true);
                    // $html = $this->load->view('email_templates/baseTemplate', $email_body, true);
                    
                    // //new - mandrill
                    // $to_email = $email;
                    // $to_name = '';
                    // $from_email = SEND_FROM_EMAIL_ID;
                    // $from_name = '';
                    // $subject = WEBSITE_NAME .' - Newsletter Subscription';
                    // $tag = "newsletter_subscription_email";
                    // $body = $html;
                    
                    // $email_data = array(
                    //     "tag"             =>  $tag,
                    //     "subject"         =>  $subject,
                    //     "body"            =>  $body,
                    //     "status"          =>  'email_added',
                    //     "from_email"      =>  $from_email,
                    //     "to_email"        =>  $to_email,
                    //     "to_name"         =>  $to_name,
                    //     "from_name"       =>  $from_name,
                    //     "added_on"        =>  time()
                    // );

                    // $this->Email_model->add_email_stats($email_data);

                    $lang_code = $this->session->userdata('flang');

                    $where = array(
                        'form_name' => 'newsletter-subscription-email-to-user-'.strtoupper($lang_code),
                        'form_type' => FORM_TYPE_PREDEFINED_EMAIL_TEMPLATE
                    );
                    $emails = $this->Email_model->get_all_emails($where);

                    if($emails['rc'])
                    {
                        foreach ($emails['data'] as $e_key => $email)
                        {
                            $email_data['email'] = $email;

                            $email_data['form_data'] = array(
                                'cancel_subscription_link' => site_url( $lang_code . '/user/cancel_newsletter_subscription/'.$add_result['data']['id'] . '/' . $subscriber_data['subscription_key'])
                            );
                            $email_data['form_fields'] = array();
                            $from_email = SEND_FROM_EMAIL_ID;
                            $to_email = $email_input;
                            $to_name =  '';
                            $subject = $email['subject'];

                            $from_name = SEND_FROM_EMAIL_NAME;
                            $tag = $email['title'];
                            $body = $this->load->view('email_module/email', $email_data, TRUE);

                            $email_data = array(
                                "tag"             =>  $tag,
                                "subject"         =>  $subject,
                                "body"            =>  $body,
                                "status"          =>  'email_added',
                                "from_email"      =>  $from_email,
                                "to_email"        =>  $to_email,
                                "to_name"         =>  $to_name,
                                "from_name"       =>  $from_name,
                                "added_on"        =>  time(),
                                "vendor_type"     =>  EMAIL_VENDOR_MANDRILL
                            );

                            $email_stats_add_result_id = $this->Email_model->add_email_stats($email_data);
                        }
                    }

                    $response['rc'] = TRUE;
                    $response['msg'] = $add_result['msg'];
                }
                else
                {
                    $response['rc'] = FALSE;
                    $response['msg'] = $add_result['msg'];
                }               
            }
            else
            {
                $response['rc'] = FALSE;
                $response['form_error']['rc'] = TRUE;
                $response['form_error']['email'] = form_error('email');
            }
            //echo validation_errors(); exit;
            echo json_encode($response);
        }
    }

    function check_user_is_subscribed($email)
    {
        $this->load->model('Newsletter_model');
        $condition = array(
            'email_id' => $email,
            'is_unsubscribe_to_newsletter' => 'no'
        );
        $newsletter_subscriber = $this->Newsletter_model->get_newsletter_subscriber( $condition );

        if( $newsletter_subscriber['rc'] )
        {
            $this->form_validation->set_message('check_user_is_subscribed', 'You are already subscribed to our newsletter');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function cancel_newsletter_subscription($id, $key)
    {
        $this->load->model('Newsletter_model');

        $condition = array(
            'id' => $id,
            'subscription_key' => $key
        );
        $newsletter_subscriber = $this->Newsletter_model->get_newsletter_subscriber( $condition );

        if( $newsletter_subscriber['rc'] )
        {
            $this->User_model->unsubscribe_to_newsletter( $id, $newsletter_subscriber['data'] );
            $data['email'] = $newsletter_subscriber['data']['email_id'];
            
            $data['header'] = TRUE;
            $data["footer"]   = TRUE;
            $data["_view"]  = "newsletters/cancel_newsletter_subscription_success";
            $this->load->view("layouts/frontend/basetemplate", $data);
        }
        else
        {
            redirect($this->session->userdata('lang').'/home'); exit;
        }
    }

    function update_language()
    {
        $language_code = $this->input->post('language');
        $user_id = $this->input->post('user_id');
        $url = $this->input->post('url');
        $language_id = $this->User_model->get_id_by_language_code($language_code);
        $language = $this->User_model->get_language_by_id($language_id['data']['id']);
       
        $new_url = explode('/',$url);
        $new_url[4] = $language_code;
        $final_url = implode('/', $new_url);
        if($this->is_logged_in())
        {
           $result = $this->User_model->update_user_language($language,$user_id);
            redirect($final_url);exit; 
        }
        else
        {
            $flang = $language_code;
            $this->session->set_userdata('flang',$flang);
            redirect($final_url);exit; 

        }
      
    }

    function newsletter_unsubscription( $id, $newsletter_sent_id, $subscription_key )
    {
        $this->load->model('Newsletter_model');

        $response = $this->Newsletter_model->unsubscribe_to_newsletter( $id, $newsletter_sent_id, $subscription_key );
        
        if( $response['rc'] )
        {
            $data['email'] = $response['data']['email'];
        
            $data['url_link_segment'] = substr($this->uri->uri_string(), 3);
            $data["inner_header"]   = TRUE;
            $data["hide_bottom_wrapper"] = TRUE;
            $data["footer"] = TRUE;
            $data["_view"] = "newsletters/cancel_newsletter_subscription_success";
            $this->load->view("layouts/frontend/basetemplate", $data);
        }
        else
        {
            redirect($this->session->userdata('lang').'/home'); exit;
        }
    }

    function contact_us()
    {
        $data['header'] = TRUE;
        $data["footer"]   = TRUE;
        $data['_view'] = 'frontend/static_pages/contact_us';
        $this->load->view('layouts/frontend/basetemplate', $data);
    }
}