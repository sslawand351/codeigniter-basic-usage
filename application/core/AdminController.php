<?php

class AdminController extends CI_Controller {
    
    public $user = array(
        'id' => FALSE,
        'type' => FALSE,
        'is_admin' => FALSE,
        'permissions' => array()
    );
    
    public $language_flag = '';

	function __construct()
    {
        parent::__construct();

        if(isset($_COOKIE['admin_rememberme']) && !($this->is_logged_in()))
        {
            $admin_rememberme = $_COOKIE['admin_rememberme'];
            $remember_me_parts = explode("-",$admin_rememberme);

            if(count($remember_me_parts) == 2)
            {
                $this->load->model('Admin_model');

                $customer = $this->Admin_model->auto_login_via_cookie($remember_me_parts);
            }
        }

        $this->set_language_info();
        // Setting the global variables
        if($this->session->userdata('admin_id'))
        {
            // set the user vars 
            $this->user['id'] = $this->session->userdata('admin_id');

            $this->load->model('User_model');
            $user_data = $this->User_model->get_users($this->user['id']);

            if( ! empty($user_data) )
            {
                $this->user['name'] = ucfirst($user_data['first_name'])." ".ucfirst($user_data['last_name']);
            }
            // Check if its admin or not
            if($this->session->userdata('is_admin'))
            {
                $this->user['is_admin'] = TRUE;
            }
            else
            {
                $this->user['is_admin'] = FALSE;
            }

            $this->user['type'] = $this->session->userdata('user_type');
            $this->load->model('Admin_model');
            $data = $this->Admin_model->get_usergroup_id($this->user['id'],$this->user['type']);
      
            $this->user['usergroup_id'] = $data['usergroup_id'];
            $this->session->set_userdata('usergroup_id', $data['usergroup_id']);

            $this->load->model('Usergroup_model');
        
            $this->allowed_permissions = $this->Usergroup_model->get_allowed_permissions_codes($this->user['usergroup_id']);

            $admin_data = $this->Admin_model->get_users($this->user['id']);

            if( ! empty($admin_data) )
            {
                $this->user['name'] = ucfirst($admin_data['firstname'])." ".ucfirst($admin_data['lastname']);
            }

            $this->session->set_userdata('name', $this->user['name']);    
       }
    }

    function is_logged_in()
    {   
        $this->load->model('Admin_model');
        
        if($this->session->userdata('admin_id'))
        {
            $does_auto_token = $this->Admin_model->is_auto_token_present($this->session->userdata('admin_id'));
            if($does_auto_token != "")
                return TRUE;
            else
                return FALSE;
        }
        else
        { return FALSE; }
    }

    function authenticate()
    {   
        if( !$this->is_logged_in())
        {
            redirect ('admin/login');
        }
    }
 
    function ajax_authenticate($authentication_flag = FALSE)
    {
        if($this->input->is_ajax_request())
        {
            if($authentication_flag && ! $this->is_logged_in())
            {
                $response['login']['rc'] = FALSE;
                $response['login']['msg'] = lang('your_session_is_expired_please_login_again');
 
                echo json_encode($response);
                exit;
            }
            else
            {
                $response['login']['rc'] = TRUE;
                return $response;
            }
        }
        else
        {
            echo lang('only_ajax_call_request'); exit;
        }
    }

    function set_language_info()
    {
        $lang_array = get_languages();
 
        $cookie_lang = (!get_cookie('lang')) ? PRIMARY_LANG : get_cookie('lang');
        $cookie_lang = in_array($cookie_lang, $lang_array) ? $cookie_lang : PRIMARY_LANG;
  
        $lang = $cookie_lang;
  
        if( $this->input->post('language') || $this->input->get('language') )
        {
            $lang = $this->input->post('language') ? $this->input->post('language') : $this->input->get('language');
        }
        else if ( ! get_cookie('lang'))
        {
            // create cookie to avoid hitting this case again
            $cookie = array(
                'name'   => 'lang',
                'value'  => PRIMARY_LANG,
                'expire' => 86500,
                'domain' => '',
                'prefix' => ''
            );
            set_cookie($cookie);
            $lang = 'nl';
        }
        else
        {
            $lang = get_cookie('lang');
            $lang =  ! in_array($lang, $lang_array) ? PRIMARY_LANG : $lang;
  
            $cookie = array(
                'name'   => 'lang',
                'value'  => $lang,
                'expire' => 86500,
                'domain' => '',
                'prefix' => ''
            );
              
            set_cookie($cookie);
        }
  
        switch($lang)
        {
            case 'en': $language = 'English';
                break;
  
            case 'nl': $language = 'Dutch';
                break;
  
            case 'fr': $language = 'French';
                break;
  
            case 'es': $language = 'Spanish';
                break;
  
            default: $lang = 'en'; $language = 'English';
                break;
        }
          
        $this->session->set_userdata('language', $language);
        $this->session->set_userdata('lang', $lang);
        $this->lang->load(array('text'), $lang);
        $this->config->set_item('language', strtolower($language));
    }
 
    function set_user_info()
    {
        // Setting the global variables
        if($this->session->userdata('admin_id'))
        {
            $this->load->model(array('Admin_model'/*, 'User_role_model'*/));
            // set the user vars 
            $this->user['id'] = $this->session->userdata('admin_id');
            $user_params = array('master_admin' => TRUE, 'is_deleted' => 0);
            $user = $this->Admin_model->get_user($this->user['id'], $user_params);
 
            if($user['rc'])
            {
                $this->user['name'] = ucfirst($user['data']['first_name'])." ".ucfirst($user['data']['last_name']);
            }
            // Check if its admin or not
            $this->user['is_admin'] = $this->session->userdata('is_admin');
            $this->user['type'] = $this->session->userdata('user_type');
        
            // $this->user['user_role_id'] = $user['data']['user_role_id'];
            // $this->allowed_permissions = $this->User_role_model->get_allowed_permissions_codes($this->user['user_role_id']);
        }
    }
}