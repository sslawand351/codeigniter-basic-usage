<?php

class BaseController extends CI_Controller {

    function __construct()
    {
        parent::__construct(); 

        if( $this->input->post('flanguage') || $this->input->get('flanguage') )
        {
            $flang = $this->input->post('flanguage') ? $this->input->post('flanguage') : $this->input->get('flanguage');
        }
        else
        {
            if (!$this->session->userdata('flang')) 
            {
                $flang = 'nl';
                $this->session->set_userdata('flang',$flang);
                $this->language['flang'] = $flang;
            }
            else 
            {
               $flang = $this->session->userdata('flang');

                if(!($flang == 'nl' || $flang == 'en' || $flang == 'fr')) 
                {
                    $flang = 'nl';
                }

                $this->session->set_userdata('flang',$flang);
                $this->language['flang'] = $flang;
            }
        }

       if( $this->session->userdata('flang') )
        {
            $flang = $this->session->userdata('flang');
        }
        else
        {
            $flang = 'en';
        }

        //Why is the $language variable set ?
        $language = $this->db->get_where('meta_front_end_languages', array('code'=> strtoupper($flang)))->row_array();
        
        $this->language_id = $language['id'];
        $this->lang->load('text', $flang);

        if($this->session->userdata('user_id'))
        {
            
            $this->user['id'] = $this->session->userdata('user_id');

            // Check if its admin or not
            if($this->session->userdata('is_admin'))
            {
                $this->user['is_admin'] = TRUE;
            }
            else
            {
                $this->user['is_admin'] = FALSE;
            }
        }
    }

    /**
     * @author Prachi Mhatre <prachim@codebox.co.in>
     */
    function authenticate()
    {   
        if( !$this->is_logged_in())
        {
            redirect ('frontend/user/login');
        }
    }

    function is_logged_in()
    {
        $user_id = @(int)$this->session->userdata('user_id');
        
        if( $user_id )
            return TRUE;
        else
            return FALSE;
    }



}

include(dirname(__FILE__)."/AdminController.php");
