<?php defined('BASEPATH') OR exit('No direct script access allowed');

// English Language

//*******************ADMIN panel****************

//-----header-----------//
$lang['admin_panel']								= "Admin Panel";
$lang['sales']										= "Sales";
$lang['beerens']									= "Beerens";
$lang['navigation']									= "Navigation";
$lang['cars_management']							= "cars management";
$lang['sales_management']							= "sales management";
$lang['notifications']								= "Notifications";
$lang['you_have_total_of']							= "You have total of";
$lang['activities']									= "Activities";
$lang['activity_log']								= "Activity Log";
$lang['select_activity_type']						= "Select activity type";

//------common----------//
$lang['car'] 										= 'Car';
$lang['cars'] 										= 'Cars';
$lang['car_brand'] 									= "Brand";
$lang['car_model'] 									= "Model";
$lang['please_select']								= "Please Select";
$lang['manufacturing_date'] 						= "Manufacturing Date";
$lang['registration_date'] 							= "Registration Date";
$lang['location']									= "Location";
$lang['chassis_number'] 							= "Chassis Number";
$lang['commission_number'] 							= "Commission Number";
$lang['type'] 										= "Type";
$lang['owner']										= "Owner";
$lang['color']										= "Color";
$lang['cost'] 										= "Cost";
$lang['car_life_status'] 							= "Car Life Status";
$lang['stock_status'] 								= "Stock Status";
$lang['client'] 									= "Client";
$lang['btw_type'] 									= "Btw Type";
$lang['transmission'] 								= "Transmission";
$lang['vat_type'] 									= "Vat type";
$lang['fuel'] 										= "fuel";
$lang['mileage'] 									= "Mileage";
$lang['category'] 									= "Category";
$lang['images'] 									= "Images";
$lang['save']										= "Save";
$lang['copy_car'] 									= "Copy Car";
$lang['send'] 										= "Send";
$lang['options']									= "Options";
$lang['publications']								= "Publications";
$lang['car_brands'] 								= "Car Brands";
$lang['car_models'] 								= "Car Models";
$lang['select_all'] 								= "Select All";
$lang['upload'] 									= "Upload";
$lang['car_subtitle']								= "Car Subtitle";
$lang['action']										= "Action";
$lang['update'] 									= "Update";
$lang['email_address'] 								= "Email address";
$lang['add_images'] 								= "Add Images";
$lang['this_field_is_required']						= "This field is required";
$lang['close'] 										= "Close";
$lang['ok']											= "OK";
$lang['from']										= " from ";
$lang['confirm']									= "Confirm";
$lang['cancel'] 									= "Cancel";
$lang['first_name']									= "First Name";
$lang['last_name']									= "Last Name";
$lang['street'] 									= "Street";
$lang['house_number'] 								= "House Number";
$lang['postal_code'] 								= "Postal code";
$lang['town'] 										= "Town";
$lang['accessories']								= "Accessories";
$lang['language'] 									= "Language";
$lang['phone_number']								= "Phone Number";
$lang['selling_cost'] 								= "Selling Cost";
$lang['choose_your_accessories'] 					= "Choose your accessories";
$lang['no_car_accessories']							= "No Car accessories!";
$lang['brand'] 										= "Brand";
$lang['brands'] 									= "Brands";
$lang['model'] 										= "Model";
$lang['models']										= "Models";
$lang['description'] 								= "Description";
$lang['select_a_model']								= "Select a model";
$lang['no_data_found'] 								= "No data found";
$lang['some_error_occurred']						= "Some error occurred";
$lang['add'] 										= "Add";
$lang['actions']									= "Actions";
$lang['view']										= "View";
$lang['accept'] 									= "Accept";
$lang['reject'] 									= "Reject";
$lang['edit']										= "Edit";
$lang['owners']										= "Owners";
$lang['guarantee']									= "Guarantee";
$lang['accessaries'] 								= "Accessories";
$lang['id']											= "ID";
$lang['name']										= "Name";
$lang['publish']									= "Publish";
$lang['delete']										= "Delete";
$lang['error_occurred_while_updating_data']			= "Error occurred while updating data";
$lang['there_was_an_error_please_contact_administrator']= "There was an error. Please contact administrator";
$lang['Car_rejected'] 								= 'Car rejected.';
$lang['the_uploaded_file_doesnt_seem_to_be_an_image'] = "The uploaded file doesnt seem to be an image";
$lang['only_pdf_doc_docx_xls_csv_file_types_are_allowed'] = "Only pdf doc docx xls csv file types are allowed";
$lang['invalid_data'] 								= "Invalid data";
$lang['invalid_url'] 								= "Invalid Url";
$lang['please_enter_a_valid_cost_name_and_amount'] 	= "Please enter a valid cost name and amount";
$lang['invalid_id'] 								= "Invalid id";
$lang['bought']										= "Bought";
$lang['people']										= "People";
$lang['clients']									= "Clients";
$lang['dealers']									= "Dealers";
$lang['leads']										= "Leads";
$lang['users']										= "Users";
$lang['there_was_some_error']						= "There was some error.";
$lang['vat']										= "VAT";
$lang['vat_number']									= "VAT Number";
$lang['pay']										= "Pay";
$lang['state']										= "State";
$lang['post_code']									= "Post Code";
$lang['address'] 									= "Address";
$lang['deleted_successfully']						= "Deleted successfully";
$lang['notes']										= "Notes";
$lang['added_on']									= "Added On";
$lang['added'] 										= 'Added';
$lang['sold'] 										= "THIS CAR IS SOLD";
$lang['nissan'] 									= 'Nissan';
$lang['B2CARS']										= "B2cars";
$lang['leasense'] 									= "Leasense";
$lang['image'] 										= "Image";
$lang['username']									= "Username";
$lang['questions']									= "Questions";
$lang['settings']									= "Settings";
$lang['english'] 									= "English";
$lang['dutch'] 										= "Dutch";
$lang['french'] 									= "French";
$lang['there_was_an_error']							= "There was an error";
$lang['error_occurred_while_deleting_data']			= "Error occurred while deleting data";
$lang['all_fields_compalsory'] 						= 'All fields are mandatory';
$lang['image_deleted_successfully']					= "Image deleted successfully";
$lang['categories']									= "Categories";
$lang['admin_log_in']								= "Admin Log in";
$lang['activity']									= "Activity";
$lang['ago']										= "ago";
$lang['invalid_access'] 							= "Invalid access";
$lang['select']										= "Select";
$lang['car_seller'] 								= "Car seller";
$lang['incl_btw'] 									= "incl. Btw";
$lang['no'] 										= 'no';
$lang['b2'] 										= 'B2';
$lang['color_codes']								= 'Color codes';
$lang['no_cars_found']								= "No cars found";
$lang['hi'] 										= "Hi";
$lang['kilometers']									= "Kilometers";
$lang['for']										= " for ";
//----------dashboard-------------------//
$lang['dashboard']									= "Dashboard";
$lang['your_workspace_has_been_set_to'] 			= "Your workspace has been set to ";
$lang['welcome_to_beerens_admin_dashboard'] 		= "Welcome to beerens admin dashboard";

//-----------------car add, edit-----------------------//
$lang['car_add'] 									= "Car Add";
$lang['basic_details'] 								= "Basic Details";
$lang['exterior_color'] 							= "Exterior Color";
$lang['exterior_code'] 								= "Exterior Code";
$lang['interior_material'] 							= "Interior Material";
$lang['interior_code'] 								= "Interior Code";
$lang['interior_cover'] 							= "Interior Cover";
$lang['inclusive_purchase_price']					= "Inclusive Purchase Price";
$lang['exclusive_purchase_price'] 					= "Exclusive Purchase Price";
$lang['inclusive_promotion_price'] 					= "Inclusive Promotion Price";
$lang['exclusive_promotion_price'] 					= "Exclusive Promotion Price";
$lang['inclusive_selling_cost'] 					= "Inclusive Selling Price B2C";
$lang['exclusive_selling_cost'] 					= "Exclusive Selling Price B2C";
$lang['inclusive_dealer_price'] 					= "Inclusive Dealer Price B2B";
$lang['exclusive_dealer_price'] 					= "Exclusive Dealer price B2B";
$lang['inclusive_repair_cost'] 						= "Inclusive Estimated Repair Cost";
$lang['exclusive_repair_cost'] 						= "Exclusive Estimated Repair Cost";
$lang['inclusive_trading_price'] 					= "Inclusive Trading Price B2B";
$lang['exclusive_trading_price'] 					= "Exclusive Trading Price B2B";
$lang['technical_specifications'] 					= "Technical Specifications";
$lang['sold_price'] 								= "Sold price";
$lang['specification']								= "Specification";
$lang['doors'] 										= "doors";
$lang['gears'] 										= "Gears";
$lang['seats'] 										= "Seats";
$lang['horsepower'] 								= "Horsepower";
$lang['cc'] 										= "Cc";
$lang['co2'] 										= "co2";
$lang['model_year'] 								= "Model Year";
$lang['drive_wheel']								= "Drive Wheel";
$lang['car_added_successfully']						= "Car added successfully";
$lang['car_updated_successfully']					= "Car updated successfully";
$lang['car_view']									= "Car View";
$lang['car_added_success'] 							= 'Car Added Successfully.';
$lang['car_added_error'] 							= 'Error while adding car.';
$lang['mileage_cannot_be_zero_for_used_cars'] 		= "Mileage cannot be zero for used cars.";

//---------------car edit----------------------//
$lang['car_edit'] 									= "Car Edit";
$lang['car_details_incomplete'] 					= "Car details is incomplete";
$lang['remove_this_car'] 							= "Remove this car";
$lang['export'] 									= "Export";
$lang['get_barcode'] 								= "Get Barcode";
$lang['delete_and_redownload_images']				= "Delete and Redownload Images";
$lang['information']								= "Information";
$lang['technical']									= "Technical";
$lang['price'] 										= "Price";
$lang['photos']										= "Photos";
$lang['damaged_photos']								= "Damaged Photos";
$lang['autoscout']									= "Autoscout";
$lang['autoscout_comments']							= "Autoscout Comments";
$lang['internal_comments']							= "Internal Comments";
$lang['documents']									= "Documents";
$lang['quick_check']          						= "Quick Check";
$lang['general'] 									= "General";
$lang['arrival'] 									= "Arrival";
$lang['arrival_status'] 							= "Arrival Status";
$lang['arrival_date'] 								= "Arrival Date";
$lang['delivery'] 									= "Delivery";
$lang['where_the_car_is_parked'] 					= "Where the car is parked";
$lang['BPM'] 										= "BPM";
$lang['mmt'] 										= "MMT";
$lang['add_a_new_cost'] 							= "Add a New Cost";
$lang['enable_finance_settings'] 					= "Enable Finance settings";
$lang['equipments'] 								= "Equipments";
$lang['publish_cars'] 								= "Publish Cars";
$lang['damaged_images'] 							= "Damaged Images";
$lang['document_name']								= "Document name";
$lang['certificate_of_confirmity']					= "Certificate Of Confirmity";
$lang['maintainance_book']							= "Maintainance Book";
$lang['inspaction_report']							= "Inspection Report";
$lang['carpass']									= "Carpass";
$lang['proof_of_registration']						= "Proof of registration";
$lang['keys']										= "Keys";
$lang['comments']									= "Comments";
$lang['send_pictures'] 								= "Send Pictures";
$lang['send_proposal'] 								= "Send Proposal";
$lang['a3_pricetag'] 								= "A3 - Pricetag.pdf";
$lang['a3_alphabet'] 								= "A3 - Pricetag Alphabet.pdf";
$lang['a4_pricetag'] 								= "A4 - Pricetag.pdf";
$lang['send_car_pictures'] 							= "Send Car Pictures";
$lang['add_new_note'] 								= "Add New Note";
$lang['cost_name'] 									= "Cost name";
$lang['cost_amount'] 								= "Cost amount";
$lang['delete_cost'] 								= "Delete Cost";
$lang['btw_percentage']								= "Btw Percentage";
$lang['please_enter_a_valid_cost_name'] 			= "Please enter a valid cost name.";
$lang['please_enter_btw_percentage'] 				= "Please enter Btw Percentage.";
$lang['please_enter_a_valid_cost_amount'] 			= "Please enter a valid cost amount.";
$lang['please_enter_a_valid_email_address'] 		= "Please enter a valid email address.";
$lang['are_you_sure_you_want_to_remove_this_car_cost?'] = "Are you sure you want to remove this car cost?";
$lang['are_you_sure_you_want_to_delete_this_cost'] 	= "Are you sure you want to delete this cost";
$lang['delete_image'] 								= "Delete Image";
$lang['are_you_sure_you_want_to_delete_this_image'] = "Are you sure you want to delete this image";
$lang['delete_comment'] 							= "Delete Comment";
$lang['are_you_sure_you_want_to_delete_this_comment'] = "Are you sure you want to delete this comment";
$lang['delete_document'] 							= "Delete Document";
$lang['are_you_sure_you_want_to_delete_this_document'] = "Are you sure you want to delete this document";
$lang['successfully_sorted'] 						= "Successfully Sorted.";
$lang['something_went_wrong'] 						= "Something went wrong.";
$lang['confirm_remove_logo'] 						= "Are you sure you want to remove this logo?";
$lang['confirm_remove_car'] 						= "Are you sure you want to remove this document?";
$lang['no_images_exist_for_this_car'] 				= "No images exist for this car";
$lang['pictures_have_been_sent_to'] 				= "Pictures have been sent to";
$lang['error_occurred_while_sending_email_to'] 		= "Error occurred while sending email to";
$lang['car_does_not_exist'] 						= "Car does not exist";
$lang['add_document']								= "Add document";
$lang['no_documents_found']							= "No documents found";
$lang['files_less_than_10_MB_can_only_be_uploaded'] = "Files less than 10 MB can only be uploaded.";
$lang['only_pdf_doc_docx_xls_csv_file_types_are_allowed'] = "Only pdf,doc,docx,xls,csv file types are allowed";
$lang['there_was_an_error_while_uploading_document'] = "There was an error while uploading document";
$lang['error_encountered_please_try_again'] 		= "Error encountered, please try again!";
$lang['car_edit_reserved_error'] 					= 'Car is reserved and status can not be changed.';
$lang['document_uploaded_successfully'] 			= 'Document uploaded successfully.';
$lang['please_select_a_car'] 						= "Please select a car";
$lang['email_sent_successfully'] 					= "Email sent successfully.";
$lang['flex']										= "Flex";
$lang['drive']										= "Drive";
$lang['green']										= "Green";

//-----duplicate car-------//
$lang['duplicate'] 									= "Duplicate";
$lang['duplication'] 								= "Duplication";
$lang['no_of_cars_to_copy'] 						= "Number of cars to copy";
$lang['car_duplicated_successfully'] 				= "Car Duplicated Successfully.";
$lang['commission_number_must_be_unique'] 			= "Commission numbers must be uniue";
$lang['chassis_number_must_be_unique'] 				= "Chassis numbers must be uniue";
$lang['number_of_cars_must_be_greater_than_zero'] 	= "Number of cars must be greater than zero.";
$lang['search_result_will_be_found_in_car_dropdown']= "Search results will found in car's dropdown";
$lang['filtered']									= "Filtered";

//-----remove car-------//
$lang['remove_car']									= "Remove Car";
$lang['are_you_sure_you_want_to_remove_this_car']	= "Are you sure you want to remove this car";

//-----copy car--------//
$lang['car_status_can_not_be_copied']				= "Car status can not be copied";
$lang['please_select_at_least_one_parameter_to_copy'] = "Please select at least one parameter to copy.";
$lang['please_search_and_select_at_least_one_car']  = "Please search and select at least one car.";
$lang['please_select_at_least_one_car_to_copy'] 	= "Please select at least one car to copy.";
$lang['are_you_sure_you_want_to_copy'] 				= "Are you sure you want to copy";
$lang['no_car_found_for_this_search'] 				= "No car found for this search.";
$lang['car_copied_successfully']					= "Car copied successfully";

//-----generate proposal----//
$lang['generate_proposals'] 						= "Generate Proposals";
$lang['generate_proposal'] 							= "Generate Proposal";
$lang['customer_information'] 						= "Customer information";
$lang['price_information'] 							= "Price information";
$lang['additional_options'] 						= "Additional Options";
$lang['genrate_proposals'] 							= "Generate Proposals";
$lang['print'] 										= "Print";
$lang['save_as_pdf'] 								= "Save as PDF";
$lang['send_email'] 								= "Send Email";
$lang['first']										= "First";
$lang['last']										= "Last";
$lang['previous']									= "Previous";
$lang['next'] 										= "Next";
$lang['your_margin'] 								= "Your Margin";
$lang['your_price'] 								= "Your Price";
$lang['vat_amount'] 								= "VAT Amount";
$lang['amount_including_vat'] 						= "Amount including VAT";
$lang['enter_the_text_you_want_to_mention_above_the_offer'] = "Enter the text you want to mention above the offer";
$lang['enter_the_text_you_want_to_mention_under_the_offer'] = "Enter the text you want to mention under the offer";
$lang['save_as_pdf'] 								= "Save as PDF";
$lang['choose_file'] 								= "Choose file";
$lang['some_error_occured_while_generating_the_proposal']= "Some error occured while generating the proposal";
$lang['print'] 										= "Print";
$lang['create_pdf']									= "Create PDF";
$lang['payment'] 									= "Payment";
$lang['own_contribution']							= "Own Contribution";
$lang['financing'] 									= "Financing";
$lang['offerte'] 									= "OFFERTE";
$lang['power'] 										= 'Power';
$lang['fuel_capital'] 								= "Fuel";
$lang['car_ready_charge_title'] 					= "Kosten rijklaar maken eindgebruiker Belgi&euml;";
$lang['car_ready_charge_subtitle'] 					= "Offici&euml;le voorplaat, driehoek, brandblusser, fluojasje, verbanden, aanvraag div, cleaning, keuring";
$lang['car_proposals_email_body1'] 					= "Attached to this email you can find the proposal for the car you are interested in.";
$lang['total']										= "TOTAL";
$lang['with_kind_regards'] 							= "With kind regards,";
$lang['subject_car_proposals_email'] 				= "Beerens - Car Offer";
$lang['please_select_lead_first']					= "Please select a lead first.";
$lang['click_here_to_update']						= "Click here to update";

//-------search---------//
$lang['search']										= "Search";
$lang['search_for_cars']							= "Search for cars";
$lang['car_type'] 									= "Car Type";
$lang['advanced_search']							= "Advanced Search";
$lang['technical_details']							= "Technical Details";
$lang['cc_van'] 									= "Cc van";
$lang['kw_van'] 									= "kW van";
$lang['pk_van'] 									= "pk van";
$lang['kms_van'] 									= "KM's van";
$lang['dib_van'] 									= "DIB van";
$lang['place'] 										= "Place";
$lang['car_specifications'] 						= "Car Specifications";
$lang['fuel_type'] 									= "Fuel Type";
$lang['date'] 										= "Date";
$lang['created_on'] 								= "Created on";
$lang['arrival_on'] 								= "Arrival on";
$lang['sale_on'] 									= "Sale on";
$lang['invoice_on'] 								= "Invoice on";
$lang['delivered_on'] 								= "Delivered on";
$lang['search_cars']								= "Search Cars";
$lang['basic_search']								= "Basic Search";
$lang['search_by_id']								= "Search by ID";
$lang['no_record_found'] 							= "No Record found";

//-------reserved cars-------//
$lang['reserved'] 									= "Reserved";
$lang['reserved_from_frontend'] 					= "Reserved From Frontend";
$lang['reserved_by_icar_user'] 						= "Reserved By Icar User";
$lang['reserved_by_admin'] 							= "Reserved by  admin";
$lang['reserved_for'] 								= "Reserved For";
$lang['source']										= "Source";
$lang['car_reserved_successfully']					= "Car reserved successfully";
$lang['reserved_on'] 								= "Reserved On";
$lang['reserved_to'] 								= "Reserved To";
$lang['transaction_id'] 							= "Transaction ID";
$lang['payment_type'] 								= "Payment Type";
$lang['reserve_car']								= "Reserve Car";
$lang['choose_reservation_type'] 					= "CHOOSE A RESERVATION TYPE";
$lang['total_accessories_cost'] 					= 'Total accessory cost';
$lang['car_reserved_successfully'] 					= "Car Reserved Successfully.";

//-----queued cars--------//
$lang['queued']										= "Queued";
$lang['queued_cars_listing'] 						= "Queued Cars Listing";
$lang['no_queued_cars_found'] 						= "No Queued Cars found";
$lang['by_chasis'] 									= "By Chasis";
$lang['by_commission_number'] 						= "By Commission Number";
$lang['by_numintern'] 								= "By Numinterno";
$lang['version'] 									= "Version";
$lang['queued_car_listing'] 						= "Queued Car Listing";
$lang['queued_car_approval'] 						= "Queued Car Approval/Rejection";


// car view in queued cars-----//
$lang['car_details'] 								= "Car details";
$lang['is_deleted_from_the_system']					= "is deleted from the system";
$lang['purchase_price'] 							= "Purchase Price";
$lang['retail_price'] 								= "Retail Price";
$lang['dealer_price'] 								= "Dealer Price";
$lang['this_car_whith_numinterno'] 					= "This car whith Numinterno ";
$lang['to']											= "to";
$lang['changes_successfully_accepted']				= "Changes successfully accepted";
$lang['car_successfully_moved_from_queued_car_to_bcar']= "Car successfully moved from queued car to Bcar";
$lang['car_rejected']								= "Car Rejected.";

//-------incomplete cars---------//
$lang['incomplete']									= "Incomplete";
$lang['incomplete_cars_listing'] 					= "Incomplete Cars Listing";
$lang['no_incomplete_cars_found'] 					= "No Incomplete Cars found";
$lang['car_edit_incomplete_data'] 					= "Car Edit Incomplete Data";

//------cars without images---------//
$lang['without_images']								= "Without Images";
$lang['cars_without_images_listing'] 				= "Cars Without Images Listing";
$lang['no_cars_found_without_images']				= "No cars found without images";

//-------car options ----------//
$lang['car_option_add'] 							= "Car Option Add";
$lang['car_option_edit'] 							= "Car Option Edit";
$lang['car_options'] 								= "Car Options";
$lang['autoscout_car_options'] 						= "Autoscout Car Options";
$lang['option_name'] 								= "Option Name";
$lang['autoscout_equipment_name'] 					= "Autoscout Equipment Name";
$lang['car_options_listing'] 						= "Car Options Listing";
$lang['add_option'] 								= "Add option";
$lang['no_car_option_found']						= "No Car option found";
$lang['car_option_successfully_updated']			= "Car option Successfully updated";
$lang['car_option_added']							= "Car option added";
$lang['error_adding_car_option_please_try_again']	= "Error adding car option. Please try again";
$lang['car_option_removed']							= "Car option removed";
$lang['error_removing_car_option_please_try_again']	= "Error removing car option. Please try again";

//---------------sold cars------------//
$lang['sold_link'] 									= "Sold";
$lang['sold_cars_listing'] 						    = "Sold Cars Listing";
$lang['Car_sold_successfully'] 						= "Car sold successfully";
$lang['delivery_date_updated_successfully'] 		= "Delivery Date updated Successfully.";
$lang['incomplete_cars_can_not_be_copied'] 			= "Incomplete cars can not be copied.";
$lang['update_delivery_date']						= "Update Delivery Date";
$lang['car_delivery_date'] 							= "Car Delivery Date";
$lang['car_delivery_time'] 							= "Car Delivery Time";
$lang['paper_delivery_date'] 						= "Paper Delivery Date";
$lang['paper_delivery_time'] 						= "Paper Delivery Time";
$lang['please_select_a_date']	        			= "Please select a date.";
$lang['bought_by'] 									= "Bought By";
$lang['bought_for'] 								= "Bought For";
$lang['bought_on'] 									= "Bought On";
$lang['generate_order_note']      					= "Generate Order Note";
$lang['mark_delivered'] 							= "Mark As Delivered";
$lang['set_delivery']								= "Set";
$lang['delivery_date'] 								= "Delivery Date";
$lang['update_status'] 								= "Update Car Status To In-stock";
$lang['sell_car']									= "Sell A Car";
$lang['this_car_having_name'] 						= "This car having name";
$lang['is_bought_by'] 								= "is bought by";
$lang['on'] 										= "on";
$lang['for_price']									= "for price";
$lang['confirm_msg_for_update_status_of_sold_car1'] = "Are you sure you want to delete this data and make this car 'In Stock' again ?";
$lang['confirm_msg_for_update_status_of_sold_car2'] = "Are you sure you want to delete this data and make this car 'Reserved' again ?";
$lang['customer_name'] 								= "Customer name";
$lang['car_status_updated'] 						= "Car status updated successfully";
$lang['no_bought_cars_found'] 						= "No bought Cars found";
$lang['add_delivery_date'] 							= "Add Delivery Date";
$lang['car_status_changed_successfully_to_in_stock']= "Car status changed successfully to in stock.";
$lang['sell_car']									= "Sell A Car";
$lang['view_order_note']      						= "View order note";
$lang['sold_cars_by_this_lead']						= "Sold cars by this lead";
$lang['please_select_time'] 						= "Please select a time";
$lang['upload_cars_via_csv']					    = "Upload cars via csv";
$lang['please_select_valid_file'] 			    	= "Please select a valid file";

//-------------delivered cars----------//
$lang['delivered_cars_listing'] 					= "Delivered Car Listing";
$lang['delivered'] 									= "Delivered";
$lang['no_delivered_cars_found'] 					= "No delivered cars found";

//------------accessory add,edit,listing----//
$lang['add_accessories']							= "Add Accessory";
$lang['publish_car_accessory']						= "Publish Car Accessory";
$lang['edit_accessories']							= "Edit Accessory";
$lang['accessory_listing']							= "Accessory Listing";
$lang['discounted_cost'] 							= "Discounted cost";
$lang['no_car_accessories_added_yet']				= "No car accessories added yet";
$lang['delete_accessory']							= "Delete Accessory";
$lang['are_you_sure_you_want_to_delete_this_accessory']	= "Are you sure you want to delete this accessory ?";
$lang['accessory_successfully_added'] 				= "Accessory successfully added";
$lang['error_adding_accessory'] 					= "Error adding Accessory";
$lang['accessory_successfully_updated'] 			= "Accessory successfully updated";
$lang['error_updating_accessory'] 					= "Error updating Accessory";
$lang['this_accessory_cannot_be_deleted_as_it_has_been_ordered_by_a_customer']= "This Accessory cannot be deleted as it has been ordered by a customer";
$lang['accessory_successfully_deleted'] 			= "Accessory successfully deleted";
$lang['error_deleting_accessory'] 					= "Error deleting Accessory";
$lang['accessory_channel_added'] 					= "Accessory channel added";
$lang['error_adding_accessory_channel_please_try_again']= "Error adding accessory channel. Please try again";
$lang['accessory_channel_removed'] 					= "Accessory channel removed";
$lang['error_removing_accessory_channel_please_try_again'] = "Error removing accessory channel. Please try again";
$lang['discounted_cost_must_be_less_than_the_original_cost'] = "Discounted cost must be less than the original cost";
$lang['no_accessories_found_for_this_car']			= "No accessories founf for this car.";

//-----brand add,edit,listing----------//
$lang['car_brands_listing'] 						= "Car brands listing";
$lang['add_car_brand'] 								= "Add Car Brand";
$lang['brand_name'] 								= "Brand Name";
$lang['autoscout_name']								= "Autoscout Name";
$lang['car_brand_added_success'] 					= 'Car brand added successfully.';
$lang['car_brand_added_error'] 						= 'Error while adding car brand.';
$lang['edit_car_brand'] 							= "Edit Car Brand";
$lang['car_brand_is_updated_successfully']			= "Car brand is updated successfully";
$lang['no_car_brands_found']						= "No car brands found.";
$lang['confirm_remove_car_brand'] 					= "Are you sure, you want to delete this car brand ?";
$lang['error']										= "Error";
$lang['car_brand_deleted_successfully'] 			= "Car brand deleted successfully.";
$lang['car_brand_can_not_be_deleted'] 				= "Car brand can not be deleted.";

//-----model add,edit,listing----------//
$lang['add_model']									= "Add Model";
$lang['model_deleted_successfully']                 = "Model Deleted Successfully";
$lang['you_can_not_delete_this_car_model']          = "You can not delete this car model.";
$lang['car_models_listing_for_brand']				= "Car Model Listing for Brand";
$lang['model_name'] 								= "Model Name";
$lang['no_car_models_found']						= "No car models found.";
$lang['add_car_model_for_brand']					= "Add Car Model For Brand";
$lang['edit_car_model_for_brand']					= "Edit Car Model For Brand";
$lang['autoscout_model_name']						= "Autoscout Model Name";
$lang['are_you_sure_you_want_to_delete_this_model'] = "Are you sure you want to delete this model";
$lang['delete_model']								= "Delete Model";
$lang['error_while_deleting_model'] 				= "You can not delete this model";
$lang['model_deleted_successfully'] 				= "Model deleted successfully.";
$lang['car_model_added_successfully']				= "Car model added successfully";
$lang['car_model_updated_successfully']				= "Car model updated successfully";

//----------interior cover add,edit,listing-----//
$lang['interior_covers'] 							= "Interior covers";
$lang['interior_cover_listing'] 					= "Interior cover liting";
$lang['interior_cover_name'] 						= "Interior cover name";
$lang['add_interior_cover'] 						= "Add Interior cover";
$lang['autoscout_interior_cover_name_updated_successfully'] = "Autoscout interior cover name updated successfully";
$lang['interior_cover_successfully_updated'] 		= "interior cover updated successfully";
$lang['autoscout_interior_cover_name'] 				= "Autoscout interior cover name";
$lang['interior_cover_name'] 						= "Interior cover name";
$lang['no_interior_cover_found']    				= "No interior cover found";
$lang['interior_cover_add']							= "Interior cover add";
$lang['interior_cover_edit']						= "Interior cover edit";
$lang['interior_cover_added']						= "interior cover added successfully";
$lang['car_interior_cover_add'] 					= "Add Interior cover";

//----------------removed cars-----------------//
$lang['removed'] 									= "Removed";
$lang['removed_cars_listing'] 						= "Removed Cars Listing";
$lang['undo_delete'] 								= "Undo Delete";
$lang['no_removed_cars_found'] 						= "No Removed Cars Found.";
$lang['restore_car'] 								= "Restore Car";
$lang['are_you_sure_you_want_to_restore_this_car'] 	= "Are you sure you want to restore this car?";
$lang['car_restored_successfully'] 					= "Car Restored Successfully.";
$lang['error_while_restoring_car'] 					= "Error while restoring car.";
$lang['car_already_sold_error'] 					= 'Car has been sold and can not be deleted.';
$lang['car_reserved_error'] 						= 'Car is already reserved and can not be deleted.';
$lang['there_was_some_error_while_deleting_the_data']= "There was some error while deleting the data";
$lang['car_deleted_successfully']					= "Car deleted successfully";

//------------reports-----------------//
$lang['reports'] 									= 'Reports';
$lang['stocklist'] 									= 'Stocklist';
$lang['sales_list'] 								= 'Sales list';
$lang['statics_website'] 							= 'Statics website';
$lang['step_1'] 									= "Step 1";
$lang['step_2'] 									= "Step 2";
$lang['step_3'] 									= "Step 3";
$lang['sales_person'] 								= "Sales Person";
$lang['date_from'] 									= "Date From";
$lang['date_to'] 									= "Date To";
$lang['amount_of_sold_cars'] 						= "Amount of sold cars";
$lang['amount_of_created_proposals'] 				= "Amount of created proposals";
$lang['amount_of_turnover_generated'] 				= "Amount of turnover generated";
$lang['amount_of_profit_generated'] 				= "Amount of profit generated";
$lang['create_csv'] 								= "Generate CSV";
$lang['select_from_which_data_you_would_like_the_create_a_report']= "select data from which you would like the create a report";
$lang['select_the_checkboxes_of_which_fields_you_would_like_to_include_in_your_report'] = "select the checkboxes from which fields you would like to include in your report";
$lang['at_least_one_value_should_be_selected'] 		= "At least 1 value should be selected from above fields.";
$lang['report_generated_successfully'] 				= "Report generated successfully.";
$lang['please_select_atleast_one_option'] 			= "Please select atleast one option";
$lang['amount_of_views'] 							= "Amount of views on the detail page";
$lang['amount_of_information_requests'] 			= "Amount of information requests";
$lang['Choose_a_timeframe']							= "Choose a timeframe";
$lang['amount_reservations'] 						= "Amount of reservations";
$lang['amount_call_us_forms']					 	= "Amount of call us forms";

//-----------user add,edit,listing---------------//
$lang['user_listing']								= "User Listing";
$lang['user_add']									= "User Add";
$lang['email']										= "E-mail";
$lang['role']										= "Role";
$lang['assign_roles']								= "Assign Roles";
$lang['add_user']									= "Add User";
$lang['password']									= "Password";
$lang['change_password']							= "Change password";
$lang['confirm_password']   						= "Confirm Password";
$lang['workspace'] 									= "Workspace";
$lang['user_added_successfully']					= "User added successfully";
$lang['user_updated_successfully']					= "User updated successfully";
$lang['choose_your_workspace'] 						= "Choose your workspace";
$lang['user_edit'] 									= "User edit";
$lang['no_users_found'] 							= "No users found";
$lang['delete_user'] 								= "Delete User";
$lang['are_you_sure_you_want_to_delete_this_user'] 	= "Are you sure you want to delete this user";
$lang['no_workspace_found']							= "No workspace found";
$lang['workspace_added_successfully']				= "Workspace added successfully";
$lang['no_users_exists']							= "No users exists";
$lang['user_has_no_assigned_roles']					= "User has no assigned roles";
$lang['user_deleted_successfully']					= "User deleted successfully";
$lang['the_user_cannot_be_deleted']					= "The user cannot be deleted";
$lang['you_are_not_admin']							= "You are not admin";
$lang['error_msg_remove_location']					= "You can not uncheck this location as this is assigned to a lead.";

//----------------client add,edit,listing-------------//
$lang['client_add'] 								= "Client Add";
$lang['add_client'] 								= "Add Client";
$lang['client_listing'] 							= "Client Listing";
$lang['no_clients_found'] 							= "No clients found";
$lang['delete_client'] 								= "Delete Client";
$lang['are_you_sure_you_want_to_delete_this_client']= "Are you sure you want to delete this client";
$lang['client_edit'] 								= "Client Edit";
$lang['bus_number'] 								= "Bus Number";
$lang['city'] 										= "City";
$lang['zipcode'] 									= "Zipcode";
$lang['country'] 									= "Country";
$lang['company_name'] 								= "Company Name";
$lang['company_street'] 							= "Company Street";
$lang['company_house_number'] 						= "Company House Number";
$lang['company_bus_number'] 						= "Company Bus Number";
$lang['company_zipcode'] 							= "Company Zipcode";
$lang['company_city'] 								= "Company City";
$lang['company_country'] 							= "Company Country";
$lang['no_clients_found']							= "No clients found";
$lang['client_added_successfully']					= "Client added successfully";
$lang['successfully_updated']						= "Successfully updated.";
$lang['the_client_deleted_successfully']			= "The client deleted successfully";
$lang['the_client_cannot_be_deleted']				= "The client cannot be deleted";

//-------------dealers listing---------------//
$lang['dealer_listing'] 							= "Dealer Listing";
$lang['no_dealer_found'] 							= "No dealer found.";
$lang['view_proposals'] 							= "View Proposals";
$lang['previous_made_offer_listing_of_dealer'] 		= "Previous Made Offer Listing of Dealer";
$lang['car_price'] 									= "Car Price";
$lang['text_mentioned_above_offer'] 				= "Text mentioned above offer";
$lang['text_mentioned_below_offer'] 				= "Text mentioned below offer";
$lang['no_proposals_found'] 						= "No proposals found";
$lang['accepted']									= "Accepted";
$lang['rejected']									= "Rejected";

//-----------------owner add,edit,listing-----------------//
$lang['add_owner']									= "Add Owner";
$lang['edit_owner']									= "Edit Owner";
$lang['owner_listing']								= "Owner Listing";
$lang['are_you_sure_you_want_to_delete_this_owner']	= "Are you sure,you want to delete this owner ?";
$lang['delete_owner']								= "Delete Owner";
$lang['no_owners_found']							= "No Owners found";
$lang['data_not_found']								= "Data not found";
$lang['owner_added_successfully']					= "Owner added successfully";
$lang['error_while_adding_owner']					= "Error while adding owner";
$lang['owner_updated_successfully']					= "Owner Updated successfully";
$lang['error_while_updating_owner']					= "Error while updating owner";
$lang['error_while_deleting_owner']					= "Error while deleting owner";
$lang['owner_not_found']							= "Owner not found";
$lang['you_can_not_delete_this_owner'] 				= 'You can not delete this owner.';

//----------------lead add,edit,listing-------------//
$lang['no_lead_found_for_this_category'] 			= "No lead found for this category";
$lang['no_lead_found'] 								= "No lead found";
$lang['lead_added'] 								= "Lead added";
$lang['lead_status_has_changed_from'] 				= "lead status has changed from ";
$lang['edit_lead'] 									= "Edit Lead";
$lang['lead_listing'] 								= "Lead Listing";
$lang['lead_assigned'] 								= "Lead Assigned";
$lang['lead_contacted'] 							= "Lead Contacted";
$lang['lead_interested'] 							= "Lead Interested";
$lang['lead_not_interested'] 						= "Lead Not Interested";
$lang['lead_sold'] 									= "Lead Sold";
$lang['edit_lead_status'] 							= "Edit Lead";
$lang['for_additional_information_please_use_the_notes_below'] = "For additional information please use the notes below";
$lang['add_lead_status'] 							= "Add Lead Status";
$lang['add_lead'] 									= "Add Lead";
$lang['add_the_event_to_your_office_365_calendar'] 	= "Add the event to your office 365 calendar";
$lang['logs'] 										= "Logs";
$lang['no_lead_statuses_found'] 					= "No lead Statuses found";
$lang['added_a_lead']								= " added a lead named ";
$lang['updated_lead']								= " updated a lead named ";
$lang['deleted_lead']								= " deleted a lead named ";
$lang['lead_added_successfully']					= "Lead added successfully";
$lang['lead_updated_successfully']					= "Lead updated successfully";
$lang['lead_deleted_successfully']					= "Lead deleted successfully";
$lang['there_was_some_error_while_updating_the_lead']= "There was some error while updating the lead";
$lang['the_lead_can_not_be_deleted_the_lead_is_assigned_to_salesperson']= "The lead can not be deleted. The lead is assigned to salesperson";
$lang['lead_status_added_successfully']				= "Lead status added successfully";
$lang['lead_status_updated_successfully']			= "Lead status updated successfully";
$lang['there_are_no_lead_statuses']					= "There are no lead statuses";
$lang['there_are_no_interested_cars']				= "There are no interested cars";
$lang['mobile']										= "Mobile";
$lang['intersted_cars'] 							= "Interested Cars";
$lang['previously_intersted_cars'] 					= "Previously interested cars";
$lang['car_seller'] 								= "Car seller";
$lang['no_sales_person'] 							= "Sorry, There are no car sellers assigned to this location & channel. First add the car seller to update the status.";
$lang['is_already_registered_with_us']				= "is already registered with us!";
$lang['lead_assigned_successfully'] 				= "Lead Assigned Successfully";
$lang['change_lead_status']							= "Change Lead Status";
$lang['are_you_sure_you_want_to_change_lead_status']= "Are you sure you want to change lead status";
$lang['lead_status_changed_successfully'] 			= "Lead status changed successfully.";
$lang['status'] 									= "Status";
$lang['select_car'] 								= "Select Car";
$lang['delete_lead'] 								= "Delete Lead";
$lang['are_you_sure_you_want_to_delete_this_lead']  = "Are you sure you want to delete this lead";
$lang['lead_notes_updated_successfully'] 			= "Lead notes updated successfully";
$lang['has_changed_the_lead_status_of']				= " has changed the lead status of ";
$lang['added_notes_for_lead']						= " added notes for lead ";
$lang['generate_proposal_of_car']					= " generate proposal of car ";
$lang['lead_with_id'] 								= "Lead with id :";
$lang['select_existing_lead'] 						= "Select an existing lead";
$lang['add_new_lead'] 								= "Add a new lead";
$lang['no_car_seller_has_been_assigned_to_this_lead'] = "No car seller has been assigned to this lead currently";
$lang['please_fill_up_all_the_required_fields'] 	= "Please Fill up all the required fields.";
$lang['no_salesperson_assiged'] 					= "No salesperson assiged";
$lang['salesperson_assiged'] 						= "Salesperson assiged";
$lang['must_be_valid_date'] 						= "must be valid date.";
$lang['please_enter_a_valid_cost'] 					= "Please enter a valid cost";
$lang['lead_has_been_assigned'] 					= " Lead has been assigned";
$lang['lead_has_been_assigned_1'] 					= "A new lead has been assigned to you. Below are the details :";
$lang['please_select_new_car_seller'] 				= "Please select new car seller according to this location & channel";

//--------------attendance--------------------------//
$lang['sales_person_attendance']					= "Car Seller Attendance";
$lang['my_attendance']								= "My Attendance";
$lang['calendar_events'] 							= "Calendar Events";
$lang['add_leave'] 									= "Add Leave";
$lang['edit_leave'] 								= "Edit Leave";
$lang['delete_event'] 								= "Delete Event";
$lang['are_you_sure_you_want_to_delete_this_event']	= "Are you sure you want to delete this event";
$lang['calendar'] 									= "Calendar";
$lang['assign_holiday'] 							= "Assign Holiday";
$lang['title'] 										= "Title";
$lang['please_enter_title'] 						= "Please enter Title.";
$lang['please_enter_a_valid_date'] 					= "Please enter a valid date.";
$lang['leaves_added_successfully'] 					= "Leaves added successfully";
$lang['leaves_updated_successfully'] 				= "Leaves updated successfully";
$lang['leave_added_successfully'] 					= 'Leaves added successfully.';
$lang['added_leaves_for']							= " added leaves for ";
$lang['no_leave_events_found']						= "No leave events found";
$lang['event_deleted_successfully']					= "Event deleted successfully";

//--------global autoscout translations-------------//
$lang['global_autoscout_comments'] 					= "Global autoscout comments";
$lang['global_autoscout_translations']				= "Global Autoscout Translations";
$lang['autoscout_translations']						= "Autoscout Translations";
$lang['gear_type'] 									= "Gear Type";
$lang['translation']								= "Translation";
$lang['internal_name'] 								= "Internal Name";
$lang['autoscout_translation'] 						= "Autoscout Translation";
$lang['autoscout_translations_saved_succesfully']	= "Autoscout Translations saved succesfully";
$lang['error_occured_while_saving_the_translations']= "Error occured while saving the translations";
$lang['autoscout_comments_success'] 				= "Autoscout Comments added successfully.";
$lang['b2_car_comments'] 							= "B2 Car Comments";
$lang['autoscount_comment1']						= "To make text bold prepend and append with ** (ex. **bold** -> ";
$lang['autoscount_comment2']						= "To make list please prepend with * and append with \\ \\(ex. *list1\\ \\-> ";
$lang['autoscount_comment2_next']					= "list1 )";
$lang['autoscount_comment3']						= "Line breaks use \\ \\(equivalent to ";
$lang['autoscount_comment4']						= "Horizontal Line use ---- (equivalent to";
$lang['bold']										= "bold";
$lang['br']							 				= "br";
$lang['hr']											= "hr";
$lang['new_car'] 									= "New Car";
$lang['second_hand_car'] 							= "Second hand Car";
$lang['classic_car'] 								= "Classic Car";

//---------------nissan gamma add,edit,listing---------------//
$lang['gamma'] 										= 'Gamma';
$lang['gammas'] 									= "Gammas";
$lang['gamma_listing'] 								= "Gamma Listing";
$lang['gamma_add'] 									= "Add Gamma";
$lang['subtitle_en'] 								= "Subtitle (English)";
$lang['subtitle_nl'] 								= "Subtitle (Dutch)";
$lang['subtitle_fr'] 								= "Subtitle (French)";
$lang['subtitle_de'] 								= "Subtitle (German)";
$lang['gamma_addded_successfully'] 					= "New Gamma added successfully.";
$lang['gamma_updated_successfully'] 				= "Gamma updated successfully.";
$lang['gamma_updated_error'] 						= "Error while updating Gamma..";
$lang['gamma_error'] 								= "Invalid Data";
$lang['gamma_add'] 									= "Add Gamma";
$lang['gamma_edit'] 								= "Gamma Edit";
$lang['gamma_deleted'] 								= "Gamma Deleted successfully";
$lang['confirm_delete'] 							= "Are you sure you want to delete?";
$lang['invalid_image'] 								= "Please select Valid Image";
$lang['invalid_url'] 								= "Invalid URL";
$lang['external_link'] 								= "External Link";
$lang['choose_location'] 							= "Choose Location";

//--------------frontend forms-------------//
$lang['frontend_forms'] 							= "Frontend Forms";
$lang['call_me_back_tab_name'] 						= "Call Me Back";
$lang['email_us_tab_name'] 							= "Email Us";
$lang['mail_friend_tab_name'] 						= "Mail A Friend";
$lang['send_us_an_email']							= "Send us an e-mail";
$lang['forms'] 										= "Forms";
$lang['friend_email']								= "Friend's Email";
$lang['sell_a_car']									= "Sell A Car";
$lang['build_year'] 								= "Build Year";
$lang['telephone'] 									= 'Telephone';
$lang['is_car_damaged'] 							= "Car Damage";

//-------------roles add,edit,listing, assign----------------------//
$lang['roles'] 										= "Roles";
$lang['assign_role'] 								= "Assign Role";
$lang['role_add'] 									= "Role add";
$lang['role_edit'] 									= "Role edit";
$lang['add_role'] 									= "Add Role";
$lang['select_any_workspace'] 						= "Select any workspace";
$lang['role_listing'] 								= "Role Listing";
$lang['delete_role'] 								= "Delete Role";
$lang['no_roles_to_assign'] 						= "No roles to assign";
$lang['are_you_sure_you_want_to_delete_this_role'] 	= "Are you sure you want to delete this role";
$lang['are_you_sure_you_want_to_delete_this_location'] 	= "Are you sure you want to delete this Location";
$lang['role_added_successfully']					= "Role added successfully";
$lang['there_was_an_error_whiled_adding_the_role']	= "There was an error whiled adding the role";
$lang['there_were_no_permission_to_add']			= "There were no permission to add";
$lang['no_roles_found']								= "No roles found";
$lang['role_updated_successfully']					= "Role updated successfully";
$lang['there_was_an_error_whiled_updating_the_role']= "There was an error whiled updating the role";
$lang['no_role_found']								= "No role found";
$lang['role_assigned_successfully']					= "Role assigned successfully";
$lang['role_removed_successfully']					= "Role removed successfully";
$lang['workspace_added_to_role']					= "workspace added to role";
$lang['role_deleted_successfully']					= "Role deleted successfully";
$lang['predefined_role_cannot_be_deleted']			= "Predefined role cannot be deleted";
$lang['assigned_roles_cannot_be_deleted']			= "Assigned roles cannot be deleted";
$lang['permissions']								= "Permissions";
$lang['extra_permissions_removed_successfully']		= "Extra permissions removed successfully";
$lang['permission_granted']							= "permission granted";
$lang['extra_permissions_added_successfully']		= "Extra permissions added successfully";
$lang['no_permissions_found']						= "No permissions found";
$lang['permissions_found']							= "Permissions found";
$lang['no_such_permission']							= "No such permission";
$lang['invalid_permission']							= "Invalid permission";
$lang['permission_delete_successfully']				= "permission delete successfully";
$lang['no_permission_found']						= "No permission found";
$lang['there_were_no_permission_to_update']			= "There were no permission to update";
$lang['permission_added_successfully'] 				= "Permission added successfully";
$lang['has_changed_the_permission'] 				= "has changed the permission.";

//-----------------------lead times-------------------------//
$lang['lead_times']									= "Lead times";
$lang['link_office_365']							= "Link office 365";
$lang['settings_updated_successfully']				= "Settings updated successfully";
$lang['do_you_want_to_sync_your_office_365_account']= "Do you want to Sync your office 365 account";
$lang['you_have_already_linked_office_365_account']	= "You have already linked Office 365 account";
$lang['lead_times_settings'] 						= "Lead Times Settings";
$lang['settings_updated_sucessfully'] 				= "Settings updated sucessfully";
$lang['no_settings_found']							= "No settings found";

//-----------------location add,edit,listing---------------//
$lang['add_location'] 								= "Add Location";
$lang['edit_location'] 								= "Edit Location";
$lang['location_listing'] 							= "Location Listing";
$lang['location_name'] 								= "Location Name";
$lang['location_code'] 								= "Location Code";
$lang['user_name'] 									= "User Name";
$lang['no_locations_present'] 						= "No Locations present";
$lang['is_car_seller_assigned'] 					= "Is car seller assigned";
$lang['is_visible'] 								= "Is visible";
$lang['contact_email']								= "Contact Email";
$lang['contact_telephone']							= "Contact Telephone";
$lang['contact_fax']								= "Contact Fax";
$lang['opening_and_closing_time']					= "Opening and closing time";
$lang['add_autoscout_account'] 						= "Add autoscout account";
$lang['no_autoscout_accounts_added'] 				= "No autoscout accounts added yet.";
$lang['autoscout_accounts']							= "Autoscout accounts";
$lang['delete_autoscout_account'] 					= "Delete autoscout account";
$lang['are_you_sure_you_want_to_delete_this_autoscout_account'] = "Are you sure you want to delete this autoscout account?";
$lang['autoscout_account_add_msg'] 					= 'All respective autoscout accounts will be added once the location is added.';
$lang['autoscout_username'] 						= "Autoscout Username";
$lang['autoscout_password'] 						= "Autoscout Password";
$lang['autoscout_email'] 							= "Autoscout Email";
$lang['autoscout_dealer_id'] 						= "Autoscout Dealer id";
$lang['location_added_successfully']				= "Location added successfully";
$lang['location_updated_successfully']				= "Location updated successfully.";
$lang['location_deleted_successfully']				= "Location deleted successfully.";
$lang['cannot_delete_location']						= "Cannot delete location.";
$lang['delete_location']							= "Delete location.";
$lang['no_locations_added_yet']						= "No locations added yet";
$lang['locations_added_successfully']				= "Locations added successfully";
$lang['error_encountered_while_adding_locations']	= "Error encountered while adding Locations";

//----------------finance-------------------//
$lang['finance'] 									= "Finance";
$lang['finance_module'] 							= "Finance Module";
$lang['advance'] 									= "Advance";
$lang['months'] 									= "Months";
$lang['co_efficient_values'] 						= "Co-efficient Values";
$lang['used_cars'] 									= "Used Cars";
$lang['warranty'] 									= "Warranty";
$lang['years'] 										= "Years";
$lang['year'] 										= "Year";
$lang['year_small'] 								= "year";
$lang['amount'] 									= "Amount";
$lang['car_ready_charge'] 							= "Car Ready Charge";
$lang['rest'] 										= "Rest";
$lang['has_updated_finance_settings']				= " has updated finance settings";
$lang['finance_parameters_successfully_updated']	= "Finance parameters successfully updated.";
$lang['please_enter_advance_greater_than_the_minimum_advance_of']= "Please enter advance greater than the minimum advance of ";
$lang['and_a_maximum_of']							= " and a maximum of ";
$lang['car_finance_not_available_for_balance_amount_less_than_3700']= "Car Finance not available for balance amount less than 3700";

//---------------------newsletters-----------------//
$lang['newsletters']							    = "Newsletters";
$lang['newsletters_listing']						= "Newsletters Listing";
$lang['no_newsletters_found'] 						= "No newsletters found";

//----------------------password settings------------------//
$lang['enter_existing_password']					= "Enter Existing Password";
$lang['enter_new_password']							= "Enter New Password";
$lang['confirm_new_password']						= "Confirm New Password";
$lang['password_changed_successfully']				= "Password Changed Successfully.";
$lang['error_in_setting_new_password']				= "Error in setting new password.";
$lang['log_out']									= "Logout";
$lang['log_in']										= "Login";

//--------------orders------------------------//
$lang['orders']										= "Orders";
$lang['ordered_by'] 								= "Ordered By";
$lang['pending'] 									= "Pending";
$lang['cancelled'] 									= "Cancelled";
$lang['paid'] 										= "Paid";
$lang['no_details_to_display']						= "No details to display as the order is not completed.";
$lang['order_has_been_updated_successfully']		= "Order has been updated successfully";
$lang['error_occurred_while_updating_the_order']	= "Error occurred while updating the order";
$lang['order_has_been_confirmed_successfully']		= "Order has been confirmed successfully";
$lang['error_occurred_while_confirming_the_order']	= "Error occurred while confirming the order";
$lang['no_pending_orders_for_this_car_found']		= "No pending orders for this car found";
$lang['orders_already_pending_for_this_car_please_try_again_after_15_minutes']= "Orders already pending for this car. Please try again after 15 minutes";
$lang['pending_orders_are_cancelled_successfully']	= "Pending orders are cancelled successfully";
$lang['error_occurred_while_cancelling_the_pending_orders']= "Error occurred while cancelling the pending orders";
$lang['no_orders_found']							= "No orders found";
$lang['first_use']									= "First Use";
$lang['capital']									= "Capital";
$lang['displacement']								= "Displacement";
$lang['exterior_interior']							= "Exterior/interior";
$lang['coachwork']									= "Coachwork";
$lang['yes']										= "Yes";
$lang['automedia_link']								= "Automedia Link";
$lang['tax_deductible']								= "Tax deductible";
$lang['no_commission']								= "No Commission";
$lang['main_equipment']								= "Main Equipment";

//-------------------other data------------------//
$lang['last_sync_happened_on']						= "Last sync happened on";
$lang['which_is_more_than_24_hours_from_now']		= " which is more than 24 hours from now";
$lang['admin']										= "Admin";
$lang['car_manager']								= "Car Manager";
$lang['sales_manager'] 								= "Sales Manager";
$lang['car_inputter'] 								= "Car Inputter";
$lang['administration'] 							= "Administration";
$lang['customer_information'] 						= "Customer information";
$lang['user_with_id'] 								= "User with id :";

//---------------------car updates-------------------//
$lang['car_updates'] 								= "Car Updates";
$lang['is_deleted_from_the_icar_system']			= "is deleted from the Icar system";
$lang['ICar_updates']								= "ICar Updates";
$lang['Bcar_updates']								= "Bcar Updates";
$lang['car_details_from_bcar'] 						= "Car details from Bcar";
$lang['you_must_be_logged_in'] 						= "You must be logged in";


//-----------------activities-----------------------//
$lang['no_activity_found']							= "No activity found";
$lang['there_are_no_activities_added_yet'] 			= "There are no activities added yet.";
$lang['there_are_no_activities_added_yet']			= "there are no activities added yet";
$lang['see_all_the_activities']						= "see all the activities";
$lang['has_logged_in_to_the_system_as_admin']		= " has logged in to the system as admin";
$lang['has_logged_in_to_the_system_as_client']		= " has logged in to the system as client";
$lang['has_logged_in_to_the_system_as_dealer']		= " has logged in to the system as dealer";
$lang['has_logged_out_of_the_system_as_admin']		= " has logged out of the system as admin";
$lang['added_a_car']								= " added a car ";
$lang['updated_a_car']								= " updated a car ";
$lang['deleted_a_car']								= " deleted a car ";
$lang['has_searched_cars_by_car_specifications']	= " has searched cars by car specifications ";
$lang['has_added_user']								= " has added user ";
$lang['has_updated_user']							= " has updated user ";
$lang['has_deleted_user']							= " has deleted user ";
$lang['has_added_role']								= " has added role ";
$lang['has_updated_role']							= " has updated role ";
$lang['has_deleted_role']							= " has deleted role ";
$lang['has_added_client']							= " has added client "; 
$lang['has_updated_client']							= " has updated client ";
$lang['has_deleted_client']							= " has deleted client ";
$lang['has_applied_for_the_reservation_of']			= " has applied for the reservation of ";
$lang['added_accessory']							= " has added accessory.";
$lang['edited_accessory']							= " has updated accessory.";
$lang['deleted_accessory']							= " has deleted accessory.";
$lang['published_accessory_on']						= " published accessory ";
$lang['unpublished_accessory_on']					= " unpublished accessory ";
$lang['added_option']								= " added option ";
$lang['removed_option']								= " removed option ";
$lang['published_car_in_channel']					= " published car ";
$lang['unpublished_car_in_channel']					= " unpublished car ";
$lang['registered_to_the_system_as_client']			= " registered to the system as client ";
$lang['registered_to_the_system_as_dealer']			= " registered to the system as dealer ";
$lang['has_logged_out_of_the_system_as_client']		= " has logged out of the system as client ";
$lang['has_logged_out_of_the_system_as_deale']		= " has logged out of the system as dealer ";
$lang['has_requested_for_notification_of_car']		= " has requested for notification of car ";
$lang['has_searched_cars_by_car_parameters']		= " has searched cars by car parameters ";
$lang['has_added_location']							= " has added location ";
$lang['has_updated_location']						= " has updated location ";
$lang['has_deleted_location']						= " has deleted location ";
$lang['has_set_delivery_date_of_car_to']			= " has set delivery date of car ";
$lang['has_updated_delivery_date_of_car_to']		= " has updated delivery date of car ";
$lang['has_accepted_queued_car']					= " has accepted queued car ";
$lang['has_rejected_queued_car']					= " has rejected queued car ";
$lang['has_unsubscribed_for_newsletter']			= " has unsubscribed newsletter.";

//-------------image upload and delete-----------//
$lang['add_files'] 									= "Add files";
$lang['start_upload'] 								= "Start upload";
$lang['cancel_upload'] 								= "Cancel upload";
$lang['processing'] 								= "Processing";
$lang['start'] 										= "Start";
$lang['the_uploaded_file_doesnt_seem_to_be_an_image']= "The uploaded file doesn't seem to be an image";
$lang['there_was_an_error_while_uploading_image']   = "There was an error while uploading image";
$lang['Image_deleted_success'] 						= 'Image deleted successfully.';
$lang['Image_deleted_error'] 						= 'Error while deleting data.';
$lang['image_uploaded_successfully'] 				= 'Image uploaded successfully.';
$lang['are_you_sure_you_want_to_delete_this_image'] = "Are you sure you want to delete this image?";

//----------pdf data-----------------//
$lang['Automatic_for_pdf'] 							= 'Automaat';
$lang['Manual_for_pdf'] 							= 'Manueel';
$lang['Petrol_for_pdf'] 							= 'Benzine';
$lang['Diesel_for_pdf'] 							= 'Diesel';
$lang['Electric_for_pdf'] 							= 'Elektrisch';
$lang['Hybrid_for_pdf'] 							= 'Hybrid';
$lang['doors_for_pdf'] 								= "deuren";
$lang['a4_alphabate_footer_msg_4'] 					= ". Informeer bij je B2-adviseur. Let op, geld lenen kost ook geld.";
$lang['advance_for_pdf'] 							= "voorschot";
$lang['residual_for_pdf'] 							= "restwaarde";
$lang['maturity_for_pdf'] 							= "looptijd";
$lang['general_information'] 						= "General Information";
$lang['yes_for_pdf'] 								= "Ja";
$lang['headline_for_car_alarm_template'] 			= "The cars below are newly added to Bcar. You can click on the car to see the car details.";
$lang['please_find_the_attached_images_below'] 		= "Attached to this email you can find the pictures of the car you are interested in.";
$lang['dear'] 										= "Dear";
$lang['team'] 										= "Team";
$lang['logo_op_offerte'] 							= "Logo op offerte";
$lang['Beerens_B2_Noorderlaan'] 					= "Beerens B2";
$lang['Best_in_second_hand_cars'] 					= "Best in second hand cars";
$lang['Technische_gegevens'] 						= "Technische gegevens";
$lang['Eerste_registratie']  						= "Eerste registratie";
$lang['KM_stand']  									= "KM-stand";
$lang['Vermogen'] 									= "Vermogen";
$lang['Cilinderinhoud'] 							= "Cilinderinhoud";
$lang['Brandstof'] 									= "Brandstof";
$lang['Transmissie'] 								= "Transmissie";
$lang['Koetswerk'] 									= "Koetswerk";
$lang['Exterieur_Interieur'] 						= "Exterieur / Interieur ";
$lang['Btw_aftrekbaar'] 			    			= "Btw aftrekbaar";
$lang['Opties'] 									= "Opties";
$lang['Opmerkingen'] 								= "Opmerkingen";
$lang['CNR_1BVA642'] 								= "CNR";
$lang['no_options_available'] 						= "No options available";
$lang['1_ste_gebruik']								= "1-ste gebruik";
$lang['voorschot']									= "voorschot";
$lang['INCLUSIEF_5_JAAR_GARANTIE']      			= "INCLUSIEF 5 JAAR GARANTIE";
$lang['Belangrijkste_uitrusting']					= "Belangrijkste uitrusting";
$lang['Extra_diensten']								= "Extra diensten";
$lang['Tot_5_jaar_garantie']						= "Tot 5 jaar garantie";
$lang['Express_levering']							= "Express levering";
$lang['B2_Finance']									= "B2 Finance";
$lang['Overname']									= "Overname";
$lang['Comm_nr']									= "Comm. nr";
$lang['Comm_nr_Chassis']							= "Comm. nr / Chassis";
$lang['a4_alphabate_footer_msg_1']					= "Exclusief 5 jaar garantie, exclusief € 175 kosten rijklaar eindgebruiker België. (officiële voorplaat, driehoek, brandblusser, fluojasje, verbanden, aanvraag div, cleaning, keuring)";
$lang['a4_alphabate_footer_msg_2']					= "Inclusief 5 jaar garantie, inclusief kosten rijklaar eindgebruiker België. B2 finance geldig mits goedkeuring van het dossier door de kredietgever en onder voorbehoud tariefwijziging.";
$lang['a4_alphabate_footer_msg_3']					= "Verkoop op afbetaling,";


//----------------dutch data for barcode---------------
$lang['aftomen_motors'] 							= "Afstomen motor+";
$lang['aftomen_weikasten'] 							= "Afstomen weilkasten";
$lang['aftomen_uitspuiten'] 						= "Afstomen + uitspuiten sluitkanten, koffer en duren";
$lang['reinigen_velgen'] 							= "Reinigen velgen";
$lang['reinigen_wintervelgen']						= "Reinigen wintervelgen, verpakken en in wagen leggen";
$lang['Klok_juist'] 								= "Klok juist zetten indien nodig";
$lang['alle_stickers'] 								= "Alle stickers verwijderen interieur en exterieur";
$lang['banden_invetten'] 							= "Banden invetten, waterpeil en olie controleren";
$lang['chrome_lijsten'] 							= "Chrome lijsten extra reinigen";
$lang['alle_papieren'] 								= "Alle papieren en documenten uit de wagen halen";
$lang['handschoenkastje'] 							= "Handschoenkastje, opbergvakken, middenconsole reinigen";
$lang['plaathouder'] 								= "Plaathouder Beerens recht bevestigen";
$lang['voor_en'] 									= "Voor- en achterplaten Beerens publiciteit laten hangen";
$lang['spiegelhouder'] 								= "Spiegelhouder laten hangen";
$lang['prijslabel'] 								= "Prijslabel A3 formaat laten liggen";
$lang['koffer_reinigen'] 							= "Koffer reinigen";
$lang['wagen_poetsen'] 								= "Wagen poetsen";
$lang['simoniseren_wagen'] 							= "Simoniseren wagen";
$lang['DIT_DOCUMENT_AFGEVEN'] 						= "DIT DOCUMENT AFGEVEN AAN BART NA UITVOERING";
$lang['handtekening_verantwoordelijke'] 			= "handtekening verantwoordelijke reiniging";
$lang['opkuis_na_keuring'] 							= "Opkuis na keuring";
$lang['opkuis_intake'] 								= "Opkuis intake";
$lang['unique_identifier'] 							= "Unique identifier";
$lang['comission_number'] 							= "Commission Number";

//-----------------order note----------------------//
$lang['on_order_note_title'] 						= 'VERKOOPOVEREENKOMST';
$lang['on_verkoper'] 								= 'VERKOPER';
$lang['on_koper'] 									= 'KOPER';
$lang['on_particulier'] 							= 'Particulier';
$lang['on_business'] 								= 'Bedrijf';
$lang['on_name'] 									= 'Naam';
$lang['on_first_name'] 								= 'Voornaam';
$lang['on_address'] 								= 'Adres';
$lang['on_familyname'] 								= 'Familienaam';
$lang['on_place'] 									= 'Plaats';
$lang['on_street'] 									= 'Straat + nr';
$lang['on_tel'] 									= 'Tel';
$lang['on_postcode'] 								= 'Postcode';
$lang['on_fax'] 									= 'Fax';
$lang['on_btw'] 									= 'BTW';
$lang['on_email'] 									= 'E-mail';
$lang['on_reknr'] 									= 'Reknr';
$lang['on_website'] 								= 'Website';
$lang['on_business_name'] 							= 'Bedrijfsnaam';
$lang['on_btw_nr'] 									= 'BTW Nr.';
$lang['on_order_note_text_1'] 					= 'De ondergetekende verkoper verklaart op zijn erewoord wettelijk bevoegd te zijn om kleinhandelverkoop van nieuwe en tweedehandse voertuigen uit te voeren en verklaart te voldoen aan alle wettelijke en voorgeschreven voorwaarden inzake de toegang tot het beroep. De koper BESTELT, aan de bijzondere voorwaarden die volgen en aan de algemene verkoop- en garantievoorwaarden op de keerzijde, waarvan de koper verklaart kennis te hebben genomen en ze te aanvaarden, het hieronder beschreven voertuig:';
$lang['on_make'] 									= 'Merk';
$lang['on_1st_registration'] 						= '1 ste inschrijving';
$lang['on_Beschikbare_documenten'] 					= 'Beschikbare documenten';
$lang['on_model'] 									= 'Model';
$lang['on_pk'] 										= 'PK';
$lang['on_Inschrijvingsbewijs'] 					= 'Inschrijvingsbewijs';
$lang['on_color'] 									= 'Kleur';
$lang['on_cc'] 										= 'Cc';
$lang['on_Gelijkvormigheidsattest'] 				= 'Gelijkvormigheidsattest';
$lang['on_fuel'] 									= 'Brandstof';
$lang['on_mileage'] 								= 'Kilometerstand';
$lang['on_Onderhoudsboekje'] 						= 'Onderhoudsboekje';
$lang['on_chassis_number'] 							= 'Chassis nr';
$lang['on_carpass'] 								= 'Carpass';
$lang['on_price_excl_btw'] 							= 'Prijs ex. BTW';
$lang['on_Verkoop_onderworpen'] 					= 'Verkoop onderworpen';
$lang['on_Verkoper'] 								= 'Verkoper';
$lang['on_Bedrag_BTW'] 								= 'Bedrag BTW';
$lang['on_aan_bijzondere_marge_regeling'] 			= 'aan bijzondere marge regeling';
$lang['on_Afspraak_afhaling_wagen'] 				= 'Afspraak afhaling wagen';
$lang['on_price_incl_btw'] 							= 'Prijs incl. BTW';
$lang['on_date'] 									= 'Datum';
$lang['on_advance'] 								= 'Voorschot';
$lang['on_btw_niet_aftrekbaar'] 					= 'btw niet aftrekbaar';
$lang['on_hours'] 									= 'Uur';
$lang['on_extras'] 									= 'Extra\'s';
$lang['on_yes'] 									= 'Ja';
$lang['on_no'] 										= 'Neen';
$lang['on_afhaling_ati'] 							= 'Afhaling ATI';
$lang['on_extra_garantie'] 							= 'Extra garantie';
$lang['on_saldo'] 									= 'Saldo';
$lang['on_accessories_included'] 					= 'Uit te voeren werken / toebehoren inbegrepen / notities';
$lang['on_accessories_pay'] 						= 'Uit te voeren werken / toebehoren te betalen';
$lang['on_order_note_text_3'] 						= 'Overname van het voertuig toebehorend aan de koper, in goede wettelijke en mechanische staat';
$lang['on_make_type'] 								= 'Merk en type';
$lang['on_Opmerkingen'] 							= 'Opmerkingen';
$lang['on_Opmerkingen_bij_verkoop'] 				= 'Opmerkingen bij verkoop';
$lang['on_order_note_text_4'] 						= 'Wettelijke garantie van 1 jaar volgens bepalingen van Federauto.';
$lang['on_order_note_text_2'] 						= 'Enkel in geval van verkoop afgesloten buiten de vestiging van de verkoper. Binnen de zeven werkdagen te tellen vanaf de dag na ondertekening van onderhavige overeenkomst heeft de koper het recht om zonder kosten af te zien van zijn aankoop op voorwaarde dat hij de verkoper hiervan verwittigd bij een ter post aangetekend schrijven. Elke clausule waardoor de koper zou afzien van dit recht is nietig. Aangaande de eerbieding v/d termijn volstaat het dat de kennisgeving verstuurd wordt voor het verstrijken van de termijn.';
$lang['on_order_note_text_5'] 						= 'Verkoop afgesloten in het bedrijf van de verkoper.';
$lang['on_order_note_text_6'] 						= 'In duplo opgesteld, waarvan elke partij erkent zijn exemplaar te hebben ontvangen.';
$lang['on_de_verkoper'] 							= 'De Verkoper';
$lang['on_de_koper'] 								= 'De Koper';
//-------color code-------------------------------------------//
$lang['color_codes_added_successfully'] 			= "Color code added successfully";
$lang['add_color_codes'] 							= "Add color code";
$lang['color_code_listing'] 						= "Color code listing";
$lang['delete_color_code'] 							= "Delete color code";
$lang['are_you_sure_you_want_to_delete_this_color_code'] = "Are you sure you want to delete this color code";
$lang['color_id'] 									= "Color id";
$lang['edit_color_codes'] 							= "Edit color code";

//-----new oreder document text----------------//
$lang['view_order_note']							= 'View order note';
$lang['car_accessories_carried'] 					= 'Uit te voeren werken / toebehoren inbegrepen';
$lang['price_incl_btw'] 							= 'Price incl. btw';
$lang['takeover_clients_previous_car'] 				= 'Takeover client\'s previous car';
$lang['please_update_car_delivery_date'] 			= 'Please Update Delivery date to generate order note.';

//---------change lead status from sold----------//
$lang['please_choose_an_option_from_option_given_below'] = "Please choose an option from options given below";
$lang['undo_sold_lead']								= "Undo Sold Lead";
$lang['customer_interested_again']					= "Customer Interested Again";
$lang['following_actions_will_automatically_get_placed'] = "Following actions will automatically get placed";
$lang['confirm_msg_for_undo_sold'] 					= "Are you sure? You want to change status from sold to";
$lang['following_data_will_be_deleted']             = "Follwing data will be deleted.";
$lang['following_cars_will_be_placed_in_reserved_state'] = "Following cars will get placed in reserved state.";
$lang['this_lead_have_not_sold_any_car_yet'] 		= "This lead haven't sold any car yet.";

/*-------end revision 1----------add new translatons here*/
$lang['Verkoper'] 									= "Verkoper";
$lang['duration'] 									= "Maanden";
$lang['per_month'] 									= "per month";

/*-------car edit-----------------------------------*/
//export button
$lang['no_for_pdf']                                 = "Neen";

/**********************color codes********************************/
$lang['exterior_color_code']                        = "Exterior color codes";
$lang['is_color_id_associated']						= "Is color id associated";
$lang['color_code']									= "Color code";
$lang['please_enter_unique_color_id']				= "This color id already exists please enter other color id";
$lang['please_enter_unique_color_code']             = "Please enter unique color code";
$lang['edit_exterior_color_codes']					= "Edit exterior color codes";
$lang['are_you_sure_you_want_to_delete_this_color_code']  = "Are you sure you want to delete this color code";
$lang['this_color_is_associated']                   = "This color cannot be deleted as it is associated with a car";

/**********************Activities********************************/
$lang['recent_activities']                         = "Recent Activities";

/*******************color code************************************/
$lang['color_listing']									  = "Color listing";
$lang['no_color_found']                             = "No color found";

//---------update sold car data----------------
$lang['update_sold_car_data']                      = "Update Sold Car Data";   
$lang['sold_car_data_updated_successfully'] 	   = "Sold Car data updated successfully.";

//---------reports-----------------------------------------------------------------------
$lang['amount_of_accessories']                     = "Amount of accessories";
$lang['amount_of_guarantee']                       = "Amount of guarantee";
$lang['amount_of_financing']                       = "Amount of financing";
$lang['amount_of_leads_lost']                      = "Amount of leads lost";

//------------clients-----------------------------------------------------------------
$lang['search_for_clients']                        = "Search for clients";

//-----------organisations-----------------------//
$lang['organisations']                        		= "Organisations";
$lang['organisation']                        		= "Organisation";
$lang['organisation_listing']                       = "Organisation Listing";
$lang['no_organisation_found']						= "No organisation found.";
$lang['are_you_sure_you_want_to_delete_this_organisation'] = "Are You sure you want to delete this organisation?";
$lang['organisation_added_successfully']			= "Organisation added successfully.";
$lang['error_while_adding_organisation']			= "Error while adding organisation";
$lang['organisations_updated_successfully']			= "Organisations updated successfully.";
$lang['error_while_updating_organisation']			= "Error while updating organisation.";
$lang['you_can_not_delete_this_organisation']		= "You can not delete this organisation as this is associated with an user.";
$lang['btw']                        				= "BTW";
$lang['car_seller_info']							= 'Car seller info';
$lang['associate']									= 'Associate';
$lang['associate_users_to_organisation']			= 'Associate users to organisation';

//----------------car seller serach in sold cars--------------//
$lang['car_sold_by_car_seller']                     = "Car sold by car seller";

// -------------Lead new text -------------------------//
$lang['select_existing_lead_or_customers']          = "Select existing lead or customers";
$lang['enable_organization']						= "Enable company";
$lang['add_new_company']							= "Add New Organization";

// order note text
$lang['enable_company'] 							= 'Enable company';
$lang['add_organisation'] 							= 'Add Organisation';
$lang['select_organisation'] 						= 'Select Organisation';
$lang['statistics_for_the_past_30_days']            = "Statistics for the past 30 days ";

//car list
$lang['registeration_date']							= "Registration date";

//RevISION 2-- add new langs here -----------------------------

$lang['no_organisation_associated']					= 'No Organisations Added Yet!';
$lang['organisation_associated_with_lead']			= 'Organisations Associated With This lead :';
$lang['mail_sent_successfully']						= "Mail sent successfully.";
$lang['finance_not_available'] 						= 'It’s not possible to have a finance on this car.';
$lang['Financing_amount_is_too_low_to_be_financed']	= "Financing amount is too low to be financed.";


$lang['price_incl_percent_btw'] = 'Price incl. %s&#37; btw';
$lang['price_excl_percent_btw'] = 'Price excl. %s&#37; btw';
$lang['price_excl_btw'] = 'Price excl. btw';
$lang['on_car_ready_charge'] = 'Kosten rijklaar';
$lang['basic_guarantee_1_year'] = 'Basic guarantee 1 year';

$lang['view_proposal'] 							= "View Proposal";

$lang['car_channels'] 							    = "Car channels";
$lang['there_was_an_error_please_contact_administrator_permission'] = "You don't have permission to update lead status. Please contact administrator.";
$lang['Petrol'] 									= 'Petrol';
$lang['Diesel'] 									= 'Diesel';
$lang['Electric'] 									= 'Electric';
$lang['Hybrid'] 									= 'Hybrid';
$lang['by'] 										= 'by';

$lang['offer_data_msg']                             = 'and proposals generated for this car';
$lang['Car_reserved_cannot_be_sold']                = 'Car reserved by another user and cannot be sold.';
$lang['publish_system_user']						= "Publish System User";
$lang['you_can_not_unpublish_this_channel_as_this_is_assigned_to_a_lead'] = "You can not unpublish this channel as this is assigned to a lead.";
$lang['channel_published_successfully']				= "Channel published successfully.";
$lang['channel_unpublished_successfully']			= "Channel unpublished successfully.";


$lang['email_for_offer'] 							= "Email";
$lang['desired_date']								= "Desired Date";
$lang['after_sales_url'] 							= "Aftersales";
//generate proposal pdf 
$lang['koper']                                      = "Client";

$lang['group'] 		= 'Group';
$lang['groups'] 	= 'Groups';
$lang['add_group'] 	= 'Add group';
$lang['group_listing'] 	= 'Group listing';
$lang['no_group_found'] 	= 'No group found';
$lang['add_users'] 	= 'Add users';

$lang['delete_group'] 	= 'Delete group';
$lang['are_you_sure_you_want_to_delete_this_group'] 	= 'Are you sure you want to delete this group ?';
$lang['edit_group'] 	= 'Edit group';
$lang['group_added_successfully'] 	= 'Group added successfully.';
$lang['group_deleted_successfully'] 	= 'Group deleted successfully.';
$lang['you_can_not_delete_this_group'] 	= 'You can not delete this group.';
$lang['send_newsletter'] 	= 'Send newsletter';

$lang['group_updated_successfully'] 	= 'Group updated successfully.';
$lang['error_while_updating_group'] 	= 'Error occurred while updating group';
$lang['error_while_adding_group'] 	= 'Error occurred while adding group';
$lang['user_deleted_from_group_successfully'] 	= 'User deleted from this group successfully.';

$lang['select_groups_and_users'] 	= 'Select groups and users';
$lang['select_groups'] 	= 'Select groups';
$lang['select_users'] 	= 'Select users';
$lang['select_cars'] 	= 'Select cars';
$lang['email_body'] 	= 'Email Content';
$lang['email_preview'] 	= 'Email Preview';

$lang['subject'] 	= 'Subject';
$lang['body'] 	= 'Body';
$lang['please_select_atleast_one_user_or_one_group'] 	= 'Please select atleast one user or one group';

$lang['generate_proposal_email_msg']                = "A proposal will be generated for this lead,but lead email will be not be updated as this email already exists in user.";

$lang['please_enter_unique_chassis_number'] 	= 'Please enter unique chassis number';
$lang['please_enter_unique_commission_number'] 	= 'Please enter unique commission number';

$lang['extract_search_results']					= "Extract Search Results";
$lang['per']									= "per";
$lang['vaa']									= "V.A.A";
$lang['error_msg_in_leasense_price']			= "You have to select at least one price(per month)";
$lang['extract_search_results']					= "Extract Search Results";
$lang['calculate_dealer_price']                 = "Calculate dealer price";

// Banner
$lang['banner'] 								= "Banner";
$lang['banners'] 								= "Banners";
$lang['add_banner']  							= "Add Banner";
$lang['banner_listing']							= "Banner Listing";
$lang['banner_error'] 							= "Invalid Data";
$lang['banner_addded_successfully'] 			= "Banner added successfully.";
$lang['banner_deleted'] 						= "Banner Deleted successfully";
$lang['image_dimension']						= "( Please select image of dimension 2246x893 )";

//order note location listing
$lang['on_margin_cars']							= "Margin cars";
$lang['order_note']								= "Order note";
$lang['no_location_found'] 						= "No location found";
$lang['website'] 								= "Website";
$lang['account_number'] 						= "Account number";
$lang['choose_car_seller_company_locations'] 	= "Choose car seller company locations";
$lang['order_note_locations'] 					= "Order note locations";

//leasense home page cars
$lang['home_page_cars'] 						= "Home Page Cars";
$lang['updated_successfully']                   = "Updated successfully"; 
$lang['selling_price']                          = "Selling price";


// Blogs
$lang['blogs'] = "Blogs";
$lang['url'] = "Url";
$lang['add_blog'] = "Add blog";
$lang['edit_blog'] = "Edit blog";
$lang['delete_blog'] = "Delete blog";
$lang['blog_listing'] = "Blog listing";
$lang['no_blogs_added_yet'] = "No blogs added yet";
$lang['are_you_sure_you_want_to_delete_this_blog'] = "Are you sure, you want to delete this blog ?";
$lang['publish_blogs'] = "Publish blogs";
$lang['select_channel'] = "Select channel";
$lang['blog_channel_added'] = "Blog is successfully published.";
$lang['error_adding_blog_channel_please_try_again'] = "Error occured while publish blog.Please try again.";
$lang['blog_channel_removed'] = "Blog is successfully unpublished.";
$lang['error_removing_blog_channel_please_try_again'] = "Error occured while unpublish blog.Please try again.";
$lang['no_car_found']								= "No car found";
//leasense tab
$lang['save_for_leasense_tab'] 						= "Save";
$lang['prices'] 			 						= "Prices";
$lang['has_checked_car_finance_option'] 			= "has checked car finance option.";
$lang['has_reserved_a_car'] 						= 'has reserved a car ';

//settings tab:blogs
$lang['blog_successfully_added'] 					= 'Blog successfully added';
$lang['error_adding_blog'] 							= 'Error adding blog';


$lang['b2b'] 										= 'B-Rent';
$lang['with_images']								= "with images";
$lang['b2b'] 										= 'B-Rent';
$lang['b2b'] 										= 'B-Rent';
$lang['subscription'] 								= 'Subscription';
$lang['finance_enabled']							= "Enable Finance";
$lang['history']									= "History";
$lang['car_status_history']							= "Car Status History";
$lang['car_owner_history']							= "Car Owner History";
$lang['car_import_history']							= "Car Import History";
$lang['car_publish_history']						= "Car Publish History";
$lang['no_results_found'] 							= "No results found.";
$lang['import']										= "Import";
$lang['reservation_cancelled_successfully']			= "Reservation cancelled successfully.";
$lang['cancel_reservation']							= "Cancel Reservation";
$lang['task']										= "Task";
$lang['added_by']									= "Added By";
$lang['different_car_seller_msg']					= "This lead is already assigned to ";
$lang['lead_take_over_msg']							= "Do you really want to assign this lead to yourself?";
$lang['leads_with_my_channels_and_locations']		= "leads with my channels and locations";
$lang['you_can_not_change_the_status_of_this_lead_as_this_lead_is_assigned_to'] = "You can not change the status of this lead as this lead is assigned to ";
$lang['you_can_not_change_the_status_of_this_lead_as_this_lead_as_this_is_not_assigned_to_you'] = "you can not change the status of this lead as this lead as this is not assigned to you.";

$lang['send_car_pictures_email_subject'] 			= "Beerens Cars: Pictures";
$lang['Your_assigned_lead_shifted_to']				= "Your assigned lead shifted to";
$lang['lead_take_over_email_param_1']				= "On request of Car seller";
$lang['lead_take_over_email_param_2']				= "has been taken away from you and has been shifted.";
$lang['add_client_as_lead']							= "Add client as lead";
$lang['client_succefully_added_as_lead']			= "Client successfully added as a lead.";

$lang['add_interested_cars']						= "Add interested cars";
$lang['car_seller_log']								= "Car Seller logs";
$lang['assigned_to']								= "Assigned to";
$lang['assigned_on']								= "Assigned on";
$lang['car_proposals_listing'] 						= "Car proposals listing";
$lang['view_generate_proposal']						= "View / Generate Proposals";
$lang['location_codes']								= "Location Codes";
$lang['location_codes_listing']						= "Location Codes Listing";
$lang['channel']									= "Channel";
$lang['view_car_details']							= "View car details";
$lang['reservation_cancelled_on']					= "Reservation Cancelled On";
$lang['processed']									= "Processed";

$lang['newsletter_listing']							= "Newsletter listing";
$lang['create_newsletter']							= "Create Newsletter";
$lang['newsletter_template']						= "Newsletter Template";
$lang['delete_newsletter_template']					= "Delete Newsletter Template";
$lang['are_you_sure_you_want_to_delete_this_newsletter_template'] = "Are you sure, you want to delete this newsletter template ?";
$lang['newsletter_template_deleted_successfully']	= "Newsletter Template deleted successfully";
$lang['select_newsletter']							= "Select Newsletter";
$lang['newsletter_template_created_successfully']	= "Newsletter template created successfully";
$lang['newsletter']									= "Newsletter";

//-----months--------//
$lang['January']									= 'Januari';
$lang['February']									= 'Februari';
$lang['March']										= 'Maart';
$lang['April']										= 'April';
$lang['May']										= 'Mei';
$lang['June']										= 'Juni';
$lang['July']										= 'Juli';
$lang['August']										= 'Augustus';
$lang['September']									= 'September';
$lang['October']									= 'Oktober';
$lang['November']									= 'November';
$lang['December']									= 'December';

$lang['layout']										= 'Layout';
$lang['layout_1']									= 'Layout 1';
$lang['layout_2']									= 'Layout 2';
$lang['layout_3']									= 'Layout 3';

$lang['alphabet_regards_name'] = "Alphabet Car Square";
$lang['alphabet_email_footer_left_content']				= "<b>Alphabet Car Square</b><br>Boomsesteenweg 42 <br>2630 Aartselaar<br>(achter garage Beerens/Nissan)<br><br>Openingsuren:9u-16u30";
$lang['alphabet_email_footer_right_content']			= "Verkoop: +32 3 340 40 90 <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: +32 3 870 94 74 <br>Administratie: +32 459 55 70<br><br>Email: carsquare@alphabet.be<br>Website: www.alphabetcarsquare.be";
$lang['report']										= 'Report';
$lang['save_csv']									= 'Save CSV';
$lang['report_preset_name']							= "Report Preset Name";
$lang['report_saved_successfully']					= "Report saved successfully.";
$lang['error_occurred_while_saving_report']			= "Error occurred while saving report.";
$lang['delete_report']								= "Delete Report";
$lang['are_you_sure_you_want_to_delete_this_report']= "Are you sure, you want to delete this report?";
$lang['report_deleted_successfully']				= "Report Deleted Successfully.";

$lang['has_publish_user_in']						= "has published user in ";
$lang['has_unpublish_user_from']					= "has unpublished user from ";
$lang['has_cancelled_reservation_for_car']			= "has cancelled reservation for a car";
$lang['has_saved_report_having_name']				= "has saved a report having name ";
$lang['has_deleted_saved_report_having_name']		= "has deleted saved report having name ";
$lang['has_added_banner_for_nissan_website']		= "has added banner for nissan website";
$lang['has_deleted_banner_for_nissan_website']		= "has deleted banner for nissan website";
$lang['has_added_gamma']							= "has added a gamma";
$lang['has_updated_gamma']							= "has updated a gamma";
$lang['has_deleted_gamma']							= "has deleted a gamma";
$lang['has_changed_workspace_to']					= "has changed workspace to ";
$lang['has_changed_password']						= "has changed password";
$lang['has_deleted_image_for_location']				= "has deleted image for location ";
$lang['has_added_a_blog']							= "has added a blog ";
$lang['has_updated_a_blog']							= "has updated blog ";
$lang['has_deleted_a_blog']							= "has deleted blog ";
$lang['has_published_blog']							= "has published blog in ";
$lang['has_unpublished_blog']						= "has unpublished blog from ";
$lang['updated_leaves_for']							= " updated leave for ";
$lang['cancelled_leave_for']						= " cancelled leave for ";
$lang['clears_all_lead_related_data']				= " clears all lead related data.";
$lang['clears_all_user_related_data']				= " clears all user related data.";
$lang['clears_all_car_related_data']				= " clears all car related data.";
$lang['clears_all_data']							= " clears all data in the system.";
$lang['has_accepted_dealer']						= " has accepted dealer ";
$lang['has_rejected_dealer']						= " has rejected dealer ";
$lang['has_added_newletter_group']					= " has added newsletter group ";
$lang['has_updated_newletter_group']				= " has updated newsletter group ";
$lang['has_deleted_newletter_group']				= " has deleted newsletter group ";
$lang['has_added_users_to_newletter_group']			= " has added users to newsletter group ";
$lang['has_removed_users_from_newletter_group']		= " has removed users from newsletter group ";
$lang['has_added_banner_for_leasense_website']		= " has added banner for leasense website";
$lang['has_deleted_banner_for_leasense_website']	= " has deleted banner from leasense website";
$lang['has_added_newsletter_template']				= " has added newsletter template ";
$lang['has_deleted_newsletter_template']			= " has deleted newsletter template ";
$lang['has_added_order_note_location']				= " has added order note location ";
$lang['has_updated_order_note_location']			= " has updated order note location ";
$lang['has_deleted_order_note_location']			= " has deleted order note location ";
$lang['has_added_organisation']						= " has added organisation ";
$lang['has_updated_organisation']					= " has updated organisation ";
$lang['has_deleted_organisation']					= " has deleted organisation ";
$lang['has_updated_users_associated_with_organisation']= " has updated users associated with organisation ";
$lang['has_added_owner']							= " has added owner ";
$lang['has_updated_owner']							= " has updated owner ";
$lang['has_deleted_owner']							= " has deleted owner ";
$lang['has_updated_lead_time_settings']				= " has updated lead time settings";
$lang['has_marked_car']								= " has marked car ";
$lang['as_delivered']								= " as delivered.";
$lang['has_copied_car_from']						= " has copied a car from ";
$lang['has_generated_barcode_for_car']				= " has generated barcode for a car ";
$lang['has_updated_car_option']						= " has updated car option ";
$lang['has_added_new_cost']							= " has added new cost ";
$lang['for_a_car']									= " for a car ";
$lang['has_removed_cost']							= " has removed car cost ";
$lang['has_deleted_car_document']					= " has deleted car document ";
$lang['has_send_pictures_of_a_car']					= " has send pictures of a car ";
$lang['has_generated_proposal_for_a_car']			= " has generated proposal for a car ";
$lang['has_exported_a3_pricetag']					= " has exported A3 pricetag pdf for a car ";
$lang['has_exported_a3_pricetag_alphabet']			= " has exported A3 pricetag aphabet pdf for a car ";
$lang['has_exported_a4_pricetag']					= " has exported A4 pricetag pdf for a car ";
$lang['has_added_car_model']						= " has added car model ";
$lang['has_updated_car_model']						= " has updated car model ";
$lang['has_deleted_car_model']						= " has deleted car model ";
$lang['has_added_car_brand']						= " has added car brand ";
$lang['has_updated_car_brand']						= " has updated car brand ";
$lang['has_deleted_car_brand']						= " has deleted car brand ";
$lang['has_added_an_interior_cover']				= " has added an interior cover ";
$lang['has_updated_an_interior_cover']				= " has updated an interior cover ";
$lang['has_restored_removed_car']					= " has restored a car  ";
$lang['has_deleted_and_redownloaded_images_for_a_car']= " has deleted and redownloaded images for a car  ";
$lang['has_made_car']								= " has made a car ";
$lang['in_stock_again']								= " in stock again.";
$lang['has_bought_car']								= " has bought a car ";
$lang['has_duplicated_car']							= " has duplicated a car ";
$lang['times']										= " times.";
$lang['has_generated_order_note_for_car']			= " has generated order note for a car ";
$lang['has_added_color_code']						= " has added color ";
$lang['has_updated_color_code']						= " has updated color ";
$lang['has_deleted_color_code']						= " has deleted color ";
$lang['has_updated_sold_car_data']					= " has updated sold car data.";
$lang['has_updated_exterior_color_code']			= " has updated exterior color code ";
$lang['has_uploaded_cars_via_csv']					= " has uploaded cars via csv.";
$lang['has_deleted_image_of_a_Car']					= " has deleted image of a car ";
$lang['car_alarm_newsletter_have_been_sent_from_cron_with_channel_b2']= "Car alarm newsletters have sent from cron with channel b2.";
$lang['car_alarm_newsletter_have_been_sent_from_cron_with_channel_nissan']= "Car alarm newsletters have sent from cron with channel nissan.";
$lang['car_alarm_newsletter_have_been_sent_from_cron_with_channel_b2b']= "Car alarm newsletters have sent from cron with channel b2b.";
$lang['car_alarm_newsletter_have_been_sent_from_cron_with_channel_alphabet']= "Car alarm newsletters have sent from cron with channel alphabet.";
$lang['have_been_unreserved_by_cron']				= " have been unreserved by cron.";
$lang['have_been_updated_by_cron']					= " have been updated by cron.";
$lang['exterior_color_of_cars']						= "Exterior color of cars ";
$lang['newly_added_leads_have_been_shifted_by_cron']= "Newly added leads have been shifted by cron.";
$lang['newly_contacted_leads_have_been_shifted_by_cron']= "Newly contacted leads have been shifted by cron.";
$lang['duplicate_cars_with_same_commission_number_have_been_deleted_by_cron']= "Duplicate cars with same commission number have been deleted by cron.";
$lang['xml_file_for_autoscout_uploaded_by_cron']	= "XML file for autoscout have been uploaded by cron.";
$lang['emails_for_smart_suggestions_are_sent_by_cron']	= "Emails for smart suggestions are sent by cron.";
$lang['has_added_a_car']							= " has added a car ";
$lang['in_favorite_list']							= " in favorite list.";
$lang['has_removed_a_car']							= " has removed a car ";
$lang['from_favorite_list']							= " from favorite list.";
$lang['has_added_options_for_smart_suggestions']	= " has requested options of Smart suggestions ";
$lang['has_subscribed_for_newsletter']				= " has subscribed for a newletter ";
$lang['set_car_alarm_for']							= " set car alarm for ";
$lang['has_deleted_car_alarm_for']					= " has deleted car alarm set as ";
$lang['gets_added_as_a_lead']						= " gets added as a lead.";
$lang['the_status_of_lead']							= "The status of lead ";
$lang['has_changed_to']								= " has changed to ";
$lang['has_added_a_request_for_selling_a_car']		= " has added a request for selling a car ";
$lang['new_sell_a_car_request_added_from_user']		= "New sell a car request added from user ";
$lang['in']											= " in ";



$lang['smart_suggestions_for_car'] 					= "Smart suggestions for cars";
$lang['blog_successfully_deleted'] 					= "Blog deleted successfully.";
$lang['blog_successfully_updated'] 					= "Blog updated successfully.";
$lang['order_note_location_deleted_successfully'] 	= "Order note location deleted successfully.";

$lang['please_add_atleast_one_zone']				= "Please add atleast one zone";
$lang['bulk_upload']								= "Bulk upload";
$lang['sample_csv'] 								= "Sample csv";

$lang['newsletter_unsubscription_msg'] = "If you haven't subscribed to our mailinglist and have received this email by mistake.<br>Click the button below to remove this email address from our list.";
$lang['newsletter_unsubscription'] = "Newsletter Unsubscription";
$lang['newsletter_unsubscription_desc'] = "Thank you. You are successfully unsubscribed to our newsletter.";

$lang['unsubscribe_me_please'] = "UNSUBSCRIBE ME PLEASE";
$lang['selected_users_or_groups_has_unsubscribed_to_newsletters'] = 'or Selected users or groups has unsubscribed to newsletters';
$lang['unsubscribe_users_listing'] = "Unsubscribe users listing";
$lang['unsubscription'] = "Unsubscription";
$lang['unsubscribed_on'] = "Unsubscribed on";
$lang['listing'] = "listing";
$lang['article_name'] = "article name";
$lang['page_title'] = "page title";
$lang['user_roles'] = "user roles";
$lang['meta_description'] = "meta descirption";
$lang['meta_keywords'] = "meta keywords";

$lang['form'] = "Form";
$lang['form_listing'] = "Form listing";
$lang['add_form'] = "Add form";
$lang['form_created_successfully'] = "Form created successfully";
$lang['delete_form'] = "Delete Form";
$lang['are_you_sure_you_want_to_delete_this_form'] = "Are you sure, you want to delete this form ?";
$lang['form_deleted_successfully'] = "Form deleted successfully";
$lang['form_updated_successfully'] = "Form updated successfully";

$lang['configuration'] = "Configuration";
$lang['definition'] = "DEFINITION";
$lang['value'] = "VALUE";
$lang['confirmation_email_message_content'] = "Confirmation email message content";
$lang['message_confirmation_mail_before_content'] = "Message confirmation mail before content";
$lang['message_confirmation_mail_after_content'] = "Message confirmation mail after content";
$lang['message_sent_successfully'] = "Message sent successfully";
$lang['error_sending_message'] = "Error sending message";
$lang['value_submit_button'] = "Value submit button";
$lang['value_delete_button'] = "Value delete button";
$lang['send_to_email_address'] = "Send to the following e-mail address";
$lang['email_subject'] = "Email Subject";
$lang['bcc'] = "BCC";
$lang['email_address_sender'] = "Email address sender";
$lang['conversion_code'] = "Conversion Code";
$lang['sender_name'] = "Sender Name";
$lang['copy_to_customer'] = "Copy to customer?";

$lang['configuration_update_success'] = "Form configuration updated successfully";
$lang['configuration_add_success'] = "Form configuration added successfully";
$lang['configuration_add_error'] = "Error occured while adding form configuration";

$lang['short_description'] = 'Short description';
$lang['add_thumbnail'] = "Add thumbnail";
$lang['thumbnail'] = "Thumbnail";
$lang['delete_thumb'] = "Delete thumbnail";
$lang['are_you_sure_you_want_to_delete_this_thumbnail'] = "Are you sure you want to delete this thumbnail?";
$lang['thumbnail_deleted_successfully'] = "Thumbnail deleted successfully";

//static_page
$lang['static_pages'] = "Static Pages";
$lang['listing'] = "Listing";
$lang['pages'] = "Pages";
$lang['add_pages'] = "Add Pages";
$lang['pages_listing'] = "Pages listing";
$lang['are_you_sure_you_want_to_delete_this_page'] 	= "Are you sure you want to delete this page";
$lang['page_successfully_added'] = "Page successfully added";
$lang['page_successfully_deleted'] = "Page successfully deleted";
$lang['page_successfully_updated'] = "Page successfully updated";
$lang['edit_page'] = "Edit Page";
$lang['page_title'] = 'Page title';
$lang['page_name'] = "Page Name";


$lang['newsletter_unsubscription_msg'] = "If you haven't subscribed to our mailinglist and have received this email by mistake.<br>Click the button below to remove this email address from our list.";
$lang['newsletter_unsubscription'] = "Newsletter Unsubscription";
$lang['newsletter_unsubscription_desc'] = "Thank you. You are successfully unsubscribed to our newsletter.";

$lang['unsubscribe_me_please'] = "UNSUBSCRIBE ME PLEASE";
$lang['selected_users_or_groups_has_unsubscribed_to_newsletters'] = 'or Selected users or groups has unsubscribed to newsletters';
$lang['unsubscribe_users_listing'] = "Unsubscribe users listing";
$lang['unsubscription'] = "Unsubscription";
$lang['unsubscribed_on'] = "Unsubscribed on";
$lang['delete_and_redownload_images_from_b2_galleries'] = "Delete and Redownload Images from B2 Galleries";
$lang['page_title'] = "page title";
$lang['meta_description'] = "meta description";
$lang['meta_keywords'] = "meta keywords";


$lang['list_of_users_of_group'] = "List of users of group ";
$lang['build'] = "Build";
$lang['list_of_zones_of_newsletter_template'] = "List of zones of newsletter template - ";
$lang['build_zone'] = "Build zone";
$lang['zone_layout_1'] = "car zone 1 column";
$lang['zone_layout_2'] = "car zone 2 columns";
$lang['zone_layout_3'] = "car zone 3 columns";
$lang['zone_layout_4'] = "Text zone";
$lang['newsletter_zone_created_successfully'] = "Newsletter zone created successfully.";
$lang['newsletter_zone_updated_successfully'] = "Newsletter zone updated successfully.";
$lang['newsletter_zone_deleted_successfully'] = "Newsletter zone deleted successfully.";
$lang['delete_newsletter_zone'] = "Delete Newsletter Zone";
$lang['are_you_sure_you_want_to_delete_this_zone'] = "Are you sure, you want to delete this zone ?";
$lang['add_users_to_group'] = "Add users to group";

$lang['newsletter_stats'] = "Newsletter stats";
$lang['list_of_newsletter_sent'] = "List of Newsletter sent";
$lang['add_zone'] = "Add zone";
$lang['zone_type'] = "Zone type";
$lang['template'] = "Template";
$lang['newsletter_unsubscription_text_msg'] = "If you haven't subscribed to our mailinglist and have received this email by mistake. Click the button below to remove this email address from our list.";
$lang['add_users_from_csv'] = 'Add users from csv';
$lang['select_group'] = 'Select group';
$lang['contacts'] = 'Contacts';
$lang['unsubscribed'] = 'Unsubscribed';
$lang['are_you_sure_you_want_to_delete_this_user_from_group'] = 'Are you sure, you want to delete this user from group ?';
$lang['from_email'] = "From";
$lang['to_email'] = "To";
$lang['send_preview'] = "Send Preview";

$lang['language_translations'] = "Language Translations";

$lang['select_one'] = "Select One";
$lang['text_field'] = "Text Field";
$lang['password_field'] = "Password Field";
$lang['textarea'] = "Textarea";
$lang['date_field'] = "Date Field";
$lang['email_field'] = "Email Field";
$lang['website_field'] = "Website Field";
$lang['radio_buttons'] = "Radio Button";
$lang['upload_field'] = "Upload Field";
$lang['checkbox'] = "Checkbox";
$lang['dropdown'] = "Dropdown";
$lang['update_english_translations'] = "Update English Translations";
$lang['update_dutch_translations'] = "Update Dutch Translations";
$lang['update_french_translations'] = "Update French Translations";