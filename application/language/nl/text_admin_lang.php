<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Dutch Language

//*******************ADMIN panel****************

//-----header-----------//
$lang['admin_panel']								= "Admin paneel";
$lang['sales']										= "Verkoop";
$lang['beerens']									= "Beerens";
$lang['navigation']									= "Navigatie";
$lang['cars_management']							= "wagen management";
$lang['sales_management']							= "verkoop management";
$lang['notifications']								= "Notificaties";
$lang['you_have_total_of']							= "U heeft een totaal van";
$lang['activities']									= "Activiteiten";
$lang['activity_log']								= "Activiteiten notificaties";
$lang['select_activity_type']						= "Selecteer activiteit type";

//------common----------//
$lang['car'] 										= 'Wagen';
$lang['cars'] 										= 'Wagens';
$lang['car_brand'] 									= "Merk";
$lang['car_model'] 									= "Model";
$lang['please_select']								= "Selecteer aub";
$lang['manufacturing_date'] 						= "Fabrieks datum";
$lang['registration_date'] 							= "Inschrijvingsdatum";
$lang['location']									= "Locatie";
$lang['chassis_number'] 							= "Chassisnummer";
$lang['commission_number'] 							= "Commissienummer";
$lang['type'] 										= "Type";
$lang['owner']										= "Eigenaar";
$lang['color']										= "Kleur";
$lang['cost'] 										= "Kost";
$lang['car_life_status'] 							= "Wagen status";
$lang['stock_status'] 								= "Stock Status";
$lang['client'] 									= "Klant";
$lang['btw_type'] 									= "Btw Type";
$lang['transmission'] 								= "Transmissie";
$lang['vat_type'] 									= "Btw type";
$lang['fuel'] 										= "Brandstof";
$lang['mileage'] 									= "kilometerstand";
$lang['category'] 									= "Categorie";
$lang['images'] 									= "Afbeeldingen";
$lang['save']										= "Opslagen";
$lang['copy_car'] 									= "Kopieër wagen";
$lang['send'] 										= "Versturen";
$lang['options']									= "Opties";
$lang['publications']								= "Publicaties";
$lang['car_brands'] 								= "Wagen merken";
$lang['car_models'] 								= "Wagen Modellen";
$lang['select_all'] 								= "Selecteer alles";
$lang['upload'] 									= "Upload";
$lang['car_subtitle']								= "Wagen Subtitel";
$lang['action']										= "Actie";
$lang['update'] 									= "Update";
$lang['email_address'] 								= "Email addres";
$lang['add_images'] 								= "Afbeeldingen toevoegen";
$lang['this_field_is_required']						= "Dit veld is verplicht";
$lang['close'] 										= "Sluiten";
$lang['ok']											= "OK";
$lang['from']										= "Van";
$lang['confirm']									= "Bevestigen";
$lang['cancel'] 									= "Annuleer";
$lang['first_name']									= "Voornaam";
$lang['last_name']									= "Achternaam";
$lang['street'] 									= "Straat";
$lang['house_number'] 								= "Huis nummer";
$lang['postal_code'] 								= "Postcode";
$lang['town'] 										= "Stad";
$lang['accessories']								= "Accessoires";
$lang['language'] 									= "Taal";
$lang['phone_number']								= "Telefoonnummer";
$lang['selling_cost'] 								= "Verkoopskost";
$lang['choose_your_accessories'] 					= "Kies je accessoires";
$lang['no_car_accessories']							= "Geen wagen accessoires!";
$lang['brand'] 										= "Merk";
$lang['brands'] 									= "Merken";
$lang['model'] 										= "Model";
$lang['models']										= "Modellen";
$lang['description'] 								= "Beschrijving";
$lang['select_a_model']								= "Selecteer een model";
$lang['no_data_found'] 								= "Geen data gevonden";
$lang['some_error_occurred']						= "Some error occurred";
$lang['add'] 										= "Toevoegen";
$lang['actions']									= "Acties";
$lang['view']										= "Bekijk";
$lang['accept'] 									= "Accepteren";
$lang['reject'] 									= "Weigeren";
$lang['edit']										= "Bewerken";
$lang['owners']										= "Eigenaars";
$lang['guarantee']									= "Garantie";
$lang['accessaries'] 								= "Accessoires";
$lang['id']											= "ID";
$lang['name']										= "Naam";
$lang['publish']									= "Publiceren";
$lang['delete']										= "Verwijderen";
$lang['error_occurred_while_updating_data']			= "Error occurred while updating data";
$lang['there_was_an_error_please_contact_administrator']= "There was an error. Please contact administrator";
$lang['Car_rejected'] 								= 'Wagen geweigerd.';
$lang['the_uploaded_file_doesnt_seem_to_be_an_image'] = "The uploaded file doesnt seem to be an image";
$lang['only_pdf_doc_docx_xls_csv_file_types_are_allowed'] = "Only pdf doc docx xls csv file types are allowed";
$lang['invalid_data'] 								= "Invalid data";
$lang['invalid_url'] 								= "Invalid Url";
$lang['please_enter_a_valid_cost_name_and_amount'] 	= "Please enter a valid cost name and amount";
$lang['invalid_id'] 								= "Invalid id";
$lang['bought']										= "Verkocht";
$lang['people']										= "People";
$lang['clients']									= "Klanten";
$lang['dealers']									= "Dealers";
$lang['leads']										= "Leads";
$lang['users']										= "Gebruikers";
$lang['there_was_some_error']						= "There was some error.";
$lang['vat']										= "BTW";
$lang['vat_number']									= "BTW Nummer";
$lang['pay']										= "Betalen";
$lang['state']										= "Gemeente";
$lang['post_code']									= "Postcode";
$lang['address'] 									= "Addres";
$lang['deleted_successfully']						= "Succesvol verwijderd";
$lang['notes']										= "Notities";
$lang['added_on']									= "Toegevoegd op";
$lang['added'] 										= 'Toegevoegd';
$lang['sold'] 										= "DEZE WAGEN IS VERKOCHT";
$lang['nissan'] 									= 'Nissan';
$lang['B2CARS']										= "B2cars";
$lang['leasense'] 									= "Leasense";
$lang['image'] 										= "Afbeelding";
$lang['username']									= "Gebruikersnaam";
$lang['questions']									= "Vragen";
$lang['settings']									= "Instellingen";
$lang['english'] 									= "Engels";
$lang['dutch'] 										= "Nederlands";
$lang['french'] 									= "Frans";
$lang['there_was_an_error']							= "There was an error";
$lang['error_occurred_while_deleting_data']			= "Error occurred while deleting data";
$lang['all_fields_compalsory'] 						= 'All fields are mandatory';
$lang['image_deleted_successfully']					= "Image deleted successfully";
$lang['categories']									= "Categorieën";
$lang['admin_log_in']								= "Admin Login";
$lang['activity']									= "Activiteit";
$lang['ago']										= "geleden";
$lang['invalid_access'] 							= "Invalid access";
$lang['select']										= "Selecteer";
$lang['car_seller'] 								= "Wagen verkoper";
$lang['incl_btw'] 									= "incl. BTW";
$lang['no'] 										= 'nee';
$lang['b2'] 										= 'B2';
$lang['color_codes']								= 'Kleur codes';
$lang['no_cars_found']								= "Geen wagen gevonden";
$lang['hi'] 										= "Hallo";
$lang['kilometers']									= "Kilometers";
$lang['for']										= " voor ";

//----------dashboard-------------------//
$lang['dashboard']									= "Dashboard";
$lang['your_workspace_has_been_set_to'] 			= "Uw werkruimte is ingesteld op ";
$lang['welcome_to_beerens_admin_dashboard'] 		= "Welkom op het admin dashboard";

//-----------------car add, edit-----------------------//
$lang['car_add'] 									= "Wagen toevoegen";
$lang['basic_details'] 								= "Basis Details";
$lang['exterior_color'] 							= "Exterieur Color";
$lang['exterior_code'] 								= "Exterieur Code";
$lang['interior_material'] 							= "Interieur Material";
$lang['interior_code'] 								= "Interieur Code";
$lang['interior_cover'] 							= "Interieur Cover";
$lang['inclusive_purchase_price']					= "Inclusief Aankoop prijs";
$lang['exclusive_purchase_price'] 					= "Exclusief Aankoop prijs";
$lang['inclusive_promotion_price'] 					= "Inclusief Promotie prijs";
$lang['exclusive_promotion_price'] 					= "Exclusief Promotie prijs";
$lang['inclusive_selling_cost'] 					= "Inclusief Verkoop prijs B2C";
$lang['exclusive_selling_cost'] 					= "Exclusief Verkoop prijs B2C";
$lang['inclusive_dealer_price'] 					= "Inclusief Dealer prijs B2B";
$lang['exclusive_dealer_price'] 					= "Exclusief Dealer prijs B2B";
$lang['inclusive_repair_cost'] 						= "Inclusief Verwachte Herstelling kost";
$lang['exclusive_repair_cost'] 						= "Exclusief Verwachte Herstelling kost";
$lang['inclusive_trading_price'] 					= "Inclusief Trading prijs B2B";
$lang['exclusive_trading_price'] 					= "Exclusief Trading prijs B2B";
$lang['technical_specifications'] 					= "Technische specificaties";
$lang['sold_price'] 								= "Verkoop prijs";
$lang['specification']								= "Specificaties";
$lang['doors'] 										= "Deuren";
$lang['gears'] 										= "Versnellingen";
$lang['seats'] 										= "Plaatsen";
$lang['horsepower'] 								= "Paardekracht";
$lang['cc'] 										= "Cc";
$lang['co2'] 										= "co2";
$lang['model_year'] 								= "Model jaar";
$lang['drive_wheel']								= "Wielaandrijving";
$lang['car_added_successfully']						= "Wagen succesvol toegevoegd";
$lang['car_updated_successfully']					= "Wagen succesvol aangepast";
$lang['car_view']									= "Wagen bekijken";
$lang['car_added_success'] 							= 'Wagen succesvol toegevoegd.';
$lang['car_added_error'] 							= 'Error while adding car.';
$lang['mileage_cannot_be_zero_for_used_cars'] 		= "KM kan niet 0 zijn voor gebruikte wagens.";

//---------------car edit----------------------//
$lang['car_edit'] 									= "Wagen bewerken";
$lang['car_details_incomplete'] 					= "Car details is incomplete";
$lang['remove_this_car'] 							= "Verwijder deze wagen";
$lang['export'] 									= "Exporteren";
$lang['get_barcode'] 								= "Barcode";
$lang['delete_and_redownload_images']				= "Verwijder en herdownload afbeeldingen";
$lang['information']								= "Informatie";
$lang['technical']									= "Technisch";
$lang['price'] 										= "Prijs";
$lang['photos']										= "Afbeeldingen";
$lang['damaged_photos']								= "Afbeeldingen beschadigingen";
$lang['autoscout']									= "Autoscout";
$lang['autoscout_comments']							= "Autoscout opmerkingen";
$lang['internal_comments']							= "Interne opmerkingen";
$lang['documents']									= "Documenten";
$lang['quick_check']          						= "Quick Check";
$lang['general'] 									= "Algemeen";
$lang['arrival'] 									= "Aankomst";
$lang['arrival_status'] 							= "Aankomst Status";
$lang['arrival_date'] 								= "Aankomst Datum";
$lang['delivery'] 									= "Levering";
$lang['where_the_car_is_parked'] 					= "Waar de wagen geparkeerd is";
$lang['BPM'] 										= "BPM";
$lang['mmt'] 										= "MMT";
$lang['add_a_new_cost'] 							= "Nieuwe kost toevoegen";
$lang['enable_finance_settings'] 					= "Financiering activeren";
$lang['equipments'] 								= "Uitrustingen";
$lang['publish_cars'] 								= "Wagens publiceren";
$lang['damaged_images'] 							= "Damaged Images";
$lang['document_name']								= "Document naam";
$lang['certificate_of_confirmity']					= "Gelijkvormigheidsattest";
$lang['maintainance_book']							= "Onderhoudsboek";
$lang['inspaction_report']							= "Inspectie rapport";
$lang['carpass']									= "Carpass";
$lang['proof_of_registration']						= "Bewijs van registratie";
$lang['keys']										= "Sleutels";
$lang['comments']									= "Opmerkingen";
$lang['send_pictures'] 								= "Afbeeldingen versturen";
$lang['send_proposal'] 								= "Offerte versturen";
$lang['a3_pricetag'] 								= "A3 PDF - B2";
$lang['a3_alphabet'] 								= "A3 PDF - ALPHABET";
$lang['a4_pricetag'] 								= "A4 PDF - B2";
$lang['send_car_pictures'] 							= "Verstuur wagen afbeeldingen";
$lang['add_new_note'] 								= "Nieuwe notitie toevoegen";
$lang['cost_name'] 									= "Kost naam";
$lang['cost_amount'] 								= "Kost bedrag";
$lang['delete_cost'] 								= "Kost verwijderen";
$lang['btw_percentage']								= "Btw Percentage";
$lang['please_enter_a_valid_cost_name'] 			= "Please enter a valid cost name.";
$lang['please_enter_btw_percentage'] 				= "Please enter Btw Percentage.";
$lang['please_enter_a_valid_cost_amount'] 			= "Please enter a valid cost amount.";
$lang['please_enter_a_valid_email_address'] 		= "Please enter a valid email address.";
$lang['are_you_sure_you_want_to_remove_this_car_cost?'] = "Are you sure you want to remove this car cost?";
$lang['are_you_sure_you_want_to_delete_this_cost'] 	= "Are you sure you want to delete this cost";
$lang['delete_image'] 								= "Delete Image";
$lang['are_you_sure_you_want_to_delete_this_image'] = "Are you sure you want to delete this image";
$lang['delete_comment'] 							= "Delete Comment";
$lang['are_you_sure_you_want_to_delete_this_comment'] = "Are you sure you want to delete this comment";
$lang['delete_document'] 							= "Delete Document";
$lang['are_you_sure_you_want_to_delete_this_document'] = "Are you sure you want to delete this document";
$lang['successfully_sorted'] 						= "Successfully Sorted.";
$lang['something_went_wrong'] 						= "Something went wrong.";
$lang['confirm_remove_logo'] 						= "Are you sure you want to remove this logo?";
$lang['confirm_remove_car'] 						= "Are you sure you want to remove this document?";
$lang['no_images_exist_for_this_car'] 				= "No images exist for this car";
$lang['pictures_have_been_sent_to'] 				= "Pictures have been sent to";
$lang['error_occurred_while_sending_email_to'] 		= "Error occurred while sending email to";
$lang['car_does_not_exist'] 						= "Car does not exist";
$lang['add_document']								= "Add document";
$lang['no_documents_found']							= "No documents found";
$lang['files_less_than_10_MB_can_only_be_uploaded'] = "Files less than 10 MB can only be uploaded.";
$lang['only_pdf_doc_docx_xls_csv_file_types_are_allowed'] = "Only pdf,doc,docx,xls,csv file types are allowed";
$lang['there_was_an_error_while_uploading_document'] = "There was an error while uploading document";
$lang['error_encountered_please_try_again'] 		= "Error encountered, please try again!";
$lang['car_edit_reserved_error'] 					= 'Car is reserved and status can not be changed.';
$lang['document_uploaded_successfully'] 			= 'Document uploaded successfully.';
$lang['please_select_a_car'] 						= "Please select a car";
$lang['email_sent_successfully'] 					= "Email sent successfully.";
$lang['flex']										= "Flex";
$lang['drive']										= "Drive";
$lang['green']										= "Green";

//-----duplicate car-------//
$lang['duplicate'] 									= "Dupliceer";
$lang['duplication'] 								= "Duplicatie";
$lang['no_of_cars_to_copy'] 						= "Aantal dublicaties";
$lang['car_duplicated_successfully'] 				= "Wagen succesvol gedupliceerd.";
$lang['commission_number_must_be_unique'] 			= "Commissienummers moeten uniek zijn";
$lang['chassis_number_must_be_unique'] 				= "Chassisnummers moeten uniek zijn";
$lang['number_of_cars_must_be_greater_than_zero'] 	= "Aantal wagen moet meer dan 0 zijn";
$lang['search_result_will_be_found_in_car_dropdown']= "Search results will found in car's dropdown";
$lang['filtered']									= "Gefilterd";

//-----remove car-------//
$lang['remove_car']									= "Verwijder wagen";
$lang['are_you_sure_you_want_to_remove_this_car']	= "Bent u zeker dat u deze wagen wilt verwijderen?";

//-----copy car--------//
$lang['car_status_can_not_be_copied']				= "Wagen status kan niet gekopieerd worden";
$lang['please_select_at_least_one_parameter_to_copy'] = "Please select at least one parameter to copy.";
$lang['please_search_and_select_at_least_one_car']  = "Please search and select at least one car.";
$lang['please_select_at_least_one_car_to_copy'] 	= "Please select at least one car to copy.";
$lang['are_you_sure_you_want_to_copy'] 				= "Are you sure you want to copy";
$lang['no_car_found_for_this_search'] 				= "No car found for this search.";
$lang['car_copied_successfully']					= "Car copied successfully";

//-----generate proposal----//
$lang['generate_proposals'] 						= "Genereer offertes";
$lang['generate_proposal'] 							= "Genereer een offerte";
$lang['customer_information'] 						= "Klant informatie";
$lang['price_information'] 							= "Prijs informatie";
$lang['additional_options'] 						= "Optionele opties";
$lang['genrate_proposals'] 							= "Creeër offerte";
$lang['print'] 										= "Afdrukken";
$lang['save_as_pdf'] 								= "Opslagen als PDF";
$lang['send_email'] 								= "Verstuur via email";
$lang['first']										= "Eerste";
$lang['last']										= "Laatste";
$lang['previous']									= "Vorige";
$lang['next'] 										= "Volgende";
$lang['your_margin'] 								= "Uw Marge";
$lang['your_price'] 								= "Uw Prijs";
$lang['vat_amount'] 								= "BTW Bedrag";
$lang['amount_including_vat'] 						= "Bedrag inclusief BTW";
$lang['enter_the_text_you_want_to_mention_above_the_offer'] = "Geef de tekst die je boven het voorstel wenst te vermelden";
$lang['enter_the_text_you_want_to_mention_under_the_offer'] = "Geef de tekst die je onder het voorstel wenst te vermelden";
$lang['save_as_pdf'] 								= "Opslagen als PDF";
$lang['choose_file'] 								= "Kies bestand";
$lang['some_error_occured_while_generating_the_proposal']= "Er is een fout opgetreden tijdens het genereren van de pdf";
$lang['print'] 										= "Afdrukken";
$lang['create_pdf']									= "Maak PDF";
$lang['payment'] 									= "Looptijd";
$lang['own_contribution']							= "Voorschot";
$lang['financing'] 									= "Financiering";
$lang['offerte'] 									= "OFFERTE";
$lang['power'] 										= 'Vermogen';
$lang['fuel_capital'] 								= "Brandstof";
$lang['car_ready_charge_title'] 					= "Kosten rijklaar maken eindgebruiker Belgi&euml;";
$lang['car_ready_charge_subtitle'] 					= "Offici&euml;le voorplaat, driehoek, brandblusser, fluojasje, verbanden, aanvraag div, cleaning, keuring";
$lang['car_proposals_email_body1'] 					= "In bijlage van deze email kan u de offerte vinden.";
$lang['total']										= "Totaal";
$lang['with_kind_regards'] 							= "Met vriendelijke groeten,";
$lang['subject_car_proposals_email'] 				= "Beerens - Car Offer";
$lang['please_select_lead_first']					= "Selecteer aub eerst een lead.";
$lang['click_here_to_update']						= "Klik hier om te updaten";

//-------search---------//
$lang['search']										= "Zoeken";
$lang['search_for_cars']							= "Wagens zoeken";
$lang['car_type'] 									= "Wagen Type";
$lang['advanced_search']							= "Geavanceerd zoeken";
$lang['technical_details']							= "Technische details";
$lang['cc_van'] 									= "Cc van";
$lang['kw_van'] 									= "kW van";
$lang['pk_van'] 									= "pk van";
$lang['kms_van'] 									= "KM's van";
$lang['dib_van'] 									= "DIB van";
$lang['place'] 										= "Place";
$lang['car_specifications'] 						= "Wagen specificaties";
$lang['fuel_type'] 									= "Brandstof Type";
$lang['date'] 										= "Datum";
$lang['created_on'] 								= "Aangemaakt op";
$lang['arrival_on'] 								= "Aangekomen op";
$lang['sale_on'] 									= "Verkocht op";
$lang['invoice_on'] 								= "Gefactureerd op";
$lang['delivered_on'] 								= "Geleverd op";
$lang['search_cars']								= "Wagens zoeken";
$lang['basic_search']								= "Basis zoeken";
$lang['search_by_id']								= "Zoeken via ID";
$lang['no_record_found'] 							= "Geen resultaten gevonden";

//-------reserved cars-------//
$lang['reserved'] 									= "Gereserveerd";
$lang['reserved_from_frontend'] 					= "Reserved From Frontend";
$lang['reserved_by_icar_user'] 						= "Reserved By Icar User";
$lang['reserved_by_admin'] 							= "Reserved by  admin";
$lang['reserved_for'] 								= "Reserved For";
$lang['source']										= "Bron";
$lang['car_reserved_successfully']					= "Wagen succesvol gereserveerd";
$lang['reserved_on'] 								= "Gereserveerd op";
$lang['reserved_to'] 								= "Reserved To";
$lang['transaction_id'] 							= "Transaction ID";
$lang['payment_type'] 								= "Payment Type";
$lang['reserve_car']								= "Reserve Car";
$lang['choose_reservation_type'] 					= "CHOOSE A RESERVATION TYPE";
$lang['total_accessories_cost'] 					= 'Total accessory cost';
$lang['car_reserved_successfully'] 					= "Car Reserved Successfully.";

//-----queued cars--------//
$lang['queued']										= "Wachtrij";
$lang['queued_cars_listing'] 						= "Lijst - wagens in wachtrij";
$lang['no_queued_cars_found'] 						= "Geen wagens in de wachtrij";
$lang['by_chasis'] 									= "Via Chasis";
$lang['by_commission_number'] 						= "Via Commissienummer";
$lang['by_numintern'] 								= "Via Numinterno";
$lang['version'] 									= "Versie";
$lang['queued_car_listing'] 						= "Wagens in wachtrij - overzicht";
$lang['queued_car_approval'] 						= "Wagen in wachterij goedkeuring / weigering";


// car view in queued cars-----//
$lang['car_details'] 								= "Wagen details";
$lang['is_deleted_from_the_system']					= "is verwijderd van het systeem";
$lang['purchase_price'] 							= "Aankoop Prijs";
$lang['retail_price'] 								= "Retail Prijs";
$lang['dealer_price'] 								= "Dealer Prijs";
$lang['this_car_whith_numinterno'] 					= "Deze wagen met Numinterno ";
$lang['to']											= "naar";
$lang['changes_successfully_accepted']				= "Wijzigingen succesvol geaccepteerd";
$lang['car_successfully_moved_from_queued_car_to_bcar']= "Wagen succesvol verplaatst van wachtrij naar Bcar";
$lang['car_rejected']								= "Wagen geweigerd.";

//-------incomplete cars---------//
$lang['incomplete']									= "Niet volledig";
$lang['incomplete_cars_listing'] 					= "Lijst - wagens die niet compleet zijn";
$lang['no_incomplete_cars_found'] 					= "Geen incomplete wagens gevonden";
$lang['car_edit_incomplete_data'] 					= "Wagen bewerken - incomplete data";

//------cars without images---------//
$lang['without_images']								= "Zonder afbeeldingen";
$lang['cars_without_images_listing'] 				= "Lijst - wagens zonder afbeeldingen";
$lang['no_cars_found_without_images']				= "Geen wagens zonder afbeeldingen gevonden";

//-------car options ----------//
$lang['car_option_add'] 							= "Wagen optie toevoegen";
$lang['car_option_edit'] 							= "Wagen optie bewerken";
$lang['car_options'] 								= "Wagen opties";
$lang['autoscout_car_options'] 						= "Autoscout wagen opties";
$lang['option_name'] 								= "Optie naam";
$lang['autoscout_equipment_name'] 					= "Autoscout uitrusting naam";
$lang['car_options_listing'] 						= "Wagen opties - overzicht";
$lang['add_option'] 								= "Optie toeveogen";
$lang['no_car_option_found']						= "Geen wagen opties gevonden";
$lang['car_option_successfully_updated']			= "Wagen optie succesvol aangepast";
$lang['car_option_added']							= "Wagen optie toegevoegd";
$lang['error_adding_car_option_please_try_again']	= "Error adding car option. Please try again";
$lang['car_option_removed']							= "Wagen optie verwijderd";
$lang['error_removing_car_option_please_try_again']	= "Error removing car option. Please try again";

//---------------sold cars------------//
$lang['sold_link'] 									= "Verkocht";
$lang['sold_cars_listing'] 						    = "Verkochte wagens - overzicht";
$lang['Car_sold_successfully'] 						= "Wagen succesvol verkocht";
$lang['delivery_date_updated_successfully'] 		= "Leveringsdatum succesvol aangepast.";
$lang['incomplete_cars_can_not_be_copied'] 			= "Onvolledige wagens kunnen niet gekopieërd worden";
$lang['update_delivery_date']						= "Update leveringsdatum";
$lang['car_delivery_date'] 							= "Wagen leveringsdatum";
$lang['car_delivery_time'] 							= "Wagen leveringstijd";
$lang['paper_delivery_date'] 						= "Papieren leveringsdatum";
$lang['paper_delivery_time'] 						= "Papieren leveringstijd";
$lang['please_select_a_date']	        			= "Selecteer aub een datum.";
$lang['bought_by'] 									= "Gekocht via";
$lang['bought_for'] 								= "Gekocht voor";
$lang['bought_on'] 									= "Gekocht op";
$lang['generate_order_note']      					= "Genereer bestelbon";
$lang['mark_delivered'] 							= "Markeer als geleverd";
$lang['set_delivery']								= "Stel in";
$lang['delivery_date'] 								= "Leveringsdatum";
$lang['update_status'] 								= "Update Car Status To In-stock";
$lang['sell_car']									= "Verkoop een wagen";
$lang['this_car_having_name'] 						= "This car having name";
$lang['is_bought_by'] 								= "is bought by";
$lang['on'] 										= "on";
$lang['for_price']									= "for price";
$lang['confirm_msg_for_update_status_of_sold_car1'] = "Are you sure you want to delete this data and make this car 'In Stock' again ?";
$lang['confirm_msg_for_update_status_of_sold_car2'] = "Are you sure you want to delete this data and make this car 'Reserved' again ?";
$lang['customer_name'] 								= "Customer name";
$lang['car_status_updated'] 						= "Car status updated successfully";
$lang['no_bought_cars_found'] 						= "No bought Cars found";
$lang['add_delivery_date'] 							= "Add Delivery Date";
$lang['car_status_changed_successfully_to_in_stock']= "Car status changed successfully to in stock.";
$lang['sell_car']									= "Sell A Car";
$lang['view_order_note']      						= "View order note";
$lang['sold_cars_by_this_lead']						= "Sold cars by this lead";
$lang['please_select_time'] 						= "Please select a time";
$lang['upload_cars_via_csv']						= "Upload cars via csv";
$lang['please_select_valid_file'] 			    	= "Please select a valid file";

//-------------delivered cars----------//
$lang['delivered_cars_listing'] 					= "Delivered Car Listing";
$lang['delivered'] 									= "Delivered";
$lang['no_delivered_cars_found'] 					= "No delivered cars found";

//------------accessory add,edit,listing----//
$lang['add_accessories']							= "Add Accessory";
$lang['publish_car_accessory']						= "Publish Car Accessory";
$lang['edit_accessories']							= "Edit Accessory";
$lang['accessory_listing']							= "Accessory Listing";
$lang['discounted_cost'] 							= "Discounted cost";
$lang['no_car_accessories_added_yet']				= "No car accessories added yet";
$lang['delete_accessory']							= "Delete Accessory";
$lang['are_you_sure_you_want_to_delete_this_accessory']	= "Are you sure you want to delete this accessory ?";
$lang['accessory_successfully_added'] 				= "Accessory successfully added";
$lang['error_adding_accessory'] 					= "Error adding Accessory";
$lang['accessory_successfully_updated'] 			= "Accessory successfully updated";
$lang['error_updating_accessory'] 					= "Error updating Accessory";
$lang['this_accessory_cannot_be_deleted_as_it_has_been_ordered_by_a_customer']= "This Accessory cannot be deleted as it has been ordered by a customer";
$lang['accessory_successfully_deleted'] 			= "Accessory successfully deleted";
$lang['error_deleting_accessory'] 					= "Error deleting Accessory";
$lang['accessory_channel_added'] 					= "Accessory channel added";
$lang['error_adding_accessory_channel_please_try_again']= "Error adding accessory channel. Please try again";
$lang['accessory_channel_removed'] 					= "Accessory channel removed";
$lang['error_removing_accessory_channel_please_try_again'] = "Error removing accessory channel. Please try again";
$lang['discounted_cost_must_be_less_than_the_original_cost'] = "Discounted cost must be less than the original cost";
$lang['no_accessories_found_for_this_car']			= "No accessories founf for this car.";

//-----brand add,edit,listing----------//
$lang['car_brands_listing'] 						= "Wagen merken - overzicht";
$lang['add_car_brand'] 								= "Voeg een merk toe";
$lang['brand_name'] 								= "Brand Name";
$lang['autoscout_name']								= "Autoscout Name";
$lang['car_brand_added_success'] 					= 'Car brand added successfully.';
$lang['car_brand_added_error'] 						= 'Error while adding car brand.';
$lang['edit_car_brand'] 							= "Edit Car Brand";
$lang['car_brand_is_updated_successfully']			= "Car brand is updated successfully";
$lang['no_car_brands_found']						= "No car brands found.";
$lang['confirm_remove_car_brand'] 					= "Are you sure, you want to delete this car brand ?";
$lang['error']										= "Error";
$lang['car_brand_deleted_successfully'] 			= "Car brand deleted successfully.";
$lang['car_brand_can_not_be_deleted'] 				= "Car brand can not be deleted.";

//-----model add,edit,listing----------//
$lang['add_model']									= "Add Model";
$lang['model_deleted_successfully']                 = "Model Deleted Successfully";
$lang['you_can_not_delete_this_car_model']          = "You can not delete this car model.";
$lang['car_models_listing_for_brand']				= "Car Model Listing for Brand";
$lang['model_name'] 								= "Model Name";
$lang['no_car_models_found']						= "No car models found.";
$lang['add_car_model_for_brand']					= "Add Car Model For Brand";
$lang['edit_car_model_for_brand']					= "Edit Car Model For Brand";
$lang['autoscout_model_name']						= "Autoscout Model Name";
$lang['are_you_sure_you_want_to_delete_this_model'] = "Are you sure you want to delete this model";
$lang['delete_model']								= "Delete Model";
$lang['error_while_deleting_model'] 				= "You can not delete this model";
$lang['model_deleted_successfully'] 				= "Model deleted successfully.";
$lang['car_model_added_successfully']				= "Car model added successfully";
$lang['car_model_updated_successfully']				= "Car model updated successfully";

//----------interior cover add,edit,listing-----//
$lang['interior_covers'] 							= "Interior covers";
$lang['interior_cover_listing'] 					= "Interior cover liting";
$lang['interior_cover_name'] 						= "Interior cover name";
$lang['add_interior_cover'] 						= "Add Interior cover";
$lang['autoscout_interior_cover_name_updated_successfully'] = "Autoscout interior cover name updated successfully";
$lang['interior_cover_successfully_updated'] 		= "interior cover updated successfully";
$lang['autoscout_interior_cover_name'] 				= "Autoscout interior cover name";
$lang['interior_cover_name'] 						= "Interior cover name";
$lang['no_interior_cover_found']    				= "No interior cover found";
$lang['interior_cover_add']							= "Interior cover add";
$lang['interior_cover_edit']						= "Interior cover edit";
$lang['interior_cover_added']						= "interior cover added successfully";
$lang['car_interior_cover_add'] 					= "Add Interior cover";

//----------------removed cars-----------------//
$lang['removed'] 									= "Removed";
$lang['removed_cars_listing'] 						= "Removed Cars Listing";
$lang['undo_delete'] 								= "Undo Delete";
$lang['no_removed_cars_found'] 						= "No Removed Cars Found.";
$lang['restore_car'] 								= "Restore Car";
$lang['are_you_sure_you_want_to_restore_this_car'] 	= "Are you sure you want to restore this car?";
$lang['car_restored_successfully'] 					= "Car Restored Successfully.";
$lang['error_while_restoring_car'] 					= "Error while restoring car.";
$lang['car_already_sold_error'] 					= 'Car has been sold and can not be deleted.';
$lang['car_reserved_error'] 						= 'Car is already reserved and can not be deleted.';
$lang['there_was_some_error_while_deleting_the_data']= "There was some error while deleting the data";
$lang['car_deleted_successfully']					= "Car deleted successfully";

//------------reports-----------------//
$lang['reports'] 									= 'Rapporten';
$lang['stocklist'] 									= 'Stocklist';
$lang['sales_list'] 								= 'Sales list';
$lang['statics_website'] 							= 'Statics website';
$lang['step_1'] 									= "Step 1";
$lang['step_2'] 									= "Step 2";
$lang['step_3'] 									= "Step 3";
$lang['sales_person'] 								= "Verkoper";
$lang['date_from'] 									= "Date From";
$lang['date_to'] 									= "Date To";
$lang['amount_of_sold_cars'] 						= "Amount of sold cars";
$lang['amount_of_created_proposals'] 				= "Amount of created proposals";
$lang['amount_of_turnover_generated'] 				= "Amount of turnover generated";
$lang['amount_of_profit_generated'] 				= "Amount of profit generated";
$lang['create_csv'] 								= "Generate CSV";
$lang['select_from_which_data_you_would_like_the_create_a_report']= "select data from which you would like the create a report";
$lang['select_the_checkboxes_of_which_fields_you_would_like_to_include_in_your_report'] = "select the checkboxes from which fields you would like to include in your report";
$lang['at_least_one_value_should_be_selected'] 		= "At least 1 value should be selected from above fields.";
$lang['report_generated_successfully'] 				= "Report generated successfully.";
$lang['please_select_atleast_one_option'] 			= "Please select atleast one option";
$lang['amount_of_views'] 							= "Amount of views on the detail page";
$lang['amount_of_information_requests'] 			= "Amount of information requests";
$lang['Choose_a_timeframe']							= "Choose a timeframe";
$lang['amount_reservations'] 						= "Amount of reservations";
$lang['amount_call_us_forms']					 	= "Amount of call us forms";

//-----------user add,edit,listing---------------//
$lang['user_listing']								= "Gebruikers - overzicht";
$lang['user_add']									= "Gebruiker toevoegen";
$lang['email']										= "E-mail";
$lang['role']										= "Rechten";
$lang['assign_roles']								= "Rechten toewijzen";
$lang['add_user']									= "Gebruiker toevoegen";
$lang['password']									= "Wachtwoord";
$lang['change_password']							= "Wachtwoord wijzigen";
$lang['confirm_password']   						= "Bevestig wachtwoord";
$lang['workspace'] 									= "Werkruimte";
$lang['user_added_successfully']					= "Gebruiker succesvol toegevoegd";
$lang['user_updated_successfully']					= "User updated successfully";
$lang['choose_your_workspace'] 						= "Choose your workspace";
$lang['user_edit'] 									= "User edit";
$lang['no_users_found'] 							= "No users found";
$lang['delete_user'] 								= "Delete User";
$lang['are_you_sure_you_want_to_delete_this_user'] 	= "Are you sure you want to delete this user";
$lang['no_workspace_found']							= "No workspace found";
$lang['workspace_added_successfully']				= "Workspace added successfully";
$lang['no_users_exists']							= "No users exists";
$lang['user_has_no_assigned_roles']					= "User has no assigned roles";
$lang['user_deleted_successfully']					= "User deleted successfully";
$lang['the_user_cannot_be_deleted']					= "The user cannot be deleted";
$lang['you_are_not_admin']							= "You are not admin";
$lang['error_msg_remove_location']					= "You can not uncheck this location as this is assigned to a lead.";

//----------------client add,edit,listing-------------//
$lang['client_add'] 								= "voeg klant toe";
$lang['add_client'] 								= "Klant toevoegen";
$lang['client_listing'] 							= "Klanten - overzicht";
$lang['no_clients_found'] 							= "Geen klanten gevonden";
$lang['delete_client'] 								= "Verwijderd klant";
$lang['are_you_sure_you_want_to_delete_this_client']= "Bent u zeker dat u deze klant wilt verwijderen";
$lang['client_edit'] 								= "Klant bewerken";
$lang['bus_number'] 								= "Bus nummer";
$lang['city'] 										= "Stad";
$lang['zipcode'] 									= "Postcode";
$lang['country'] 									= "Land";
$lang['company_name'] 								= "Bedrijfsnaam";
$lang['company_street'] 							= "Bedrijf Straat";
$lang['company_house_number'] 						= "Bedrijf Huisnummer";
$lang['company_bus_number'] 						= "Bedrijf Busnummer";
$lang['company_zipcode'] 							= "Bedrijf Postcode";
$lang['company_city'] 								= "Bedrijf Stad";
$lang['company_country'] 							= "Bedrijf Land";
$lang['no_clients_found']							= "Geen klanten gevonden";
$lang['client_added_successfully']					= "Klant succesvol toegevoegd";
$lang['successfully_updated']						= "Succesvol aangepast";
$lang['the_client_deleted_successfully']			= "Deze klant is succesvol verwijderd";
$lang['the_client_cannot_be_deleted']				= "Deze klant kan niet verwijderd worden";

//-------------dealers listing---------------//
$lang['dealer_listing'] 							= "Dealers - overzicht";
$lang['no_dealer_found'] 							= "Geen dealers gevonden.";
$lang['view_proposals'] 							= "Bekijk offertes";
$lang['previous_made_offer_listing_of_dealer'] 		= "Previous Made Offer Listing of Dealer";
$lang['car_price'] 									= "Wagen prijs";
$lang['text_mentioned_above_offer'] 				= "Text mentioned above offer";
$lang['text_mentioned_below_offer'] 				= "Text mentioned below offer";
$lang['no_proposals_found'] 						= "No proposals found";
$lang['accepted']									= "Accepted";
$lang['rejected']									= "Rejected";

//-----------------owner add,edit,listing-----------------//
$lang['add_owner']									= "Add Owner";
$lang['edit_owner']									= "Edit Owner";
$lang['owner_listing']								= "Owner Listing";
$lang['are_you_sure_you_want_to_delete_this_owner']	= "Are you sure,you want to delete this owner ?";
$lang['delete_owner']								= "Delete Owner";
$lang['no_owners_found']							= "No Owners found";
$lang['data_not_found']								= "Data not found";
$lang['owner_added_successfully']					= "Owner added successfully";
$lang['error_while_adding_owner']					= "Error while adding owner";
$lang['owner_updated_successfully']					= "Owner Updated successfully";
$lang['error_while_updating_owner']					= "Error while updating owner";
$lang['error_while_deleting_owner']					= "Error while deleting owner";
$lang['owner_not_found']							= "Owner not found";
$lang['you_can_not_delete_this_owner'] 				= 'You can not delete this owner.';

//----------------lead add,edit,listing-------------//
$lang['no_lead_found_for_this_category'] 			= "Er zijn geen leads gevonden in deze categorie";
$lang['no_lead_found'] 								= "Geen lead gevonden";
$lang['lead_added'] 								= "Lead toegevoegd";
$lang['lead_status_has_changed_from'] 				= "lead status is veranderd van ";
$lang['edit_lead'] 									= "Bewerk Lead";
$lang['lead_listing'] 								= "Lead Overzicht";
$lang['lead_assigned'] 								= "Lead Toegewezen";
$lang['lead_contacted'] 							= "Lead Gecontacteerd";
$lang['lead_interested'] 							= "Lead Geintresseerd";
$lang['lead_not_interested'] 						= "Lead Niet geintresseerd";
$lang['lead_sold'] 									= "Lead Verkocht";
$lang['edit_lead_status'] 							= "Bewerk Lead";
$lang['for_additional_information_please_use_the_notes_below'] = "For additional information please use the notes below";
$lang['add_lead_status'] 							= "Voeg Lead Status toe";
$lang['add_lead'] 									= "Lead Toevoegen";
$lang['add_the_event_to_your_office_365_calendar'] 	= "Add the event to your office 365 calendar";
$lang['logs'] 										= "Logs";
$lang['no_lead_statuses_found'] 					= "Geen lead statussen gevonden";
$lang['added_a_lead']								= " voegde een lead toe ";
$lang['updated_lead']								= " paste een lead aan ";
$lang['deleted_lead']								= " verwijderde een lead ";
$lang['lead_added_successfully']					= "Lead succesvol toegevoegd";
$lang['lead_updated_successfully']					= "Lead succesvol aangepast";
$lang['lead_deleted_successfully']					= "Lead succesvol verwijderd";
$lang['there_was_some_error_while_updating_the_lead']= "Er was een fout tijdens het updaten van de lead";
$lang['the_lead_can_not_be_deleted_the_lead_is_assigned_to_salesperson']= "The lead can not be deleted. The lead is assigned to salesperson";
$lang['lead_status_added_successfully']				= "Lead status succesvol toegevoegd";
$lang['lead_status_updated_successfully']			= "Lead status succesvol aangepast";
$lang['there_are_no_lead_statuses']					= "Er zijn geen lead statussen";
$lang['there_are_no_interested_cars']				= "Er zijn geen geintreseerde wagens";
$lang['mobile']										= "Mobiel";
$lang['intersted_cars'] 							= "Geintresseerde wagens";
$lang['previously_intersted_cars'] 					= "Andere wagens waar de lead interesse in had";
$lang['car_seller'] 								= "Wagen verkoper";
$lang['no_sales_person'] 							= "Sorry, There are no car sellers assigned to this location";
$lang['is_already_registered_with_us']				= "is reeds geregistreerd met ons!";
$lang['lead_assigned_successfully'] 				= "Lead succesvol toegewezen";
$lang['change_lead_status']							= "Wijzig lead status";
$lang['are_you_sure_you_want_to_change_lead_status']= "Bent u zeker dat u de lead status wilt wijzigen";
$lang['lead_status_changed_successfully'] 			= "Lead status is succesvol gewijzigd.";
$lang['status'] 									= "Status";
$lang['select_car'] 								= "Selecteer wagen";
$lang['delete_lead'] 								= "Verwijder Lead";
$lang['are_you_sure_you_want_to_delete_this_lead']  = "Bent u zeker dat u deze lead wilt verwijderen";
$lang['lead_notes_updated_successfully'] 			= "Lead notes updated successfully";
$lang['has_changed_the_lead_status_of']				= " has changed the lead status of ";
$lang['added_notes_for_lead']						= " added notes for lead ";
$lang['generate_proposal_of_car_for_lead']			= " generate proposal of car for lead ";
$lang['lead_with_id'] 								= "Lead met id :";
$lang['select_existing_lead'] 						= "Selecteer een bestaande lead";
$lang['add_new_lead'] 								= "Lead toevoegen";
$lang['no_car_seller_has_been_assigned_to_this_lead'] = "Op dit moment is er geen wagen verkoper toegewezen aan deze lead";
$lang['please_fill_up_all_the_required_fields'] 	= "Vul aub alle verplichte velden in.";
$lang['no_salesperson_assiged'] 					= "Geen verkoper toegewezen";
$lang['salesperson_assiged'] 						= "Verkoper toegewezen";
$lang['must_be_valid_date'] 						= "must be valid date.";
$lang['please_enter_a_valid_cost'] 					= "Please enter a valid cost";
$lang['lead_has_been_assigned'] 					= " Lead is toegewezen";
$lang['lead_has_been_assigned_1'] 					= "Een nieuwe lead is toegewezen aan u. Hieronder vind je de details :";
$lang['please_select_new_car_seller'] 				= "Please select new car seller according to this location";

//--------------attendance--------------------------//
$lang['sales_person_attendance']					= "Car Seller Attendance";
$lang['my_attendance']								= "My Attendance";
$lang['calendar_events'] 							= "Calendar Events";
$lang['add_leave'] 									= "Add Leave";
$lang['edit_leave'] 								= "Edit Leave";
$lang['delete_event'] 								= "Delete Event";
$lang['are_you_sure_you_want_to_delete_this_event']	= "Are you sure you want to delete this event";
$lang['calendar'] 									= "Calendar";
$lang['assign_holiday'] 							= "Assign Holiday";
$lang['title'] 										= "Title";
$lang['please_enter_title'] 						= "Please enter Title.";
$lang['please_enter_a_valid_date'] 					= "Please enter a valid date.";
$lang['leaves_added_successfully'] 					= "Leaves added successfully";
$lang['leaves_updated_successfully'] 				= "Leaves updated successfully";
$lang['leave_added_successfully'] 					= 'Leaves added successfully.';
$lang['added_leaves_for']							= " added leaves for ";
$lang['no_leave_events_found']						= "No leave events found";
$lang['event_deleted_successfully']					= "Event deleted successfully";

//--------global autoscout translations-------------//
$lang['global_autoscout_comments'] 					= "Global autoscout comments";
$lang['global_autoscout_translations']				= "Global Autoscout Translations";
$lang['autoscout_translations']						= "Autoscout Translations";
$lang['gear_type'] 									= "Gear Type";
$lang['translation']								= "Translation";
$lang['internal_name'] 								= "Internal Name";
$lang['autoscout_translation'] 						= "Autoscout Translation";
$lang['autoscout_translations_saved_succesfully']	= "Autoscout Translations saved succesfully";
$lang['error_occured_while_saving_the_translations']= "Error occured while saving the translations";
$lang['autoscout_comments_success'] 				= "Autoscout Comments added successfully.";
$lang['b2_car_comments'] 							= "B2 Car Comments";
$lang['autoscount_comment1']						= "To make text bold prepend and append with ** (ex. **bold** -> ";
$lang['autoscount_comment2']						= "To make list please prepend with * and append with \\ \\(ex. *list1\\ \\-> ";
$lang['autoscount_comment2_next']					= "list1 )";
$lang['autoscount_comment3']						= "Line breaks use \\ \\(equivalent to ";
$lang['autoscount_comment4']						= "Horizontal Line use ---- (equivalent to";
$lang['bold']										= "bold";
$lang['br']							 				= "br";
$lang['hr']											= "hr";
$lang['new_car'] 									= "New Car";
$lang['second_hand_car'] 							= "Second hand Car";
$lang['classic_car'] 								= "Classic Car";

//---------------nissan gamma add,edit,listing---------------//
$lang['gamma'] 										= 'Gamma';
$lang['gammas'] 									= "Gammas";
$lang['gamma_listing'] 								= "Gamma Listing";
$lang['gamma_add'] 									= "Add Gamma";
$lang['subtitle_en'] 								= "Subtitle (English)";
$lang['subtitle_nl'] 								= "Subtitle (Dutch)";
$lang['subtitle_fr'] 								= "Subtitle (French)";
$lang['subtitle_de'] 								= "Subtitle (German)";
$lang['gamma_addded_successfully'] 					= "New Gamma added successfully.";
$lang['gamma_updated_successfully'] 				= "Gamma updated successfully.";
$lang['gamma_updated_error'] 						= "Error while updating Gamma..";
$lang['gamma_error'] 								= "Invalid Data";
$lang['gamma_add'] 									= "Add Gamma";
$lang['gamma_edit'] 								= "Gamma Edit";
$lang['gamma_deleted'] 								= "Gamma Deleted successfully";
$lang['confirm_delete'] 							= "Are you sure you want to delete?";
$lang['invalid_image'] 								= "Please select Valid Image";
$lang['invalid_url'] 								= "Invalid URL";
$lang['external_link'] 								= "External Link";
$lang['choose_location'] 							= "Choose Location";

//--------------frontend forms-------------//
$lang['frontend_forms'] 							= "Formulieren";
$lang['call_me_back_tab_name'] 						= "Bel mij terug";
$lang['email_us_tab_name'] 							= "Email ons";
$lang['mail_friend_tab_name'] 						= "Mail een vriend";
$lang['send_us_an_email']							= "Verstuur ons een email";
$lang['forms'] 										= "Formulieren";
$lang['friend_email']								= "Email vriend";
$lang['sell_a_car']									= "Verkoop een wagen";
$lang['build_year'] 								= "Bouwjaar";
$lang['telephone'] 									= 'Telefoonnummer';
$lang['is_car_damaged'] 							= "Wagen beschadigd";

//-------------roles add,edit,listing, assign----------------------//
$lang['roles'] 										= "Roles";
$lang['assign_role'] 								= "Assign Role";
$lang['role_add'] 									= "Role add";
$lang['role_edit'] 									= "Role edit";
$lang['add_role'] 									= "Add Role";
$lang['select_any_workspace'] 						= "Select any workspace";
$lang['role_listing'] 								= "Role Listing";
$lang['delete_role'] 								= "Delete Role";
$lang['no_roles_to_assign'] 						= "No roles to assign";
$lang['are_you_sure_you_want_to_delete_this_role'] 	= "Are you sure you want to delete this role";
$lang['are_you_sure_you_want_to_delete_this_location'] 	= "Are you sure you want to delete this Location";
$lang['role_added_successfully']					= "Role added successfully";
$lang['there_was_an_error_whiled_adding_the_role']	= "There was an error whiled adding the role";
$lang['there_were_no_permission_to_add']			= "There were no permission to add";
$lang['no_roles_found']								= "No roles found";
$lang['role_updated_successfully']					= "Role updated successfully";
$lang['there_was_an_error_whiled_updating_the_role']= "There was an error whiled updating the role";
$lang['no_role_found']								= "No role found";
$lang['role_assigned_successfully']					= "Role assigned successfully";
$lang['role_removed_successfully']					= "Role removed successfully";
$lang['workspace_added_to_role']					= "workspace added to role";
$lang['role_deleted_successfully']					= "Role deleted successfully";
$lang['predefined_role_cannot_be_deleted']			= "Predefined role cannot be deleted";
$lang['assigned_roles_cannot_be_deleted']			= "Assigned roles cannot be deleted";
$lang['permissions']								= "Permissions";
$lang['extra_permissions_removed_successfully']		= "Extra permissions removed successfully";
$lang['permission_granted']							= "permission granted";
$lang['extra_permissions_added_successfully']		= "Extra permissions added successfully";
$lang['no_permissions_found']						= "No permissions found";
$lang['permissions_found']							= "Permissions found";
$lang['no_such_permission']							= "No such permission";
$lang['invalid_permission']							= "Invalid permission";
$lang['permission_delete_successfully']				= "permission delete successfully";
$lang['no_permission_found']						= "No permission found";
$lang['there_were_no_permission_to_update']			= "There were no permission to update";
$lang['permission_added_successfully'] 				= "Permission added successfully";
$lang['has_changed_the_permission'] 				= "has changed the permission.";

//-----------------------lead times-------------------------//
$lang['lead_times']									= "Lead times";
$lang['link_office_365']							= "Link office 365";
$lang['settings_updated_successfully']				= "Settings updated successfully";
$lang['do_you_want_to_sync_your_office_365_account']= "Do you want to Sync your office 365 account";
$lang['you_have_already_linked_office_365_account']	= "You have already linked Office 365 account";
$lang['lead_times_settings'] 						= "Lead Times Settings";
$lang['settings_updated_sucessfully'] 				= "Settings updated sucessfully";
$lang['no_settings_found']							= "No settings found";

//-----------------location add,edit,listing---------------//
$lang['add_location'] 								= "Locatie toevoegen";
$lang['edit_location'] 								= "Bewerk locatie";
$lang['location_listing'] 							= "Locatie overzicht";
$lang['location_name'] 								= "Location Naam";
$lang['location_code'] 								= "Location Code";
$lang['user_name'] 									= "Gebruikersnaam";
$lang['no_locations_present'] 						= "No Locations present";
$lang['is_car_seller_assigned'] 					= "Is car seller assigned";
$lang['is_visible'] 								= "Is visible";
$lang['contact_email']								= "Contact Email";
$lang['contact_telephone']							= "Contact Telephone";
$lang['contact_fax']								= "Contact Fax";
$lang['opening_and_closing_time']					= "Opening and closing time";
$lang['add_autoscout_account'] 						= "Add autoscout account";
$lang['no_autoscout_accounts_added'] 				= "No autoscout accounts added yet.";
$lang['autoscout_accounts']							= "Autoscout accounts";
$lang['delete_autoscout_account'] 					= "Delete autoscout account";
$lang['are_you_sure_you_want_to_delete_this_autoscout_account'] = "Are you sure you want to delete this autoscout account?";
$lang['autoscout_account_add_msg'] 					= 'All respective autoscout accounts will be added once the location is added.';
$lang['autoscout_username'] 						= "Autoscout Username";
$lang['autoscout_password'] 						= "Autoscout Password";
$lang['autoscout_email'] 							= "Autoscout Email";
$lang['autoscout_dealer_id'] 						= "Autoscout Dealer id";
$lang['location_added_successfully']				= "Location added successfully";
$lang['location_updated_successfully']				= "Location updated successfully.";
$lang['location_deleted_successfully']				= "Location deleted successfully.";
$lang['cannot_delete_location']						= "Cannot delete location.";
$lang['delete_location']							= "Delete location.";
$lang['no_locations_added_yet']						= "No locations added yet";
$lang['locations_added_successfully']				= "Locations added successfully";
$lang['error_encountered_while_adding_locations']	= "Error encountered while adding Locations";

//----------------finance-------------------//
$lang['finance'] 									= "Finance";
$lang['finance_module'] 							= "Finance Module";
$lang['advance'] 									= "Voorschot";
$lang['months'] 									= "Maanden";
$lang['co_efficient_values'] 						= "Co-efficient Values";
$lang['used_cars'] 									= "Used Cars";
$lang['warranty'] 									= "Garantie";
$lang['years'] 										= "Jaren";
$lang['year'] 										= "Jaar";
$lang['year_small'] 								= "Jaar";
$lang['amount'] 									= "Bedrag";
$lang['car_ready_charge'] 							= "Car Ready Charge";
$lang['rest'] 										= "Rest";
$lang['has_updated_finance_settings']				= " has updated finance settings";
$lang['finance_parameters_successfully_updated']	= "Finance parameters successfully updated.";
$lang['please_enter_advance_greater_than_the_minimum_advance_of']= "Please enter advance greater than the minimum advance of ";
$lang['and_a_maximum_of']							= " and a maximum of ";
$lang['car_finance_not_available_for_balance_amount_less_than_3700']= "Car Finance not available for balance amount less than 3700";

//---------------------newsletters-----------------//
$lang['newsletters']							    = "Newsletters";
$lang['newsletters_listing']						= "Newsletters Listing";
$lang['no_newsletters_found'] 						= "No newsletters found";

//----------------------password settings------------------//
$lang['enter_existing_password']					= "Huidig wachtwoord";
$lang['enter_new_password']							= "Nieuw wachtwoord";
$lang['confirm_new_password']						= "Bevestig wachtwoord";
$lang['password_changed_successfully']				= "Wachtwoord succesvol gewijzigd.";
$lang['error_in_setting_new_password']				= "Er is een fout opgetreden.";
$lang['log_out']									= "Uitloggen";
$lang['log_in']										= "Login";

//--------------orders------------------------//
$lang['orders']										= "Orders";
$lang['ordered_by'] 								= "Ordered By";
$lang['pending'] 									= "Pending";
$lang['cancelled'] 									= "Cancelled";
$lang['paid'] 										= "Paid";
$lang['no_details_to_display']						= "No details to display as the order is not completed.";
$lang['order_has_been_updated_successfully']		= "Order has been updated successfully";
$lang['error_occurred_while_updating_the_order']	= "Error occurred while updating the order";
$lang['order_has_been_confirmed_successfully']		= "Order has been confirmed successfully";
$lang['error_occurred_while_confirming_the_order']	= "Error occurred while confirming the order";
$lang['no_pending_orders_for_this_car_found']		= "No pending orders for this car found";
$lang['orders_already_pending_for_this_car_please_try_again_after_15_minutes']= "Orders already pending for this car. Please try again after 15 minutes";
$lang['pending_orders_are_cancelled_successfully']	= "Pending orders are cancelled successfully";
$lang['error_occurred_while_cancelling_the_pending_orders']= "Error occurred while cancelling the pending orders";
$lang['no_orders_found']							= "No orders found";
$lang['first_use']									= "Eerste gebruike";
$lang['capital']									= "Capital";
$lang['displacement']								= "Cilinderinhoud";
$lang['exterior_interior']							= "Exterieur/Interieur";
$lang['coachwork']									= "Koetswerk";
$lang['yes']										= "Ja";
$lang['automedia_link']								= "Automedia Link";
$lang['tax_deductible']								= "BTW aftrekbaar";
$lang['no_commission']								= "Referentie";
$lang['main_equipment']								= "Main Equipment";

//-------------------other data------------------//
$lang['last_sync_happened_on']						= "Last sync happened on";
$lang['which_is_more_than_24_hours_from_now']		= " which is more than 24 hours from now";
$lang['admin']										= "Admin";
$lang['car_manager']								= "Car Manager";
$lang['sales_manager'] 								= "Sales Manager";
$lang['car_inputter'] 								= "Car Inputter";
$lang['administration'] 							= "Administration";
$lang['customer_information'] 						= "Customer information";
$lang['user_with_id'] 								= "User with id :";

//---------------------car updates-------------------//
$lang['car_updates'] 								= "Car Updates";
$lang['is_deleted_from_the_icar_system']			= "is deleted from the Icar system";
$lang['ICar_updates']								= "ICar Updates";
$lang['Bcar_updates']								= "Bcar Updates";
$lang['car_details_from_bcar'] 						= "Car details from Bcar";
$lang['you_must_be_logged_in'] 						= "You must be logged in";


//-----------------activities-----------------------//
$lang['no_activity_found']							= "No activity found";
$lang['there_are_no_activities_added_yet'] 			= "There are no activities added yet.";
$lang['there_are_no_activities_added_yet']			= "there are no activities added yet";
$lang['see_all_the_activities']						= "see all the activities";
$lang['has_logged_in_to_the_system_as_admin']		= " has logged in to the system as admin";
$lang['has_logged_in_to_the_system_as_client']		= " has logged in to the system as client";
$lang['has_logged_in_to_the_system_as_dealer']		= " has logged in to the system as dealer";
$lang['has_logged_out_of_the_system_as_admin']		= " has logged out of the system as admin";
$lang['added_a_car']								= " added a car ";
$lang['updated_a_car']								= " updated a car ";
$lang['deleted_a_car']								= " deleted a car ";
$lang['has_searched_cars_by_car_specifications']	= " has searched cars by car specifications ";
$lang['has_added_user']								= " has added user ";
$lang['has_updated_user']							= " has updated user ";
$lang['has_deleted_user']							= " has deleted user ";
$lang['has_added_role']								= " has added role ";
$lang['has_updated_role']							= " has updated role ";
$lang['has_deleted_role']							= " has deleted role ";
$lang['has_added_client']							= " has added client "; 
$lang['has_updated_client']							= " has updated client ";
$lang['has_deleted_client']							= " has deleted client ";
$lang['has_applied_for_the_reservation_of']			= " has applied for the reservation of ";
$lang['added_accessory']							= " added accessory ";
$lang['edited_accessory']							= " edited accessory ";
$lang['deleted_accessory']							= " deleted accessory ";
$lang['published_accessory_on']						= " published accessory on ";
$lang['unpublished_accessory_on']					= " unpublished accessory on ";
$lang['added_option']								= " added option ";
$lang['removed_option']								= " removed option ";
$lang['published_car_in_channel']					= " published car in channel ";
$lang['unpublished_car_in_channel']					= " unpublished car in channel ";
$lang['registered_to_the_system_as_client']			= " registered to the system as client ";
$lang['registered_to_the_system_as_dealer']			= " registered to the system as dealer ";
$lang['has_logged_out_of_the_system_as_client']		= " has logged out of the system as client ";
$lang['has_logged_out_of_the_system_as_deale']		= " has logged out of the system as deale ";
$lang['has_requested_for_notification_of_car']		= " has requested for notification of car ";
$lang['has_searched_cars_by_car_parameters']		= " has searched cars by car parameters ";
$lang['has_added_location']							= " has added location ";
$lang['has_updated_location']						= " has updated location";
$lang['has_deleted_location']						= " has deleted location ";
$lang['has_set_delivery_date_of_car_to']			= " has set delivery date of car to ";
$lang['has_updated_delivery_date_of_car_to']		= " has updated delivery date of car to ";
$lang['has_accepted_queued_car']					= " has accepted queued car ";
$lang['has_rejected_queued_car']					= " has rejected queued car ";

//-------------image upload and delete-----------//
$lang['add_files'] 									= "Add files";
$lang['start_upload'] 								= "Start upload";
$lang['cancel_upload'] 								= "Cancel upload";
$lang['processing'] 								= "Processing";
$lang['start'] 										= "Start";
$lang['the_uploaded_file_doesnt_seem_to_be_an_image']= "The uploaded file doesn't seem to be an image";
$lang['there_was_an_error_while_uploading_image']   = "There was an error while uploading image";
$lang['Image_deleted_success'] 						= 'Image deleted successfully.';
$lang['Image_deleted_error'] 						= 'Error while deleting data.';
$lang['image_uploaded_successfully'] 				= 'Image uploaded successfully.';
$lang['are_you_sure_you_want_to_delete_this_image'] = "Are you sure you want to delete this image?";

//----------pdf data-----------------//
$lang['Automatic_for_pdf'] 							= 'Automaat';
$lang['Manual_for_pdf'] 							= 'Manueel';
$lang['Petrol_for_pdf'] 							= 'Benzine';
$lang['Diesel_for_pdf'] 							= 'Diesel';
$lang['Electric_for_pdf'] 							= 'Elektrisch';
$lang['Hybrid_for_pdf'] 							= 'Hybrid';
$lang['doors_for_pdf'] 								= "deuren";
$lang['a4_alphabate_footer_msg_4'] 					= ". Informeer bij je B2-adviseur. Let op, geld lenen kost ook geld.";
$lang['advance_for_pdf'] 							= "voorschot";
$lang['residual_for_pdf'] 							= "restwaarde";
$lang['maturity_for_pdf'] 							= "looptijd";
$lang['general_information'] 						= "General Information";
$lang['yes_for_pdf'] 								= "Ja";
$lang['headline_for_car_alarm_template'] 			= "The cars below are newly added to Bcar. You can click on the car to see the car details.";
$lang['please_find_the_attached_images_below'] 		= "Attached to this email you can find the pictures of the car you are interested in.";
$lang['dear'] 										= "Dear";
$lang['team'] 										= "Team";
$lang['logo_op_offerte'] 							= "Logo op offerte";
$lang['Beerens_B2_Noorderlaan'] 					= "Beerens B2";
$lang['Best_in_second_hand_cars'] 					= "Best in second hand cars";
$lang['Technische_gegevens'] 						= "Technische gegevens";
$lang['Eerste_registratie']  						= "Eerste registratie";
$lang['KM_stand']  									= "KM-stand";
$lang['Vermogen'] 									= "Vermogen";
$lang['Cilinderinhoud'] 							= "Cilinderinhoud";
$lang['Brandstof'] 									= "Brandstof";
$lang['Transmissie'] 								= "Transmissie";
$lang['Koetswerk'] 									= "Koetswerk";
$lang['Exterieur_Interieur'] 						= "Exterieur / Interieur ";
$lang['Btw_aftrekbaar'] 			    			= "Btw aftrekbaar";
$lang['Opties'] 									= "Opties";
$lang['Opmerkingen'] 								= "Opmerkingen";
$lang['CNR_1BVA642'] 								= "CNR";
$lang['no_options_available'] 						= "No options available";
$lang['1_ste_gebruik']								= "1-ste gebruik";
$lang['voorschot']									= "voorschot";
$lang['INCLUSIEF_5_JAAR_GARANTIE']      			= "INCLUSIEF 5 JAAR GARANTIE";
$lang['Belangrijkste_uitrusting']					= "Belangrijkste uitrusting";
$lang['Extra_diensten']								= "Extra diensten";
$lang['Tot_5_jaar_garantie']						= "Tot 5 jaar garantie";
$lang['Express_levering']							= "Express levering";
$lang['B2_Finance']									= "B2 Finance";
$lang['Overname']									= "Overname";
$lang['Comm_nr']									= "Comm. nr";
$lang['Comm_nr_Chassis']							= "Comm. nr / Chassis";
$lang['a4_alphabate_footer_msg_1']					= "Exclusief 5 jaar garantie, exclusief € 175 kosten rijklaar eindgebruiker België. (officiële voorplaat, driehoek, brandblusser, fluojasje, verbanden, aanvraag div, cleaning, keuring)";
$lang['a4_alphabate_footer_msg_2']					= "Inclusief 5 jaar garantie, inclusief kosten rijklaar eindgebruiker België. B2 finance geldig mits goedkeuring van het dossier door de kredietgever en onder voorbehoud tariefwijziging.";
$lang['a4_alphabate_footer_msg_3']					= "Verkoop op afbetaling,";


//----------------dutch data for barcode---------------
$lang['aftomen_motors'] 							= "Afstomen motor+";
$lang['aftomen_weikasten'] 							= "Afstomen weilkasten";
$lang['aftomen_uitspuiten'] 						= "Afstomen + uitspuiten sluitkanten, koffer en duren";
$lang['reinigen_velgen'] 							= "Reinigen velgen";
$lang['reinigen_wintervelgen']						= "Reinigen wintervelgen, verpakken en in wagen leggen";
$lang['Klok_juist'] 								= "Klok juist zetten indien nodig";
$lang['alle_stickers'] 								= "Alle stickers verwijderen interieur en exterieur";
$lang['banden_invetten'] 							= "Banden invetten, waterpeil en olie controleren";
$lang['chrome_lijsten'] 							= "Chrome lijsten extra reinigen";
$lang['alle_papieren'] 								= "Alle papieren en documenten uit de wagen halen";
$lang['handschoenkastje'] 							= "Handschoenkastje, opbergvakken, middenconsole reinigen";
$lang['plaathouder'] 								= "Plaathouder Beerens recht bevestigen";
$lang['voor_en'] 									= "Voor- en achterplaten Beerens publiciteit laten hangen";
$lang['spiegelhouder'] 								= "Spiegelhouder laten hangen";
$lang['prijslabel'] 								= "Prijslabel A3 formaat laten liggen";
$lang['koffer_reinigen'] 							= "Koffer reinigen";
$lang['wagen_poetsen'] 								= "Wagen poetsen";
$lang['simoniseren_wagen'] 							= "Simoniseren wagen";
$lang['DIT_DOCUMENT_AFGEVEN'] 						= "DIT DOCUMENT AFGEVEN AAN BART NA UITVOERING";
$lang['handtekening_verantwoordelijke'] 			= "handtekening verantwoordelijke reiniging";
$lang['opkuis_na_keuring'] 							= "Opkuis na keuring";
$lang['opkuis_intake'] 								= "Opkuis intake";
$lang['unique_identifier'] 							= "Unique identifier";
$lang['comission_number'] 							= "Commission Number";

//-----------------order note----------------------//
$lang['on_order_note_title'] 						= 'VERKOOPOVEREENKOMST';
$lang['on_verkoper'] 								= 'VERKOPER';
$lang['on_koper'] 									= 'KOPER';
$lang['on_particulier'] 							= 'Particulier';
$lang['on_business'] 								= 'Bedrijf';
$lang['on_name'] 									= 'Naam';
$lang['on_first_name'] 								= 'Voornaam';
$lang['on_address'] 								= 'Adres';
$lang['on_familyname'] 								= 'Familienaam';
$lang['on_place'] 									= 'Plaats';
$lang['on_street'] 									= 'Straat + nr';
$lang['on_tel'] 									= 'Tel';
$lang['on_postcode'] 								= 'Postcode';
$lang['on_fax'] 									= 'Fax';
$lang['on_btw'] 									= 'BTW';
$lang['on_email'] 									= 'E-mail';
$lang['on_reknr'] 									= 'Reknr';
$lang['on_website'] 								= 'Website';
$lang['on_business_name'] 							= 'Bedrijfsnaam';
$lang['on_btw_nr'] 									= 'BTW Nr.';
$lang['on_order_note_text_1'] 					= 'De ondergetekende verkoper verklaart op zijn erewoord wettelijk bevoegd te zijn om kleinhandelverkoop van nieuwe en tweedehandse voertuigen uit te voeren en verklaart te voldoen aan alle wettelijke en voorgeschreven voorwaarden inzake de toegang tot het beroep. De koper BESTELT, aan de bijzondere voorwaarden die volgen en aan de algemene verkoop- en garantievoorwaarden op de keerzijde, waarvan de koper verklaart kennis te hebben genomen en ze te aanvaarden, het hieronder beschreven voertuig:';
$lang['on_make'] 									= 'Merk';
$lang['on_1st_registration'] 						= '1 ste inschrijving';
$lang['on_Beschikbare_documenten'] 					= 'Beschikbare documenten';
$lang['on_model'] 									= 'Model';
$lang['on_pk'] 										= 'PK';
$lang['on_Inschrijvingsbewijs'] 					= 'Inschrijvingsbewijs';
$lang['on_color'] 									= 'Kleur';
$lang['on_cc'] 										= 'Cc';
$lang['on_Gelijkvormigheidsattest'] 				= 'Gelijkvormigheidsattest';
$lang['on_fuel'] 									= 'Brandstof';
$lang['on_mileage'] 								= 'Kilometerstand';
$lang['on_Onderhoudsboekje'] 						= 'Onderhoudsboekje';
$lang['on_chassis_number'] 							= 'Chassis nr';
$lang['on_carpass'] 								= 'Carpass';
$lang['on_price_excl_btw'] 							= 'Prijs ex. BTW';
$lang['on_Verkoop_onderworpen'] 					= 'Verkoop onderworpen';
$lang['on_Verkoper'] 								= 'Verkoper';
$lang['on_Bedrag_BTW'] 								= 'Bedrag BTW';
$lang['on_aan_bijzondere_marge_regeling'] 			= 'aan bijzondere marge regeling';
$lang['on_Afspraak_afhaling_wagen'] 				= 'Afspraak afhaling wagen';
$lang['on_price_incl_btw'] 							= 'Prijs incl. BTW';
$lang['on_date'] 									= 'Datum';
$lang['on_advance'] 								= 'Voorschot';
$lang['on_btw_niet_aftrekbaar'] 					= 'btw niet aftrekbaar';
$lang['on_hours'] 									= 'Uur';
$lang['on_extras'] 									= 'Extra\'s';
$lang['on_yes'] 									= 'Ja';
$lang['on_no'] 										= 'Neen';
$lang['on_afhaling_ati'] 							= 'Afhaling ATI';
$lang['on_extra_garantie'] 							= 'Extra garantie';
$lang['on_saldo'] 									= 'Saldo';
$lang['on_accessories_included'] 					= 'Uit te voeren werken / toebehoren inbegrepen / notities';
$lang['on_accessories_pay'] 						= 'Uit te voeren werken / toebehoren te betalen';
$lang['on_order_note_text_3'] 						= 'Overname van het voertuig toebehorend aan de koper, in goede wettelijke en mechanische staat';
$lang['on_make_type'] 								= 'Merk en type';
$lang['on_Opmerkingen'] 							= 'Opmerkingen';
$lang['on_Opmerkingen_bij_verkoop'] 				= 'Opmerkingen bij verkoop';
$lang['on_order_note_text_4'] 						= 'Wettelijke garantie van 1 jaar volgens bepalingen van Federauto.';
$lang['on_order_note_text_2'] 						= 'Enkel in geval van verkoop afgesloten buiten de vestiging van de verkoper. Binnen de zeven werkdagen te tellen vanaf de dag na ondertekening van onderhavige overeenkomst heeft de koper het recht om zonder kosten af te zien van zijn aankoop op voorwaarde dat hij de verkoper hiervan verwittigd bij een ter post aangetekend schrijven. Elke clausule waardoor de koper zou afzien van dit recht is nietig. Aangaande de eerbieding v/d termijn volstaat het dat de kennisgeving verstuurd wordt voor het verstrijken van de termijn.';
$lang['on_order_note_text_5'] 						= 'Verkoop afgesloten in het bedrijf van de verkoper.';
$lang['on_order_note_text_6'] 						= 'In duplo opgesteld, waarvan elke partij erkent zijn exemplaar te hebben ontvangen.';
$lang['on_de_verkoper'] 							= 'De Verkoper';
$lang['on_de_koper'] 								= 'De Koper';

//-------color code-------------------------------------------//
$lang['color_codes_added_successfully'] 			= "Color code added successfully";
$lang['add_color_codes'] 							= "Add color code";
$lang['color_code_listing'] 						= "Color code listing";
$lang['delete_color_code'] 							= "Delete color code";
$lang['are_you_sure_you_want_to_delete_this_color_code'] = "Are you sure you want to delete this color code";
$lang['color_id'] 									= "Color id";
$lang['edit_color_codes'] 							= "Edit color code";

//-----new oreder document text----------------//
$lang['view_order_note']							= 'Bekijk bestelbon';
$lang['car_accessories_carried'] 					= 'Uit te voeren werken / toebehoren inbegrepen / notities';
$lang['price_incl_btw'] 							= 'Prijs incl. BTW';
$lang['takeover_clients_previous_car'] 				= 'Overname van vorige wagen';
$lang['please_update_car_delivery_date'] 			= 'Vul aub de leveringsdatum aan voordat u deze bon genereert';

//---------change lead status from sold----------//
$lang['please_choose_an_option_from_option_given_below'] = "Please choose an option from options given below";
$lang['undo_sold_lead']								= "Undo Sold Lead";
$lang['customer_interested_again']					= "Customer Interested Again";
$lang['following_actions_will_automatically_get_placed'] = "Following actions will automatically get placed";
$lang['confirm_msg_for_undo_sold'] 					= "Are you sure? You want to change status from sold to";
$lang['following_data_will_be_deleted']             = "Follwing data will be deleted.";
$lang['following_cars_will_be_placed_in_reserved_state'] = "Following cars will get placed in reserved state.";
$lang['this_lead_have_not_sold_any_car_yet'] 		= "This lead haven't sold any car yet.";

/*-------end revision 1----------add new translatons here*/
$lang['Verkoper'] 									= "Verkoper";
$lang['duration'] 									= "Maanden";
$lang['per_month'] 									= "per maand";

/*-------car edit-----------------------------------*/
//export button
$lang['no_for_pdf']                                 = "Neen";

/**********************color codes********************************/
$lang['exterior_color_code']                        = "Exterior color codes";
$lang['is_color_id_associated']						= "Is color id associated";
$lang['color_code']									= "Color code";
$lang['please_enter_unique_color_id']				= "This color id already exists please enter other color id";
$lang['please_enter_unique_color_code']             = "Please enter unique color code";
$lang['edit_exterior_color_codes']					= "Edit exterior color codes";
$lang['are_you_sure_you_want_to_delete_this_color_code']  = "Are you sure you want to delete this color code";
$lang['this_color_is_associated']                   = "This color cannot be deleted as it is associated with a car";

/**********************Activities********************************/
$lang['recent_activities']                         = "Recent Activities";

/*******************color code************************************/
$lang['color_listing']									  = "Color listing";
$lang['no_color_found']                             = "No color found";

//---------update sold car data----------------
$lang['update_sold_car_data']                      = "Pas instellingen aan";   
$lang['sold_car_data_updated_successfully'] 	   = "Sold Car data updated successfully.";

//---------reports-----------------------------------------------------------------------
$lang['amount_of_accessories']                     = "Aantal accessoires";
$lang['amount_of_guarantee']                       = "Aantal garantie";
$lang['amount_of_financing']                       = "Aantal financieringen";
$lang['amount_of_leads_lost']                      = "Aantal verloren leads";

//------------clients-----------------------------------------------------------------
$lang['search_for_clients']                        = "Klanten zoeken";

//-----------organisations-----------------------//
$lang['organisations']                        		= "Organisaties";
$lang['organisation']                        		= "Organisatie";
$lang['organisation_listing']                       = "Organisatie - overzicht";
$lang['no_organisation_found']						= "Geen organisaties gevonden.";
$lang['are_you_sure_you_want_to_delete_this_organisation'] = "Bent u zeker dat u deze organisatie wilt verwijderen?";
$lang['organisation_added_successfully']			= "Organisatie succesvol toegevoegd.";
$lang['error_while_adding_organisation']			= "fout tijdens het toevoegen van de organisatie";
$lang['organisations_updated_successfully']			= "Organisatie succesvol aangepast.";
$lang['error_while_updating_organisation']			= "Error while updating organisation.";
$lang['you_can_not_delete_this_organisation']		= "You can not delete this organisation as this is associated with an user.";
$lang['btw']                        				= "BTW";
$lang['car_seller_info']							= 'Wagen verkoper informatie';
$lang['associate']									= 'Koppelen';
$lang['associate_users_to_organisation']			= 'Koppel gebruiker aan organisatie';

//----------------car seller serach in sold cars--------------//
$lang['car_sold_by_car_seller']                     = "Wagen verkocht door wagen verkoper";

// -------------Lead new text -------------------------//
$lang['select_existing_lead_or_customers']          = "Selecteer een bestaande lead of klant";

$lang['associate_users_to_organisation']			= 'Koppel gebruiker aan organisatie';

// order note text
/* $lang['enable_company'] 							= 'Activeer organisatie'; */
$lang['enable_company'] 							= 'Enable company';
$lang['enable_organization'] 							= 'Enable company';
$lang['add_organisation'] 							= 'Voeg organisatie toe';
$lang['select_organisation'] 						= 'Selecteer een organisatie';
$lang['statistics_for_the_past_30_days']            = "Statistics for the past 30 days ";

//car list
$lang['registeration_date']							= "Registration Date";
//RevISION 2-- add new langs here -----------------------------

$lang['no_organisation_associated']					= 'No Organisations Added Yet!';
$lang['organisation_associated_with_lead']			= 'Organisations Associated With This lead :';
$lang['mail_sent_successfully']						= "Mail sent successfully.";
$lang['finance_not_available'] 						= 'It’s not possible to have a finance on this car.';
$lang['Financing_amount_is_too_low_to_be_financed']	= "Financing amount is too low to be financed.";


$lang['price_incl_percent_btw'] = 'Prijs incl. %s&#37; btw';
$lang['price_excl_percent_btw'] = 'Prijs excl. %s&#37; btw';
$lang['price_excl_btw'] = 'Prijs excl. btw';
$lang['on_car_ready_charge'] = 'Kosten rijklaar';
$lang['basic_guarantee_1_year'] = 'Basic guarantee 1 year';


$lang['view_proposal'] 							= "View Proposal";

$lang['car_channels'] 							    = "Car channels";
$lang['Petrol'] 									= 'Benzine';
$lang['Diesel'] 									= 'Diesel';
$lang['Electric'] 									= 'Elektrisch';
$lang['Hybrid'] 									= 'Hybrid';
$lang['by'] 										= 'via';

$lang['offer_data_msg']                             = 'and proposals generated for this car';
$lang['Car_reserved_cannot_be_sold']                = 'Car reserved by another user and cannot be sold.';
$lang['publish_system_user']						= "Publish System User";
$lang['you_can_not_unpublish_this_channel_as_this_is_assigned_to_a_lead'] = "You can not unpublish this channel as this is assigned to a lead.";
$lang['channel_published_successfully']				= "Channel published successfully.";
$lang['channel_unpublished_successfully']			= "Channel unpublished successfully.";

$lang['email_for_offer'] 							= "Email";
$lang['after_sales_url'] 							= "naverkoop";
//generate proposal pdf 
$lang['koper']                                      = "Klant";

$lang['group'] 		= '';
$lang['groups'] 	= '';
$lang['add_group'] 	= '';
$lang['group_listing'] 	= '';
$lang['no_group_found'] 	= '';
$lang['add_users'] 	= '';

$lang['delete_group'] 	= '';
$lang['are_you_sure_you_want_to_delete_this_group'] 	= '';
$lang['edit_group'] 	= '';
$lang['group_added_successfully'] 	= '';
$lang['group_deleted_successfully'] 	= '';
$lang['you_can_not_delete_this_group'] 	= '';
$lang['send_newsletter'] 	= '';

$lang['group_updated_successfully'] 	= '';
$lang['error_while_updating_group'] 	= '';
$lang['error_while_adding_group'] 	= '';
$lang['user_deleted_from_group_successfully'] 	= '';

$lang['select_groups_and_users'] 	= '';
$lang['select_groups'] 	= '';
$lang['select_users'] 	= '';
$lang['select_cars'] 	= '';
$lang['email_body'] 	= '';
$lang['email_preview'] 	= '';

$lang['subject'] 	= '';
$lang['body'] 	= '';
$lang['please_select_atleast_one_user_or_one_group'] 	= '';
$lang['generate_proposal_email_msg']                = "A proposal will be generated for this lead,but lead email will be not be updated as this email already exists in user.";

$lang['please_enter_unique_chassis_number'] 	= '';
$lang['please_enter_unique_commission_number'] 	= '';

$lang['extract_search_results']					= "Extract Search Results";
$lang['per']									= "per";
$lang['vaa']									= "V.A.A";
$lang['error_msg_in_leasense_price']			= "You have to select at least one price(per month)";
$lang['extract_search_results']					= "Extract Search Results";
$lang['calculate_dealer_price']                 = "Calculate dealer price";

// Banner
$lang['banner'] 								= "Banner";
$lang['banners'] 								= "Banners";
$lang['add_banner']  							= "Add Banner";
$lang['banner_listing']							= "Banner Listing";
$lang['banner_error'] 							= "Invalid Data";
$lang['banner_addded_successfully'] 			= "Banner added successfully.";
$lang['banner_deleted'] 						= "Banner Deleted successfully";
$lang['image_dimension']						= "( Please select image of dimension 2246x893 )";

//order note location listing
$lang['on_margin_cars']	= "Marge wagens";
$lang['order_note']	= "";
$lang['no_location_found'] = "";
$lang['website'] = "";
$lang['account_number'] = "";
$lang['choose_car_seller_details'] = "";
$lang['order_note_locations'] 	= "Order note locations";
//leasense home page cars
$lang['home_page_cars'] 						= "Home Page Cars";
$lang['updated_successfully']                   = "Updated successfully"; 
$lang['selling_price']                          = "Selling price";

// Blogs
$lang['blogs'] = "Blogs";
$lang['url'] = "";
$lang['add_blog'] = "";
$lang['edit_blog'] = "";
$lang['delete_blog'] = "";
$lang['blog_listing'] = "";
$lang['no_blogs_added_yet'] = "";
$lang['are_you_sure_you_want_to_delete_this_blog'] = "";
$lang['publish_blogs'] = "";
$lang['select_channel'] = "";
$lang['blog_channel_added'] = "";
$lang['error_adding_blog_channel_please_try_again'] = "";
$lang['blog_channel_removed'] = "";
$lang['error_removing_blog_channel_please_try_again'] = "";
$lang['no_car_found']								= "No car found";
//leasense tab
$lang['save_for_leasense_tab'] = "Save";
$lang['prices'] 			 	= "Prices";

//settings tab:blogs
$lang['blog_successfully_added'] 						= 'Blog successfully added';
$lang['error_adding_blog'] 								= 'Error adding blog';


$lang['b2b'] 								= 'B-Rent';
$lang['with_images']								= "with images";
$lang['b2b'] 								= 'B-Rent';
$lang['b2b'] 								= 'B-Rent';
$lang['subscription'] = '';
$lang['finance_enabled']							= "Enable Finance";
$lang['history']									= "History";
$lang['car_status_history']							= "Car Status History";
$lang['car_owner_history']							= "Car Owner History";
$lang['no_results_found'] 							= "No results found.";
$lang['car_import_history']							= "Car Import History";
$lang['car_publish_history']						= "Car Publish History";
$lang['import']										= "Import";
$lang['reservation_cancelled_successfully']			= "Reservation cancelled successfully.";
$lang['cancel_reservation']							= "Cancel Reservation";
$lang['task']										= "Task";
$lang['added_by']									= "Added By";
$lang['different_car_seller_msg']					= "You can not update this lead as this is already assigned to ";
$lang['leads_with_my_channels_and_locations']		= "leads with my channels and locations";

$lang['send_car_pictures_email_subject'] 			= "Beerens Wagens: foto's";
$lang['lead_take_over_msg']							= "Do you really want to assign this lead to yourself?";
$lang['Your_assigned_lead_shifted_to']				= "Your assigned lead shifted to";
$lang['lead_take_over_email_param_1']				= "On request of Car seller";

$lang['lead_take_over_email_param_2']				= "has been taken away from you and has been shifted.";
$lang['car_seller_log']								= "Car Seller logs";
$lang['assigned_to']								= "Assigned to";
$lang['assigned_on']								= "Assigned on";
$lang['car_proposals_listing']						= "Car proposals listing";
$lang['add_client_as_lead']							= "Add client as lead";
$lang['client_succefully_added_as_lead']			= "Client successfully added as a lead.";
$lang['view_generate_proposal']						= "View / Generate Proposals";
$lang['location_codes']								= "Location Codes";
$lang['location_codes_listing']						= "Location Codes Listing";
$lang['view_car_details']							= "Bekijk wagen details";
$lang['reservation_cancelled_on']					= "Reservation Cancelled On";
$lang['processed']									= "Processed";

$lang['newsletter_listing']							= "";
$lang['create_newsletter']							= "";

//-----months--------//
$lang['January']									= 'Januari';
$lang['February']									= 'Februari';
$lang['March']										= 'Maart';
$lang['April']										= 'April';
$lang['May']										= 'Mei';
$lang['June']										= 'Juni';
$lang['July']										= 'Juli';
$lang['August']										= 'Augustus';
$lang['September']									= 'September';
$lang['October']									= 'Oktober';
$lang['November']									= 'November';
$lang['December']									= 'December';

$lang['layout']										= 'Layout';
$lang['layout_1']									= 'Layout 1';
$lang['layout_2']									= 'Layout 2';
$lang['layout_3']									= 'Layout 3';

$lang['alphabet_regards_name'] = "Alphabet Car Square";
$lang['alphabet_email_footer_left_content']				= "<b>Alphabet Car Square</b><br>Boomsesteenweg 42 <br>2630 Aartselaar<br>(achter garage Beerens/Nissan)<br><br>Openingsuren:9u-16u30";
$lang['alphabet_email_footer_right_content']			= "Verkoop: +32 3 340 40 90 <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: +32 3 870 94 74 <br>Administratie: +32 459 55 70<br><br>Email: carsquare@alphabet.be<br>Website: www.alphabetcarsquare.be";
$lang['report']										= 'Report';
$lang['save_csv']									= 'Save CSV';
$lang['report_preset_name']							= "Report Preset Name";
$lang['report_saved_successfully']					= "Report saved successfully.";
$lang['error_occurred_while_saving_report']			= "Error occurred while saving report.";
$lang['delete_report']								= "Delete Report";
$lang['are_you_sure_you_want_to_delete_this_report']= "Are you sure, you want to delete this report?";
$lang['report_deleted_successfully']				= "Report Deleted Successfully.";

$lang['has_publish_user_in']						= "has published user in ";
$lang['has_unpublish_user_from']					= "has unpublished user from ";
$lang['has_cancelled_reservation_for_car']			= "has cancelled reservation for a car.";
$lang['has_saved_report_having_name']				= "has saved a report having name ";
$lang['has_deleted_saved_report_having_name']		= "has deleted saved report having name ";

$lang['smart_suggestions_for_car'] = "Smart suggestions for cars";


$lang['has_publish_user_in']						= "has published user in ";
$lang['has_unpublish_user_from']					= "has unpublished user from ";
$lang['has_cancelled_reservation_for_car']			= "has cancelled reservation for a car";
$lang['has_saved_report_having_name']				= "has saved a report having name ";
$lang['has_deleted_saved_report_having_name']		= "has deleted saved report having name ";
$lang['has_added_banner_for_nissan_website']		= "has added banner for nissan website";
$lang['has_deleted_banner_for_nissan_website']		= "has deleted banner for nissan website";
$lang['has_added_gamma']							= "has added a gamma";
$lang['has_updated_gamma']							= "has updated a gamma";
$lang['has_deleted_gamma']							= "has deleted a gamma";
$lang['has_changed_workspace_to']					= "has changed workspace to ";
$lang['has_changed_password']						= "has changed password";
$lang['has_deleted_image_for_location']				= "has deleted image for location ";
$lang['has_added_a_blog']							= "has added a blog ";
$lang['has_updated_a_blog']							= "has updated blog ";
$lang['has_deleted_a_blog']							= "has deleted blog ";
$lang['has_published_blog']							= "has published blog in ";
$lang['has_unpublished_blog']						= "has unpublished blog from ";
$lang['updated_leaves_for']							= " updated leave for ";
$lang['cancelled_leave_for']						= " cancelled leave for ";
$lang['clears_all_lead_related_data']				= " clears all lead related data.";
$lang['clears_all_user_related_data']				= " clears all user related data.";
$lang['clears_all_car_related_data']				= " clears all car related data.";
$lang['clears_all_data']							= " clears all data in the system.";
$lang['has_accepted_dealer']						= " has accepted dealer ";
$lang['has_rejected_dealer']						= " has rejected dealer ";
$lang['has_added_newletter_group']					= " has added newsletter group ";
$lang['has_updated_newletter_group']				= " has updated newsletter group ";
$lang['has_deleted_newletter_group']				= " has deleted newsletter group ";
$lang['has_added_users_to_newletter_group']			= " has added users to newsletter group ";
$lang['has_removed_users_from_newletter_group']		= " has removed users from newsletter group ";
$lang['has_added_banner_for_leasense_website']		= " has added banner for leasense website";
$lang['has_deleted_banner_for_leasense_website']	= " has deleted banner from leasense website";
$lang['has_added_newsletter_template']				= " has added newsletter template ";
$lang['has_deleted_newsletter_template']			= " has deleted newsletter template ";
$lang['has_added_order_note_location']				= " has added order note location ";
$lang['has_updated_order_note_location']			= " has updated order note location ";
$lang['has_deleted_order_note_location']			= " has deleted order note location ";
$lang['has_added_organisation']						= " has added organisation ";
$lang['has_updated_organisation']					= " has updated organisation ";
$lang['has_deleted_organisation']					= " has deleted organisation ";
$lang['has_updated_users_associated_with_organisation']= " has updated users associated with organisation ";
$lang['has_added_owner']							= " has added owner ";
$lang['has_updated_owner']							= " has updated owner ";
$lang['has_deleted_owner']							= " has deleted owner ";
$lang['has_updated_lead_time_settings']				= " has updated lead time settings";
$lang['has_marked_car']								= " has marked car ";
$lang['as_delivered']								= " as delivered.";
$lang['has_copied_car_from']						= " has copied a car from ";
$lang['has_generated_barcode_for_car']				= " has generated barcode for a car ";
$lang['has_updated_car_option']						= " has updated car option ";
$lang['has_added_new_cost']							= " has added new cost ";
$lang['for_a_car']									= " for a car ";
$lang['has_removed_cost']							= " has removed car cost ";
$lang['has_deleted_car_document']					= " has deleted car document ";
$lang['has_send_pictures_of_a_car']					= " has send pictures of a car ";
$lang['has_generated_proposal_for_a_car']			= " has generated proposal for a car ";
$lang['has_exported_a3_pricetag']					= " has exported A3 pricetag pdf for a car ";
$lang['has_exported_a3_pricetag_alphabet']			= " has exported A3 pricetag aphabet pdf for a car ";
$lang['has_exported_a4_pricetag']					= " has exported A4 pricetag pdf for a car ";
$lang['has_added_car_model']						= " has added car model ";
$lang['has_updated_car_model']						= " has updated car model ";
$lang['has_deleted_car_model']						= " has deleted car model ";
$lang['has_added_car_brand']						= " has added car brand ";
$lang['has_updated_car_brand']						= " has updated car brand ";
$lang['has_deleted_car_brand']						= " has deleted car brand ";
$lang['has_added_an_interior_cover']				= " has added an interior cover ";
$lang['has_updated_an_interior_cover']				= " has updated an interior cover ";
$lang['has_restored_removed_car']					= " has restored a car  ";
$lang['has_deleted_and_redownloaded_images_for_a_car']= " has deleted and redownloaded images for a car  ";
$lang['has_made_car']								= " has made a car ";
$lang['in_stock_again']								= " in stock again.";
$lang['has_bought_car']								= " has bought a car ";
$lang['has_duplicated_car']							= " has duplicated a car ";
$lang['times']										= " times.";
$lang['has_generated_order_note_for_car']			= " has generated order note for a car ";
$lang['has_added_color_code']						= " has added color ";
$lang['has_updated_color_code']						= " has updated color ";
$lang['has_deleted_color_code']						= " has deleted color ";
$lang['has_updated_sold_car_data']					= " has updated sold car data.";
$lang['has_updated_exterior_color_code']			= " has updated exterior color code ";
$lang['has_uploaded_cars_via_csv']					= " has uploaded cars via csv.";
$lang['has_deleted_image_of_a_Car']					= " has deleted image of a car ";
$lang['car_alarm_newsletter_have_been_sent_from_cron_with_channel_b2']= "Car alarm newsletters have sent from cron with channel b2.";
$lang['car_alarm_newsletter_have_been_sent_from_cron_with_channel_nissan']= "Car alarm newsletters have sent from cron with channel nissan.";
$lang['car_alarm_newsletter_have_been_sent_from_cron_with_channel_b2b']= "Car alarm newsletters have sent from cron with channel b2b.";
$lang['car_alarm_newsletter_have_been_sent_from_cron_with_channel_alphabet']= "Car alarm newsletters have sent from cron with channel alphabet.";
$lang['have_been_unreserved_by_cron']				= " have been unreserved by cron.";
$lang['have_been_updated_by_cron']					= " have been updated by cron.";
$lang['exterior_color_of_cars']						= "Exterior color of cars ";
$lang['newly_added_leads_have_been_shifted_by_cron']= "Newly added leads have been shifted by cron.";
$lang['newly_contacted_leads_have_been_shifted_by_cron']= "Newly contacted leads have been shifted by cron.";
$lang['duplicate_cars_with_same_commission_number_have_been_deleted_by_cron']= "Duplicate cars with same commission number have been deleted by cron.";
$lang['xml_file_for_autoscout_uploaded_by_cron']	= "XML file for autoscout have been uploaded by cron.";
$lang['emails_for_smart_suggestions_are_sent_by_cron']	= "Emails for smart suggestions are sent by cron.";
$lang['has_added_a_car']							= " has added a car ";
$lang['in_favorite_list']							= " in favorite list.";
$lang['has_removed_a_car']							= " has removed a car ";
$lang['from_favorite_list']							= " from favorite list.";
$lang['has_added_options_for_smart_suggestions']	= " has added options for smart suggessions.";
$lang['has_subscribed_for_newsletter']				= " has subscribed for a newletter.";
$lang['set_car_alarm_for']							= " set car alarm for ";
$lang['has_deleted_car_alarm_for']					= " has deleted car alarm for ";
$lang['gets_added_as_a_lead']						= " gets added as a lead.";
$lang['the_status_of_lead']							= "The status of lead ";
$lang['has_changed_to']								= " has changed to ";
$lang['has_added_a_request_for_selling_a_car']		= " has added a request for selling a car ";
$lang['new_sell_a_car_request_added_from_user']		= "New sell a car request added from user ";




$lang['smart_suggestions_for_car'] 					= "Smart suggestions for cars";
$lang['blog_successfully_deleted'] 					= "Blog deleted successfully.";
$lang['blog_successfully_updated'] 					= "Blog updated successfully.";
$lang['order_note_location_deleted_successfully'] 	= "Order note location deleted successfully.";

$lang['please_add_atleast_one_zone']				= "Please add atleast one zone";
$lang['bulk_upload']								= "Bulk upload";
$lang['sample_csv'] 								= "Sample csv";

$lang['newsletter_unsubscription_msg'] = "Als je jezelf hiervoor niet hebt ingeschreven en deze mails foutief ontvangt, kan je op onderstaande knop klikken om je mailadres te verwijderen van onze lijst.
";
$lang['newsletter_unsubscription'] = "Newsletter Unsubscription";
$lang['newsletter_unsubscription_desc'] = "Thank you. You are successfully unsubscribed to our newsletter.";

$lang['unsubscribe_me_please'] = "UITSCHRIJVEN";
$lang['selected_users_or_groups_has_unsubscribed_to_newsletters'] = 'or Selected users or groups has unsubscribed to newsletters';
$lang['unsubscribe_users_listing'] = "Unsubscribe users listing";
$lang['unsubscription'] = "Unsubscription";
$lang['unsubscribed_on'] = "Unsubscribed on";
$lang['listing'] = "listing";
$lang['article_name'] = "article name";
$lang['page_title'] = "page title";
$lang['user_roles'] = "user roles";
$lang['meta_description'] = "meta descirption";
$lang['meta_keywords'] = "meta keywords";
$lang['short_description'] = 'Short description';

$lang['add_thumbnail'] = "Add thumbnail";
$lang['thumbnail'] = "Thumbnail";
$lang['delete_thumb'] = "Delete thumbnail";
$lang['are_you_sure_you_want_to_delete_this_thumbnail'] = "Are you sure you want to delete this thumbnail?";
$lang['thumbnail_deleted_successfully'] = "Thumbnail deleted successfully";
$lang['send_preview'] = "Send Preview";
//form
$lang['form'] = "Form";
$lang['form_listing'] = "Form listing";
$lang['add_form'] = "Add form";
$lang['form_created_successfully'] = "Form created successfully";
$lang['delete_form'] = "Delete Form";
$lang['are_you_sure_you_want_to_delete_this_form'] = "Are you sure, you want to delete this form ?";
$lang['form_deleted_successfully'] = "Form deleted successfully";
$lang['form_updated_successfully'] = "Form updated successfully";

//static_page
$lang['static_pages'] = "Static Pages";
$lang['listing'] = "Listing";
$lang['pages'] = "Pages";
$lang['add_pages'] = "Add Pages";
$lang['pages_listing'] = "Pages listing";
$lang['are_you_sure_you_want_to_delete_this_page'] 	= "Are you sure you want to delete this page";
$lang['page_successfully_added'] = "Page successfully added";
$lang['page_successfully_deleted'] = "Page successfully deleted";
$lang['page_successfully_updated'] = "Page successfully updated";
$lang['edit_page'] = "Edit Page";
$lang['page_title'] = 'Page title';
$lang['page_name'] = "Page Name";