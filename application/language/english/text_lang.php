<?php defined('BASEPATH') OR exit('No direct script access allowed');

// English Language

//ADMIN panel

//Header page
$lang['home']		= "Home";
$lang['roles']		= "Roles";
$lang['users']		= "Users";
$lang['settings']		= "Settings";

//Sidebar page
$lang['search']		= "Search";
$lang['dashboard']	= "Dashboard";
$lang['documents']	= "Documents";
$lang['elearning']	= "Elearning";
$lang['forum']		= "Forum";
$lang['quiz']		= "Quiz";
$lang['categories']	= "Categories";
$lang['wikis']		= "Wikis";

//login
$lang['username']		= "User Name";
$lang['password']		= "Password";
$lang['email']			= "Email";
$lang['sign_in']		= "Sign In";
$lang['log_in']			= "Login";
$lang['log_out']		= "Logout";
$lang['register_here'] 	= "Register Here";

//Register
$lang['register']			= "Register";
$lang['confirm_password']   = "Confirm Password";
$lang['first_name']			= "First Name";
$lang['last_name']			= "Last Name";
$lang['phone']				= "Phone Number";
$lang['name']				= "Name";
$lang['user_type']			= "User Type";
$lang['added_on']			= "Added On";
$lang['modified_on']		= "Modified On";
$lang['added_by']			= "Added By";
$lang['save']				= "Save";

//User Add
$lang['user_add']	= "User Add";

//User Listing
$lang['id']				= "ID";
$lang['action']			= "Action";
$lang['edit']			= "Edit";
$lang['assign_roles']	= "Assign roles";
$lang['user_listing']	= "User Listing";

//Roles
$lang['admin']	= "Admin";
$lang['car_manager']	= "Car Manager";
$lang['sales_manager'] = "Sales Manager";
$lang['car_inputter'] = "Car Inputter";
$lang['administration'] = "Administration";
$lang['sales_person'] = "Sales Person";

//Roles Add
$lang['permissions']	= "Permissions";
$lang['role']			= "Role";
$lang['queued_car_listing'] = "Queued Car Listing";
$lang['queued_car_approval'] = "Queued Car Approval/Rejection";

