<?php
require_once 'vendor/mandrill/src/Mandrill.php'; 

class Mailer {
	
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('Email_model');
    }

    function send_queued_email_mandrill()
    {
        $queued_emails = $this->CI->Email_model->fetch_queued_emails();

        if(count($queued_emails['data'])>0)
        {
            $to_array = array();
            // echo"<pre>";print_r($queued_emails);echo "</pre>";exit;
            foreach($queued_emails['data'] as $queued_email)
            {
                $message = array(
                    'html' => $queued_email['body'],
                    'subject' => $queued_email['subject'],
                    'from_email' => $queued_email['from_email'],
                    'from_name' => $queued_email['from_name'],
                    'to' => array(
                                array(
                                        'email' => $queued_email['to_email'],
                                        'name' =>  $queued_email['to_name'],
                                        'type' => 'to'
                                    )
                            ),
                    'subaccount' => MANDRILL_SUBACCOUNT,
                    'inline_css' => TRUE,
                    'headers' => '',
                    'important' => '',
                    'tags' => array($queued_email['tag'])
                );

                //pr($message); exit;
                $status = $this->send_email_via_mandrill( $message );
                
                if(($status[0]['status'] == 'sent') || ($status[0]['status'] == 'queued'))
                {
                    $response = 'sent';
                    $this->CI->Email_model->update_queued_emails($queued_email['id'],$status[0]['_id'], $status[0]['status'],time());
                }
                // echo"<pre>";print_r($queued_email);echo "</pre>";exit;
            }
        }

    }

    function send_queued_newsletter_email_mandrill()
    {
        $vendor_type = EMAIL_VENDOR_MANDRILL;
        $queued_emails = $this->CI->Email_model->fetch_queued_newsletter_emails( $vendor_type );
        /* for every email building a bulk mandrill array */
        $count = 0;
        $toArray = array();

        if(count($queued_emails['data'])>0)
        {
            foreach ($queued_emails['data'] as $queued_email) {

                if( $queued_email['tag'] == 'send_newsletter' )
                {
                    $newsletter_sent_id = $queued_email['newsletter_sent_id'];
                    $toArray[$count]['name'] = $queued_email['to_name'];
                    $toArray[$count]['email'] = $queued_email['to_email'];
                    $newsletter_body = $queued_emails['body_html'];
                    $newsletter_tag = $queued_email['tag'];
                    $count++;
                    
                    $merge_vars[] = array(
                      'rcpt' => strlen($queued_email['to_email'])?$queued_email['to_email']:"info@codebox.in",
                      'vars' => array(array(
                        'name' => 'UNSUBSCRIBE_HREF_VALUE',
                        'content' => $queued_email['body']
                      ))
                    );
                }            
            }

            /* Mandrill array for every mail ID */
                
            $newsletter_data = array(
                  "html" => $newsletter_body,
                  "subject" => $queued_email['subject'],
                  "from_email" => $queued_email['from_email'],
                  "to" => $toArray,
                  "track_opens" => true,
                  "track_clicks" => true,
                  "auto_text" => true,
                  "merge" => true,
                  "merge_vars" => $merge_vars,
                  'subaccount' => MANDRILL_SUBACCOUNT,
                  'inline_css' => TRUE,
                  'tags' => array($newsletter_tag),
                  'headers' => '',
                  'important' => ''
            );

            $status = $this->send_email_via_mandrill( $newsletter_data );

            $mandrill_responses = $status;

            if( $mandrill_responses ){

                  $sent_email_stats = array();

                  /* Checking mandrill response for every email */

                  foreach($mandrill_responses as $mandrill_response){

                      if ($mandrill_response['status'] != "error") {
                          $sent_email_stats = 
                                      array(
                                            'hash' => $mandrill_response['_id'], 
                                            'status' => $mandrill_response['status'],
                                            'to_email' => $mandrill_response['email'], 
                                            'sent_on' => date('Y-m-d G:i:s'), 
                                            'event' => 'send'
                                        );

                        $response = 'sent';
                        $this->CI->Email_model->update_queued_newsletter_emails($newsletter_sent_id, $sent_email_stats);
                      }
                  }
            }
        }
    }
    
    function send_email_via_mandrill($message)
    {   
        $mandrill = new Mandrill(MANDRILL_API_KEY);
        $result = $mandrill->messages->send($message);
        
        return $result;
    }

}