<?php

class Permissions {

    function is_permission_allowed($permission_key)
    {
    	$CI =& get_instance();

        if($CI->user['type'] == "master admin" || in_array($permission_key,$CI->allowed_permissions))
            return TRUE;
        else
            return FALSE;
    }

    function is_permission_allowed_array($permission_keys)
    {
        return TRUE;
        $CI =& get_instance();
         
        $result = array_intersect($permission_keys, $CI->user['allowed_permissions']);
         
        if($CI->user['type'] == "master admin" || (is_array($result) && count($result) > 0))
            return TRUE;
        else
            return FALSE;
    }

    function authorize($permission_key)
    {
    	if($this->is_permission_allowed($permission_key))
    	{
    		return TRUE;
    	}
    	else
    	{
            $CI =& get_instance();
            $CI->session->set_flashdata('error_msg', lang('you_dont_have_permissions_to_access_this_page_please_contact_to_administrator'));
    		redirect('admin/dashboard'); exit;
    	}
    }
}