<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assets {
    
    static public function js($filename = null)
    {
        $file_path = FCPATH . 'resources/' . $filename;
        if(file_exists($file_path) && $filename != null){
			return '<script type="text/javascript" src="'.site_url('resources/'.$filename).'"></script>';
		}
		else
		{
            return '<!-- file '.site_url('resources/'.$filename).' not found -->';
		}
    }
    
    static public function css($filename = null)
    {
        $file_path = FCPATH . 'resources/' . $filename;
        if(file_exists($file_path) && $filename != null) {
			return '<link rel="stylesheet" media="all" href="'.site_url('resources/'.$filename).'" />';
		}
		else
		{
            return '<!-- file '.site_url('resources/'.$filename).' not found -->';
		}
    }

	static public function img($filename,$params=array(),$tag=true)
	{
		if($tag == true)
		{
			$params_str = "";
			foreach($params as $key=>$param)
			{
				$params_str .= $key.'="'.$param.'" ';
			}
			return '<img src="'.site_url('resources/'.$filename).'" '.$params_str.' alt="" />';
		}
		else
			return base_url()."resources/img/".$filename;
	}
}